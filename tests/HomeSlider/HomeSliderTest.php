<?php

namespace App\Tests\HomeSlider;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class UserTest
 * @package App\Tests\User
 */
class HomeSliderTest extends WebTestCase
{
    //TODO: check database is exist or not

    /**
     * Test homepage
     */
    //TODO : remove this action
    public function testCreateHomeSlider()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/homepage/add');
        dump($crawler);
        exit;
        $form = $crawler->filter('form[name=home_slider]')->form();
        $form['home_slider[title]'] = 'abcd';
        $form['home_slider[pageUrl]'] = 'abcd-123';
        $form['home_slider[sortOrder]'] = '1';
        $form['home_slider[bannerFile]'] = 'F name';
        $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('/admin/homeslider'));
    }

//    public function testUserRegister()
//    {
//        $client = static::createClient();
//        $crawler = $client->request('GET', '/register');
//        $form = $crawler->filter('form[name=register]')->form();
//        $username = 'testuser' . uniqid();
//        $form['register[username]'] = $username;
//        $form['register[profile][firstname]'] = 'F name';
//        $form['register[profile][lastname]'] = 'L name';
//        $form['register[profile][lastname]'] = $username . '@test.com';
//        $form['register[email]'] = $username . '@test.com';
//        $form['register[plainPassword][first]'] = '123456';
//        $form['register[plainPassword][second]'] = '123456';
//        $client->submit($form);
//        $this->assertTrue($client->getResponse()->isRedirect('/thank-you-register'));
//    }
}
