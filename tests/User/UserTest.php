<?php

namespace App\Tests\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class UserTest
 * @package App\Tests\User
 */
class UserTest extends WebTestCase
{
    //TODO: check database is exist or not

    /**
     * Test homepage
     */
    //TODO : remove this action
    public function testHomePage(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');
        self::assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * Test Admin login functionality
     */
    // ./bin/phpunit --filter testAdminLogin tests/User/UserTest.php
    // ./vendor/bin/simple-phpunit --filter testAdminLogin tests/User/UserTest.php
    public function testAdminLogin(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('submit')->form();
        $form['_username'] = 'admin';
        $form['_password'] = 'admin';
        $client->submit($form);
        self::assertTrue($client->getResponse()->isRedirect('http://localhost/my-account'));
    }


    /**
     * Test User login functionality
     */
    //./vendor/bin/simple-phpunit --filter testUserLogin tests/User/UserTest.php
    public function testUserLogin(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('submit')->form();
        $form['_username'] = 'user001';
        $form['_password'] = '123456';
        $client->submit($form);
        self::assertTrue($client->getResponse()->isRedirect('http://localhost/my-account'));
    }

    /**
     * Test User Register functionality
     */

    //./bin/phpunit --filter testUserRegister  tests/User/UserTest.php
    //./vendor/bin/simple-phpunit --filter testUserRegister tests/User/UserTest.php
    public function testUserRegister(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');
        $form = $crawler->filter('form[name=register]')->form();
        $username = uniqid('testuser', false);
        $form['register[username]'] = $username;
        $form['register[profile][firstname]'] = 'F name';
        $form['register[profile][lastname]'] = 'L name';
        $form['register[email]'] = $username . '@test.com';
        $form['register[plainPassword][first]'] = '123456';
        $form['register[plainPassword][second]'] = '123456';
        $client->submit($form);
        self::assertTrue($client->getResponse()->isRedirect('/register'));
        //$this->assertTrue($client->getResponse()->isRedirect('/thank-you-register/page'));
    }

    /**
     * Test forgot password functionality
     */
    //./vendor/bin/simple-phpunit --filter testForgotPassword tests/User/UserTest.php
    public function testForgotPassword(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/forgot-password/request');
        $form = $crawler->filter('form[class=login-class]')->form();
        $form['username'] = 'user001';
        $client->submit($form);
        self::assertTrue($client->getResponse()->isRedirect('/forgot-password/check-email?username=user001'));
    }
}
