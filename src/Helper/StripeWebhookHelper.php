<?php
namespace App\Helper;

class StripeWebhookHelper
{
    /**
     * @param $eventId
     *
     * @return \Stripe\StripeObject
     */
    public function findEvent($eventId)
    {
        return \Stripe\Event::retrieve($eventId);
    }
}
