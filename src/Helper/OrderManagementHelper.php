<?php

namespace App\Helper;

use App\Entity\TblCart;
use App\Entity\TblCartCoupons;
use App\Entity\TblCartProduct;
use App\Entity\TblCustomer;
use App\Entity\TblOrderDetails;
use App\Entity\TblOrders;
use App\Entity\TblPaymentMethods;
use App\Entity\TblStreamingPages;
use App\Entity\TblTransation;
use App\Entity\TblUserCcCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class OrderManagementHelper
 * @package App\Helper
 */
class OrderManagementHelper
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * OrderManagementHelper constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }


    /**
     * @param $user
     * @param $CardData
     *
     * @return TblCustomer|object|null
     * @throws \Exception
     */
    public function checkCustomerwithStripe($user, $CardData)
    {
        $em = $this->entityManager;

        $customer = $em->getRepository(TblCustomer::class)->findOneBy(['idUser' => $user]);
        self::checkSessionAuth($customer);
        if ($customer) {
            $stripeHelper = new StripeCustomerHelper();
            if ($customer->getCustomerStripeId()) {
                $stripeCust = $stripeHelper->checkCustomerExist($customer->getCustomerStripeId());
                if ($stripeCust) {
                    $stripeHelper->updateCustomerWithCard($customer->getCustomerStripeId(), $CardData);
                }
            } else {
                $custStripeId = $stripeHelper->createCustomerWithCard($customer->getIdUser()->getEmail(), $CardData);
                $customer->setCustomerStripeId($custStripeId['id']);
            }
            $em->persist($customer);
        }
        return $customer;
    }


    /**
     * @param $customer
     * @param $paymentMethod
     *
     * @return TblCart|object|null
     */
    public function checkUpdateCart($customer, $paymentMethod)
    {
        $em        = $this->entityManager;
        $cart      = $em->getRepository(TblCart::class)
                        ->findOneBy(['idCustomer' => $customer, 'status' => 0]);
        $getMethod = $em->getRepository(TblPaymentMethods::class)->find($paymentMethod);

        $cart->setIdPaymentMethods($getMethod);

        $em->persist($cart);
        return $cart;
    }


    /**
     * @param $user
     * @param $customer
     * @param $cart
     *
     * @return TblOrders
     */
    public function createOrder($user, $customer, $cart)
    {
        $order = new TblOrders();
        $order->setIdUser($user);
        $order->setIdCustomer($customer);
        $order->setIdOrderAddress($cart->getIdOrderAddress());
        $order->setOrderTime(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $order->setIdPaymentMethods($cart->getIdPaymentMethods());
        if ($cart->getIdCartCoupon()) {
            $order->setIdCartCoupon($cart->getIdCartCoupon());
        }

        $this->entityManager->persist($order);

        return $order;
    }

    /**
     * @param $user
     *
     * @return TblCustomer|object|null
     */
    public function getCustomer($user)
    {
        $customer = $this->entityManager->getRepository(TblCustomer::class)->findOneBy(['idUser' => $user]);
        self::checkSessionAuth($customer);
        return $customer;
    }


    /**
     * @param      $user
     * @param null $CardData
     * @param int  $paymentMethod
     *
     * @throws \Exception
     */
    // 3:Stripe, 1:Paypal
    public function checkandCreateOrder($user, $CardData = null, $paymentMethod = TblPaymentMethods::STRIPE)
    {
        try {
            $em = $this->entityManager;
            if ($paymentMethod === TblPaymentMethods::STRIPE) {
                $customer = $this->checkCustomerwithStripe($user, $CardData);
            } else {
                $customer = $this->getCustomer($user);
            }

            if ($customer) {
                $cart = $this->checkUpdateCart($customer, $paymentMethod);
                if ($cart) {
                    $cartProduct = $em->getRepository(TblCartProduct::class)
                                      ->findBy(['idCart' => $cart, 'status' => 0]);
                    if ($cartProduct) {
                        $order = $this->createOrder($user, $customer, $cart);

                        $totalAmount = 0;
                        $dicountedAmount = 0;
                        $shippingAmount = 0;
                        $productId   = [];
                        foreach ($cartProduct as $cp) {
                            $totAmount = $cp->getAmount() * $cp->getQuantity();
                            $totalAmount += $totAmount;

                            if(!empty($cp->shippingAmount)){
                             $shippingAmount += $cp->getShippingAmount();
                            }
                            $orderDetails = new TblOrderDetails();

                            $orderDetails->setIdOrder($order);
                            $orderDetails->setIdProduct($cp->getIdProduct());
                            $orderDetails->setProductPrice($cp->getAmount());
                            $orderDetails->setProductQty($cp->getQuantity());

                            if(!empty($cp->shippingAmount)) {
                                $orderDetails->setShippingAmount($cp->getShippingAmount());
                            }else{
                                $orderDetails->setShippingAmount(0);
                            }
                            $em->persist($orderDetails);

                            $cp->setStatus(TblCartProduct::_PLACEORDER);
                            $em->persist($cp);
                            $productId[] = $cp->getIdProduct()->getId();
                        }

                        if ($cart->getIdCartCoupon()) {
                            $dicountedAmount = self::getCouponDiscountAmount($cart->getIdCartCoupon(), $totalAmount);
                            $chargedAmount = $totalAmount + $shippingAmount - $dicountedAmount;
                            $order->setDiscountAmount($dicountedAmount);
                            $order->setFinalOrderAmount($chargedAmount);
                        } else {
                            $chargedAmount = $totalAmount + $shippingAmount;
                            $order->setFinalOrderAmount($chargedAmount);
                        }
                        $order->setShippingAmount($shippingAmount);
                        $order->setTotalAmount($totalAmount);
                        $em->persist($order);

                        $cart->setStatus(TblCart::_PLACEORDER);
                        $em->persist($cart);

                        $charges = true;
                        if ($paymentMethod === TblPaymentMethods::STRIPE) {
                            $paidAmount = $totalAmount + $shippingAmount - $dicountedAmount;
                            $stripeHelper = new StripeCustomerHelper();
                            $charges      = $stripeHelper->chargeCoupon(
                                $customer->getCustomerStripeId(),
                                $paidAmount
                            );
                        }
                        if ($charges) {
                            $order->setStatus(TblOrders::STATUS_CONFIRM);
                            $order->setIdTransaction($charges['id']);
                            $em->persist($order);

                            $transation = new TblTransation();
                            $transation->setIdCustomer($customer);
                            $transation->setIdPaymentMethods($cart->getIdPaymentMethods());
                            $transation->setIdOrder($order);
                            $transation->setSum($totalAmount + $shippingAmount - $dicountedAmount);

                            $transation->setPayerEmail($customer->getIdUser()->getEmail());
                            $transation->setTransationJson(json_encode($CardData));
                            if ($paymentMethod === TblPaymentMethods::STRIPE) {
                                $transation->setTransactionId($charges['id']);
                            } else {
                                $transation->setPayerId($CardData['PayerID']);
                            }
                            $em->persist($transation);
                            if (count($productId) > 0) {
                                $streaming = $em->getRepository(TblStreamingPages::class)
                                                ->findBy(['idProduct' => $productId]);
                                foreach ($streaming as $str) {
                                    $userCC = new TblUserCcCode();
                                    $userCC->setIdUser($user);
                                    $userCC->setIdStreamingPage($str);

                                    $em->persist($userCC);
                                }
                            }
                        }
                        $em->flush();
                    }
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * @param $customer
     */
    private function checkSessionAuth($customer)
    {
        if ($customer) {
            $session     = new Session();
            $cartSession = $this->entityManager->getRepository(TblCart::class)
                                               ->findOneBy(['nonAuthId' => $session->get('nonAuthId')]);
            if ($cartSession) {
                $cartSession->setIdCustomer($customer);
                $cartSession->setNonAuthId($customer->getId());

                $this->entityManager->persist($cartSession);
                $this->entityManager->flush();
            }
        }
    }

    private function getCouponDiscountAmount(TblCartCoupons $cartCoupons, $Amount)
    {
        $couponData = $this->entityManager->getRepository(TblCartCoupons::class)->find($cartCoupons);
        if ($couponData->getCouponType() === TblCartCoupons::PERCENTAGE_BASED) {
            $discountedAmount = ($Amount * $couponData->getDiscount()) /100;
        } else { // Amount Based
            $discountedAmount = $couponData->getDiscount();
        }

        return $discountedAmount;
    }
}
