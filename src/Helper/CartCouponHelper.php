<?php
namespace App\Helper;

/**
 * Class CartCouponHelper
 * @package App\Helper
 */
class CartCouponHelper
{

    /**
     * @param $formData
     *
     * @return array|mixed
     * @throws \Exception
     */
    public function createCoupon($formData)
    {
        //Duration : Specifies how long the discount will be in effect. Can be forever, once, or repeating
        try {
            if ($formData->getcouponType() == 1) {
                $data = [
                    "id"          => $formData->getcouponCode(),
                    "percent_off" => $formData->getdiscount(),
                    "duration"    => "forever",
                    "name"        => $formData->gettitle(),
                ];
            } else {
                $data = [
                    "id"         => $formData->getcouponCode(),
                    "amount_off" => $formData->getdiscount()*100,
                    "duration"   => "forever",
                    "currency"   => 'usd',
                    "name"       => $formData->gettitle()
                ];
            }
            $strip = \Stripe\Coupon::create($data);
            return $strip->jsonSerialize();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param $formData
     * @param $stripId
     *
     * @return \Stripe\StripeObject
     * @throws \Exception
     */
    public function updateCoupon($formData, $stripId)
    {
        try {
            $p       = \Stripe\Coupon::retrieve($stripId);
            $p->name = $formData->gettitle();
            $p->save();
            return $p;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param array $stripId
     *
     * @return \Stripe\StripeObject
     */
    public function deleteCoupon(array $stripId)
    {
        $data = [];
        foreach ($stripId as $id) {
            $plan = \Stripe\Coupon::retrieve($id);
            $plan->delete();
            $data[] = $plan;
        }
        return $plan;
    }
}
