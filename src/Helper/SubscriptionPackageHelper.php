<?php

namespace App\Helper;

/**
 * Class SubscriptionPackageHelper
 * @package App\Helper
 */
class SubscriptionPackageHelper
{
    /**
     * @param $formData
     *
     * @return array|mixed
     */
    public function createPackages($formData)
    {
        $data  = [
            "name" => $formData->getpackagename(),
            "type" => "service",
        ];
        $strip = \Stripe\Product::create($data);
        return $strip->jsonSerialize();
    }

    /**
     * @param $formData
     * @param $stripId
     *
     * @return \Stripe\Product
     */
    public function updatePackages($formData, $stripId)
    {
        $package = \Stripe\Product::retrieve($stripId);

        $package->name = $formData->getpackagename();
        $package->save();
        return $package;
    }

    /**
     * @param array $stripId
     *
     * @return
     */
    public function deletePackages(array $stripId)
    {
        $data = [];
        foreach ($stripId as $id) {
            $product = \Stripe\Product::retrieve($id);
            $product->delete();
            $data[] = $product;
        }
        return $data;
    }

    public function coachSubscription($entity)
    {
        $name = $entity->getIdUser()->getProfile()->getFirstname() ." ".
                $entity->getIdUser()->getProfile()->getLastname();
        $customer = \Stripe\Customer::create([
            "name"   => $name,
            "email"  => $entity->getIdUser()->getEmail(),
            "source" => $entity->getStripeTempToken() // obtained with Stripe.js
        ]);
        if ($entity->getIdCartCoupon()) {
            $coupon = $entity->getIdCartCoupon()->getStripId();
        } else {
            $coupon = null;
        }
        $subscription = \Stripe\Subscription::create([
            "customer"          => $customer->id,
            "default_source"    => $customer->sources->data[0]->id,
            "items"             => [
                [
                    "plan" => $entity->getIdSubscriptionCoachPlan()->getStripePlanId(),
                ],
            ],
            'coupon'            => $coupon,
            'trial_period_days' => $entity->getIdSubscriptionCoachPlan()->getTrialDays() ?
                $entity->getIdSubscriptionCoachPlan()->getTrialDays() : null
        ]);

        return $subscription->jsonSerialize();
    }
}
