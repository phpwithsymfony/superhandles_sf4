<?php

namespace App\Helper;

/**
 * Class SubscriptionPlanHelper
 * @package App\Helper
 */
class SubscriptionPlanHelper
{
    /**
     * @param $formData
     *
     * @return array|mixed
     */
    public function createPlanforUniversity($formData)
    {
        $interval = (int)$formData->getpackageInterval();
        if ($interval === 12) {
            $interval_count = 1;
            $interval       = 'year';
        } else {
            $interval_count = $interval;
            $interval       = 'month';
        }
        $trialPeriodDays = $formData->gettrialDays() ? $formData->gettrialDays() : null;
        $data            = [
            "amount"            => (int)$formData->getamount() * 100,
            "interval"          => $interval,
            "interval_count"    => $interval_count,
            "id"                => strtolower(str_replace(' ', '_', $formData->getplanname())),
            "currency"          => "usd",
            "trial_period_days" => $trialPeriodDays,
            "product"           => $formData->getidSubscriptionPackage()->getstripeProductId()

        ];

        $strip = \Stripe\Plan::create($data);
        return $strip->jsonSerialize();
    }

    public function createPlanforCoach($formData)
    {
        $interval = (int)$formData->getpackageInterval();
        if ($interval === 12) {
            $interval_count = 1;
            $interval       = 'year';
        } else {
            $interval_count = $interval;
            $interval       = 'month';
        }
        $trialPeriodDays = $formData->gettrialDays() ? $formData->gettrialDays() : null;
        $data            = [
            "amount"            => (int)$formData->getamount() * 100,
            "interval"          => $interval,
            "interval_count"    => $interval_count,
            "id"                => strtolower(str_replace(' ', '_', $formData->getplanname())),
            "currency"          => "usd",
            "trial_period_days" => $trialPeriodDays,
            "product"           => $formData->getidSubscriptionPlan()->getidSubscriptionPackage()->getstripeProductId()

        ];

        $strip = \Stripe\Plan::create($data);
        return $strip->jsonSerialize();
    }

    /**
     * @param $formData
     * @param $stripId
     *
     * @return \Stripe\Plan
     */
    public function updatePlan($formData, $stripId)
    {
        $plan           = \Stripe\Plan::retrieve($stripId);
        $plan->nickname     = strtolower(str_replace(' ', '_', $formData->getplanname()));
        $plan->save();
        return $plan;
    }

    /**
     * @param array $stripId
     *
     * @return \Stripe\Plan
     */
    public function deletePlan(array $stripId)
    {
        $data = [];
        foreach ($stripId as $id) {
            $plan = \Stripe\Plan::retrieve($id);
            $plan->delete();
            $data[] = $plan;
        }
        return $plan;
    }
}
