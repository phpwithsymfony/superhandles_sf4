<?php

namespace App\Helper;

use App\Entity\TblStreamingPages;
use App\Entity\TblUserCcCode;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CcCodeHelper
 * @package App\Helper
 */
class CcCodeHelper
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * AddtoCartHelper constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }


    /**
     * @param TblStreamingPages $streamingPages
     * @param User              $user
     */
    public function addCcCode(TblStreamingPages $streamingPages, User $user)
    {
        $ccCode = new TblUserCcCode();
        $ccCode->setIdStreamingPage($streamingPages);
        $ccCode->setIdUser($user);

        $this->entityManager->persist($ccCode);
    }

    /**
     * @param TblStreamingPages $streamingPages
     * @param User              $user
     */
    public function removeCcCode(TblStreamingPages $streamingPages, User $user)
    {
        $ccCode = $this->entityManager->getRepository(TblUserCcCode::class)
                                      ->findOneBy(['idStreamingPage'=>$streamingPages, 'idUser'=>$user]);
        if ($ccCode) {
            $this->entityManager->remove($ccCode);
        }
    }
}
