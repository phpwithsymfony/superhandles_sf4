<?php

namespace App\Helper;

use App\Entity\TblCart;
use App\Entity\TblCartProduct;
use App\Entity\TblCountries;
use App\Entity\TblCustomer;
use App\Entity\TblOrderAddress;
use App\Entity\TblProductSize;
use App\Entity\TblShippingCharges;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\TblPaymentMethods;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AddtoCartHelper
 * @package App\Helper
 */
class AddtoCartHelper
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * AddtoCartHelper constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }


    /**
     * @param $user
     *
     * @return TblCustomer|object|null
     */
    public function createCustomer($user)
    {
        $checkUser = $this->entityManager->getRepository(TblCustomer::class)
                                         ->findOneBy(['idUser' => $user]);
        if (!$checkUser) {
            $customer = new TblCustomer();
            $customer->setIdUser($user);

            $entityManager = $this->entityManager;
            $entityManager->persist($customer);
            $entityManager->flush();
        } else {
            $customer = $checkUser;
        }

        return $customer;
    }


    /**
     * @param      $user
     * @param null $customer
     *
     * @return TblCart|object|null
     */
    public function createCart($user, $customer = null)
    {
        if ($user) {
            if ($customer) {
                $chkCart = $this->entityManager->getRepository(TblCart::class)
                              ->findOneBy(['idCustomer' => $customer, 'status' => 0]);
                if (!$chkCart) {
                    $chkSession = $this->checkSession();
                    $chkCart    = $this->entityManager->getRepository(TblCart::class)
                                     ->findOneBy(['nonAuthId' => $chkSession, 'status' => 0]);
                }
                if ($chkCart) {
                    $chkCart->setIdCustomer($customer);
                    $chkCart->setNonAuthId($customer->getId());

                    $this->entityManager->persist($chkCart);
                    $this->entityManager->flush();
                } else {
                    $chkCart = new TblCart();
                    $chkCart->setIdCustomer($customer);
                    $chkCart->setHostName($_SERVER['REMOTE_ADDR']);


                    $this->entityManager->persist($chkCart);
                    $this->entityManager->flush();
                }
                return $chkCart;
            } else {
                $checkCartCust = $this->entityManager->getRepository(TblCart::class)
                                    ->findOneBy(['idCustomer' => $customer, 'status' => 0]);
                if ($checkCartCust) {
                    return $checkCartCust;
                } else {
                    $checkCartCust = new TblCart();
                    $checkCartCust->setIdCustomer($customer);
                    $checkCartCust->setHostName($_SERVER['REMOTE_ADDR']);


                    $this->entityManager->persist($checkCartCust);
                    $this->entityManager->flush();

                    return $checkCartCust;
                }
            }
        } else {
            $chkSession       = $this->checkSession();
            $checkCartSession = $this->entityManager->getRepository(TblCart::class)
                                   ->findOneBy(['nonAuthId' => $chkSession, 'status' => 0]);
            if ($checkCartSession) {
                return $checkCartSession;
            } else {
                $session     = $this->createSession();
                $cartSession = new TblCart();
                $cartSession->setNonAuthId($session);
                $cartSession->setHostName($_SERVER['REMOTE_ADDR']);

                $this->entityManager->persist($cartSession);
                $this->entityManager->flush();

                return $cartSession;
            }
        }
    }


    /**
     * @return mixed
     */
    public function createSession()
    {
        $session = new Session();
        if (!$session->has('nonAuthId')) {
            $nonAuthId = uniqid() . strtotime(date('Y-m-d H:i:s'));

            $session->start();
            $session->set('nonAuthId', $nonAuthId);
            $session->save();
        }
        return $session->get('nonAuthId');
    }

    /**
     * @return mixed
     */
    public function checkSession()
    {
        $session = new Session();
        if ($session->has('nonAuthId')) {
            return $session->get('nonAuthId');
        }
        return false;
    }


    /**
     * @param      $cartEntity
     * @param      $product
     * @param int  $productQty
     *
     * @param null $producrSize
     *
     * @return TblCartProduct|bool|object|null
     */
    public function createCartProduct($cartEntity, $product, $productQty = 1, $producrSize = null)
    {
        $getSize = $this->entityManager->getRepository(TblProductSize::class)
                                       ->findOneBy(['id' => $producrSize, 'isActive'=>TblProductSize::ACTIVE]);
        if ($getSize) {
            $checkCartProduct = $this->entityManager->getRepository(TblCartProduct::class)
                                                    ->findOneBy(
                                                        ['idCart' => $cartEntity,
                                                         'idProduct' => $product,
                                                         'status' => 0,
                                                         'idProductSize'=>$getSize]
                                                    );
        } else {
            $checkCartProduct = $this->entityManager->getRepository(TblCartProduct::class)
                                                    ->findOneBy(
                                                        ['idCart' => $cartEntity,
                                                         'idProduct' => $product,
                                                         'status' => 0]
                                                    );
        }
        if ($checkCartProduct) {
            $qty = $productQty;
            if ($productQty == 1) {
                $qty = $checkCartProduct->getQuantity() + $productQty;
            }
            $productType = $checkCartProduct->getIdProduct()->getIdProductType()->getAppProductType();
            if ($productType == 1 && $qty > 1) {
                return false;
            }


            $totalAmount = $checkCartProduct->getAmount() * $qty;
            $checkCartProduct->setQuantity($qty);
            $checkCartProduct->setTotalAmount($totalAmount);
            if ($getSize) {
                $checkCartProduct->setIdProductSize($getSize);
            }

            $em = $this->entityManager;
            $em->persist($checkCartProduct);
            $em->flush();
            return $checkCartProduct;
        } else {
            if ($cartEntity) {
                $cartProductEnity = new TblCartProduct();
                $cartProductEnity->setQuantity($productQty);
                $cartProductEnity->setAmount($product->getPrice());
                $cartProductEnity->setIdCart($cartEntity);
                $cartProductEnity->setIdProduct($product);
                $cartProductEnity->setTotalAmount($product->getPrice() * $productQty);
                if ($getSize) {
                    $cartProductEnity->setIdProductSize($getSize);
                }

                $entityManagerCP = $this->entityManager;
                $entityManagerCP->persist($cartProductEnity);
                $entityManagerCP->flush();

                return $cartProductEnity;
            }
            return false;
        }
    }


    /**
     * @param $customer
     *
     * @return TblCart|object|null
     */
    public function checkCartCustomer($customer)
    {
        if ($customer) {
            $cartCustomer = $this->entityManager->getRepository(TblCustomer::class)
                                                ->findOneBy(['idUser' => $customer]);
            $whereClose   = ['idCustomer' => $cartCustomer->getId(), 'status' => 0];
        } else {
            $nonAuthId  = $this->checkSession();
            $whereClose = ['nonAuthId' => $nonAuthId, 'status' => 0];
        }
        $checkCartCustomer = $this->entityManager->getRepository(TblCart::class)
                                                 ->findOneBy($whereClose);
        return $checkCartCustomer;
    }


    /**
     * @param $userData
     *
     * @return TblCartProduct[]|TblOrderAddress[]|TblPaymentMethods[]|\App\Entity\TblStreamingPages[]|bool|object[]
     */
    public function getCart($userData)
    {
        $checkCartCustomer = $this->getCartCustomer($userData);
        $cartArray         = [];
        if ($checkCartCustomer) {
            foreach ($checkCartCustomer as $cc) {
                array_push($cartArray, $cc->getId());
            }
        }
        if ($checkCartCustomer) {
            $productcart = $this->entityManager->getRepository(TblCartProduct::class)
                                               ->findBy(['idCart' => $cartArray]);
            return $productcart;
        }
        return false;
    }


    /**
     * @param $users
     *
     * @return mixed
     */
    public function getCartCustomer($users)
    {
        $checkCartCustomer = $this->entityManager->createQueryBuilder();
        $checkCartCustomer->select('tc')
                          ->from('App:TblCart', 'tc')
                          ->where('tc.status = :status')->setParameter('status', 0);
        if ($users) {
            $customer = $this->entityManager->getRepository(TblCustomer::class)
                                            ->findOneBy(['idUser' => $users]);
            $checkCartCustomer->andWhere('tc.idCustomer = :idCustomer')->setParameter('idCustomer', $customer);
            $session = new Session();
            if ($session->has('nonAuthId')) {
                $checkCartCustomer->orWhere('tc.nonAuthId = :nonAuthId')
                                  ->setParameter('nonAuthId', $session->get('nonAuthId'));
            }
        } else {
            $nonAuthId = $this->checkSession();
            $checkCartCustomer->andWhere('tc.nonAuthId = :nonAuthId')->setParameter('nonAuthId', $nonAuthId);
        }

        $dataId = $checkCartCustomer->getQuery()->getResult();
        return $dataId;
    }


    /**
     * @param $user
     * @param $customer
     *
     * @return TblCartProduct[]|TblOrderAddress[]|TblPaymentMethods[]|\App\Entity\TblStreamingPages[]|array|object[]
     */
    public function getAddressDetails($user, $customer)
    {
        return $this->entityManager->getRepository(TblOrderAddress::class)
                                   ->findBy(['idCustomer' => $customer, 'idUser' => $user], ['id' => 'DESC'], 5);
    }


    /**
     * @return TblCartProduct[]|TblOrderAddress[]|TblPaymentMethods[]|\App\Entity\TblStreamingPages[]|object[]
     */
    public function getPaymentMethod()
    {
        return $this->entityManager->getRepository(TblPaymentMethods::class)
                                   ->findBy(['isActive' => TblPaymentMethods::_ACTIVE]);
    }


    /**
     * @param $cart
     * @param $idOrderAddress
     *
     * @return bool
     */
    public function setCartaddressdetails($cart, $idOrderAddress)
    {
        $cart = $this->entityManager->getRepository(TblCart::class)->find($cart);
        if ($cart) {
            $cart->setIdOrderAddress($idOrderAddress);

            $this->entityManager->persist($cart);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

    /**
     * @param TblCart  $cart
     * @param TblCountries $countries
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getShippingChargeCalculate(TblCart $cart, $country)
    {
        $totalShippingAmount = 0;
        $remainingAmount = 0;
        $cartQuery = $this->entityManager->getRepository(TblCartProduct::class)->findBy(['idCart'=>$cart]);
        if ($cartQuery) {
            $countries = $this->entityManager->getRepository(TblCountries::class)
                ->findOneBy(['countryName'=>$country]);
            foreach ($cartQuery as $carts) {
                if ($carts->getIdProduct()->getIdProductType()->getIsCloth()) {
                    $shiAmount = 0.95 * $carts->getQuantity();
                } else {
                    $shippingQuery = $this->entityManager->getRepository(TblShippingCharges::class)
                                                         ->findShippingCharge($carts->getIdProduct(), $countries);
                    $shiAmount     = $shippingQuery->getPrice() * $carts->getQuantity();
                }
                $totalAmount   = ($carts->getAmount() * $carts->getQuantity()) + $shiAmount;
                $carts->setShippingAmount($shiAmount);
                $carts->setTotalAmount($totalAmount);
                $totalShippingAmount += $shiAmount;
                $remainingAmount     += $carts->getTotalAmount();
            }
        }
        $this->entityManager->flush();

        return ["shippingAmount" => $totalShippingAmount, "remainingAmount" => $remainingAmount];
    }
}
