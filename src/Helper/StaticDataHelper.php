<?php
namespace App\Helper;

/**
 * Class StaticDataHelper
 * @package App\Helper
 */
class StaticDataHelper
{
    /**
     * Used subscriptionplan data
     * @return array
     */
    public function subscriptionPlanOption()
    {
        $array = array(
            "Monthly"=>1,
            "Quarterly"=>3,
            "Half Year"=>6,
            "Yearly"=>12
        );
        return $array;
    }
}
