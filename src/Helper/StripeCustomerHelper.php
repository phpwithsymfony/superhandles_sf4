<?php

namespace App\Helper;

/**
 * Class StripeCustomerHelper
 * @package App\Helper
 */
class StripeCustomerHelper
{

    /**
     * @param $custId
     *
     * @return int|\Stripe\Customer
     * @throws \Exception
     */
    public function checkCustomerExist($custId)
    {
        try {
            $customer = \Stripe\Customer::retrieve($custId);
            return $customer->jsonSerialize();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param        $email
     * @param string $des
     *
     * @return array|\Exception|mixed
     */
    public function createCustomer($email, $des = 'Create Customer')
    {
        try {
            $customerData = [
                "email"       => $email,
                "description" => $des
            ];
            $customer     = \Stripe\Customer::create($customerData);
            return $customer->jsonSerialize();
        } catch (\Exception $e) {
            return $e;
        }
    }


    /**
     * @param        $email
     * @param        $CardData
     * @param string $des
     *
     * @return \Stripe\ApiResource
     * @throws \Exception
     */
    public function createCustomerWithCard($email, $CardData, $des = 'Create Customer with card')
    {
        try {
            $customerData = [
                "email"       => $email,
                "description" => $des,
                "source"      => $CardData['id']
            ];
            $customer     = \Stripe\Customer::create($customerData);
            return $customer;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param        $custId
     * @param        $CardData
     * @param string $pap_custom
     *
     * @return mixed
     * @throws \Exception
     */
    public function updateCustomerWithCard($custId, $CardData, $pap_custom = 'Update customer')
    {
        try {
            $cu              = \Stripe\Customer::retrieve($custId);
            $cu->description = $pap_custom;
            $cu->source      = [
                "object"    => "card",
                "number"    => $CardData['cardNumber'],
                "exp_month" => $CardData['expiryMonth'],
                "exp_year"  => $CardData['expiryYear'],
                "cvc"       => $CardData['cvc']
            ];
            return $cu->save()->jsonSerialize();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param        $customerId
     * @param        $amt
     * @param string $des
     *
     * @return array|mixed
     * @throws \Exception
     */
    public function chargeCoupon($customerId, $amt, $des = 'Charge')
    {
        try {
            $chargeData = [
                "amount"      => round($amt * 100),
                "customer"    => $customerId,
                "currency"    => "usd",
                "description" => $des,
            ];
            $charge     = \Stripe\Charge::create($chargeData);
            return $charge->jsonSerialize();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * generate card token
     * @param $CardData
     * @return mixed
     * @throws \Exception
     */
    public function generateCardToken($CardData, $stripeApiKey)
    {
        try {
            $stripe = new \Stripe\StripeClient($stripeApiKey);
            $result = $stripe->tokens->create([
                'card' => [
                    'number' => $CardData["cardNumber"],
                    'exp_month' => $CardData["expiryMonth"],
                    'exp_year' => $CardData["expiryYear"],
                    'cvc' => $CardData["cvc"],
                ],
            ]);
            return $result->jsonSerialize();
        } catch (\Exception $e){
            throw $e;
        }
    }
}
