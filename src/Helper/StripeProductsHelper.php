<?php
namespace App\Helper;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class StripeProductsHelper
 * @package App\Helper
 */
class StripeProductsHelper extends AbstractController
{

    /**
     * @param $entity
     *
     * @return array|mixed
     * @throws \Exception
     */
    public function createProduct($entity)
    {
        $weight = ($entity->getWeight() == null) ? 0 : $entity->getWeight();
        try {
            $description = strip_tags($entity->getDescription());
            $data = [
                "name" => $entity->getTitle(),
                "type" => "good",
                "description" => $description,
                "package_dimensions" => [
                    'height' => 0,
                    'length' => 0,
                    'weight' => $weight,
                    'width' => 0,
                ],

            ];
            $strip =  \Stripe\Product::create($data);
            $returnData = $strip->jsonSerialize();
            try {
                $quantity = ($entity->getQuantity() == null) ? 1 : $entity->getQuantity();
                $skudata = [
                    "product" => $returnData['id'],
                    "price" => round($entity->getPrice()*100),
                    "currency" => "usd",
                    "inventory" => [
                        "type" => "finite",
                        "quantity" => $quantity
                    ]
                ];
                \Stripe\SKU::create($skudata);
            } catch (\Exception $e) {
                $plan = \Stripe\Product::retrieve($returnData['id']);
                $plan->delete();

                throw $e;
            }
            return $returnData;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param $entity
     * @param $stripId
     *
     * @return \Stripe\ApiResource
     * @throws \Exception
     */
    public function updateProduct($entity, $stripId)
    {
        try {
            $description = strip_tags($entity->getDescription());
            $weight      = ($entity->getWeight() == null) ? 0 : $entity->getWeight();
            $data        = [
                "name"               => $entity->getTitle(),
                "description"        => $description,
                "package_dimensions" => [
                    'height' => 0,
                    'length' => 0,
                    'weight' => $weight,
                    'width'  => 0,
                ],

            ];

            $p = \Stripe\Product::update($stripId, $data);
        } catch (\Exception $e) {
            throw $e;
        }
        try {
            $skuData = [
                "price"     => round($entity->getPrice() * 100),
                "currency"  => "usd",
                "inventory" => [
                    "type"     => "finite",
                    "quantity" => $entity->getQuantity()
                ]
            ];


            if ($p->jsonSerialize()['skus']['data']) {
                $skuId = $p->jsonSerialize()['skus']['data'][0]['id'];

                \Stripe\SKU::update($skuId, $skuData);
            }
            return $p;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $stripId
     *
     * @return \Stripe\Product
     */
    public function deleteProduct(array $stripId)
    {
        $data = [];
        foreach ($stripId as $id) {
            $plan = \Stripe\Product::retrieve($id);
            $plan->delete();
            $data[] = $plan;
        }
        return $plan;
    }
}
