<?php
declare(strict_types=1);

namespace App\Service;

class EmailService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig\Environment
     */
    private $template;

    /**
     * EmailService constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig\Environment $template
     */
    public function __construct(\Swift_Mailer $mailer, \Twig\Environment $template)
    {
        $this->mailer = $mailer;
        $this->template = $template;
    }


    /**
     * @param $template
     * @param $parameters
     * @param $to
     * @param $from
     * @param null $fromName
     * @return int|string
     */
    public function sendEmail($template, $parameters, $to, $from, $fromName = null)
    {
        $template = $this->template->loadTemplate($template . '.html.twig');
        $subject = $parameters['subject'];
        $bodyHtml = $template->render($parameters);

        try {
            $message = (new  \Swift_Message($subject))
                ->setFrom($from, $fromName)
                ->setTo($to)
                ->setBody($bodyHtml, 'text/html');
            $response = $this->mailer->send($message);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        return $response;
    }
}