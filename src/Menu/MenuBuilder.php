<?php
namespace App\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class MenuBuilder
 * @package App\Menu
 */
class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Request|null
     */
    private $request;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, RequestStack $request)
    {
        $this->factory = $factory;
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function createMainMenu(array $options)
    {
        $main_menu = $this->factory->createItem('root')
                                   ->setChildrenAttribute('class', 'navbar-nav text-uppercase ml-auto');

        $main_menu->addChild('Home', ['route' => 'front_home_index'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
        if (isset($options['isUserLogin']) && $options['isUserLogin']) {
            $main_menu->addChild('My Account', ['route' => 'my_account'])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
        }
        $main_menu->addChild('University', ['uri' => '#'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
        if (isset($options['isUserLogin']) && $options['isUserLogin']) {
            $main_menu->addChild('Coaches', ['route' => 'front_coach_coach_team_list'])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
        } else {
            $main_menu->addChild('Coaches', ['route' => 'front_coach_index'])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
        }
        $main_menu->addChild('Hoopfolio', ['route' => 'front_hoopfolio_index'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
        $main_menu->addChild('Store', ['route' => 'front_store_product'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
        $main_menu->addChild('Camps', ['route' => 'front_camps_index'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
//        $main_menu->addChild('Testimonials', ['uri' => '#'])
//            ->setAttribute('class', 'nav-item')
//            ->setLinkAttribute('class', 'nav-link');
        $main_menu->addChild('Media', ['route' => 'front_media_index'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');
        $main_menu->addChild('Train', ['uri' => '#'])
                  ->setAttribute('class', 'nav-item')
                  ->setLinkAttribute('class', 'nav-link');

        if (isset($options['isUserLogin']) && $options['isUserLogin']) {
            $main_menu->addChild('Logout', ['route'  => 'fos_user_security_logout',
                                            'label'  => "<i class='fas fa-unlock'></i>Logout",
                                            'extras' => ['safe_label' => true],
            ])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
        } else {
            $main_menu->addChild('Register', ['route'  => 'register',
                                              'label'  => "<i class='fas fa-user'></i>Register",
                                              'extras' => ['safe_label' => true],
            ])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
            $main_menu->addChild('Login', ['route'  => 'fos_user_security_login',
                                           'label'  => "<i class='fas fa-unlock'></i>Login",
                                           'extras' => ['safe_label' => true],
            ])
                      ->setAttribute('class', 'nav-item')
                      ->setLinkAttribute('class', 'nav-link');
        }

        return $main_menu;
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    public function createBottomMenu()
    {
        $bottom_menu = $this->factory->createItem('root');

        $bottom_menu->addChild('Home', ['route' => 'front_home_index']);
        $bottom_menu->addChild('About Us', ['route' => 'cms_page', 'routeParameters' => ['slug' => 'about']]);
        $bottom_menu->addChild('F.A.Q', ['route' => 'cms_page', 'routeParameters' => ['slug' => 'faq']]);
        $bottom_menu->addChild(
            'Privacy Policy',
            ['route' => 'cms_page', 'routeParameters' => ['slug' => 'privacy-policy']]
        );
        $bottom_menu->addChild(
            'Terms and Conditions',
            ['route' => 'cms_page', 'routeParameters' => ['slug' => 'terms-and-conditions']]
        );
        $bottom_menu->addChild('Contact Us', ['uri' => '#']);
        $bottom_menu->addChild('Blog', ['uri' => '#']);
        $bottom_menu->addChild('Site Map', ['uri' => '#']);

        return $bottom_menu;
    }

    /**
     * @Desc create Hoopfolio menu
     *
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function hoopfolioMenu(array $options)
    {
        $hoopfolio_menu = $this->factory->createItem('root');

        if (isset($options['routeName']) && $options['routeName'] === 'my_profile') {
            $hoopfolio_menu->addChild('hoopfolio', [
                'route'  => 'front_hoopfolio_index',
                'label'  => '<i class="fas fa-user"></i>Back to Hoopfolio',
                'extras' => ['safe_label' => true],
            ]);
        } else {
            $hoopfolio_menu->addChild('hoopfolio', [
                'route'  => 'front_hoopfolio_index',
                'label'  => '<i class="fas fa-user"></i>Hoopfolio',
                'extras' => ['safe_label' => true],
            ]);
        }
        $hoopfolio_menu->addChild('addWorkout', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-chart-bar"></i>Add a Workout',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('calender', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-calendar-day"></i>Calender',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('skillAnalytics', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-user-plus"></i>Skill Analytics',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('sendMessage', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-sms"></i>Send a Message',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('sendWorkout', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-list-alt"></i>Send Workout',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('message', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-envelope"></i>Message<span>(1)</span>',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('friends', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-users"></i>Friends',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('friendsRequest', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-user-plus"></i>Friends Request<span>(2)</span>',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('searchFriends', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-search-plus"></i>Search Friends',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('mobileDevices', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-mobile-alt"></i>My Mobile Devices',
            'extras' => ['safe_label' => true],
        ]);
        $hoopfolio_menu->addChild('trainWithSupe', [
            'uri'    => '#',
            'label'  => '<i class="fas fa-basketball-ball"></i>Train with Supe',
            'extras' => ['safe_label' => true],
        ]);

        return $hoopfolio_menu;
    }

    /**
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function createTopMenu(array $options)
    {
        $top_menu = $this->factory->createItem('root');
        if (isset($options['isUserLogin']) && $options['isUserLogin']) {
            $top_menu->addChild('Logout', ['route'  => 'fos_user_security_logout',
                                           'label'  => "<i class='fas fa-unlock'></i>Logout",
                                           'extras' => ['safe_label' => true],
            ]);
        } else {
            $top_menu->addChild('Register', ['route'  => 'register',
                                             'label'  => "<i class='fas fa-user'></i>Register",
                                             'extras' => ['safe_label' => true],
            ]);
            $top_menu->addChild('Login', ['route'  => 'fos_user_security_login',
                                          'label'  => "<i class='fas fa-unlock'></i>Login",
                                          'extras' => ['safe_label' => true],
            ]);
        }
        return $top_menu;
    }

    /**
     * @return \Knp\Menu\ItemInterface
     */
    public function createAdminMenu()
    {
        $admin_menu = $this->factory->createItem(
            'root',
            ['childrenAttributes' => ['class' => 'sidebar-panel nav']]
        );

        $admin_menu->addChild('Dashboard', [
            'route'  => 'admin_home',
            'label'  => "<span class='icon color5'><i class='fa fa-tachometer'></i></span> Dashboard",
            'extras' => ['safe_label' => true],
        ]);

        //HOME PAGE
        $admin_menu->addChild('Home_Page', [
            'route'  => 'home_slider_list',
            'label'  => "<span class='icon color5'><i class='fa fa-home'></i></span> 
                            Home Slider</span>",
            'extras' => ['safe_label' => true],
        ]);


        //CMS
        $admin_menu->addChild('CMS', [
            'route'  => 'cms_list',
            'label'  => "<span class='icon color5'><i class='fa fa-files-o'></i></span> CMS ",
            'extras' => ['safe_label' => true]
        ]);
        //MEDIA
        $admin_menu->addChild('Media & WYNV', [
            'route'  => 'media_video_list',
            'label'  => "<span class='icon color5'><i class='fa fa-video-camera'></i></span> 
                            Media & WYNV",
            'extras' => ['safe_label' => true],
        ]);
        //CAMPS
        $admin_menu->addChild('Camps', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-university'></i></span> 
                            Camps <span class='caret'></span>",
            'extras' => ['safe_label' => true]
        ]);

        $admin_menu['Camps']->addChild('Camps_Slider', [
            'route'  => 'camps_slider_list',
            'label'  => "Camps Sliders",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Camps']->addChild('Camps', [
            'route'  => 'camps_list',
            'label'  => "Camps",
            'extras' => ['safe_label' => true],
        ]);


        // Testimonials
        $admin_menu->addChild('Testimonials', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-star'></i></span> 
                            Testimonials <span class='caret'></span>",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Testimonials']->addChild('Testimonials_Posts', [
            'route'  => 'testimonials_list',
            'label'  => "Testimonials Posts",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Testimonials']->addChild('Testimonials_Videos', [
            'route'  => 'testimonials_video_list',
            'label'  => "Testimonials Videos",
            'extras' => ['safe_label' => true],
        ]);


        //USERS
        $admin_menu->addChild('Users', [
            'route'  => 'user_list',
            'label'  => "<span class='icon color5'><i class='fa fa-users'></i></span> Users",
            'extras' => ['safe_label' => true],
        ]);

        //Shopping Cart
        $admin_menu->addChild('Shopping_Cart', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-shopping-cart'></i></span>
                            Shopping Cart <span class='caret'></span>",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Shopping_Cart']->addChild('Categories', [
            'route'  => 'productcat_list',
            'label'  => "Categories",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Shopping_Cart']->addChild('Products', [
            'route'  => 'products_list',
            'label'  => "Products",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Shopping_Cart']->addChild('Streaming_Pages', [
            'route'  => 'streamingpages_list',
            'label'  => "Streaming Pages",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Shopping_Cart']->addChild('Coupons', [
            'route'    => 'promocode_coupon_list',
            'label'  => "Coupons",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Shopping_Cart']->addChild('Order Management', [
            'route'  => 'order_management_list',
            'label'  => "Order Management",
            'extras' => ['safe_label' => true],
        ]);

        // Clothing product
        $admin_menu->addChild('Clothing', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-shopping-cart'></i></span> 
                            Clothing <span class='caret'></span>",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Clothing']->addChild('ClothProduct', [
            'route'  => 'cloth_list',
            'label'  => "Clothing",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Clothing']->addChild('ClothCategory', [
            'route'  => 'cloth_category_list',
            'label'  => "Category",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Clothing']->addChild('ClothSize', [
            'route'  => 'cloth_size_list',
            'label'  => "Sizes",
            'extras' => ['safe_label' => true],
        ]);

        // Product Buy Now Page
        $admin_menu->addChild(' Product Buy Now Page', [
            'route'  => 'productlaunch_list',
            'label'  => "<span class='icon color5'><i class='fa fa-flag'></i></span>  Product Buy Now Page",
            'extras' => ['safe_label' => true],
        ]);

        //Shipping
        $admin_menu->addChild('Shipping Charges', [
            'route'  => 'shippingcharges_list',
            'label'  => "<span class='icon color5'><i class='fa fa-flag'></i></span> Shipping Charges",
            'extras' => ['safe_label' => true],
        ]);

        // Leaderboard
        $admin_menu->addChild('Leaderboard', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-star'></i></span> 
                            Leaderboard <span class='caret'></span>",
            'extras' => ['safe_label' => true],
        ]);

        $admin_menu['Leaderboard']->addChild('Streaming_Workouts', [
            'route'  => 'streamingworkout_list',
            'label'  => "Streaming Workout",
            'extras' => ['safe_label' => true],
        ]);

        //Subscrition Plan
        $admin_menu->addChild('Subscription', [
            'uri'    => 'javascript:void(0)',
            'label'  => "<span class='icon color5'><i class='fa fa-star'></i></span> 
                            Subscription <span class='caret'></span>",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Subscription']->addChild('Subscrition Package', [
            'route'  => 'subscription_package_list',
            'label'  => "<span class='icon color5'><i class='fa fa-flag'></i></span> Subscription package",
            'extras' => ['safe_label' => true],
        ]);
        $admin_menu['Subscription']->addChild('Subscrition Plan', [
            'route'  => 'subscriptionplan_list',
            'label'  => "<span class='icon color5'><i class='fa fa-flag'></i></span> Subscription Plan",
            'extras' => ['safe_label' => true],
        ]);

        switch ($this->request->get('_route')) {
            case 'home_slider_add':
                $admin_menu['Home_Page']->setCurrent(true);
                break;
        }

        return $admin_menu;
    }

    /**
     * @return ItemInterface
     */
    public function createBreadcrumbsMenu()
    {
        $menu = $this->factory->createItem(
            'root',
            ['childrenAttributes' => ['class' => 'breadcrumb']]
        );

        // this item will always be displayed
        $menu->addChild('Dashboard', ['route' => 'admin_home']);

        // create the menu according to the route
        switch ($this->request->get('_route')) {
            //home slider breadcrums
            case 'home_slider_list':
                $menu['Dashboard']->addChild('HomeSlider', [
                    'route' => 'home_slider_list',
                    'label' => "Home Slider List",
                ]);
                break;
            case 'home_slider_add':
                $menu['Dashboard']->addChild('HomeSlider', [
                    'route' => 'home_slider_list',
                    'label' => "Home Slider List",
                ]);
                $menu['Dashboard']['HomeSlider']->addChild('AddHomeSlider', [
                    'label' => "Add Home Slider",
                ]);

                break;
            case 'home_slider_edit':
                $menu['Dashboard']->addChild('HomeSlider', [
                    'route' => 'home_slider_list',
                    'label' => "Home Slider List",
                ]);
                $menu['Dashboard']['HomeSlider']->addChild('EditHomeSlider', [
                    'label' => "Edit Home Slider",
                ]);
                break;
            //cms breadcrums
            case 'cms_list':
                $menu['Dashboard']->addChild('Cms', [
                    'route' => 'cms_list',
                    'label' => "CMS List",
                ]);
                break;
            case 'cms_add':
                $menu['Dashboard']->addChild('Cms', [
                    'route' => 'cms_list',
                    'label' => "CMS List",
                ]);
                $menu['Dashboard']['Cms']->addChild('AddHomeSlider', [
                    'label' => "Add CMS",
                ]);
                break;
            case 'cms_edit':
                $menu['Dashboard']->addChild('Cms', [
                    'route' => 'cms_list',
                    'label' => "CMS List",
                ]);
                $menu['Dashboard']['Cms']->addChild('EditHomeSlider', [
                    'label' => "Edit CMS",
                ]);
                break;
            //media breadcrums
            case 'media_video_list':
                $menu['Dashboard']->addChild('MediaVideo', [
                    'route' => 'media_video_list',
                    'label' => "Media Video List",
                ]);
                break;
            case 'media_video_add':
                $menu['Dashboard']->addChild('Cms', [
                    'route' => 'media_video_list',
                    'label' => "Media Video List",
                ]);
                $menu['Dashboard']['Cms']->addChild('AddMediaVideo', [
                    'label' => "Add CMS",
                ]);
                break;
            case 'media_video_edit':
                $menu['Dashboard']->addChild('Cms', [
                    'route' => 'media_video_list',
                    'label' => "Media Video List",
                ]);
                $menu['Dashboard']['Cms']->addChild('EditMediaVideo', [
                    'label' => "Edit Media Video",
                ]);
                break;
            //cams breadcrums
            case 'camps_list':
                $menu['Dashboard']->addChild('Camps', [
                    'route' => 'camps_list',
                    'label' => "Camps List",
                ]);
                break;
            case 'camps_add':
                $menu['Dashboard']->addChild('Camps', [
                    'route' => 'camps_list',
                    'label' => "Camps List",
                ]);
                $menu['Dashboard']['Camps']->addChild('Camps', [
                    'label' => "Add Camps",
                ]);
                break;
            case 'camps_edit':
                $menu['Dashboard']->addChild('Camps', [
                    'route' => 'camps_list',
                    'label' => "Camps List",
                ]);
                $menu['Dashboard']['Camps']->addChild('Camps', [
                    'label' => "Edit Camps",
                ]);
                break;
            //cams slider breadcrums
            case 'camps_slider_list':
                $menu['Dashboard']->addChild('CampsSlider', [
                    'route' => 'camps_slider_list',
                    'label' => "Camps Slider List",
                ]);
                break;
            case 'camps_slider_add':
                $menu['Dashboard']->addChild('CampsSlider', [
                    'route' => 'camps_slider_list',
                    'label' => "Camps Slider List",
                ]);
                $menu['Dashboard']['CampsSlider']->addChild('AddCamsSlider', [
                    'label' => "Add Camps Slider",
                ]);
                break;
            case 'camps_slider_edit':
                $menu['Dashboard']->addChild('CampsSlider', [
                    'route' => 'camps_slider_list',
                    'label' => "Camps Slider List",
                ]);
                $menu['Dashboard']['CampsSlider']->addChild('EditCamsSlider', [
                    'label' => "Edit Camps Slider",
                ]);
                break;
            //Testimonial Post breadcrums
            case 'testimonials_list':
                $menu['Dashboard']->addChild('Testimonial', [
                    'route' => 'testimonials_list',
                    'label' => "Testimonial List",
                ]);
                break;
            case 'testimonials_add':
                $menu['Dashboard']->addChild('Testimonial', [
                    'route' => 'testimonials_list',
                    'label' => "Testimonial List",
                ]);
                $menu['Dashboard']['Testimonial']->addChild('AddTestimonial', [
                    'label' => "Add Testimonial Post",
                ]);
                break;
            case 'testimonials_edit':
                $menu['Dashboard']->addChild('Testimonial', [
                    'route' => 'testimonials_list',
                    'label' => "Testimonial List",
                ]);
                $menu['Dashboard']['Testimonial']->addChild('EditTestimonial', [
                    'label' => "Edit Testimonial Post",
                ]);
                break;
            //Testimonial Video  breadcrums
            case 'testimonials_video_list':
                $menu['Dashboard']->addChild('TestimonialVideo', [
                    'route' => 'testimonials_video_list',
                    'label' => "Testimonial Video List",
                ]);
                break;
            case 'testimonials_video_add':
                $menu['Dashboard']->addChild('TestimonialVideo', [
                    'route' => 'testimonials_video_list',
                    'label' => "Testimonial Video List",
                ]);
                $menu['Dashboard']['TestimonialVideo']->addChild('AddTestimonialVideo', [
                    'label' => "Add Testimonial Video",
                ]);
                break;
            case 'testimonials_video_edit':
                $menu['Dashboard']->addChild('TestimonialVideo', [
                    'route' => 'testimonials_video_list',
                    'label' => "Testimonial Video",
                ]);
                $menu['Dashboard']['TestimonialVideo']->addChild('EditTestimonialVideo', [
                    'label' => "Edit Testimonial Video",
                ]);
                break;
            //User breadcrums
            case 'user_list':
                $menu['Dashboard']->addChild('User', [
                    'route' => 'user_list',
                    'label' => "User List",
                ]);
                break;
            case 'user_add':
                $menu['Dashboard']->addChild('User', [
                    'route' => 'user_list',
                    'label' => "User List",
                ]);
                $menu['Dashboard']['User']->addChild('AddUser', [
                    'label' => "Add User",
                ]);
                break;
            case 'user_edit':
                $menu['Dashboard']->addChild('User', [
                    'route' => 'user_list',
                    'label' => "User List",
                ]);
                $menu['Dashboard']['User']->addChild('EditUser', [
                    'label' => "Edit User",
                ]);
                break;
            case 'user_view_profile':
                $menu['Dashboard']->addChild('User', [
                    'route' => 'user_list',
                    'label' => "User List",
                ]);
                $menu['Dashboard']['User']->addChild('ViewUser', [
                    'label' => "View User",
                ]);
                break;
            //Shopping cart  Category breadcrums
            case 'productcat_list':
                $menu['Dashboard']->addChild('Category', [
                    'route' => 'productcat_list',
                    'label' => "Category List",
                ]);
                break;
            case 'productcat_add':
                $menu['Dashboard']->addChild('Category', [
                    'route' => 'productcat_list',
                    'label' => "Category List",
                ]);
                $menu['Dashboard']['Category']->addChild('AddCategory', [
                    'label' => "Add Category",
                ]);
                break;
            case 'productcat_edit':
                $menu['Dashboard']->addChild('Category', [
                    'route' => 'productcat_list',
                    'label' => "Category List",
                ]);
                $menu['Dashboard']['Category']->addChild('EditCategory', [
                    'label' => "Edit Category",
                ]);
                break;
            //Shopping cart  Product breadcrums
            case 'products_list':
                $menu['Dashboard']->addChild('Products', [
                    'route' => 'products_list',
                    'label' => "Products List",
                ]);
                break;
            case 'products_add':
                $menu['Dashboard']->addChild('Products', [
                    'route' => 'products_list',
                    'label' => "Products List",
                ]);
                $menu['Dashboard']['Products']->addChild('AddProducts', [
                    'label' => "Add Product",
                ]);
                break;
            case 'products_edit':
                $menu['Dashboard']->addChild('Products', [
                    'route' => 'products_list',
                    'label' => "Products List",
                ]);
                $menu['Dashboard']['Products']->addChild('EditProducts', [
                    'label' => "Edit Product",
                ]);
                break;
            //Shopping cart  Streaming pages breadcrums
            case 'streamingpages_list':
                $menu['Dashboard']->addChild('StreamingPage', [
                    'route' => 'streamingpages_list',
                    'label' => "Streaming Page List",
                ]);
                break;
            case 'streamingpages_add':
                $menu['Dashboard']->addChild('StreamingPage', [
                    'route' => 'streamingpages_list',
                    'label' => "Streaming Page List",
                ]);
                $menu['Dashboard']['StreamingPage']->addChild('AddStreamingPage', [
                    'label' => "Add Streaming Page",
                ]);
                break;
            case 'streamingpages_edit':
                $menu['Dashboard']->addChild('StreamingPage', [
                    'route' => 'streamingpages_list',
                    'label' => "Streaming Page List",
                ]);
                $menu['Dashboard']['StreamingPage']->addChild('EditStreamingPage', [
                    'label' => "Edit Streaming Page",
                ]);
                break;
            //Shopping cart  promocoe coupon breadcrums
            case 'promocode_coupon_list':
                $menu['Dashboard']->addChild('couponCode', [
                    'route' => 'promocode_coupon_list',
                    'label' => "Coupon Code List",
                ]);
                break;
            case 'promocode_coupon_add':
                $menu['Dashboard']->addChild('couponCode', [
                    'route' => 'promocode_coupon_list',
                    'label' => "Coupon Code List",
                ]);
                $menu['Dashboard']['couponCode']->addChild('AddcouponCode', [
                    'label' => "Add Coupon",
                ]);
                break;
            case 'promocode_coupon_edit':
                $menu['Dashboard']->addChild('couponCode', [
                    'route' => 'promocode_coupon_list',
                    'label' => "Coupon Code List",
                ]);
                $menu['Dashboard']['couponCode']->addChild('EditcouponCode', [
                    'label' => "Edit Coupon",
                ]);
                break;
            //Shopping cart  Order Management breadcrums
            case 'order_management_list':
                $menu['Dashboard']->addChild('Orders', [
                    'route' => 'order_management_list',
                    'label' => "Order List",
                ]);
                break;
            // Clothing cloth breadcrums
            case 'cloth_list':
                $menu['Dashboard']->addChild('Cloth', [
                    'route' => 'cloth_list',
                    'label' => "Cloth List",
                ]);
                break;
            case 'cloth_add':
                $menu['Dashboard']->addChild('Cloth', [
                    'route' => 'cloth_list',
                    'label' => "Cloth list",
                ]);
                $menu['Dashboard']['Cloth']->addChild('ClothAdd', [
                    'label' => "Add Cloth",
                ]);
                break;
            case 'cloth_edit':
                $menu['Dashboard']->addChild('Cloth', [
                    'route' => 'cloth_list',
                    'label' => "Cloth List",
                ]);
                $menu['Dashboard']['Cloth']->addChild('ClothEdit', [
                    'label' => "Edit Cloth",
                ]);
                break;
            // Clothing cloth Category breadcrums
            case 'cloth_category_list':
                $menu['Dashboard']->addChild('ClothCategory', [
                    'route' => 'cloth_category_list',
                    'label' => "Category List",
                ]);
                break;
            case 'cloth_category_add':
                $menu['Dashboard']->addChild('ClothCategory', [
                    'route' => 'cloth_category_list',
                    'label' => "Category list",
                ]);
                $menu['Dashboard']['ClothCategory']->addChild('ClothCategoryAdd', [
                    'label' => "Add Category",
                ]);
                break;
            case 'cloth_category_edit':
                $menu['Dashboard']->addChild('ClothCategory', [
                    'route' => 'cloth_category_list',
                    'label' => "Category List",
                ]);
                $menu['Dashboard']['ClothCategory']->addChild('ClothCategoryEdit', [
                    'label' => "Edit Category",
                ]);
                break;
            // Clothing cloth Size breadcrums
            case 'cloth_size_list':
                $menu['Dashboard']->addChild('ClothSize', [
                    'route' => 'cloth_size_list',
                    'label' => "Size List",
                ]);
                break;
            case 'cloth_size_add':
                $menu['Dashboard']->addChild('ClothSize', [
                    'route' => 'cloth_size_list',
                    'label' => "Size list",
                ]);
                $menu['Dashboard']['ClothSize']->addChild('ClothSizeAdd', [
                    'label' => "Add Size",
                ]);
                break;
            case 'cloth_size_edit':
                $menu['Dashboard']->addChild('ClothSize', [
                    'route' => 'cloth_size_list',
                    'label' => "Size List",
                ]);
                $menu['Dashboard']['ClothSize']->addChild('ClothSizeEdit', [
                    'label' => "Edit Size",
                ]);
                break;
            //LeaderBoard  Add Workouts breadcrums
            case 'streamingworkout_list':
                $menu['Dashboard']->addChild('StreamingWorkout', [
                    'route' => 'streamingworkout_list',
                    'label' => "Streaming Page Workout List",
                ]);
                break;
            case 'streamingworkout_add':
                $menu['Dashboard']->addChild('StreamingWorkout', [
                    'route' => 'streamingworkout_list',
                    'label' => "Streaming Page Workout List",
                ]);
                $menu['Dashboard']['StreamingWorkout']->addChild('AddStreamingWorkout', [
                    'label' => "Add Streaming Page Workout",
                ]);
                break;
            case 'streamingworkout_edit':
                $menu['Dashboard']->addChild('StreamingWorkout', [
                    'route' => 'streamingworkout_list',
                    'label' => "Streaming Page Workout List",
                ]);
                $menu['Dashboard']['StreamingWorkout']->addChild('EditStreamingWorkout', [
                    'label' => "Edit Streaming Page Workout",
                ]);
                break;
            //Subscription Plan breadcrums
            case 'subscriptionplan_list':
                $menu['Dashboard']->addChild('SubscriptionPlan', [
                    'route' => 'subscriptionplan_list',
                    'label' => "Subscription Plan List",
                ]);
                break;
            case 'subscriptionplan_add':
                $menu['Dashboard']->addChild('SubscriptionPlan', [
                    'route' => 'subscriptionplan_list',
                    'label' => "Subscription Plan List",
                ]);
                $menu['Dashboard']['SubscriptionPlan']->addChild('AddSubscriptionPlan', [
                    'label' => "Add Subscription Plan",
                ]);
                break;
            case 'subscriptionplan_edit':
                $menu['Dashboard']->addChild('SubscriptionPlan', [
                    'route' => 'subscriptionplan_list',
                    'label' => "Subscription Plan List",
                ]);
                $menu['Dashboard']['SubscriptionPlan']->addChild('EditSubscriptionPlan', [
                    'label' => "Edit Subscription Plan",
                ]);
                break;

            //Shipping Charges breadcrums
            case 'shippingcharges_list':
                $menu['Dashboard']->addChild('ShippingCharges', [
                    'route' => 'shippingcharges_list',
                    'label' => "Shipping Charges List",
                ]);
                break;
            case 'shippingcharges_add':
                $menu['Dashboard']->addChild('ShippingCharges', [
                    'route' => 'shippingcharges_list',
                    'label' => "Shipping Charges List",
                ]);
                $menu['Dashboard']['ShippingCharges']->addChild('AddShippingCharges', [
                    'label' => "Add Shipping Charge",
                ]);
                break;
            case 'shippingcharges_edit':
                $menu['Dashboard']->addChild('ShippingCharges', [
                    'route' => 'shippingcharges_list',
                    'label' => "Shipping Charges List",
                ]);
                $menu['Dashboard']['ShippingCharges']->addChild('EditShippingCharges', [
                    'label' => "Edit Shipping Charge",
                ]);
                break;
        }

        return $menu;
    }
}
