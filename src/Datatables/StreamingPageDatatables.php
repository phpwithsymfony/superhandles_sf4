<?php

namespace App\Datatables;

use App\Entity\TblStreamingPages;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class StreamingPageDatatables
 *
 * @package App\Datatables
 */
class StreamingPageDatatables extends AbstractDatatable
{
    /**
     * @return callable|\Closure|null
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['isActive']) {
                case TblStreamingPages::_ACTIVE:
                    $line['isActive'] = TblStreamingPages::ACTIVE_LABEL;
                    break;
                case TblStreamingPages::_INACTIVE:
                    $line['isActive'] = TblStreamingPages::INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => false,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html'   => '<div class="start_checkboxes">',
                    'end_html'     => '</div>',
                    'value'        => 'id',
                    'value_prefix' => true,
                    'class_name'   => 'text-center',
                    'actions'      => [
                        [
                            'route'           => 'streamingpages_bulk_delete',
                            'icon'            => 'glyphicon glyphicon-trash',
                            'label'           => 'Delete',
                            'attributes'      => [
                                'rel'   => 'tooltip',
                                'title' => 'Delete',
                                'id'    => 'btn-bulk-delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role'  => 'button',
                            ],
                            'confirm'         => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                            'start_html'      => '<div class="start_delete_action">',
                            'end_html'        => '</div>',

                        ],
                    ],
                ]
            )
            ->add('id', Column::class, ['title' => 'Id'])
            ->add('pageTitle', Column::class, [
                'title'      => 'Title',
                'searchable' => true,
                'orderable'  => true,

            ])
            ->add('idProduct.title', Column::class, [
                'title'      => 'Product Name',
                'searchable' => true,
                'orderable'  => true
            ])
            ->add('idProductType.productType', Column::class, [
                'title'      => 'Product Type',
                'class_name' => 'text-center',
            ])
            ->add('productCcCode', Column::class, [
                'title'      => 'Product CC Code',
                'searchable' => true,
                'orderable'  => true,
            ])
            ->add('isActive', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'editable'   => [SelectEditable::class,
                                 [
                                     'source'     => [
                                         ['value' => TblStreamingPages::_ACTIVE,
                                          'text'  => TblStreamingPages::ACTIVE_LABEL],
                                         ['value' => TblStreamingPages::_INACTIVE,
                                          'text'  => TblStreamingPages::INACTIVE_LABEL],
                                     ],
                                     'mode'       => 'inline',
                                     'empty_text' => 'null',
                                 ],
                ]
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'text-center',
                'actions'    => [
                    [
                        'route'            => 'streamingpages_edit',
                        'label'            => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-primary btn-xs',
                            'role'  => 'button'
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                    [
                        'label'               => 'Delete',
                        'button'              => true,
                        'button_value_prefix' => false,
                        "icon"                => "glyphicon glyphicon-trash",
                        'attributes'          => [
                            'rel'   => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role'  => 'button'
                        ],
                        'start_html'          => '<div class="start_show_action">',
                        'end_html'            => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\TblStreamingPages';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'streamingpages_datatable';
    }
}
