<?php
namespace App\Datatables;

use App\Entity\TblStreamingPageContent;
use App\Entity\TblStreamingPages;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class StreamingPageDatatables
 *
 * @package App\Datatables
 */
class StreamingWorkoutDatatables extends AbstractDatatable
{
    /**
     * @return callable|\Closure|null
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['isActive']) {
                case TblStreamingPageContent::_ACTIVE:
                    $line['isActive'] = TblStreamingPageContent::ACTIVE_LABEL;
                    break;
                case TblStreamingPageContent::_INACTIVE:
                    $line['isActive'] = TblStreamingPageContent::INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        self::getStreamingPage();
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html'   => '<div class="start_checkboxes">',
                    'end_html'     => '</div>',
                    'value'        => 'id',
                    'value_prefix' => true,

                    'class_name' => 'text-center',
                    'actions'    => [
                        [
                            'route'           => 'streamingworkout_bulk_delete',
                            'icon'            => 'glyphicon glyphicon-trash',
                            'label'           => 'Delete',
                            'attributes'      => [
                                'rel'   => 'tooltip',
                                'title' => 'Delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role'  => 'button',
                            ],
                            'confirm'         => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                        ],
                    ],
                ]
            )
            ->add(
                'id',
                Column::class,
                ['title' => 'Id','filter'     => [TextFilter::class, ['classes' => 'hide']],]
            )
            ->add('idStreamingPage.pageTitle', Column::class, [
                'title'      => 'Streaming Page',
                'searchable' => true,
                'orderable'  => true,
                'filter'     => [SelectFilter::class, [
                    'classes'        => 'test1 test2',
                    'search_type'    => 'eq',
                    'select_options' => array_merge([''=>'Any'], self::getStreamingPage()),
                    'cancel_button'  => true,
                ]],

            ])
            ->add('title', Column::class, [
                'title'      => 'Title',
                'searchable' => true,
                'orderable'  => true,
                'filter'     => [TextFilter::class, ['classes' => 'hide']],
            ])
            ->add('video', Column::class, [
                'searchable' => true,
                'title'      => 'Video',
                'filter'     => [TextFilter::class, ['classes' => 'hide']],
            ])
            ->add('isActive', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'filter'     => [TextFilter::class, ['classes' => 'hide']],
                'editable'   => [SelectEditable::class,
                                 [
                                     'source'     => [
                                         ['value' => TblStreamingPageContent::_ACTIVE,
                                          'text'  => TblStreamingPageContent::ACTIVE_LABEL],
                                         ['value' => TblStreamingPageContent::_INACTIVE,
                                          'text'  => TblStreamingPageContent::INACTIVE_LABEL],
                                     ],
                                     'mode'       => 'inline',
                                     'empty_text' => 'null',
                                 ],
                ]
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'actionWidth text-center',
                'actions'    => [
                    [
                        'route'            => 'streamingworkout_edit',
                        'label'            => 'Edit',
                        'route_parameters' =>[
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-primary btn-xs',
                            'role'  => 'button'
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                    [
                        'label'               => 'Delete',
                        'button'              => true,
                        'button_value_prefix' => false,
                        "icon"                => "glyphicon glyphicon-trash",
                        'attributes'          => [
                            'rel'   => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role'  => 'button'
                        ],
                        'start_html'          => '<div class="start_show_action">',
                        'end_html'            => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\TblStreamingPageContent';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'streamingpagecontent_datatable';
    }

    /**
     * @return array
     */
    private function getStreamingPage()
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('sp.id')->addSelect('sp.pageTitle')
            ->from(TblStreamingPages::class, 'sp')
            ->where('sp.isDelete = :delete')
            ->setParameter('delete', TblStreamingPages::_NOTDELETE)
            ->getQuery()->getResult();
        $dataArray = [];
        if ($query) {
            foreach ($query as $q) {
                $dataArray[$q['pageTitle']] = $q['pageTitle'];
            }
        }
        return $dataArray;
    }
}
