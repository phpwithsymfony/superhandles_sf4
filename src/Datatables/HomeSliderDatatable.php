<?php
namespace App\Datatables;

use App\Entity\TblHomeSlider;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class HomeSliderDatatable
 *
 * @package App\Datatables
 */
class HomeSliderDatatable extends AbstractDatatable
{
    /**
     * @return callable|\Closure|null
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['isActive']) {
                case TblHomeSlider::_ACTIVE:
                    $line['isActive'] = TblHomeSlider::_ACTIVE_LABEL;
                    break;
                case TblHomeSlider::_INACTIVE:
                    $line['isActive'] = TblHomeSlider::_INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => false,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html'   => '<div class="start_checkboxes">',
                    'end_html'     => '</div>',
                    'value'        => 'id',
                    'value_prefix' => true,
                    'class_name'   => 'text-center',
                    'actions'      => [
                        [
                            'route'           => 'home_slider_bulk_delete',
                            'icon'            => 'glyphicon glyphicon-trash',
                            'label'           => 'Delete',
                            'attributes'      => [
                                'rel'   => 'tooltip',
                                'title' => 'Delete',
                                'id'    => 'btn-bulk-delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role'  => 'button',
                            ],
                            'confirm'         => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                            'start_html'      => '<div class="start_delete_action">',
                            'end_html'        => '</div>',

                        ],
                    ],
                ]
            )
            ->add('id', Column::class, [
                'title'  => 'Id',
            ])
            ->add('bannerFile', ImageColumn::class, [
                'title' => 'Image',
                'imagine_filter' => 'thumbnail_50_x_50',
                'imagine_filter_enlarged' => 'thumbnail_50_x_50',
                'relative_path' => '/uploads/home_slider',
                'holder_url' => 'https://placehold.it',
                'enlarge' => true,
            ])
            ->add('title', Column::class, [
                'title'      => 'Title',
                'searchable' => true,
                'orderable'  => true,

            ])
            ->add('pageUrl', Column::class, [
                'title'      => 'Camps Category',
                'searchable' => true,
                'orderable'  => true,

            ])
            ->add('isActive', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'editable'   => [SelectEditable::class,
                         [
                             'source'     => [
                                 ['value' => TblHomeSlider::_ACTIVE, 'text' => TblHomeSlider::_ACTIVE_LABEL],
                                 ['value' => TblHomeSlider::_INACTIVE, 'text' => TblHomeSlider::_INACTIVE_LABEL],
                             ],
                             'mode'       => 'inline',
                             'empty_text' => 'null',
                         ],
                ]
            ])
            ->add('createdDate', DateTimeColumn::class, [
                'title'       => 'Date Created',
                'date_format' => 'MM/DD/YYYY',
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',

                'class_name' => 'text-center',
                'actions'    => [
                    [
                        'route'            => 'home_slider_edit',
                        'label'            => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-primary btn-xs',
                            'role'  => 'button'
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                    [
                        'label'               => 'Delete',
                        'button'              => true,
                        'button_value_prefix' => false,
                        "icon"                => "glyphicon glyphicon-trash",
                        'attributes'          => [
                            'rel'   => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role'  => 'button'
                        ],
                        'start_html'          => '<div class="start_show_action">',
                        'end_html'            => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\TblHomeSlider';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'homeSlider_datatable';
    }
}
