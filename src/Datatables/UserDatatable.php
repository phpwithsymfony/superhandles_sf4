<?php
declare(strict_types=1);

namespace App\Datatables;

use App\Entity\User;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class UserDatatable
 *
 * @package App\Datatables
 */
class UserDatatable extends AbstractDatatable
{
    /**
     * format line
     * @return string
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['enabled']) {
                case User::ACTIVE:
                    $line['enabled'] = User::ACTIVE_LABEL;
                    break;
                case User::INACTIVE:
                    $line['enabled'] = User::INACTIVE_LABEL;
                    break;
                default:
                    $line['enabled'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }


    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => false,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html'   => '<div class="start_checkboxes">',
                    'end_html'     => '</div>',
                    'value'        => 'id',
                    'value_prefix' => true,
                    'class_name'   => 'text-center',
                    'actions'      => [
                        [
                            'route'           => 'user_bulk_delete',
                            'icon'            => 'glyphicon glyphicon-trash',
                            'label'           => 'Delete',
                            'attributes'      => [
                                'rel'   => 'tooltip',
                                'title' => 'Delete',
                                'id'    => 'btn-bulk-delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role'  => 'button',
                            ],
                            'confirm'         => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                            'start_html'      => '<div class="start_delete_action">',
                            'end_html'        => '</div>',

                        ],
                    ],
                ]
            )
            ->add('id', Column::class, ['title' => 'Id'])
            ->add('username', Column::class, ['title' => 'Username'])
            ->add('email', Column::class, ['title' => 'Email Id'])
            ->add('enabled', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'editable'   => [SelectEditable::class,
                                 [
                                     'source'     => [
                                         ['value' => User::ACTIVE, 'text' => User::ACTIVE_LABEL],
                                         ['value' => User::INACTIVE, 'text' => User::INACTIVE_LABEL],
                                     ],
                                     'mode'       => 'inline',
                                     'empty_text' => 'null',
                                 ],
                ]
            ])
            ->add('profile.gender', BooleanColumn::class, [
                'title'           => 'Gender',
                'class_name'      => 'text-center',
                'true_label'      => 'Male',
                'false_label'     => 'Female',
                'default_content' => 'Male',
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'text-center',
                'actions'    => [
                    [
                        'route'            => 'user_edit',
                        'label'            => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-default btn-xs',
                            'role'  => 'button',
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                    [
                        'label'               => 'Delete',
                        'button'              => true,
                        'button_value_prefix' => false,
                        "icon"                => "glyphicon glyphicon-trash",
                        'attributes'          => [
                            'rel'   => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role'  => 'button'
                        ],
                        'start_html'          => '<div class="start_show_action">',
                        'end_html'            => '</div>',
                    ],
                    [
                        'route'            => 'user_cc_code',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'label'            => 'Manage CC code',
                        'icon'             => 'glyphicon glyphicon-plus',
                        'attributes'       => [
                            'onclick' => 'return loadModalURL(this)',
                            'title'   => 'User CC code',
                            'class'   => 'btn btn-warning btn-xs'
                        ]
                    ],
                    [
                        'route'            => 'user_view_profile',
                        'label'            => 'view',
                        'route_parameters' => [
                            'username' => 'username'
                        ],
                        "icon"             => "glyphicon glyphicon-eye-open",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'view',
                            'class' => 'btn btn-option4 btn-xs',
                            'role'  => 'button',
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\User';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_datatable';
    }
}
