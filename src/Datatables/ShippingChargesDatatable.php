<?php

namespace App\Datatables;

use App\Entity\TblShippingCharges;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class ShippingChargesDatatable
 *
 * @package App\Datatables
 */
class ShippingChargesDatatable extends AbstractDatatable
{
    /**
     * format line
     * @return string
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['isActive']) {
                case TblShippingCharges::ACTIVE:
                    $line['isActive'] = TblShippingCharges::ACTIVE_LABEL;
                    break;
                case TblShippingCharges::INACTIVE:
                    $line['isActive'] = TblShippingCharges::INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes' => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes' => ['strip1', 'strip2', 'strip3'],
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => [[1, 'desc']],
            'order_cells_top' => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html' => '<div class="start_checkboxes">',
                    'end_html' => '</div>',
                    'value' => 'id',
                    'value_prefix' => true,
                    'class_name' => 'text-center',
                    'actions' => [
                        [
                            'route' => 'shippingcharges_bulk_delete',
                            'icon' => 'glyphicon glyphicon-trash',
                            'label' => 'Delete',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => 'Delete',
                                'id' => 'btn-bulk-delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role' => 'button',
                            ],
                            'confirm' => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                            'start_html' => '<div class="start_delete_action">',
                            'end_html' => '</div>',

                        ],
                    ],
                ]
            )
            ->add('id', Column::class, [
                'title' => 'Id'
            ])
            ->add('minWeight', Column::class, [
                'title' => 'Min Weight'
            ])
            ->add('maxWeight', Column::class, [
                'title' => 'Max Weight'
            ])
            ->add('price', Column::class, [
                'title' => 'Price'
            ])
            ->add('idCountry.countryName', Column::class, [
                'title' => 'Country'
            ])
            ->add('isActive', Column::class, [
                'title' => 'Active',
                'class_name' => 'text-center',
                'editable' => [SelectEditable::class,
                    [
                        'source' => [
                            ['value' => TblShippingCharges::ACTIVE, 'text' => TblShippingCharges::ACTIVE_LABEL],
                            ['value' => TblShippingCharges::INACTIVE, 'text' => TblShippingCharges::INACTIVE_LABEL],
                        ],
                        'mode' => 'inline',
                        'empty_text' => 'null',
                    ],
                ],
                'filter' => [
                    SelectFilter::class,
                    [
                        'search_type' => 'eq',
                        'select_options' => [
                            '' => '- Status -',
                            TblShippingCharges::ACTIVE => TblShippingCharges::ACTIVE_LABEL,
                            TblShippingCharges::INACTIVE => TblShippingCharges::INACTIVE_LABEL
                        ],
                        'cancel_button' => true,
                    ],
                ],
            ])
            ->add('createdDate', DateTimeColumn::class, [
                'title' => 'Date Created',
                'date_format' => 'MM/DD/YYYY',
                'searchable' => true,
                'filter' => [TextFilter::class, ['classes' => 'hide']]
            ])
            ->add(null, ActionColumn::class, [
                'title' => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html' => '</div>',
                'class_name' => 'text-center',
                'width' => '120px',
                'actions' => [
                    [
                        'route' => 'shippingcharges_edit',
                        'label' => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon" => "glyphicon glyphicon-edit",
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-default btn-xs',
                            'role' => 'button'
                        ],
                        'start_html' => '<div class="start_show_action">',
                        'end_html' => '</div>',
                    ],
                    [
                        'label' => 'Delete',
                        'button' => true,
                        'button_value_prefix' => false,
                        "icon" => "glyphicon glyphicon-trash",
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role' => 'button'
                        ],
                        'start_html' => '<div class="start_show_action">',
                        'end_html' => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return TblShippingCharges::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'shippingcharges_datatable';
    }
}
