<?php

namespace App\Datatables;

use App\Entity\TblCartCoupons;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class CartCouponDatatable
 * @package App\Datatables
 */
class CartCouponDatatable extends AbstractDatatable
{
    /**
     * format line
     * @return string
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            if ($line['idProduct'] == null) {
                $line['idProduct']['title'] = '';
            }
            if ($line['idProductType'] == null) {
                $line['idProductType']['productType'] = '';
            }
            switch ($line['isActive']) {
                case TblCartCoupons::ACTIVE:
                    $line['isActive'] = TblCartCoupons::ACTIVE_LABEL;
                    break;
                case TblCartCoupons::INACTIVE:
                    $line['isActive'] = TblCartCoupons::INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            switch ($line['couponType']) {
                case TblCartCoupons::PERCENTAGE_BASED:
                    $line['couponType'] = TblCartCoupons::PERCENTAGE_BASED_LABEL;
                    break;
                case TblCartCoupons::PRICE_BASED:
                    $line['couponType'] = TblCartCoupons::PRICE_BASED_LABEL;
                    break;
                default:
                    $line['couponType'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }


    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => false,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add(
                null,
                MultiselectColumn::class,
                [
                    'start_html'   => '<div class="start_checkboxes">',
                    'end_html'     => '</div>',
                    'value'        => 'id',
                    'value_prefix' => true,
                    'class_name'   => 'text-center',
                    'actions'      => [
                        [
                            'route'           => 'promocode_coupon_bulk_delete',
                            'icon'            => 'glyphicon glyphicon-trash',
                            'label'           => 'Delete',
                            'attributes'      => [
                                'rel'   => 'tooltip',
                                'title' => 'Delete',
                                'id'    => 'btn-bulk-delete',
                                'class' => 'btn btn-danger btn-xs',
                                'role'  => 'button',
                            ],
                            'confirm'         => true,
                            'confirm_message' => 'Are you sure you want to delete selected item(s) ?',
                            'start_html'      => '<div class="start_delete_action">',
                            'end_html'        => '</div>',

                        ],
                    ],
                ]
            )
            ->add('id', Column::class, [
                'title'  => 'Id',
                'filter' => [SelectFilter::class, ['classes' => 'hide']]
            ])
            ->add('idProductType.productType', Column::class, [
                'title'      => 'Product Type',
                'class_name' => 'text-center',
                'filter'     => [SelectFilter::class, ['classes' => 'hide']],
                'content_padding' => null
            ])
            ->add('idProduct.title', Column::class, [
                'title'      => 'Product Name',
                'searchable' => true,
                'orderable'  => true,
                'filter'     => [SelectFilter::class, ['classes' => 'hide']]
            ])
            ->add('title', Column::class, [
                'title'      => 'Title',
                'searchable' => true,
                'orderable'  => false,
            ])
            ->add('couponType', Column::class, [
                'title'      => 'Type',
                'searchable' => true,
                'orderable'  => false,
            ])
            ->add('couponCode', Column::class, [
                'title'      => 'Code',
                'searchable' => true,
                'orderable'  => false,
            ])
            ->add('discount', Column::class, [
                'title'      => 'Discount',
                'searchable' => true,
                'orderable'  => false,
            ])
            ->add('startDate', DateTimeColumn::class, [
                'title'       => 'Start date',
                'date_format' => 'MM/DD/YYYY',
                'searchable'  => true,
                'filter'      => [DateRangeFilter::class,
                                  [
                                      'cancel_button' => true,
                                  ],
                ],
            ])
            ->add('endDate', DateTimeColumn::class, [
                'title'       => 'End Date',
                'date_format' => 'MM/DD/YYYY',
                'searchable'  => true,
                'filter'      => [DateRangeFilter::class,
                                  [
                                      'cancel_button' => true,
                                  ],
                ],
            ])
            ->add('isActive', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'editable'   => [SelectEditable::class,
                                 [
                                     'source'     => [
                                         ['value' => TblCartCoupons::ACTIVE,
                                          'text' => TblCartCoupons::ACTIVE_LABEL],
                                         ['value' => TblCartCoupons::INACTIVE,
                                          'text' => TblCartCoupons::INACTIVE_LABEL],
                                     ],
                                     'mode'       => 'inline',
                                     'empty_text' => 'null',
                                 ],
                ]
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'text-center',
                'width'      => '120px',
                'actions'    => [
                    [
                        'route'            => 'promocode_coupon_edit',
                        'label'            => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-primary btn-xs',
                            'role'  => 'button'
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ],
                    [
                        'label'               => 'Delete',
                        'button'              => true,
                        'button_value_prefix' => false,
                        "icon"                => "glyphicon glyphicon-trash",
                        'attributes'          => [
                            'rel'   => 'tooltip',
                            'title' => 'Delete',
                            'class' => 'btn btn-danger btn-xs fnConfirmRemove',
                            'role'  => 'button'
                        ],
                        'start_html'          => '<div class="start_show_action">',
                        'end_html'            => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return TblCartCoupons::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'coupon_datatable';
    }
}
