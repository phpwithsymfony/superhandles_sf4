<?php

namespace App\Datatables;

use App\Entity\TblSubscriptionPackage;
use App\Entity\TblSubscriptionPlan;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class SubscriptionPackageDatatables
 *
 * @package App\Datatables
 */
class SubscriptionPackageDatatables extends AbstractDatatable
{
    /**
     * @return callable|\Closure|null
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['isActive']) {
                case TblSubscriptionPlan::ACTIVE:
                    $line['isActive'] = TblSubscriptionPackage::ACTIVE_LABEL;
                    break;
                case TblSubscriptionPlan::INACTIVE:
                    $line['isActive'] = TblSubscriptionPackage::INACTIVE_LABEL;
                    break;
                default:
                    $line['isActive'] = 'null';
            }
            return $line;
        };
        return $formatter;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => false,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add('id', Column::class, ['title' => 'Id'])
            ->add('packageName', Column::class, [
                'title'      => 'Title',
                'searchable' => true,
                'orderable'  => true,

            ])
            ->add('isActive', Column::class, [
                'title'      => 'Active',
                'class_name' => 'text-center',
                'editable'   => [SelectEditable::class,
                                 [
                                     'source'     => [
                                         ['value' => TblSubscriptionPlan::ACTIVE,
                                          'text'  => TblSubscriptionPlan::ACTIVE_LABEL],
                                         ['value' => TblSubscriptionPlan::INACTIVE,
                                          'text'  => TblSubscriptionPlan::INACTIVE_LABEL],
                                     ],
                                     'mode'       => 'inline',
                                     'empty_text' => 'null',
                                 ],
                ]
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'text-center',
                'actions'    => [
                    [
                        'route'            => 'subscription_package_edit',
                        'label'            => 'Edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        "icon"             => "glyphicon glyphicon-edit",
                        'attributes'       => [
                            'rel'   => 'tooltip',
                            'title' => 'Edit',
                            'class' => 'btn btn-primary btn-xs',
                            'role'  => 'button'
                        ],
                        'start_html'       => '<div class="start_show_action">',
                        'end_html'         => '</div>',
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\TblSubscriptionPackage';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'subscriptionpackage_datatable';
    }
}
