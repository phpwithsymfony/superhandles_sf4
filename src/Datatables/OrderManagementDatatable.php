<?php

namespace App\Datatables;

use App\Entity\TblOrders;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class OrderManagementDatatable
 * @package App\Datatables
 */
class OrderManagementDatatable extends AbstractDatatable
{
    /**
     * format line
     * @return string
     */
    public function getLineFormatter()
    {
        $formatter = function ($line) {
            switch ($line['status']) {
                case TblOrders::STATUS_CONFIRM:
                    $line['status'] = "Confirm";
                    break;
                case TblOrders::STATUS_PENDING:
                    $line['status'] = "Pending";
                    break;
                case TblOrders::STATUS_SUCCESS:
                    $line['status'] = "Success";
                    break;
                case TblOrders::STATUS_ONHOLD:
                    $line['status'] = "On hold";
                    break;
                case TblOrders::STATUS_PROCESSING:
                    $line['status'] = "Processsing";
                    break;
                case TblOrders::STATUS_CANCEL:
                    $line['status'] = "Cancel";
                    break;
                default:
                    $line['status'] = "Other";
                    break;
            }
            return $line;
        };
        return $formatter;
    }


    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = [])
    {
        $this->ajax->set(['pipeline' => 10]);
        $this->options->set([
            'classes'                       => 'cls-sgDatatable ' . Style::BOOTSTRAP_4_STYLE,
            'stripe_classes'                => ['strip1', 'strip2', 'strip3'],
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'order'                         => [[1, 'desc']],
            'order_cells_top'               => true,
            'search_in_non_visible_columns' => false,
        ]);

        $this->columnBuilder
            ->add('id', Column::class, [
                'title'  => 'Id',
                'filter' => [SelectFilter::class, ['classes' => 'hide']]
            ])
            ->add('idTransaction', Column::class, [
                'title'      => 'Transaction ID',
                'searchable' => true,
                'orderable'  => false,
                'filter'     => [SelectFilter::class, ['classes' => 'hide']]
            ])
            ->add('idUser.email', Column::class, [
                'title'      => 'Customer Email',
                'searchable' => true,
                'orderable'  => true,
                'filter'     => [TextFilter::class, [
                    'cancel_button' => false,
                    'classes'       => 'hide'
                ],
                ],

            ])
            ->add('status', Column::class, [
                'title'      => 'Status',
                'searchable' => true,
                'orderable'  => true,
                'filter'     => [
                    SelectFilter::class,
                    [
                        'search_type'    => 'eq',
                        'select_options' => array_merge(["" => "Any"], TblOrders::STATUS_ARRAY),
                        'cancel_button'  => true,
                    ],
                ],
                'editable'   => [SelectEditable::class,
                                 [
                                     'source' => self::getStatusArray(),
                                     'mode'   => 'inline',
                                 ],
                ],
            ])
//            ->add('orderTime', DateTimeColumn::class, [
//                'title'       => 'Order Time',
//                'date_format' => 'HH:mm A',
//                'filter'      => [TextFilter::class, ['classes' => 'hide']]
//            ])
            ->add('createdDate', DateTimeColumn::class, [
                'title'       => 'Date Created',
                'date_format' => 'MM/DD/YYYY HH:mm A',
                'searchable'  => true,
                'timeago'     => true,
                'filter'      => [DateRangeFilter::class,
                                  [
                                      'cancel_button' => true,
                                  ],
                ],
            ])
            ->add('updatedDate', DateTimeColumn::class, [
                'title'       => 'Date Updated',
                'date_format' => 'MM/DD/YYYY HH:mm A',
                'searchable'  => true,
                'filter'      => [TextFilter::class, ['classes' => 'hide']]
            ])
            ->add(null, ActionColumn::class, [
                'title'      => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html'   => '</div>',
                'class_name' => 'text-center',
                'width'      => '120px',
                'actions'    => [
                    [
                        'route'            => 'order_management_view_order_details',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'label'            => 'View',
                        'icon'             => 'glyphicon glyphicon-eye-open',
                        'attributes'       => [
                            'onclick' => 'return loadModalURL(this)',
                            'title'   => 'User CC code',
                            'class'   => 'btn btn-primary btn-xs'
                        ]
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return TblOrders::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'order_datatable';
    }

    /**
     * @return array
     */
    private function getStatusArray()
    {
        $array     = TblOrders::STATUS_ARRAY;
        $dataArray = [];
        foreach ($array as $key => $value) {
            $dataArray[] = ['value' => $key, 'text' => $value];
        }
        return $dataArray;
    }
}
