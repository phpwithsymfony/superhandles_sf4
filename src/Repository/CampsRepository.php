<?php

namespace App\Repository;

use App\Entity\TblCamps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class CampsRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblCamps::class);
    }


    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByActiveOrderByAsc()
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->setParameters(['isActive'=>TblCamps::_ACTIVE, 'isDelete'=>TblCamps::_NOTDELETE])
            ->addOrderBy('p.sortOrder', 'ASC')
            ->addOrderBy('p.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
