<?php

namespace App\Repository;

use App\Entity\TblHomeSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class HomeSliderRepository
 * @package App\Repository
 */
class HomeSliderRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblHomeSlider::class);
    }

    /**
     * @return mixed
     */
    public function findByActiveOrderByAsc()
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->setParameters(['isActive'=>TblHomeSlider::_ACTIVE, 'isDelete'=>TblHomeSlider::_NOTDELETE])
            ->addOrderBy('p.sortOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
