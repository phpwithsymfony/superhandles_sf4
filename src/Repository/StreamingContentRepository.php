<?php

namespace App\Repository;

use App\Entity\TblStreamingPageContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class StreamingContentRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblStreamingPageContent::class);
    }


    /**
     * @param int $isWorkouts
     *
     * @param int $streamingId
     *
     * @return array
     */
    public function findAllWorkOut($streamingId, $isWorkouts)
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->andWhere('p.isWorkout = :isWorkout')
            ->andWhere('p.idStreamingPage = :idStreamingPage')
            ->andWhere('p.isWorkoutDisplay = :isWorkoutDisplay')
            ->setParameters(
                ['isActive'=>TblStreamingPageContent::_ACTIVE,
                 'isDelete'=>TblStreamingPageContent::_NOTDELETE,
                 'isWorkout'=>$isWorkouts,
                 'idStreamingPage'=>$streamingId,
                 'isWorkoutDisplay'=>TblStreamingPageContent::_SETWORKOUTDISPLAY
                ]
            )
            ->addOrderBy('p.sortOrder', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}
