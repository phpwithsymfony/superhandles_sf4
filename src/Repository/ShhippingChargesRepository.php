<?php

namespace App\Repository;

use App\Entity\TblCountries;
use App\Entity\TblProducts;
use App\Entity\TblShippingCharges;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ShhippingChargesRepository
 * @package App\Repository
 */
class ShhippingChargesRepository extends ServiceEntityRepository
{
    /**
     * CountryRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblShippingCharges::class);
    }

    /**
     * @param TblProducts  $products
     * @param TblCountries $countries
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findShippingCharge(TblProducts $products, TblCountries $countries)
    {
        $query = $this->createQueryBuilder('s')
                      ->where('s.idCountry = :country')
                      ->andWhere('s.minWeight < :minWeight')
                      ->andWhere('s.maxWeight >= :maxWeight')
                      ->setParameters(
                          ['country'   => $countries->getId(),
                           'minWeight' => $products->getWeight(),
                           'maxWeight' => $products->getWeight()
                          ]
                      )->getQuery()->getSingleResult();
        return $query;
    }
}
