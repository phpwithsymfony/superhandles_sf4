<?php

namespace App\Repository;

use App\Entity\TblStatic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class StaticRepository
 * @package App\Repository
 */
class StaticRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblStatic::class);
    }

    /**
     * @param $pageUrl
     * @return TblStatic|null
     */
    public function findOneByPageUrl($pageUrl): ?TblStatic
    {
        try {
            return $this->createQueryBuilder('s')
                ->andWhere('s.pageUrl = :pageUrl')
                ->andWhere('s.isActive = :isActive')
                ->setParameter('pageUrl', $pageUrl)
                ->setParameter('isActive', TblStatic::ACTIVE)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return $e->getMessage();
        }
    }

}