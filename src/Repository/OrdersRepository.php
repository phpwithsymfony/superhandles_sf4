<?php
namespace App\Repository;

use App\Entity\TblOrderDetails;
use App\Entity\TblOrders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class OrdersRepository
 * @package App\Repository
 */
class OrdersRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblOrders::class);
    }


    /**
     * @return mixed
     */
    public function getOrdersandDetails($user)
    {
        $orders = $this->createQueryBuilder('o')
            ->where('o.idUser = :user')
            ->andWhere('o.status = :status')
            ->setParameters(['user'=> $user, 'status'=>TblOrders::STATUS_SUCCESS])
            ->getQuery()->getResult();

        return $orders;
    }
}
