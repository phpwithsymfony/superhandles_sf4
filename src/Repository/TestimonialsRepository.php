<?php

namespace App\Repository;

use App\Entity\TblTestimonials;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class TestimonialsRepository
 * @package App\Repository
 */
class TestimonialsRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblTestimonials::class);
    }

    /**
     * @return mixed
     */
    public function findByActiveOrderByAsc()
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->setParameters(['isActive'=>TblTestimonials::_ACTIVE, 'isDelete'=>TblTestimonials::_NOTDELETE])
            ->addOrderBy('p.sortOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
