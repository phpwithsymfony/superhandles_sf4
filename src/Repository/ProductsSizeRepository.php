<?php

namespace App\Repository;

use App\Entity\TblProductCategory;
use App\Entity\TblProductSize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ProductsSizeRepository
 * @package App\Repository
 */
class ProductsSizeRepository extends ServiceEntityRepository
{
    /**
     * ProductsSizeRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblProductSize::class);
    }

    public function findArrayResult($size)
    {
        $query = $this->createQueryBuilder('ps')
             ->where('ps.id in (:id)')
             ->setParameter('id', $size)
             ->getQuery()->getArrayResult();
        return $query;
    }
}
