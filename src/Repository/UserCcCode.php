<?php

namespace App\Repository;

use App\Entity\TblUserCcCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class UserCcCode extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblUserCcCode::class);
    }


    /**
     * @param $user
     *
     * @return mixed
     */
    public function findByUserCcCode($user)
    {
        return $this->createQueryBuilder('p')
            ->where('p.idUser = :idUser')
            ->setParameters(['idUser'=>$user])
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $user
     *
     * @return array
     */
    public function findByAssignedCcCode($user)
    {
        $data =  $this->createQueryBuilder('p')
                    ->where('p.idUser = :idUser')
                    ->setParameters(['idUser'=>$user])
                    ->getQuery()
                    ->getResult();
        $userCcCodeArray = [];
        if ($data) {
            foreach ($data as $d) {
                $userCcCodeArray[] = $d->getIdStreamingPage()->getId();
            }
        }
        return $userCcCodeArray;
    }
}
