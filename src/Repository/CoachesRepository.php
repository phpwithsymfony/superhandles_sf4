<?php

namespace App\Repository;

use App\Entity\TblCoaches;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CoachesRepository
 * @package App\Repository
 */
class CoachesRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblCoaches::class);
    }

    public function findCoachUser($coachId)
    {
        $query      = $this->createQueryBuilder('p')
                           ->where('p.id = :coach')
                           ->setParameters(['coach' => $coachId])
                           ->getQuery()
                           ->getResult();
        $coachArray = [];
        if ($query) {
            foreach ($query as $data) {
                foreach ($data->getCoachUser()->getValues() as $d) {
                    $coachArray[] = $d->getId();
                }
            }
        }
        return $coachArray;
    }

    public function displayData($search, $start, $length, $sortOrder)
    {
        $order = $sortOrder[0]['dir'];

//        switch ($sortOrder[0]['column']) {
//            case 0:
//                $sort = 'c.name';
//                break;
//            case 1:
//                $sort = 'c.state';
//                break;
//            case 2:
//                $sort = 'c.status';
//                break;
//            default:
//                $sort  = 'c.name';
//                $order = 'DESC';
//                break;
//        }

        $query = $this->createQueryBuilder('c')
                      ->innerJoin('c.CoachUser', 'u', 'u.idUser=c.id')
                      ->where('c.isDelete = :delete')
                      ->andWhere('c.isActive = :active')
                      ->setParameters([
                          'delete' => TblCoaches::NOTDELETE,
                          'active' => TblCoaches::ACTIVE
                      ]);

        if ($search['value']) {
            $query->andWhere('u.email LIKE :email')
                   ->setParameter('email', $search['value'] . '%');
        }

        $query = $query->getQuery()
                      ->setFirstResult($start)
                      ->setMaxResults($length)
                      ->getOneOrNullResult();
        return $result = [
            'resultData' => ($query) ? $query->getCoachUser()->getValues() : [],
            'totalCount' => ($query) ? count($query->getCoachUser()->getValues()) : 0
        ];
    }
}
