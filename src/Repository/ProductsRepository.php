<?php

namespace App\Repository;

use App\Entity\TblProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ProductsRepository
 * @package App\Repository
 */
class ProductsRepository extends ServiceEntityRepository
{
    /**
     * ProductsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblProducts::class);
    }


    /**
     * @param $productCategory
     *
     * @return mixed
     */
    public function findAllActiveProducts($productCategory)
    {
        return $this->createQueryBuilder('p')
                    ->innerJoin('p.productCategories', 'productCategories')
                    ->leftJoin('p.getStreamingPages', 'streamingPages')
                    ->where('p.isActive = :isActive')
                    ->andWhere('p.isVisible = :isVisible')
                    ->andWhere('p.isDelete = :isDelete')
                    ->andWhere('productCategories = :pc')
                    ->setParameters(
                        ['isActive'=>TblProducts::_ACTIVE,
                           'isVisible'=>TblProducts::_VISIBLE,
                           'isDelete'=>TblProducts::_NOTDELETE,
                           'pc' => $productCategory
                          ]
                    )
                    ->orderBy('p.sortOrder', 'ASC')
                    ->getQuery()->getResult();
    }


    /**
     * @param $productId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findProductsInArray($productId)
    {
        return $this->createQueryBuilder('p')
                    ->select('p')
                    ->addSelect('IDENTITY(p.idProductOfDvd) as idProductOfDvd')
                    ->where('p.id = :id')
                    ->setParameters(['id'=>$productId])
                    ->getQuery()
                    //->setHint(\Doctrine\ORM\Query::HINT_INCLUDE_META_COLUMNS, true)
                    ->getSingleResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @param $idProductType
     *
     * @return mixed
     */
    public function findcategorytoProduct($idProductType)
    {
        $resultArray =  $this->createQueryBuilder('p')
                    ->where('p.idProductType = :idProductType')
                    ->setParameters(['idProductType'=>$idProductType])
                    ->getQuery()->getArrayResult();

        return $resultArray;
    }
}
