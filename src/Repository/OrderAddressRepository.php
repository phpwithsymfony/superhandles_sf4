<?php

namespace App\Repository;

use App\Entity\TblOrderAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class OrderAddressRepository
 * @package App\Repository
 */
class OrderAddressRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblOrderAddress::class);
    }

    /**
     * @return mixed
     */
    public function findOneByArrayResult($id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameters(['id'=>$id])
            ->getQuery()
            ->getSingleResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
