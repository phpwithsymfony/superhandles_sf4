<?php

namespace App\Repository;

use App\Entity\TblMediaVideos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class MediaRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblMediaVideos::class);
    }

    /**
     * @return mixed
     */
    public function findByActiveOrderByAsc()
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->setParameters(['isActive'=>TblMediaVideos::ACTIVE, 'isDelete'=>TblMediaVideos::NOTDELETE])
            ->addOrderBy('p.sortOrder', 'ASC')
            ->addOrderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
