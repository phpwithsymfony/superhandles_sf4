<?php

namespace App\Repository;

use App\Entity\TblStreamingPages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class StreamingPagesRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblStreamingPages::class);
    }


    /**
     * @return array
     */
    public function findActiveStreamingPages()
    {
        return $this->createQueryBuilder('p')
                    ->where('p.isActive = :isActive')
                    ->andWhere('p.isDelete = :isDelete')
                    ->setParameters(
                        ['isActive'=>TblStreamingPages::_ACTIVE,
                         'isDelete'=>TblStreamingPages::_NOTDELETE
                        ]
                    )
                    ->addOrderBy('p.sortOrder', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
    }

    /**
     * @return array
     */
    public function findKeyValArray()
    {
        return $this->createQueryBuilder('p')
                    ->select("p.id, CONCAT(p.pageTitle, ' | ', p.productCcCode) AS titleName")
                    ->where('p.isActive = :isActive')
                    ->andWhere('p.isDelete = :isDelete')
                    ->setParameters(
                        ['isActive'=>TblStreamingPages::_ACTIVE,
                         'isDelete'=>TblStreamingPages::_NOTDELETE
                        ]
                    )
                    ->addOrderBy('p.sortOrder', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
    }


    /**
     * @param $idProductType
     * @param $idProduct
     *
     * @return array
     */
    public function getSubscriptionProduct($idProductType, $idProduct)
    {
        $resultArray =  $this->createQueryBuilder('s')
            ->where('s.idProductType = :idProductType')
            ->andWhere('s.idProduct = :idProduct')
            ->setParameters(['idProductType'=>$idProductType, 'idProduct'=>$idProduct])
            ->getQuery()->getArrayResult();

        return $resultArray;
    }
}
