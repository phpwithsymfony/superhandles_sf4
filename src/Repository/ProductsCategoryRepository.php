<?php

namespace App\Repository;

use App\Entity\TblProductCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ProductsCategoryRepository
 * @package App\Repository
 */
class ProductsCategoryRepository extends ServiceEntityRepository
{
    /**
     * ProductsCategoryRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblProductCategory::class);
    }

    public function findArrayResult()
    {
        $query = $this->createQueryBuilder('pc')
             ->where('pc.isActive = :active')
             ->setParameter('active', TblProductCategory::ACTIVE)
             ->getQuery()->getArrayResult();
        return $query;
    }
}
