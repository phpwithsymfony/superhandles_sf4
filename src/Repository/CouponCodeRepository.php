<?php

namespace App\Repository;

use App\Entity\TblCartCoupons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CouponCodeRepository
 * @package App\Repository
 */
class CouponCodeRepository extends ServiceEntityRepository
{
    /**
     * ProductsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblCartCoupons::class);
    }

    /**
     * @param $couponCode
     *
     * @return mixed
     */
    public function checkCouponCode($couponCode)
    {
        $resultArray = $this->createQueryBuilder('c')
                            ->where('c.couponCode = :couponCode')
                            ->andWhere('c.isActive = :isActive')
                            ->andWhere('c.startDate <= :startDate ')
                            ->andWhere('c.endDate >= :endDate')
                            ->setParameters(
                                ['couponCode' => $couponCode,
                                 'isActive'   => TblCartCoupons::ACTIVE,
                                 'startDate'  => date('Y-m-d'),
                                 'endDate'    => date('Y-m-d')
                                ]
                            )
                            ->getQuery()->getOneOrNullResult();

        return $resultArray;
    }
}
