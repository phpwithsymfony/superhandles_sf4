<?php

namespace App\Repository;

use App\Entity\TblCampsSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CampsRepository
 * @package App\Repository
 */
class CampsSliderRepository extends ServiceEntityRepository
{
    /**
     * CampsSliderRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblCampsSlider::class);
    }

    /**
     * @return mixed
     */
    public function findByActiveOrderByAsc()
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :isActive')
            ->andWhere('p.isDelete = :isDelete')
            ->setParameters(['isActive'=>TblCampsSlider::_ACTIVE, 'isDelete'=>TblCampsSlider::_NOTDELETE])
            ->addOrderBy('p.sortOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
