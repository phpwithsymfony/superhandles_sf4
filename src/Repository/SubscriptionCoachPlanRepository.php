<?php

namespace App\Repository;

use App\Entity\TblSubscriptionCoachPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class SubscriptionCoachPlanRepository
 * @package App\Repository
 */
class SubscriptionCoachPlanRepository extends ServiceEntityRepository
{
    /**
     * CampsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TblSubscriptionCoachPlan::class);
    }


    /**
     * @param $idSubscriptionPlan
     *
     * @return array
     */
    public function getSubscriptionCoachPlan($idSubscriptionPlan)
    {
        $resultArray =  $this->createQueryBuilder('scp')
            ->where('scp.idSubscriptionPlan = :idSubscriptionPlan')
            ->setParameters(['idSubscriptionPlan'=>$idSubscriptionPlan])
            ->getQuery()->getArrayResult();

        return $resultArray;
    }
}
