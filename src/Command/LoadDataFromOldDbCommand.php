<?php

namespace App\Command;

use App\Entity\TblProductCategory;
use App\Entity\TblProductSize;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDataFromOldDbCommand extends Command
{
    protected static $defaultName = 'load-data-from-old-db';

    private $entityManager;
    private $container;


    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('load-data-from-old-db will load row from old db to current')
            ->setHelp('load data of static table from old db to current');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('------ START ------');
        $customLogger = new Logger('logger');
        $customLogger->pushHandler(new StreamHandler('var/log/cmd_load-data-from-old-db.log'));
        $customLogger->addInfo('===============================================================================');
        $customLogger->addInfo('=                cmd_load-data-from-old-db                =');
        $customLogger->addInfo('===============================================================================');

        $io->success('------ Add Product Size------');
        $customLogger->addInfo('------ Add Product Size------');
    //    $this->addProductSize($customLogger, $io);
        $io->success('------ Add Product Category------');
        $customLogger->addInfo('------ Add Product Category------');
        $this->addProductCategory($customLogger, $io);

        $io->success('------ END ------');
        return 0;
    }

    public function addProductSize($customLogger, $io)
    {
        $ps = ['Small', 'Medium', 'Large', 'XL', 'XXL', 'Youth Small', 'Youth Medium', 'Youth Large', 'Youth XL',
            'Regular Size', '4" Sticker', '8" Sticker', '18" Sticker'];
        try {
            $adminOdj = $this->entityManager->createQueryBuilder()->select('u')
                ->from(User::Class, 'u')
                ->where('u.roles LIKE :roles')
                ->setParameter('roles', '%"ROLE_ADMIN"%')
                ->setMaxResults(1)
                ->orderBy('u.id')
                ->getQuery()
                ->getSingleResult();

            foreach ($ps as $val) {
                $psOdj = $this->entityManager->getRepository(TblProductSize::class)->findOneBy([
                    'size' => $val]);
                if (empty($psOdj)) {
                    $product_size = new TblProductSize();
                    $product_size->setSize($val);
                    $product_size->setIsActive(TblProductSize::ACTIVE);
                    $product_size->setCreatedDate(new \DateTime());
                    $product_size->setCreatedBy($adminOdj);

                    $entityManager = $this->entityManager;
                    $entityManager->persist($product_size);
                    $entityManager->flush();

                    $io->writeln('------ added : ' . $val . ' ------');
                    $customLogger->addInfo('------ added : ' . $val . ' ------');
                } else {
                    $io->writeln('------ already exists : ' . $val . ' ------');
                    $customLogger->addInfo('------ already exists : ' . $val . ' ------');
                }
            }
        } catch (\Exception $e) {
            $customLogger->addInfo('=== : ' . $e->getMessage());
            $io->error('=== : ' . $e->getMessage());
        }
    }

    public function addProductCategory($customLogger, $io)
    {
        $product_category_arr = [
            ['Youth Training', 'youth-training', 1, 1],
            ['Shooting & Ball-Handling', 'shooting-ball-handling', 3, 0],
            ['Game-Ready Membership', 'game-ready-membership', 0, 0],
            ['Super Moves', 'super-moves', 4, 0],
            ['Middle School & High School', 'middle-school-high-school', 5, 0],
            ['Starter Workouts', 'starter-workouts', 6, 0],
            ['Extra Workouts', 'extra-workouts', 7, 0],
            ['Online Coaching', 'online-coaching', 8, 0],
            ['Clothing', 'clothing', 4, 1],
            ['Intermediate to Advanced', 'intermediate-to-advanced', 2, 1],
            ['Coaches', 'teamuniversity', 3, 1],
        ];
        try {
            $adminOdj = $this->entityManager->createQueryBuilder()->select('u')
                ->from(User::Class, 'u')
                ->where('u.roles LIKE :roles')
                ->setParameter('roles', '%"ROLE_ADMIN"%')
                ->setMaxResults(1)
                ->orderBy('u.id')
                ->getQuery()
                ->getSingleResult();

            foreach ($product_category_arr as $val) {

                $pcOdj = $this->entityManager->getRepository(TblProductCategory::class)->findOneBy([
                    'title' => $val[0], 'categoryUrl'=>$val[1]]);

                if (empty($pcOdj)) {
                    $productCategory = new TblProductCategory();
                    $productCategory->setTitle($val[0]);
                    $productCategory->setCategoryUrl($val[1]);
                    $productCategory->setSortOrder($val[2]);
                    $productCategory->setIsActive($val[3]);
                    $productCategory->setCreatedDate(new \DateTime());
                    $productCategory->setCreatedBy($adminOdj);

                    $entityManager = $this->entityManager;
                    $entityManager->persist($productCategory);
                    $entityManager->flush();

                    $io->writeln('------ added : ' . $val[1] . ' ------');
                    $customLogger->addInfo('------ added : ' . $val[1] . ' ------');
                } else {
                    $io->writeln('------ already exists : ' . $val[1] . ' ------');
                    $customLogger->addInfo('------ already exists : ' . $val[1] . ' ------');
                }
            }
        } catch (\Exception $e) {
            $customLogger->addInfo('=== : ' . $e->getMessage());
            $io->error('=== : ' . $e->getMessage());
        }
    }
}