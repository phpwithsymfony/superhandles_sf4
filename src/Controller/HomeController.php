<?php

namespace App\Controller;

use App\Entity\TblHomeSlider;
use App\Entity\TblTestimonials;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 * @Route("/", name="front_home_")
 */
class HomeController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }
    /**
     * @Route("/", name="index")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function index() : Response
    {
        ///$entityManager = $this->getDoctrine()->getManager();

        $homeSlider = $this->entityManager->getRepository(TblHomeSlider::class)
                           ->findByActiveOrderByAsc();

        $testimonial = $this->entityManager->getRepository(TblTestimonials::class)
                            ->findByActiveOrderByAsc();
        return $this->render('front/home/index.html.twig', [
            'homeSlider'=>$homeSlider,
            'testimonial'=>$testimonial
        ]);
    }

    /**
     *
     * @Route("/sendemail", name="sendemail")
     *
     */
    public function sendEmail(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('kirit@ilogixinfotech.com')
            ->setTo('kirit@ilogixinfotech.com')
            ->setBody('You should see me from the profiler!');
        $mailer->send($message, $errors);
        die('You should see me from the profiler!');
    }
}
