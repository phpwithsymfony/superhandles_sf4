<?php

namespace App\Controller;

use App\Entity\TblCartCoupons;
use App\Entity\TblCoaches;
use App\Entity\TblCoachTransation;
use App\Entity\TblPaymentMethods;
use App\Entity\TblSubscriptionCoachPlan;
use App\Entity\TblTransation;
use App\Entity\User;
use App\Form\Front\AddCoachUserType;
use App\Form\Front\CoachRegisterType;
use App\Form\Front\CoachUserType;
use App\Form\PaymentFormType;
use App\Helper\CcCodeHelper;
use App\Helper\SubscriptionPackageHelper;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CoachController
 * @package App\Controller
 * @Route("/", name="front_coach_")
 */
class CoachController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var UserManagerInterface
     */
    private $ccCodeHelper;
    /**
     * CoachController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserManagerInterface $userManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserManagerInterface $userManager,
        CcCodeHelper $ccCodeHelper
    ) {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->ccCodeHelper = $ccCodeHelper;
    }

    /**
     * @Route("/teamuniversity", name="index")
     */
    public function index(): Response
    {
        return $this->render('front/coach/index.html.twig');
    }

    /**
     * @Route("/coachRegister/{plan}", name="coach_register", options = {"expose" = true})
     */
    public function coachRegister(Request $request): Response
    {
        $entity      = [];
        $addEntity   = new TblCoaches();
        $paymentForm = $this->createForm(PaymentFormType::class, $entity);
        $form        = $this->createForm(CoachRegisterType::class, $addEntity);

        $form->handleRequest($request);
        $paymentForm->handleRequest($request);
        if ($request->isXmlHttpRequest()) {
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $addEntity->getIdUser();

                $user->addRole('ROLE_REGISTER_USER');
                $user->getProfile()->setCreatedBy($user);
                $user->setEnabled(true);

                $addEntity->setDiscountAmount(0);
                $addEntity->setFinalAmount($addEntity->getIdSubscriptionCoachPlan()->getAmount());

                $this->entityManager->persist($user);
                $this->entityManager->persist($addEntity);
                $this->entityManager->flush();

                return new JsonResponse(
                    ['status'   => true,
                     'id'       => $addEntity->getId(),
                     'totalAmt' => $addEntity->getAmount()]
                );
            } else {
                $errors = $this->getErrorsFromForm($form);
                return new JsonResponse(['status' => false, 'data' => $errors]);
            }
        }
        return $this->render('front/coach/coachRegister.html.twig', [
            'paymentForm' => $paymentForm->createView(),
            'form'        => $form->createView(),
            'urlPlanName' => $request->get('plan'),
        ]);
    }

    /**
     * Ajax get subscription Coach plan
     *
     * @Route("/subscription/subscriptionPlan", name="subscription_plan",
     *     methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getSubscriptionProduct(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $idSubscriptionPlan = $request->get('idSubscriptionPlan');

            $resultData = $this->entityManager->getRepository(TblSubscriptionCoachPlan::class)
                                              ->getSubscriptionCoachPlan($idSubscriptionPlan);

            return new JsonResponse($resultData, 200);
        }
        return new JsonResponse(false, 100);
    }

    /**
     * @Route("/coach-promocode", name="ajax_coachpromocode", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxCoachPromocodeValidate(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $promocode  = $request->request->get('promocode');
            $coachId    = $request->request->get('coachId');
            $couponCode = $this->entityManager->getRepository(TblCartCoupons::class)
                                              ->checkCouponCode($promocode);

            $coach      = $this->entityManager->getRepository(TblCoaches::class)->find($coachId);
            $cartAmount = $coach->getAmount();
            if ($couponCode) {
                if ($couponCode->getCouponType() === TblCartCoupons::PERCENTAGE_BASED) {
                    $newAmount       = ($cartAmount * $couponCode->getDiscount()) / 100;
                    $remainingAmount = $cartAmount - $newAmount;
                } else { // Amount Based
                    $remainingAmount = $cartAmount - $couponCode->getDiscount();
                }
                $discountedAmount = $cartAmount - $remainingAmount;
                $couponData       = [
                    'discountAmount'  => round($discountedAmount, 2),
                    'remainingAmount' => round($remainingAmount, 2),
                    "couponApplyed"   => true,
                ];
                $coach->setIdCartCoupon($couponCode);
            } else {
                $couponData = [
                    'discountAmount'  => round(0, 2),
                    'remainingAmount' => round($cartAmount, 2),
                    "couponApplyed"   => false,
                ];
                $coach->setIdCartCoupon(null);
            }
            $coach->setDiscountAmount($couponData['discountAmount']);
            $coach->setFinalAmount($couponData['remainingAmount']);
            $coach->setUpdatedBy($coach->getIdUser());

            $this->entityManager->flush();
            return new JsonResponse($couponData);
        }
        return new JsonResponse(false);
    }

    /**
     * Coach subscription plan
     *
     * @Route("/coach/subscription", name="subscription_newcoach", methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function coachSubscription(Request $request): RedirectResponse
    {
        $coach = $this->entityManager->getRepository(TblCoaches::class)->find($request->get('coachId'));

        $coach->setStripeTempToken($request->get('stripeToken'));
        $coach->getIdUser()->getProfile()->setIsCoach($request->get('coachId'));
        $subscription     = new SubscriptionPackageHelper();
        $subscriptionData = $subscription->coachSubscription($coach);

        $this->createCoachTransation($coach, $subscriptionData);
        $this->createTransation($coach, $subscriptionData);

        $coach->getIdUser()->getProfile()->setStripeCustomerId($subscriptionData['customer']);
        //$this->ccCodeHelper->addCcCode($coach->getIdSubscriptionPlan()->getIdStreamingPage(), $coach->getIdUser());

        $this->entityManager->flush();

        return $this->redirect('http://go.superhandles.com/team-u-thank-you');
    }

    /**
     * @Route("/coach/team", name="coach_team_list",options={"expose" = true})
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     *
     */
    public function coachTeamList(Request $request): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        if ($request->isXmlHttpRequest()) {
            $search = $request->request->get('search');
            $start  = $request->request->get('start');
            $length = $request->request->get('length');
            $order  = $request->request->get('order');

            $queryData      = $this->entityManager
                ->getRepository(TblCoaches::class)
                ->displayData($search, $start, $length, $order);
            $dispalyResults = [];
            foreach ($queryData['resultData'] as $data) {
                $dispalyResults[] = [
                    $data->getProfile()->getFirstName()." ".$data->getProfile()->getLastName(),
                    $data->getEmail(),
                    $data->getProfile()->getCreatedDate()->format('Y-m-d H:i:s')
                ];
            }

            $dataCount = $queryData['totalCount'];

            $result = [
                'total'           => count($dispalyResults),
                'data'            => $dispalyResults,
                'recordsTotal'    => $dataCount,
                'recordsFiltered' => $dataCount,
            ];

            return new JsonResponse($result);
        }
        $planName=$memberLimit=$avlLimit='';

        if(!empty($this->getUser()->getProfile()->getIsCoach())){
            $coachId    = $this->getUser()->getProfile()->getIsCoach()->getId();
            $coach      = $this->entityManager->getRepository(TblCoaches::class)->find($coachId);
            $avlLimit = $coach->getIdSubscriptionCoachPlan()->getMembers() - count($coach->getCoachUser()->getValues());

            $planName=$coach->getIdSubscriptionPlan()->getPlanname();
            $memberLimit=$coach->getIdSubscriptionCoachPlan()->getMembers();

        }


        return $this->render('front/coach/coachTeam.html.twig', [
            'planName'       => $planName,
            'memberLimit'    => $memberLimit,
            'availableLimit' => $avlLimit
        ]);
    }

    /**
     * Bulk delete a user entity.
     *
     * @Route("/create/coach/team", name="create_team")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createCoachTeam(Request $request): Response
    {
        $user       = $this->getUser();
        $coachId    = $user->getProfile()->getIsCoach()->getId();
        $coach      = $this->entityManager->getRepository(TblCoaches::class)->find($coachId);
        $coachArray = [];
        if ($coach) {
            foreach ($coach->getCoachUser()->getValues() as $d) {
                $coachArray[] = $d->getId();
            }
        }
        $memberLimit = $coach->getIdSubscriptionCoachPlan()->getMembers();
        $add_form    = $this->createForm(AddCoachUserType::class, []);
        $add_form->handleRequest($request);
        return $this->render('front/coach/ModelPopup/modalForm.twig', [
            'form'           => $add_form->createView(),
            'form_name'      => 'Add User',
            'userId'         => $user->getId(),
            'userCcCode'     => json_encode($coachArray),
            'planName'       => $coach->getIdSubscriptionPlan()->getPlanname(),
            'memberLimit'    => $memberLimit,
            'availableLimit' => $memberLimit - count($coach->getCoachUser()->getValues())
        ]);
    }

    /**
     * Bulk delete a user entity.
     *
     * @Route("/create/coach/newuser", name="create_team_new_user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createCoachTeamNewUser(Request $request): Response
    {
        $coachId = $this->getUser()->getProfile()->getIsCoach();
        $addEntity = new User();
        $form      = $this->createForm(CoachUserType::class, $addEntity);
        $coach     = $this->entityManager->getRepository(TblCoaches::class)->find($coachId);

        $coachData = count($coach->getCoachUser()->getValues());
        $memberLimit = $coach->getIdSubscriptionCoachPlan()->getMembers();
        $avlLimit = $memberLimit - $coachData;

        $form->handleRequest($request);
        if ($avlLimit <= 0) {
            $this->addFlash('success', 'New Member Added.');
            return $this->redirectToRoute('front_coach_coach_team_list');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $addEntity->setEnabled(true);
            $addEntity->setRoles(['ROLE_REGISTER_USER']);
            $coach->addCoachUser($addEntity);
            $this->userManager->updateUser($addEntity);
            $this->ccCodeHelper->addCcCode($coach->getIdSubscriptionPlan()->getIdStreamingPage(), $addEntity);
            /*** SEND ***/
//        $mail_params = [
//            'name'    => $user->getProfile()->getFirstName() . " " .
//                         $user->getProfile()->getLastName() . " (" . $user->getUsername() . ")",
//            'subject' => 'Your Superhandles Login Information...',
//        ];
//        //use the emailservice service to send emails
//        $emailService->sendEmail(
//            'admin/emails/registration',
//            $mail_params,
//            $user->getEmail(),
//            $this->getParameter('MAIL_FROM'),
//            $this->getParameter('MAIL_FROM_NAME')
//        );
            /*** END ***/

            $this->addFlash('success', 'New Member Added.');
            return $this->redirectToRoute('front_coach_coach_team_list');
        } else {
            return $this->render('front/coach/ModelPopup/modalFormNewUser.twig', [
                'form'           => $form->createView(),
                'form_name'      => 'Add User',
                'planName'       => $coach->getIdSubscriptionPlan()->getPlanname(),
                'memberLimit'    => $memberLimit,
                'availableLimit' => $avlLimit
            ]);
        }
    }

    /**
     * Bulk delete a user entity.
     *
     * @Route("/create/coach/add-remove", name="team_add_remove", options = {"expose" = true}, methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addRemoveCoachMember(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $user    = $this->getUser();
            $coachId = $user->getProfile()->getIsCoach()->getId();
            //dump($request->get('memberId')); exit;
            $newMember = $this->entityManager->getRepository(User::class)
                                             ->find($request->get('memberId'));
            $coach     = $this->entityManager->getRepository(TblCoaches::class)->find($coachId);

            if ($request->get('addRemove')) {
                $coach->addCoachUser($newMember);
                $this->ccCodeHelper->addCcCode($coach->getIdSubscriptionPlan()->getIdStreamingPage(), $newMember);
            } else {
                $coach->removeCoachUser($newMember);
                $this->ccCodeHelper->removeCcCode($coach->getIdSubscriptionPlan()->getIdStreamingPage(), $newMember);
            }
            $this->entityManager->flush();
            $assignMember   = count($coach->getCoachUser()->getValues());
            $memberLimit    = $coach->getIdSubscriptionCoachPlan()->getMembers();
            $availableLimit = $memberLimit - $assignMember;
            return new JsonResponse(['availableLimit' => $availableLimit], 200);
        }
        return new JsonResponse('error', 200);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    /**
     * @param $coach
     * @param $subscriptionData
     */
    public function createCoachTransation($coach, $subscriptionData)
    {
        $coachTransation = new TblCoachTransation();
        $coachTransation->setIdCoach($coach);
        $coachTransation->setIdCartCoupon($coach->getIdCartCoupon());
        $coachTransation->setStripeSubscriptionId($subscriptionData['id']);
        $coachTransation->setAmount($coach->getFinalAmount());
        $coachTransation->setPayerEmail($coach->getIdUser()->getEmail());
        $coachTransation->setStripeStatus($subscriptionData['status']);


        if ($subscriptionData['trial_start']) {
            $coachTransation->setTrialStartDate($subscriptionData['trial_start']);
            $coachTransation->setTrialEndDate($subscriptionData['trial_end']);
        }
        $this->entityManager->persist($coachTransation);
    }

    /**
     * @param $coach
     * @param $subscriptionData
     */
    public function createTransation($coach, $subscriptionData)
    {
        $paymentMethod = $this->entityManager->getRepository(TblPaymentMethods::class)
                                             ->find(TblPaymentMethods::STRIPE);

        $transation = new TblTransation();
        $transation->setIdCoach($coach);
        $transation->setIdPaymentMethods($paymentMethod);
        $transation->setSum($coach->getFinalAmount());

        $transation->setPayerEmail($coach->getIdUser()->getEmail());
        $transation->setTransationJson(json_encode($subscriptionData));

        $transation->setTransactionId($subscriptionData['id']);

        $this->entityManager->persist($transation);
    }
}
