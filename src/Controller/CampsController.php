<?php

namespace App\Controller;

use App\Entity\TblCamps;
use App\Entity\TblCampsSlider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CampsController
 * @package App\Controller
 * @Route("/", name="front_camps_")
 */
class CampsController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * get camps and camps Slider Data
     *
     * @Route("/camps", name="index")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index(): Response
    {
        $camps = $this->entityManager->getRepository(TblCamps::class)
                                     ->findOneBy(
                                         ['isDelete'=>TblCamps::_NOTDELETE, 'isActive'=>TblCamps::_ACTIVE],
                                         ['sortOrder'=>'ASC', 'id'=>'DESC']
                                     );

        $campsSlider = $this->entityManager->getRepository(TblCampsSlider::class)
                                           ->findByActiveOrderByAsc();

        return $this->render('front/camps/index.html.twig', [
            'camps'       => $camps,
            'campsSlider' => $campsSlider
        ]);
    }
}
