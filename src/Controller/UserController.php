<?php

namespace App\Controller;

use App\Entity\TblStatic;
use App\Entity\User;
use App\Form\ProfilePicType;
use App\Form\ProfileType;
use App\Form\RegisterType;
use App\Service\EmailService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * UserController constructor.
     *
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * show my account page.
     *
     * @Route("/my-account", name="my_account")
     */
    public function myAccount()
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_home');
        }
        return $this->render('front/user/my_account.html.twig');
    }

    /**
     * Show Registration form and save user in db.
     *
     * @Route("/register", name="register")
     *
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request, EmailService $emailService): Response
    {
        $user_obj = new User();
        $user_obj->addRole('ROLE_USER');
        $reg_form = $this->createForm(RegisterType::class, $user_obj);

        $reg_form->handleRequest($request);

        if ($reg_form->isSubmitted() && $reg_form->isValid()) {
            if ($this->getParameter('kernel.environment') !== 'test' &&
                !$this->captchaVerify($request->get('g-recaptcha-response'))) {
                $this->addFlash('danger', 'Please re-enter your reCAPTCHA.');
                return $this->render('front/user/registration.html.twig', [
                    'form' => $reg_form->createView(),
                ]);
            }

            $user_obj->setEnabled(true);
            $this->userManager->updateUser($user_obj);

            /*** SEND ***/
            $mail_params = [
                'name'    => $user_obj->getProfile()->getFirstName() . " " .
                             $user_obj->getProfile()->getLastName() . " (" . $user_obj->getUsername() . ")",
                'subject' => 'Your Superhandles Login Information...',
            ];
            //use the emailservice service to send emails
            $emailService->sendEmail(
                'admin/emails/registration',
                $mail_params,
                $user_obj->getEmail(),
                $this->getParameter('MAIL_FROM'),
                $this->getParameter('MAIL_FROM_NAME')
            );
            /*** END ***/

            $pageObj = $this->getDoctrine()->getRepository(TblStatic::class)
                ->findOneByPageUrl('thank-you-register');
            if ($pageObj instanceof TblStatic) {
                $this->addFlash('success', $pageObj->getPageContent());
            } else {
                $this->addFlash('success', '<i class="fa fa-check" aria-hidden="true"></i>
Thank you for registering. <a href="/login">Click here to login to ' . $this->getParameter('SITE_NAME') . '</a>');
            }

            //$this->addFlash('success', 'Thank You.');
            return $this->redirectToRoute('register');
        }

        return $this->render('front/user/registration.html.twig', [
            'form' => $reg_form->createView(),
        ]);
    }

    /**
     * @param $reCaptcha
     *
     * @return mixed
     */
    public function captchaVerify($reCaptcha)
    {
        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $curl_init  = curl_init();
        curl_setopt($curl_init, CURLOPT_URL, $google_url);
        curl_setopt($curl_init, CURLOPT_HEADER, 0);
        curl_setopt($curl_init, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_init, CURLOPT_POST, true);
        curl_setopt($curl_init, CURLOPT_POSTFIELDS, array(
            "secret" => $this->getParameter('GOOGLE_RECAPTCHA_SECRET'), "response" => $reCaptcha));
        $response = curl_exec($curl_init);
        curl_close($curl_init);
        $json_response = json_decode($response, false);

        if (!empty($json_response)) {
            return $json_response->success;
        }
        return 0;
    }

    /**
     * Show my profile page where user can update user details.
     *
     * @Route("/my-profile", name="my_profile")
     * @Route("/admin/my-profile", name="admin_my_profile")
     * @Route("/profile/edit", name="profile_edit")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    // todo: only allow png, jpeg and jpg files.
    public function myProfile(Request $request): Response
    {
        $user_obj = $this->getUser();

        $profile_form = $this->createForm(ProfileType::class, $user_obj);
        $profile_pic_form = $this->createForm(ProfilePicType::class, $user_obj);

        $profile_form->handleRequest($request);

        if ($profile_form->isSubmitted() && $profile_form->isValid()) {
            $this->userManager->updateUser($user_obj);
            $this->addFlash('success', 'Thank You.');
            return $this->isGranted('ROLE_ADMIN') ?
                $this->redirectToRoute('admin_my_profile') : $this->redirectToRoute('my_profile');
        }

        $profile_pic_form->handleRequest($request);
        if ($profile_pic_form->isSubmitted() && $profile_pic_form->isValid()) {
            $this->userManager->updateUser($user_obj);
            $this->addFlash('success', 'Profile Picture uploaded successfully.');
            return $this->redirectToRoute('my_profile');
        }

        return $this->render('front/user/profile.html.twig', [
            'basic_form' => $profile_form->createView(),
            'cp_form'    => $profile_pic_form->createView(),
        ]);
    }
}
