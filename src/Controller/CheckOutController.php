<?php

namespace App\Controller;

use App\Entity\TblCart;
use App\Entity\TblCartCoupons;
use App\Entity\TblCartProduct;
use App\Entity\TblCustomer;
use App\Entity\TblOrderAddress;
use App\Form\OrderAddressFormType;
use App\Helper\AddtoCartHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CheckOutController
 * @package App\Controller
 * @Route("/", name="front_checkout_")
 */
class CheckOutController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @Route("/checkout", name="cart", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function checkOut(Request $request): Response
    {
        if ($this->getUser()) {
            $addtoCartHelper = new AddtoCartHelper($this->entityManager);
            $cartData        = $addtoCartHelper->getCart($this->getUser());
            $customer        = $addtoCartHelper->createCustomer($this->getUser());
            $entity          = new TblOrderAddress();
            $billingForm     = $this->createForm(OrderAddressFormType::class, $entity);
            $billingForm->handleRequest($request);
            if ($billingForm->isSubmitted() && $billingForm->isValid()) {
                if ($request->get('billAddress') > 0) {
                    $entity = $this->entityManager->getRepository(TblOrderAddress::class)
                                                  ->find($request->get('billAddress'));
                    foreach ($request->get('order_address_form') as $key => $value) {
                        $name = 'set' . ucfirst($key);
                        if ($name !== 'set_token') {
                            $entity->$name($value);
                        }
                    }
                } else {
                    $entity->setIdUser($this->getUser());
                    $entity->setIdCustomer($customer);

                    $this->entityManager->persist($entity);
                }
                $this->entityManager->flush();
                $cartId = $cartData[0]->getIdCart();
                $addtoCartHelper->setCartaddressdetails($cartId, $entity);
                $shippingAmount = $addtoCartHelper->getShippingChargeCalculate($cartId, $entity->getBillingCountry());
                return new JsonResponse(
                    ["totalShippingAmount" => $shippingAmount['shippingAmount'],
                     "remainingAmount"     => $shippingAmount['remainingAmount']
                    ]
                );
            }
            $address       = $addtoCartHelper->getAddressDetails($this->getUser(), $customer);
            $paymentMethod = $addtoCartHelper->getPaymentMethod();
            return $this->render('front/cart/checkout.html.twig', [
                'cartData'      => $cartData,
                'form'          => $billingForm->createView(),
                'address'       => $address,
                'paymentMethod' => $paymentMethod
            ]);
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    /**
     * @Route("/ajaxgetaddressdetail", name="ajax_address_detail")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxGetAddressDetails(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $id = $request->request->get('id');
            if ($id > 0) {
                $data = $this->entityManager->getRepository(TblOrderAddress::class)
                                            ->findOneByArrayResult($id);
                return new JsonResponse($data);
            }
            return new JsonResponse(false);
        }
        return new JsonResponse(false);
    }

    /**
     * @Route("/ajaxaddressform", name="ajax_address_from_submit")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxAddressForm(Request $request): Response
    {
        $entity      = new TblOrderAddress();
        $billingForm = $this->createForm(OrderAddressFormType::class, $entity);
        $billingForm->handleRequest($request);

        if ($billingForm->isSubmitted()) {
            $entity->setIdUser($this->getUser());

            $entityManager = $this->entityManager;
            $entityManager->persist($entity);
            $entityManager->flush();

            return new JsonResponse(true);
        }
        return new JsonResponse(false);
    }

    /**
     * @Route("/promocode", name="ajax_promocode")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxPromocodeValidate(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $promocode  = $request->request->get('promocode');
            $couponCode = $this->entityManager->getRepository(TblCartCoupons::class)
                                              ->checkCouponCode($promocode);
            $couponData = false;
            $customer   = $this->entityManager->getRepository(TblCustomer::class)
                                              ->findOneBy(['idUser' => $this->getUser()]);
            $cart       = $this->entityManager->getRepository(TblCart::class)
                                              ->findOneBy(['idCustomer' => $customer, 'status' => TblCart::_ADDTOCART]);
            if ($couponCode) {
                $cart->setIdCartCoupon($couponCode);
                $this->entityManager->flush();
                if ($cart) {
                    $cartAmountData = self::getCartProductAmount($cart);
                    $cartAmount     = $cartAmountData['cartAmount'];
                    if ($couponCode->getCouponType() === TblCartCoupons::PERCENTAGE_BASED) {
                        $newAmount       = ($cartAmount * $couponCode->getDiscount()) / 100;
                        $remainingAmount = $cartAmount - $newAmount;
                    } else { // Amount Based
                        $remainingAmount = $cartAmount - $couponCode->getDiscount();
                    }
                    $discountedAmount = $cartAmount - $remainingAmount;
                    $couponData       = [
                        'discountAmount'  => round($discountedAmount, 2),
                        'remainingAmount' => round($remainingAmount + $cartAmountData['shippingAmount'], 2),
                        "couponApplyed"   => true,
                        "cartAmount"      => round($cartAmount, 2),
                        "shippingAmount"  => $cartAmountData['shippingAmount'],
                    ];
                }
            } else {
                $cart->setIdCartCoupon(null);
                $this->entityManager->flush();
                $remainingAmountData = self::getCartProductAmount($cart);
                $remainingAmount     = $remainingAmountData['cartAmount'];
                $couponData          = [
                    'discountAmount'  => 0,
                    'remainingAmount' => round($remainingAmount + $remainingAmountData['shippingAmount'], 2),
                    "couponApplyed"   => false,
                    "cartAmount"      => round($remainingAmount, 2),
                    "shippingAmount"  => $remainingAmountData['shippingAmount']
                ];
            }
            return new JsonResponse($couponData);
        }
        return new JsonResponse(false);
    }

    /**
     * @param $cart
     *
     * @return int|array
     */
    private function getCartProductAmount($cart)
    {
        $cartAmount     = 0;
        $shippingAmount = 0;
        $cartProduct    = $this->entityManager->getRepository(TblCartProduct::class)
                                              ->findBy(['idCart' => $cart]);
        if ($cartProduct) {
            foreach ($cartProduct as $cp) {
                $amount = $cp->getAmount()  * $cp->getQuantity();
                $cartAmount     += $amount;
                $shippingAmount += $cp->getShippingAmount();
            }
        }
        return ['cartAmount' => $cartAmount, "shippingAmount" => $shippingAmount];
    }
}
