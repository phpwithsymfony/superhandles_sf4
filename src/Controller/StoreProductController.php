<?php

namespace App\Controller;

use App\Entity\TblProductCategory;
use App\Entity\TblProducts;
use App\Entity\TblProductSize;
use App\Entity\TblProductType;
use App\Entity\TblUserCcCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StoreProductController
 * @package App\Controller
 * @Route("/", name="front_store_")
 */
class StoreProductController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @Route("store", name="product")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(): Response // product categoty list
    {
        $data = $this->entityManager->getRepository(TblProductCategory::class)
                                    ->findBy(["isActive" => 1]);


        return $this->render('front/store/productCategory.html.twig', [
            'data' => $data
        ]);
    }

    /**
     *
     * @Route("/products/{categoryUrl}", name="products_list")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function productList(TblProductCategory $productCategory): Response
    {
        $resultData = $this->entityManager->getRepository(TblProducts::class)
                                          ->findAllActiveProducts($productCategory);

        if ($resultData && $resultData[0]->getIdProductType()->getIsCloth()) {
            $productType = [];

            if ($resultData) {
                $productType = $this->entityManager->getRepository(TblProductType::class)
                                                   ->findBy(['isCloth' => TblProductType::ISCLOTH]);
            }
            return $this->render('front/store/clothProductList.html.twig', [
                'data'              => $resultData,
                'productCategory'   => $productCategory,
                'productType'       => $productType
            ]);
        } else {
            $purchasedProductsData = [];
            if ($this->getUser()) {
                $purchasedProducts = $this->entityManager->getRepository(TblUserCcCode::class)
                                                         ->findByUserCcCode($this->getUser());

                foreach ($purchasedProducts as $array) {
                    $purchasedProductsData[] = $array->getIdStreamingPage()->getIdProduct()->getId();
                }
            }

            return $this->render('front/store/productList.html.twig', [
                'data'              => $resultData,
                'productCategory'   => $productCategory,
                'purchasedProducts' => $purchasedProductsData
            ]);
        }
    }


    /**
     * @Route("/product-details/{productUrl}", name="products_details")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function productDetails(TblProducts $products): Response
    {
        $data    = $this->entityManager->getRepository(TblProducts::class)
                                       ->find($products->getId());
        $pparray = [];
        if ($this->getUser()) {
            $purchasedProducts = $this->entityManager->getRepository(TblUserCcCode::class)
                                                     ->findByUserCcCode($this->getUser());

            foreach ($purchasedProducts as $array) {
                $pparray[] = $array->getIdStreamingPage()->getIdProduct()->getId();
            }
        }
        $productSize = $this->entityManager->getRepository(TblProductSize::class)
                                           ->findArrayResult($data->getIdProductSize());
        return $this->render('front/store/productDetails.html.twig', [
            'data'              => $data,
            'purchasedProducts' => $pparray,
            'productSize'       => $productSize,
        ]);
    }
}
