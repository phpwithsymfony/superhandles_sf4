<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\TblStatic;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CmsController
 * @package App\Controller
 */
class CmsController extends AbstractController
{

    /**
     * @Route("/{slug}/page", name="cms_page" )
     *
     * @param $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($slug): Response
    {
        $cms_page = $this->getDoctrine()->getRepository(TblStatic::class);
         $page_obj = $cms_page->findOneBy(["pageUrl" => $slug, 'isActive' => 1]);
        return $this->render('front/cms/index.html.twig', [
            'cmsPage' => $page_obj
        ]);
    }
}
