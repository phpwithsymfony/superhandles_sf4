<?php

namespace App\Controller;

use App\Entity\TblStreamingPageContent;
use App\Entity\TblUserCcCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 * @Route("/", name="front_hoopfolio_")
 */
class HoopfolioController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @Route("/hoopfolio", name="index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        $mystreaming = $this->entityManager->getRepository(TblUserCcCode::class)
                                           ->findBy(['idUser' => $this->getUser()]);
        return $this->render('front/hoopfolio/index.html.twig', [
            'mystreaming' => $mystreaming
        ]);
    }


    /**
     * @Route("/streamingpagecontent", name="streaming_page_content", options = {"expose" = true}, methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function ajaxGetStreamingPageContent(Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $streamingid = $request->request->get('streamingid');
            $isWorkouts  = $request->request->get('isWorkouts');
            $content     = $this->entityManager->getRepository(TblStreamingPageContent::class)
                                               ->findAllWorkOut($streamingid, $isWorkouts);
            return new JsonResponse($content);
        }

        return new JsonResponse('Data not found');
    }
}
