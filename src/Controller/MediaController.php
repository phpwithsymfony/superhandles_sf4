<?php

namespace App\Controller;

use App\Entity\TblMediaVideos;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MediaController
 * @package App\Controller
 * @Route("/", name="front_media_")
 */
class MediaController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }
    /**
     * @Route("/mediavideo", name="index")
     */
    public function index()
    {

        $data = $this->entityManager->getRepository(TblMediaVideos::class)
                      ->findByActiveOrderByAsc();


        return $this->render('front/media/index.html.twig', [
            'data'=>$data
        ]);
    }
}
