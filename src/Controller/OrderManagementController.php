<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\TblPaymentMethods;
use App\Form\PaymentFormType;
use App\Helper\OrderManagementHelper;
use App\Helper\StripeCustomerHelper;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use JMS\Payment\CoreBundle\Form\ChoosePaymentMethodType;
use JMS\Payment\CoreBundle\Plugin\Exception\Action\VisitUrl;
use JMS\Payment\CoreBundle\Plugin\Exception\ActionRequiredException;
use JMS\Payment\CoreBundle\PluginController\PluginController;
use JMS\Payment\CoreBundle\PluginController\Result;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class OrderManagementController
 * @package App\Controller
 * @Route("/", name="front_order_")
 */
class OrderManagementController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @Route("/payment", name="payment_option")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     */
    public function index(Request $request): Response
    {
        $paymentMethod = $this->entityManager->getRepository(TblPaymentMethods::class)
            ->findOneBy(['id' => $request->request->get('checkout') , 'isActive' => TblPaymentMethods::_ACTIVE]);

        if($paymentMethod) {
            if ($paymentMethod->getTitle() == 'Stripe') {
                $entity = [];
                $paymentForm = $this->createForm(PaymentFormType::class, $entity);
                $paymentForm->handleRequest($request);

                return $this->render('front/order/orderPlace.html.twig', [
                    'form' => $paymentForm->createView()
                ]);
            } elseif ($paymentMethod->getTitle() == 'Paypal') {
                $amount = $request->request->get('paymentAmmount');
                $em = $this->getDoctrine()->getManager();

                $order = new Order($amount);
                $em->persist($order);
                $em->flush();

                return $this->redirect($this->generateUrl('front_order_app_show', [
                    'id' => $order->getId()
                ]));
            }
        } else {
            return $this->render('front/order/orderError.html.twig');
        }
    }

    /**
     * @Route("/stripe_payment/stripeCharge", name="make_payment")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function makePayment(Request $request): Response
    {
        $entity      = [];
        $paymentForm = $this->createForm(PaymentFormType::class, $entity);
        $paymentForm->handleRequest($request);
        if ($paymentForm->isSubmitted()) {
            try {
                $CardData    = $request->request->get('payment_form');
                $orderHelper = new OrderManagementHelper($this->entityManager);
                $stripeHelper = new StripeCustomerHelper();
                $token = $stripeHelper->generateCardToken($CardData, $this->getParameter('stripe_api_public_key'));
                $orderHelper->checkandCreateOrder($this->getUser(), $token);
            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
                return $this->render('front/order/orderPlace.html.twig', [
                    'form' => $paymentForm->createView()
                ]);
            }
        }
        return $this->render('front/order/orderPlace.html.twig');
    }

    /**
     * @Route("/{id}/show", name="app_show")
     * @Template
     */
    public function showAction(Request $request, Order $order, PluginController $ppc): Response
    {
        $config = [
            'paypal_express_checkout' => [
                'return_url' => $this->generateUrl('front_order_payment_return', [
                    'id' => $order->getId(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
                'cancel_url' => $this->generateUrl('front_order_payment_cancel', [
                    'id' => $order->getId(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        ];

        $form = $this->createForm(ChoosePaymentMethodType::class, null, [
            'amount'          => $order->getAmount(),
            'currency'        => 'USD',
            'predefined_data' => $config
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ppc->createPaymentInstruction($instruction = $form->getData());

            $order->setPaymentInstruction($instruction);

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush($order);

            return $this->redirect($this->generateUrl('front_order_app_paymentcreate', [
                'id' => $order->getId()
            ]));
        }

        return $this->render('front/order/orderPlacePaypal.html.twig', [
            'order' => $order,
            'form'  => $form->createView(),
        ]);
    }

    /**
     * @param                  $order
     * @param PluginController $ppc
     *
     * @return \JMS\Payment\CoreBundle\Model\PaymentInterface
     * @throws \JMS\Payment\CoreBundle\PluginController\Exception\InvalidPaymentInstructionException
     */
    private function createPayment($order, PluginController $ppc): PaymentInterface
    {
        $instruction        = $order->getPaymentInstruction();
        $pendingTransaction = $instruction->getPendingTransaction();
        if ($pendingTransaction !== null) {
            return $pendingTransaction->getPayment();
        }

        $amount = $instruction->getAmount() - $instruction->getDepositedAmount();
        return $ppc->createPayment($instruction->getId(), $amount);
    }

    /**
     * @Route("/{id}/payment/create", name="app_paymentcreate")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentCreateAction(Order $order, PluginController $ppc): RedirectResponse
    {
        $payment = $this->createPayment($order, $ppc);
        $result  = $ppc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());
        if ($result->getStatus() === Result::STATUS_PENDING) {
            $ex = $result->getPluginException();
            if ($ex instanceof ActionRequiredException) {
                $action = $ex->getAction();
                if ($action instanceof VisitUrl) {
                    return $this->redirect($action->getUrl());
                }
            }
        }
        throw $result->getPluginException();

        // In a real-world application you wouldn't throw the exception. You would,
        // for example, redirect to the showAction with a flash message informing
        // the user that the payment was not successful.
    }

    /**
     * @Route("/{id}/payment/complete", name="app_paymentcomplete")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentCompleteAction(Order $order): Response
    {
        return new Response('Payment complete');
    }

    /**
     * @Route("/payment/return", name="payment_return")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentReturn(): RedirectResponse
    {

        $paypalData  = $_GET;
        $orderHelper = new OrderManagementHelper($this->entityManager);
        $orderHelper->checkandCreateOrder($this->getUser(), $paypalData, 1);
        //return new Response('Payment complete');
        //return $this->redirectToRoute('store_product');
    }

    /**
     * @Route("/payment/cancel", name="payment_cancel")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentCancel(): Response
    {
        return new Response('Payment Cancel');
    }
}
