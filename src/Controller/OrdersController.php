<?php

namespace App\Controller;

use App\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/orders")
 */
class OrdersController extends AbstractController
{
    /**
     * @Route("/new/{amount}", name="app_orders_new")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction($amount): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();

        $order = new Order($amount);
        $em->persist($order);
        $em->flush();

        return $this->redirect($this->generateUrl('app_orders_show', [
            'id' => $order->getId()
        ]));
    }
}
