<?php

namespace App\Controller\Admin;

use App\Datatables\SubscriptionPackageDatatables;
use App\Entity\TblSubscriptionPackage;
use App\Form\SubscriptionPackageType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SubscriptionPackageController
 * @package App\Controller
 * @Route("/admin/subscription-package", name="subscription_package_")
 */
class SubscriptionPackageController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;


    /**
     * SubscriptionPackageController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of SubscriptionPackage
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(SubscriptionPackageDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblsubscriptionpackage.isDelete = :isDelete');

            //set parameters
            $qb->setParameter('isDelete', TblSubscriptionPackage::NOTDELETE);
            #return $responseService->getResponse();
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Subscription Package    ',
            'route'     => ['add' => 'subscription_package_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblSubscriptionPackage();
        $form      = $this->createForm(SubscriptionPackageType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('subscription_package_list');
        }

        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Add Subscription Package'
        ]);
    }

    /**
     * Display edit form for existing SubscriptionPackage
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request                $request
     * @param TblSubscriptionPackage $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblSubscriptionPackage $updateEntity): Response
    {
        $form = $this->createForm(SubscriptionPackageType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('subscription_package_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Edit Subscription Package'
        ]);
    }
}
