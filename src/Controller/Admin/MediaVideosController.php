<?php

namespace App\Controller\Admin;

use App\Datatables\MediaVideosDatatable;
use App\Entity\TblMediaVideos;
use App\Form\MediaVideosType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MediaVideosController
 * @package App\Controller
 * @Route("/admin/mediavideos", name="media_video_")
 */
class MediaVideosController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of mediavideos
     *
     * @Route("list", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();

        $datatable = $this->dtFactory->create(MediaVideosDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            $qb->andWhere('tblmediavideos.isDelete = :isDelete');

            $qb->setParameter('isDelete', TblMediaVideos::NOTDELETE);


            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Media Videos',
            'route'     => ['add' => 'media_video_add'],
            'datatable' => $datatable,
            //'filterbtn' => ['media_video_list' => ["Media Video" => 1, "Victim Video" => 2]]
        ]);
    }

    /**
     * Add new mediavideos
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblMediaVideos();
        $form      = $this->createForm(MediaVideosType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('media_video_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Add Media Videos',
        ]);
    }

    /**
     * Display edit form for existing mediavideos
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblMediaVideos $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblMediaVideos $updateEntity): Response
    {
        $form = $this->createForm(MediaVideosType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('media_video_list');
        }

        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Edit Media Videos',
        ]);
    }

    /**
     * Bulk delete a mediavideos entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblMediaVideos');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblMediaVideos::DELETE);
                $em->persist($entity);
            }
            $em->flush();
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
