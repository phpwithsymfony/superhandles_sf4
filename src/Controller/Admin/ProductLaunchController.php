<?php

namespace App\Controller\Admin;

use App\Datatables\ProductLaunchDatatable;
use App\Entity\TblProductLaunch;
use App\Form\ProductLaunchType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ProductLaunchController
 * @package App\Controller
 * @Route("/admin/product_launch", name="productlaunch_")
 */
class ProductLaunchController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ProductLaunchController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * Listing Product Launch
     * @Route("/", name="list")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $is_ajax = $request->isXmlHttpRequest();

        $datatable = $this->dtFactory->create(ProductLaunchDatatable::class);
        $datatable->buildDatatable();

        if ($is_ajax) {
            $this->dtResponse->setDatatable($datatable);
            $this->dtResponse->getDatatableQueryBuilder()->getQb()
                ->where("tblproductlaunch.isDelete = :isDelete")
                ->setParameter('isDelete', TblProductLaunch::NOTDELETE)
            ;
            return $this->dtResponse->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Product Launch',
            'route'     => ['add' => 'productlaunch_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New for Product Launch
     *
     * @Route("/add", name="add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblProductLaunch();
        $form = $this->createForm(ProductLaunchType::class, $addEntity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');

            return $this->redirectToRoute('productlaunch_list');
        }

        return $this->render('admin/productLaunch/addwithimage.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Product Buy Now Page',
            'editable'=> false
        ]);
    }

    /**
     * Display edit form for existing product launch
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request $request
     * @param TblProductLaunch $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProductLaunch $updateEntity)
    {
        $edit_form = $this->createForm(ProductLaunchType::class, $updateEntity);
        $edit_form->handleRequest($request);

        if ($edit_form->isSubmitted() && $edit_form->isValid()) {
            $this->entityManager->flush();
            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('productlaunch_list');
        }

        return $this->render('admin/productLaunch/addwithimage.html.twig', [
            'form'     => $edit_form->createView(),
            'title'    => 'Edit Product Buy Now Page',
            'editable'=> true
        ]);
    }

    /**
     * Bulk delete a ShippingCharges entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository(TblProductLaunch::class);
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblProductLaunch::DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
