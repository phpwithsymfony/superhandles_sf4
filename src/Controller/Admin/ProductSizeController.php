<?php

namespace App\Controller\Admin;

use App\Datatables\ProductSizeDatatables;
use App\Entity\TblProductSize;
use App\Form\ProductSizeType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductSizeController
 * @package App\Controller
 * @Route("/admin/cloth_category", name="productsize_")
 */
class ProductSizeController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ProductSizeController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List for all ProductSize
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(ProductSizeDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblproductsize.isDelete = :enabled');

            //set parameters
            $qb->setParameter('enabled', 1);

            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Cloth Size',
            'route'     => ['add' => 'productsize_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New ProductSize
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblProductSize();
        $form = $this->createForm(ProductSizeType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');

            return $this->redirectToRoute('productsize_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Edit Cloth Size',
        ]);
    }

    /**
     * Display edit form for existing ProductSize
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblProductSize $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProductSize $updateEntity): Response
    {
        $form = $this->createForm(ProductSizeType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');

            return $this->redirectToRoute('productsize_list');
        }

        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'tblTitle' => 'Edit Size'
        ]);
    }

    /**
     * Deletes a ProductZise entity.
     *
     * @param TblProductSize $deleteEntity
     *
     * @Route("/delete/{id}", name="delete", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(TblProductSize $deleteEntity): RedirectResponse
    {
        $deleteEntity->setIsDelete(0);
        $this->entityManager->flush();
        $this->addFlash('success', 'Record deleted successfully.');

        return $this->redirectToRoute('productsize_list');
    }


    /**
     * Bulk delete a ProductSize entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblProductSize');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(0);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * Update Status ProductSize
     *
     * @Route("/updateStatus/{id}", name="update_status", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblProductSize $statusEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateStatus(Request $request, TblProductSize $statusEntity): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $statusEntity->setIsActive(!$statusEntity->getIsActive());


            $em = $this->entityManager;
            $em->persist($statusEntity);
            $em->flush();

            if ($statusEntity->getIsActive() === '1') {
                $this->addFlash('success', 'Record activated successfully');
            } else {
                $this->addFlash('success', 'Record inactivated successfully');
            }

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
