<?php

namespace App\Controller\Admin;

use App\Datatables\OrderManagementDatatable;
use App\Entity\TblOrderDetails;
use App\Entity\TblOrders;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderManagementController
 * @package App\Controller
 * @Route("/admin/order-management", name="order_management_")
 */
class OrderManagementController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * Display List for Home Slider
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(OrderManagementDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblorders.isDelete = :isDelete');

            //set parameters
            $qb->setParameter('isDelete', TblOrders::_NOTDELETE);
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Order Management',
            'datatable' => $datatable,
        ]);
    }

    /**
     * view for Order details
     *
     * @Route("/view/orderdetails", name="view_order_details", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function viewOrderDetails(Request $request): Response
    {
        $id = $request->get('id');
        $orders = $this->entityManager->getRepository(TblOrders::class)->find($id);
        $orderDetails = $this->entityManager->getRepository(TblOrderDetails::class)
                                            ->findBy(['idOrder'=>$id]);
        return $this->render('admin/order/orderDetailsModalForm.twig', [
            'orders'     => $orders,
            'orderDetails' => $orderDetails,
        ]);
    }
}
