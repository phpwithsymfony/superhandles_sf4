<?php
namespace App\Controller\Admin;

use App\Datatables\CmsDatatable;
use App\Entity\TblStatic;
use App\Form\CmsType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CmsController
 * @package App\Controller
 * @Route("/admin/cms", name="cms_")
 */
class CmsController extends AbstractController
{

    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory $dtFactory
     * @param DatatableResponse $dtResponse
     * @param EntityManagerInterface $em
     */
    public function __construct(DatatableFactory $dtFactory, DatatableResponse $dtResponse, EntityManagerInterface $em)
    {
        $this->dtFactory = $dtFactory;
        $this->dtResponse = $dtResponse;
        $this->entityManager = $em;
    }

    /**
     * List of all static page
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $is_ajax = $request->isXmlHttpRequest();

        $datatable = $this->dtFactory->create(CmsDatatable::class);
        $datatable->buildDatatable();

        if ($is_ajax) {
            $this->dtResponse->setDatatable($datatable);
            $this->dtResponse->getDatatableQueryBuilder()->getQb()
                ->where("tblstatic.isDelete = :enabled")
                ->setParameter('enabled', TblStatic::NOT_DELETE);
            return $this->dtResponse->getResponse();
        }

        return $this->render('admin/cms/index.html.twig', [
            'datatable' => $datatable,
            'title' => 'CMS Listing',
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addCms(Request $request): Response
    {
        $cms_page = new TblStatic();
        $add_form = $this->createForm(CmsType::class, $cms_page);
        $add_form->handleRequest($request);

        if ($add_form->isSubmitted() && $add_form->isValid()) {
            $this->entityManager->persist($cms_page);
            $this->entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('cms_list');
        }
        return $this->render('admin/cms/add.html.twig', [
            'form' => $add_form->createView(),
            'title' => 'Add CMS'
        ]);
    }

    /**
     * Display edit form for existing cms
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request $request
     * @param TblStatic $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblStatic $updateEntity)
    {
        $edit_form = $this->createForm(CmsType::class, $updateEntity);
        $edit_form->handleRequest($request);

        if ($edit_form->isSubmitted() && $edit_form->isValid()) {
            $this->entityManager->flush();
            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('cms_list');
        }

        return $this->render('admin/cms/add.html.twig', [
            'form' => $edit_form->createView(),
            'title' => 'Edit CMS'
        ]);
    }

    /**
     * Bulk delete a cms entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request)
    {
        $is_ajax = $request->isXmlHttpRequest();
        if ($is_ajax) {
            $choices = $request->request->get('data');
            $token = $request->request->get('token');

            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $repository = $this->entityManager->getRepository('App:TblStatic');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblStatic::DELETE);
                $this->entityManager->persist($entity);
            }
            $this->entityManager->flush();
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
