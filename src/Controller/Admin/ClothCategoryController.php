<?php

namespace App\Controller\Admin;

use App\Datatables\ClothCategoryDatatables;
use App\Entity\TblProductType;
use App\Form\ClothCategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClothCategoryController
 * @package App\Controller
 * @Route("/admin/cloth/category", name="cloth_category_")
 */
class ClothCategoryController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ProductController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List Of all cloth Category
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(ClothCategoryDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->where('tblproducttype.isDelete = :isDelete');
            $qb->andWhere('tblproducttype.isCloth = :isCloth');
            $qb->andWhere('tblproducttype.idParent > 0');
            //set parameters
            $qb->setParameter('isCloth', TblProductType::ISCLOTH);
            $qb->setParameter('isDelete', TblProductType::NOTDELETE);
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Cloth Category',
            'route'     => ['add' => 'cloth_category_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New Cloth Category
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblProductType();
        $form      = $this->createForm(ClothCategoryType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($addEntity);

            $idParent = $this->entityManager->getRepository(TblProductType::class)
                                            ->find(TblProductType::ID_PARENT);

            $addEntity->setIsCloth(TblProductType::ISCLOTH);
            $addEntity->setAppProductType(TblProductType::APP_PRODUCT_TYPE);
            $addEntity->setIdParent($idParent);
            $this->entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('cloth_category_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Add Cloth Category',
        ]);
    }

    /**
     * Display edit form for existing Cloth Type
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblProductType $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProductType $updateEntity): Response
    {
        $form = $this->createForm(ClothCategoryType::class, $updateEntity);
        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($updateEntity);
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('cloth_category_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Edit Cloth Category'
        ]);
    }

    /**
     * Bulk delete a cloth Products size  entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblProductType');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblProductType::DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
