<?php
namespace App\Controller\Admin;

use App\Datatables\TestimonialsDatatable;
use App\Entity\TblTestimonials;
use App\Form\TestimonialsType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestimonialsController
 * @package App\Controller
 * @Route("/admin/testimonials", name="testimonials_")
 */
class TestimonialsController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of All testimonials
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(TestimonialsDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tbltestimonials.isDelete = :enabled');

            //set parameters
            $qb->setParameter('enabled', TblTestimonials::_NOTDELETE);

            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Testimonials',
            'route'     => ['add' => 'testimonials_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add new Testimonials
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblTestimonials();
        $form = $this->createForm(TestimonialsType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('testimonials_list');
        }
        return $this->render('admin/common/addwithimage.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Testimonial',
        ]);
    }

    /**
     * Display edit form for existing Testimonials
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request         $request
     * @param TblTestimonials $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblTestimonials $updateEntity): Response
    {
        $form = $this->createForm(TestimonialsType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $updateEntity->setUpdatedDate($this->getUser());

            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('testimonials_list');
        }

        $imageName = ($updateEntity->getAuthorPic() instanceof File) ?
            $updateEntity->getAuthorPic()->getFileName() :
            $imageName = $updateEntity->getAuthorPic();

        return $this->render('admin/common/addwithimage.html.twig', [
            'form'      => $form->createView(),
            'imageName' => $imageName,
            'title'     => 'Edit Testimonial',
        ]);
    }


    /**
     * Bulk delete a Testimonials entity.
     *
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblTestimonials');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblTestimonials::_DELETE);
                $em->persist($entity);
            }
            $em->flush();
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
