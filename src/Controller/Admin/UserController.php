<?php

namespace App\Controller\Admin;

use App\Datatables\UserDatatable;
use App\Entity\TblOrderDetails;
use App\Entity\TblOrders;
use App\Entity\TblStreamingPages;
use App\Entity\TblUserCcCode;
use App\Entity\User;
use App\Form\AddUserType;
use App\Form\UserCcCodeType;
use App\Service\EmailService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/admin/user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * UserController constructor.
     *
     * @param DatatableFactory       $dtFactory
     * @param DatatableResponse      $dtResponse
     * @param EntityManagerInterface $em
     */
    public function __construct(DatatableFactory $dtFactory, DatatableResponse $dtResponse, EntityManagerInterface $em)
    {
        $this->dtFactory     = $dtFactory;
        $this->dtResponse    = $dtResponse;
        $this->entityManager = $em;
    }

    /**
     * User listing page.
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $is_ajax   = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(UserDatatable::class);
        $datatable->buildDatatable();

        if ($is_ajax) {
            $this->dtResponse->setDatatable($datatable);
            $this->dtResponse->getDatatableQueryBuilder()->getQb()
                             ->where('profile.isDelete = :isDelete')
                             ->andWhere('user.roles LIKE :roles')
                             ->setParameter('isDelete', User::NOTDELETE)
                             ->setParameter('roles', '%"ROLE_REGISTER_USER"%');

            return $this->dtResponse->getResponse();
        }

        return $this->render('admin/user/index.html.twig', [
            'datatable' => $datatable,
        ]);
    }

    /**
     * Display add form for add new user and save in db.
     *
     * @Route("/add", name="add")
     *
     * @param Request      $request
     *
     * @param EmailService $emailService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addUser(Request $request, EmailService $emailService): Response
    {
        $user_obj = new User();
        $user_obj->addRole('ROLE_REGISTER_USER');

        $add_form = $this->createForm(AddUserType::class, $user_obj);

        $add_form->handleRequest($request);

        if ($add_form->isSubmitted() && $add_form->isValid()) {
            $user_obj->getProfile()->setCreatedBy($this->getUser());
            $user_obj->setEnabled(true);

            $this->entityManager->persist($user_obj);
            $this->entityManager->flush();

            /*** SEND ***/
            $mail_params = [
                'name'    => $user_obj->getProfile()->getFirstName() . " " .
                             $user_obj->getProfile()->getLastName() .
                             " (" . $user_obj->getUsername() . ")",
                'subject' => 'Your Superhandles Login Information...',
            ];
            //use the emailservice service to send emails
            $emailService->sendEmail(
                'admin/emails/registration',
                $mail_params,
                $user_obj->getEmail(),
                $this->getParameter('MAIL_FROM'),
                $this->getParameter('MAIL_FROM_NAME')
            );
            /*** END ***/

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('user_list');
        }
        return $this->render('admin/user/add.html.twig', [
            'form' => $add_form->createView(),
        ]);
    }

    /**
     * Display edit form for existing user and update in db.
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request              $request
     * @param User                 $user
     * @param UserManagerInterface $userManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUser(Request $request, User $user, UserManagerInterface $userManager): Response
    {
        $edit_form = $this->createForm(AddUserType::class, $user);
        $edit_form->handleRequest($request);

        if ($edit_form->isSubmitted() && $edit_form->isValid()) {
            $user->getProfile()->setUpdatedBy($this->getUser());
            $userManager->updateUser($user);
            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('user_list');
        }

        return $this->render('admin/user/add.html.twig', [
            'form'            => $edit_form->createView(),
            'formActionLabel' => 'Edit'
        ]);
    }


    /**
     * Bulk delete a user entity.
     *
     * @Route("/bulkDelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $is_ajax = $request->isXmlHttpRequest();
        if ($is_ajax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $repository = $this->entityManager->getRepository('App:User');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->getProfile()->setisDelete(1);
                $this->entityManager->persist($entity);
            }
            $this->entityManager->flush();
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * Bulk delete a user entity.
     *
     * @Route("/usercode", name="cc_code")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function userCcCode(Request $request): Response
    {
        $userId     = $request->get('id');
        $userCcCode = $this->entityManager->getRepository(TblUserCcCode::class)->findByAssignedCcCode($userId);

        $user     = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $userId]);
        $add_form = $this->createForm(UserCcCodeType::class, []);
        $label    = $user->getProfile()->getFirstname() . " " .
                    $user->getProfile()->getLastname() . "(" . $user->getEmail() . ")";
        $add_form->handleRequest($request);
        return $this->render('admin/user/modalForm.twig', [
            'formLabel'  => $label,
            'form'       => $add_form->createView(),
            'form_name'  => 'Add User',
            'userId'     => $userId,
            'userCcCode' => json_encode($userCcCode)
        ]);
    }

    /**
     * Manule Add CC Code for User
     *
     * @Route("/addusercode", name="add_cc_code", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addCcCode(Request $request): Response
    {
        $em            = $this->entityManager;
        $streamingId   = $request->get('streamingId');
        $userId        = $request->get('userId');
        $streamingPage = $em->getRepository(TblStreamingPages::class)
                            ->findOneBy(['id' => $streamingId]);
        $user          = $em->getRepository(User::class)
                            ->findOneBy(['id' => $userId]);

        $ccCode = new TblUserCcCode();
        $ccCode->setIdStreamingPage($streamingPage);
        $ccCode->setIdUser($user);

        $em->persist($ccCode);
        $em->flush();

        return new JsonResponse($ccCode->getId());
    }

    /**
     * Manule Remove CC Code for User
     *
     * @Route("/removeusercode", name="remove_cc_code", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function removeCcCode(Request $request): Response
    {
        $em = $this->entityManager;

        $streamingId = $request->get('streamingId');
        $userId      = $request->get('userId');

        $streamingPage = $em->getRepository(TblUserCcCode::class)
                            ->findOneBy(['idStreamingPage' => $streamingId, 'idUser' => $userId]);

        $em->remove($streamingPage);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * Manule Remove CC Code for User
     *
     * @Route("/{username}/profile", name="view_profile", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function viewUserProfile(User $user): Response
    {
        $ccCode = $this->entityManager->getRepository(TblUserCcCode::class)->findBy(['idUser' => $user]);

        $orders = $this->entityManager->getRepository(TblOrders::class)
                                      ->findBy(['idUser'=>$user, 'status' => TblOrders::STATUS_SUCCESS]);
        return $this->render('admin/user/viewProfile.html.twig', [
            'title'    => 'test',
            'userData' => $user,
            'ccCode'   => $ccCode,
            'orders' => $orders
        ]);
    }
}
