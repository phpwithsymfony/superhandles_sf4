<?php

namespace App\Controller\Admin;

use App\Datatables\ProductsDatatables;
use App\Entity\TblProductCategory;
use App\Entity\TblProducts;
use App\Entity\TblProductSize;
use App\Entity\TblProductType;
use App\Form\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ProductController
 * @package App\Controller
 * @Route("/admin/products", name="products_")
 */
class ProductController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;


    /**
     * ProductController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of all product
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request) : Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(ProductsDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();
            //join
            $qb->innerJoin('tblproducts.idProductType', 'pt', 'WITH', 'tblproducts.idProductType = pt.id');
            //add where conditions
            $qb->andWhere('tblproducts.isDelete = :isDelete');
            $qb->andWhere('pt.isCloth = :cloth');

            //set parameters
            $qb->setParameter('isDelete', TblProducts::_NOTDELETE);
            $qb->setParameter('cloth', TblProductType::NOTCLOTH);
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'           => 'Products',
            'route'           => ['add' => 'products_add'],
            'datatable'       => $datatable,
        ]);
    }

    /**
     * Add new Product
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request) : Response
    {
        $addEntity = new TblProducts();
        $form = $this->createForm(ProductType::class, $addEntity, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('products_list');
        }
        return $this->render('admin/common/addwithimage.html.twig', [
            'form'            => $form->createView(),
            'title'           => 'Add Product',
        ]);
    }

    /**
     * Display edit form for existing Products
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request     $request
     * @param TblProducts $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProducts $updateEntity) : Response
    {
        $form = $this->createForm(ProductType::class, $updateEntity);
        // handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($updateEntity);
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('products_list');
        }
        $imageName = ($updateEntity->getImage() instanceof File) ?
            $updateEntity->getImage()->getFileName() :
            $imageName = $updateEntity->getImage();
        return $this->render('admin/common/addwithimage.html.twig', [
            'form'            => $form->createView(),
            'title'           => 'Edit Products',
            'imageName'       => $imageName,
        ]);
    }

    /**
     * Bulk delete a Products entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request) : Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblProducts');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblProducts::_DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
