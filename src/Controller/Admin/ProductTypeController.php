<?php

namespace App\Controller\Admin;

use App\Datatables\ProductTypeDatatables;
use App\Entity\TblProductType;
use App\Form\ProductTypesType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductTypeController
 * @package App\Controller
 * @Route("/admin/cloth_category", name="productstype_")
 */
class ProductTypeController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ProductTypeController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of all Product type
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(ProductTypeDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblproducttype.isDelete = :enabled');
            $qb->andWhere('tblproducttype.isCloth = :enabled');

            //set parameters
            $qb->setParameter('enabled', 1);

            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Cloth Type',
            'route'     => ['add' => 'productstype_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add new Product type
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblProductType();
        $form = $this->createForm(ProductTypesType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addEntity->setIsCloth(1);

            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('productstype_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Cloth Type'
        ]);
    }

    /**
     * Display edit form for existing ProductType
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblProductType $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProductType $updateEntity): Response
    {
        $form = $this->createForm(ProductTypesType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('productstype_list');
        }

        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Edit Cloth Type',
        ]);
    }

    /**
     * Deletes a ProductType entity.
     *
     * @param TblProductType $deleteEntity
     *
     * @Route("/delete/{id}", name="delete", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(TblProductType $deleteEntity): RedirectResponse
    {
        $deleteEntity->setIsDelete(0);
        $this->entityManager->flush();
        $this->addFlash('success', 'Record deleted successfully.');

        return $this->redirectToRoute('productstype_list');
    }


    /**
     * Bulk delete a ProductType entity.
     *
     * @Route("/bulkdelete", name="bulk_delete",  methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblProductType');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(0);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * Update Status ProductType
     *
     * @Route("/updateStatus/{id}", name="update_status", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request        $request
     * @param TblProductType $statusEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateStatus(Request $request, TblProductType $statusEntity): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $statusEntity->setIsActive(!$statusEntity->getIsActive());

            $em = $this->entityManager;
            $em->persist($statusEntity);
            $em->flush();

            if ($statusEntity->getIsActive() === '1') {
                $this->addFlash('success', 'Record activated successfully');
            } else {
                $this->addFlash('success', 'Record inactivated successfully');
            }

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
