<?php
namespace App\Controller\Admin;

use App\Datatables\SubscriptionPlanDatatables;
use App\Entity\TblSubscriptionCoachPlan;
use App\Entity\TblSubscriptionPlan;
use App\Form\SubscriptionPlanType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SubscriptionPlanController
 * @package App\Controller
 * @Route("/admin/subscription", name="subscriptionplan_")
 */
class SubscriptionPlanController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;


    /**
     * SubscriptionPlanController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of SubscriptionPlan
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(SubscriptionPlanDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblsubscriptionplan.isDelete = :isDelete');

            //set parameters
            $qb->setParameter('isDelete', TblSubscriptionPlan::NOTDELETE);
            #return $responseService->getResponse();
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Subscription Plan',
            'route'     => ['add' => 'subscriptionplan_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblSubscriptionPlan();
        $form      = $this->createForm(SubscriptionPlanType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($addEntity->getMembers()[0]) {
                $this->createCoachSubscriptionPlan($addEntity);
            }
            $this->entityManager->persist($addEntity);
            $this->entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('subscriptionplan_list');
        }

        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Add Subscription Plan'
        ]);
    }

    /**
     * Display edit form for existing SubscriptionPlan
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request             $request
     * @param TblSubscriptionPlan $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblSubscriptionPlan $updateEntity): Response
    {
        $form = $this->createForm(SubscriptionPlanType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('subscriptionplan_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Edit Subscription Plan'
        ]);
    }

    /**
     * @param $entity
     */
    public function createCoachSubscriptionPlan($entity)
    {
        $members = $entity->getMembers();
        if (count($members) > 0) {
            foreach ($members as $member) {
                $price = round($member * $entity->getAmount(), 2);
                $planName = "Coach_".$entity->getPlanName()."_".$member."_".$entity->getAmount();

                $coachPlan = new TblSubscriptionCoachPlan();

                $coachPlan->setIdSubscriptionPlan($entity);
                $coachPlan->setMembers($member);
                $coachPlan->setAmount($price);
                $coachPlan->setPlanName($planName);
                $coachPlan->setPackageInterval($entity->getPackageInterval());
                $coachPlan->setTrialDays($entity->getTrialDays());

                $this->entityManager->persist($coachPlan);
            }
        }
    }
}
