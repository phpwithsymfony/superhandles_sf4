<?php

namespace App\Controller\Admin;

use App\Datatables\CartCouponDatatable;
use App\Entity\TblCartCoupons;
use App\Form\CartCouponType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartCouponsController
 * @package App\Controller
 * @Route("/admin/cartcoupons", name="promocode_coupon_")
 */
class CartCouponsController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * Display List for Promocode Coupons
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(CartCouponDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $this->dtResponse->setDatatable($datatable);
            $datatableQueryBuilder = $this->dtResponse->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblcartcoupons.isDelete = :isDelete');

            //set parameters
            $qb->setParameter('isDelete', TblCartCoupons::NOTDELETE);
            return $this->dtResponse->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Coupons',
            'datatable' => $datatable,
            'route'     => ['add' => 'promocode_coupon_add'],
        ]);
    }

    /**
     * Add New for promocode coupoon
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblCartCoupons();
        $form = $this->createForm(CartCouponType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->entityManager;
                $entityManager->persist($addEntity);
                $entityManager->flush();

                $this->addFlash('success', 'Record added successfully.');
                return $this->redirectToRoute('promocode_coupon_list');
            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());

                return $this->render('admin/cartCoupon/add.html.twig', [
                    'form' => $form->createView(),
                    'title'    => 'Add Coupon'
                ]);
            }
        }
        return $this->render('admin/cartCoupon/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Coupon'
        ]);
    }

    /**
     * Display edit form for existing Promocode Coupon
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request            $request
     * @param TblCartCoupons $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblCartCoupons $updateEntity): Response
    {
        $form = $this->createForm(CartCouponType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
                $this->addFlash('success', 'Record updated successfully');
                return $this->redirectToRoute('promocode_coupon_list');
            } catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());

                return $this->render('admin/cartCoupon/add.html.twig', [
                    'form'     => $form->createView(),
                    'title'    => 'Edit Coupon',
                ]);
            }
        }
        return $this->render('admin/cartCoupon/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Edit Coupon',
        ]);
    }

    /**
     * Bulk delete a Promocode Coupon entity.
     *
     * @Route("/cart/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblCartCoupons');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblCartCoupons::DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
