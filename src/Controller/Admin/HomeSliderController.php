<?php
namespace App\Controller\Admin;

use App\Entity\TblCampsSlider;
use App\Datatables\HomeSliderDatatable;
use App\Entity\TblHomeSlider;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\HomeSliderType;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeSliderController
 * @package App\Controller
 * @Route("/admin/homeslider", name="home_slider_")
 */
class HomeSliderController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * CampsController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * Display List for Home Slider
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(HomeSliderDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblhomeslider.isDelete = :isDelete');

            //set parameters
            $qb->setParameter('isDelete', TblCampsSlider::_NOTDELETE);

            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Home Slider',
            'route'     => ['add' => 'home_slider_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New Home Slider
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity  = new TblHomeSlider();
        $sliderData = $this->createForm(HomeSliderType::class, $addEntity);
        $sliderData->handleRequest($request);

        if ($sliderData->isSubmitted() && $sliderData->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');
            return $this->redirectToRoute('home_slider_list');
        }
        return $this->render('admin/common/addwithimage.html.twig', [
            'form'  => $sliderData->createView(),
            'title' => 'Add Home Slider',
        ]);
    }

    /**
     * edit form for existing HomeSlider
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request       $request
     * @param TblHomeSlider $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblHomeSlider $updateEntity): Response
    {
        $em         = $this->entityManager;
        $sliderData = $this->createForm(HomeSliderType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $sliderData->handleRequest($request);

        if ($sliderData->isSubmitted() && $sliderData->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Record updated successfully');
            return $this->redirectToRoute('home_slider_list');
        }

        $imageName = ($updateEntity->getBannerFile() instanceof File) ?
            $updateEntity->getBannerFile()->getFileName() :
            $imageName = $updateEntity->getBannerFile();

        return $this->render('admin/common/addwithimage.html.twig', [
            'form'      => $sliderData->createView(),
            'title'     => 'Edit Home Slider',
            'imageName' => $imageName,
        ]);
    }

    /**
     * Bulk delete a HomeSlider entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblHomeSlider');

            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblHomeSlider::_DELETE);
                $em->persist($entity);
            }
            $em->flush();
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
