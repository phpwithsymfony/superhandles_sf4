<?php
namespace App\Controller\Admin;

use App\Datatables\StreamingPageDatatables;
use App\Entity\TblProducts;
use App\Entity\TblStreamingPages;
use App\Form\StreamingPageType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StreamingPageController
 * @package App\Controller
 * @Route("/admin/streaming-pages", name="streamingpages_")
 */
class StreamingPageController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * StreamingPageController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     *
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List of StreamingPages
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request) : Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(StreamingPageDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblstreamingpages.isDelete = :enabled');

            //set parameters
            $qb->setParameter('enabled', 0);
            #return $responseService->getResponse();
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Streaming Page',
            'tblTitle'  => 'Streaming Page',
            'route'     => ['list' => 'streamingpages_list', 'add' => 'streamingpages_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New StreamingPages
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request) : Response
    {
        $addEntity = new TblStreamingPages();
        $form = $this->createForm(StreamingPageType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');

            return $this->redirectToRoute('streamingpages_list');
        }
        return $this->render('admin/streaming/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Streaming Page',
            'tblTitle' => 'Add Streaming Page',
            'route'    => ['list' => 'streamingpages_list', 'add' => 'streamingpages_add']
        ]);
    }

    /**
     * Display edit form for existing StreamingPage
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request           $request
     * @param TblStreamingPages $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblStreamingPages $updateEntity) : Response
    {
        $form = $this->createForm(StreamingPageType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');

            return $this->redirectToRoute('streamingpages_list');
        }

        return $this->render('admin/streaming/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Streaming Page',
            'tblTitle' => 'Edit Streaming Page',
            'route'    => ['list' => 'streamingpages_list', 'add' => 'streamingpages_add'],
        ]);
    }

    /**
     * Bulk delete a StreamingPage entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request) : Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $stripArray = [];
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblStreamingPages');
            foreach ($choices as $choice) {
                $entity       = $repository->find($choice['id']);
                $stripArray[] = $entity->getStripePlanId();
                $entity->setIsDelete(0);
                $em->persist($entity);
            }

            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * Ajax get category id to Product
     *
     * @Route("/catproduct", name="cat_product", methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function categorytoProduct(Request $request) : JsonResponse
    {
        $idProductType = $request->get('idProductType');
        $resultData = $this->entityManager->getRepository(TblProducts::class)
                                  ->findcategorytoProduct($idProductType);

        return new JsonResponse($resultData, 200);
    }

    /**
     * Ajax get subscription Streaming Product Count
     *
     * @Route("/subscriptionProduct", name="subscription_product",
     *     methods={"POST"}, options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getSubscriptionProduct(Request $request) : JsonResponse
    {
        $idProductType = $request->get('idProductType');
        $idProduct = $request->get('idProduct');

        $resultData = $this->entityManager->getRepository(TblStreamingPages::class)
                                        ->getSubscriptionProduct($idProductType, $idProduct);

        return new JsonResponse($resultData, 200);
    }
}
