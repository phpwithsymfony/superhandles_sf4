<?php

namespace App\Controller\Admin;

use App\Datatables\ProductCategoryDatatables;
use App\Entity\TblProductCategory;
use App\Form\ProductCategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductCategoryController
 * @package App\Controller
 * @Route("/admin/product/categories", name="productcat_")
 */
class ProductCategoryController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ProductCategoryController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * List All ProductCategory
     *
     * @Route("/", name="list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $isAjax    = $request->isXmlHttpRequest();
        $datatable = $this->dtFactory->create(ProductCategoryDatatables::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $qb = $datatableQueryBuilder->getQb();

            //add where conditions
            $qb->andWhere('tblproductcategory.isDelete = :enabled');

            //set parameters
            $qb->setParameter('enabled', TblProductCategory::NOTDELETE);
            #return $responseService->getResponse();
            return $responseService->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Product Category',
            'route'     => ['add' => 'productcat_add'],
            'datatable' => $datatable,
        ]);
    }

    /**
     * Add New for ProductCategory
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblProductCategory();
        $form = $this->createForm(ProductCategoryType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');


            return $this->redirectToRoute('productcat_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Product Category'
        ]);
    }

    /**
     * Display edit form for existing ProductCategory
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     *
     * @param Request            $request
     * @param TblProductCategory $updateEntity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, TblProductCategory $updateEntity): Response
    {
        $form = $this->createForm(ProductCategoryType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');

            return $this->redirectToRoute('productcat_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Edit Product Category',
        ]);
    }

    /**
     * Bulk delete a ProductCategory entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository('App:TblProductCategory');
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblProductCategory::DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
