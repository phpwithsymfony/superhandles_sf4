<?php
namespace App\Controller\Admin;

use App\Entity\TblProducts;
use App\Entity\TblSubscriptionPlan;
use App\Entity\TblSubscriptionPackage;
use App\Helper\StripeWebhookHelper;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StripWebhookController
 * @package App\Controller\Admin
 */
class StripWebhookController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var stripeWebhookHelper
     */
    private $stripeWebhookHelper;

    /**
     * CampsController constructor.
     *
     * @param StripeWebhookHelper    $stripeWebhookHelper
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, StripeWebhookHelper $stripeWebhookHelper)
    {
        $this->stripeWebhookHelper = $stripeWebhookHelper;
        $this->entityManager       = $entityManager;
    }


    /**
     * @param Request $request
     *
     * Stripe Webhook Call
     *
     * @Route("/stripewebhook", name="strip_webhook")
     *
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $em = $this->entityManager;

        $customLogger = new Logger('logger');
        $customLogger->pushHandler(new StreamHandler('../var/log/stripe_webhook.log', Logger::DEBUG));
        $data    = json_decode($request->getContent(), true);
        $customLogger->addInfo('stripeEvent > type',$data);

        $eventId = $data['id'];
        $stripeEvent = $this->stripeWebhookHelper->findEvent($eventId);
        $customLogger->addInfo("Stripe Webhook : " . $stripeEvent->type);

        $stripId = $stripeEvent->data->object->id;

        switch ($stripeEvent->type) {
            case 'plan.updated':
                $nickname          = $stripeEvent->data->object->nickname;
                $trial_period_days = $stripeEvent->data->object->trial_period_days;

                $entity = $em->getRepository(TblSubscriptionPlan::class)
                             ->findOneBy(['stripePlanId' => $stripId]);

                $entity->setTitle($nickname);
                $entity->setTrialDays($trial_period_days);
                $entity->setStripeLastActivity("Direct strip plan updated");
                $entity->setUpdatedDate(new \DateTime());

                $em->persist($entity);
                $em->flush();

                break;
            case 'plan.deleted':
                $entity = $em->getRepository(TblSubscriptionPlan::class)
                             ->findOneBy(['stripePlanId' => $stripId]);

                $entity->setIsDelete(0);
                $entity->setUpdatedDate(new \DateTime());
                $entity->setStripeLastActivity("Direct strip plan deleted");

                $em->persist($entity);
                $em->flush();

                break;
            case 'plan.created':
                $nickname          = $stripeEvent->data->object->nickname;
                $trial_period_days = $stripeEvent->data->object->trial_period_days;
                $price             = $stripeEvent->data->object->amount / 100;

                if ($stripeEvent->data->object->interval === "year") {
                    $subpackage = 365;
                } else {
                    $subpackage = $stripeEvent->data->object->interval_count * 30;
                }

                $planType = $em->getRepository(TblSubscriptionPackage::class)
                               ->findOneBy(['id' => TblSubscriptionPackage::UNIVERSITY_PACKAGES]);

                $addEntity = new TblSubscriptionPlan();
                $addEntity->setIdSubscriptionType($planType);
                $addEntity->setTitle($nickname);
                $addEntity->setDescription($nickname);
                $addEntity->setPrice($price);
                $addEntity->setStripeLastActivity("Direct strip plan created");
                $addEntity->setTrialDays($trial_period_days);
                $addEntity->setStripePlanId($stripId);
                $addEntity->setSubscriptionPackage($subpackage);

                $em->persist($addEntity);
                $em->flush();
                break;
            case 'product.updated':
                $name        = $stripeEvent->data->object->name;
                $description = $stripeEvent->data->object->description;
                $url         = $stripeEvent->data->object->url;
                //$trial_period_days = $stripeEvent->data->object->trial_period_days;

                $entity = $em->getRepository(TblProducts::class)
                             ->findOneBy(['stripId' => $stripId]);

                $entity->setTitle($name);
                $entity->setDescription($description);
                $entity->setProductUrl($url);
                $entity->setStripeLastActivity("Direct strip product updated");
                $entity->setUpdatedDate(new \DateTime());

                $em->persist($entity);
                $em->flush();

                break;
            case 'product.deleted':
                $entity = $em->getRepository(TblProducts::class)
                             ->findOneBy(['stripId' => $stripId]);

                $entity->setIsDelete(0);
                $entity->setUpdatedDate(new \DateTime());
                $entity->setStripeLastActivity("Direct strip plan deleted");

                $em->persist($entity);
                $em->flush();

                break;
            case 'product.created':
                $addEntity = new TblProducts();
                $addEntity->setTitle($stripeEvent->data->object->name);
                $addEntity->setDescription($stripeEvent->data->object->description);
                $addEntity->setPrice(200);
                $addEntity->setWeight($stripeEvent->data->object->package_dimensions->weight);
                $addEntity->setQuantity(100);
                $addEntity->setProductUrl($stripeEvent->data->object->url);
                $addEntity->setStripId($stripId);
                $addEntity->setStripeLastActivity("Direct strip product created");
                $addEntity->setCreatedDate(new \DateTime());

                $em->persist($addEntity);
                $em->flush();
                break;
            default:
                throw new \Exception('Unexpected webhook type form Stripe! ' . $stripeEvent->type);
        }
    }
}
