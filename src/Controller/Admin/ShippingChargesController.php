<?php

namespace App\Controller\Admin;

use App\Datatables\ShippingChargesDatatable;
use App\Entity\TblShippingCharges;
use App\Form\ShippingChargesType;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShippingChargesController
 * @package App\Controller
 * @Route("/admin/shippingcharges", name="shippingcharges_")
 */
class ShippingChargesController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var DatatableFactory
     */
    private $dtFactory;
    /**
     * @var DatatableResponse
     */
    private $dtResponse;

    /**
     * ShippingChargesController constructor.
     *
     * @param DatatableFactory       $datatableFactory
     * @param DatatableResponse      $datatableResponse
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DatatableFactory $datatableFactory,
        DatatableResponse $datatableResponse,
        EntityManagerInterface $entityManager
    ) {
        $this->dtFactory     = $datatableFactory;
        $this->dtResponse    = $datatableResponse;
        $this->entityManager = $entityManager;
    }

    /**
     * Listing ShippingCharges
     * @Route("/", name="list")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $is_ajax = $request->isXmlHttpRequest();

        $datatable = $this->dtFactory->create(ShippingChargesDatatable::class);
        $datatable->buildDatatable();

        if ($is_ajax) {
            $this->dtResponse->setDatatable($datatable);
            $this->dtResponse->getDatatableQueryBuilder()->getQb()
                ->where("tblshippingcharges.isDelete = :isDelete")
                ->setParameter('isDelete', TblShippingCharges::NOTDELETE)
            ;
            return $this->dtResponse->getResponse();
        }

        return $this->render('admin/common/index.html.twig', [
            'title'     => 'Shipping Charges',
            'route'     => ['add' => 'shippingcharges_add'],
            'datatable' => $datatable,
        ]);
    }


    /**
     * Add New for ShippingCharges
     *
     * @Route("/add", name="add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $addEntity = new TblShippingCharges();
        $form = $this->createForm(ShippingChargesType::class, $addEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->entityManager;
            $entityManager->persist($addEntity);
            $entityManager->flush();

            $this->addFlash('success', 'Record added successfully.');


            return $this->redirectToRoute('shippingcharges_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Add Shipping Charges'
        ]);
    }

    /**
     * Display edit form for existing ShippingCharges
     *
     * @Route("/{id}", name="edit", requirements={"id": "\d+"}, options = {"expose" = true})
     * @param Request $request
     * @param TblShippingCharges $updateEntity
     * @return Response
     */
    public function edit(Request $request, TblShippingCharges $updateEntity): Response
    {
        $form = $this->createForm(ShippingChargesType::class, $updateEntity);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Record updated successfully');

            return $this->redirectToRoute('shippingcharges_list');
        }
        return $this->render('admin/common/add.html.twig', [
            'form'     => $form->createView(),
            'title'    => 'Edit Shipping Charges',
        ]);
    }


    /**
     * Bulk delete a ShippingCharges entity.
     *
     * @Route("/bulkdelete", name="bulk_delete", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function bulkDelete(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        if ($isAjax) {
            $choices = $request->request->get('data');
            $token   = $request->request->get('token');
            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }
            $em         = $this->entityManager;
            $repository = $em->getRepository(TblShippingCharges::class);
            foreach ($choices as $choice) {
                $entity = $repository->find($choice['id']);
                $entity->setIsDelete(TblShippingCharges::DELETE);
                $em->persist($entity);
            }
            $em->flush();

            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }
}
