<?php

namespace App\Controller;

use App\Entity\TblCartProduct;
use App\Entity\TblProducts;
use App\Entity\TblProductSize;
use App\Helper\AddtoCartHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CartController
 * @package App\Controller
 * @Route("/", name="front_cart_")
 */
class CartController extends AbstractController
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @Route("/cart", name="add_to_cart")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addtoCart(): Response
    {
        $addtoCartHelper = new AddtoCartHelper($this->entityManager);

        $data = $addtoCartHelper->getCart($this->getUser());
        $amount = 0;
        if ($data) {
            foreach ($data as $cardData) {
                $amount += $cardData->getQuantity() * $cardData->getAmount();
            }
        }
        return $this->render('front/cart/cartList.html.twig', [
            'cartData' => $data,
            'cartAmount' => $amount
        ]);
    }


    /**
     * @Route("/ajaxcartadd", name="ajax_cart_add", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxCartAdd(Request $request): Response
    {
        $data = "";
        $cart = null;
        if ($request->isXmlHttpRequest()) {
            $productId  = $request->request->get('productId');
            $productQty = $request->request->get('productQty');
            $producrSize = $request->request->get('productSize');

            $addtoCartHelper = new AddtoCartHelper($this->entityManager);
            $product         = $this->entityManager->getRepository(TblProducts::class)
                                                   ->find($productId);
            if ($this->getUser()) {
                $customer = $addtoCartHelper->createCustomer($this->getUser());
                if ($customer) {
                    $cart = $addtoCartHelper->createCart($this->getUser(), $customer);
                }
            } else {
                $cart = $addtoCartHelper->createCart($this->getUser());
            }
            if ($cart) {
                $data = $addtoCartHelper->createCartProduct($cart, $product, $productQty, $producrSize);
            }
        }
        if ($data) {
            return new Response($data->getId(), 200);
        } else {
            return new Response(0, 200);
        }
    }

    /**
     * @Route("/ajaxgetcartdetail", name="ajax_cart_detail", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxGetCartDetail(Request $request): Response
    {
        $data            = [];
        $addtoCartHelper = new AddtoCartHelper($this->entityManager);
        if ($request->isXmlHttpRequest()) {
            $data = $addtoCartHelper->getCart($this->getUser());
            if ($data) {
                $data = count($data);
            } else {
                $data = 0;
            }
        }

        return new Response($data, 200);
    }

    /**
     * @Route("/ajaxCartUpdate", name="ajax_cart_update")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxCartUpdate(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $cartId   = $request->request->get('cartId');
            $operator = $request->request->get('operator');

            $entity = $this->entityManager->getRepository(TblCartProduct::class)
                                          ->findOneBy(['id' => $cartId]);

            if ($entity) {
                if ($operator == 1) {
                    $qty = $entity->getQuantity() + 1;
                } else {
                    $qty = $entity->getQuantity() - 1;
                }
                $totalAmount = $entity->getAmount() * $qty;
                $entity->setQuantity($qty);
                $entity->setTotalAmount($totalAmount);

                $em = $this->entityManager;
                $em->persist($entity);
                $em->flush();
            }
        }

        return new JsonResponse('success', 200);
    }

    /**
     * @Route("/ajaxCartRemove", name="ajax_cart_remove")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function ajaxCartRemove(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            $cartId = $request->request->get('cartId');
            $em     = $this->entityManager;
            if ($cartId) {
                $entity = $this->entityManager->getRepository(TblCartProduct::class)
                                              ->findOneBy(['id' => $cartId]);
                if ($entity) {
                    $em->remove($entity);
                    $em->flush();
                }
            } else {
                $cartHelper = new AddtoCartHelper($this->entityManager);
                $cart       = $cartHelper->checkCartCustomer($this->getUser());

                $entity = $this->entityManager->getRepository(TblCartProduct::class)
                                              ->findBy(['idCart' => $cart]);
                if ($entity) {
                    foreach ($entity as $value) {
                        $em->remove($value);
                    }
                    $em->flush();
                }
            }
        }
        return new JsonResponse("success", 200);
    }

    /**
     * @Route("/ajaxcartdualproduct", name="ajax_cart_dual_product", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAjaxCartDualProduct(Request $request): Response
    {
        $product = [];
        if ($request->isXmlHttpRequest()) {
            $productId = $request->request->get('productId');

            $product = $this->entityManager->getRepository(TblProducts::class)
                                           ->findProductsInArray($productId);
        }
        return new JsonResponse($product);
    }

    /**
     * @Route("/getproductsize", name="ajax_cart_product_size", options = {"expose" = true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     */
    public function getAjaxProductSize(Request $request): Response
    {
        $productSize = [];
        if ($request->isXmlHttpRequest()) {
            $size = $request->request->get('size');

            rtrim($size, ']');
            $sizeData =  ltrim($size, '[');
            $productSize = $this->entityManager->getRepository(TblProductSize::class)
                                           ->findArrayResult(explode(",", $sizeData));
        }
        return new JsonResponse($productSize);
    }
}
