<?php

namespace App\DataFixtures;

use App\Entity\TblPaymentFields;
use App\Entity\TblPaymentMethods;
use App\Entity\TblShippingCharges;
use App\Entity\TblStatic;
use App\Entity\TblUserProfile;
use App\Entity\User;
use App\Entity\TblCountries;
use Craue\ConfigBundle\Entity\Setting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixture
 * @package App\DataFixtures
 */
class UserFixture extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserFixture constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userObj = $this->createAdminUser($manager);
        $this->createStaticPage($manager, $userObj);
        $this->addPaymentMethods($manager);
        $this->addShippingCharges($manager, $userObj);
        $this->addCraueConfigSetting($manager);
    }


    /**
     * @param $manager
     */
    public function createAdminUser($manager)
    {
        $user = new User();
        $user->setUsername('admin');
        //$user->setPassword($this->encoder->encodePassword($user, '123456'));
        $user->setPlainPassword('admin');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEmail('kirit@ilogixinfotech.com');
        $user->setEnabled(User::ACTIVE);
        $userProfile = new TblUserProfile();
        $userProfile->setFirstName('Ashish');
        $userProfile->setLastName('Ranpara');
        $user->setProfile($userProfile);
        $manager->persist($user);
        $manager->flush();
        return $user;
    }

    /**
     * @param $manager
     */
    public function createStaticPage($manager, $userObj)
    {
        $staticPage = new TblStatic();
        $staticPage->setPageTitle('Thank You');
        $staticPage->setPageContent('<i class="fa fa-check" aria-hidden="true"></i>
Thank you for registering. <a href="/login">Click here to login to Superhandles.com</a>');
        $staticPage->setPageUrl('thank-you-register');
        $manager->persist($staticPage);
        $staticPage->setCreatedBy($userObj);
        $manager->flush();
    }

    /**
     * @param $manager
     * @throws \Exception
     */
    public function addPaymentMethods($manager)
    {
        $paymentMethodsArr = [
            ['title' => 'Paypal', 'is_active' => '1', 'description' => 'Paypal Checkout'],
            ['title' => 'Stripe', 'is_active' => '1', 'description' => 'Visa, Master card, etc. Checkout']
        ];

        $paymentFieldsArr = [
            'Paypal' => [
                [
                    'field_name' => 'MERCHANT_ID',
                    'field_value' => 'supe@superhandles.com',
                    'description' => 'testing',
                    'is_active' => '1'
                ],
                [
                    'field_name' => 'CURRENCY_CODE',
                    'field_value' => 'USD', 'description' =>
                    'testing', 'is_active' => '1'
                ]
            ],
            'Stripe' => [
                [
                    'field_name' => 'MERCHANT_ID',
                    'field_value' => 'supe@superhandles.com',
                    'description' => 'testing',
                    'is_active' => '1'
                ],
                [
                    'field_name' => 'CURRENCY_CODE',
                    'field_value' => 'USD',
                    'description' => 'testing',
                    'is_active' => '1'
                ]
            ]
        ];

        foreach ($paymentMethodsArr as $paymentMethod) {
            $paymentMethods = new TblPaymentMethods();
            $paymentMethods->setTitle($paymentMethod['title']);
            $paymentMethods->setIsActive($paymentMethod['is_active']);
            $paymentMethods->setDescription($paymentMethod['description']);
            $manager->persist($paymentMethods);
            foreach ($paymentFieldsArr[$paymentMethod['title']] as $paymentField) {
                $paymentFields = new TblPaymentFields();
                $paymentFields->setIdPaymentMethods($paymentMethods);
                $paymentFields->setFieldName($paymentField['field_name']);
                $paymentFields->setFieldValue($paymentField['field_value']);
                $paymentFields->setDescription($paymentField['description']);
                $paymentFields->setIsActive($paymentField['is_active']);
                $paymentFields->setCreatedDate(new \DateTime());
                $manager->persist($paymentFields);
            }
        }
        $manager->flush();
    }

    /**
     * @param $manager
     */
    public function addShippingCharges($manager, $user)
    {

        $countries = ['US', 'CA', 'Other'];

        foreach ($countries as $countrie) {
            $countries = new TblCountries();
            $countries->setCountryName($countrie);
            $countries->setCountryCode($countrie);
            $countries->setCreatedBy($user);
            $countries->setIsActive(TblCountries::ACTIVE);
            $countries->setIsDelete(TblCountries::INACTIVE);
            $countries->setIsShipping(TblCountries::SHIPPING);
            $countries->setCreatedDate(new \DateTime());
            $countries->setUpdatedDate(null);
            $manager->persist($countries);

            $shippginChargesArr['US'] = [
                ['min_weight' => '0.01', 'max_weight' => '2', 'price' => '6.95', 'country' => 'US', 'is_active' => 1],
                ['min_weight' => '2.01', 'max_weight' => '8', 'price' => '13.5', 'country' => 'US', 'is_active' => 1],
                ['min_weight' => '8.01', 'max_weight' => '100', 'price' => '17.5', 'country' => 'US', 'is_active' => 1],
            ];
            $shippginChargesArr['CA'] = [
                ['min_weight' => '0.01', 'max_weight' => '2', 'price' => '20', 'country' => 'CA', 'is_active' => 1],
                ['min_weight' => '2.01', 'max_weight' => '8', 'price' => '41', 'country' => 'CA', 'is_active' => 1],
                ['min_weight' => '8.01', 'max_weight' => '100', 'price' => '54', 'country' => 'CA', 'is_active' => 1],
            ];
            $shippginChargesArr['Other'] = [
                ['min_weight' => '0.01', 'max_weight' => '2', 'price' => '24.5', 'country' => 'Other', 'is_active' => 1],
                ['min_weight' => '2.01', 'max_weight' => '8', 'price' => '60', 'country' => 'Other', 'is_active' => 1],
                ['min_weight' => '8.01', 'max_weight' => '100', 'price' => '78', 'country' => 'Other', 'is_active' => 1]
            ];

            foreach ($shippginChargesArr[$countrie] as $item) {
                $shippginCharges = new TblShippingCharges();
                $shippginCharges->setMinWeight($item['min_weight']);
                $shippginCharges->setMaxWeight($item['max_weight']);
                $shippginCharges->setPrice($item['price']);
                $shippginCharges->setIdCountry($countries);
                $shippginCharges->setCreatedDate(new \DateTime());
                $shippginCharges->setUpdatedDate(null);
                $manager->persist($shippginCharges);
            }
        }
        $manager->flush();
    }


    public function addCraueConfigSetting($manager)
    {
        $settings = [
            //Global
            ['name' => 'ADMIM_EMAIL', 'section' => 'Global', 'value' => 'jon@superhandles.com'],
            ['name' => 'HOME_PAGE_VIDEO_URL', 'section' => 'Global', 'value' => '//player.vimeo.com/video/114445022?byline=0&portrait=0&color=ffffff&autoplay=1'],
            ['name' => 'NEWSLETTER_SIGN_UP_DESCRIPTION', 'section' => 'Global', 'value' => 'You\'re About to Receive a FREE Killer Crossover Workout & our Basketball Tips Newsletter . FREE!'],
            ['name' => 'NEWSLETTER_SIGN_UP_HEADER', 'section' => 'Global', 'value' => 'Free Killer Crossover Workout'],
            ['name' => 'SITE_TITLE', 'section' => 'Global', 'value' => 'Superhandles'],

            //Store
            ['name' => 'CLOTHING_FIRST_PRODUCT_SHIPPING', 'section' => 'Store', 'value' => '3.95'],
            ['name' => 'CLOTHING_PRODUCT_SHIPPING', 'section' => 'Store', 'value' => '0.95'],
            ['name' => 'CLOTHING_SELLER_EMAIL', 'section' => 'Store', 'value' => 'superhandles@shockcustomprinting.com'],
            ['name' => 'SHIPPING_API_USER_ID', 'section' => 'Store', 'value' => '698XYZCO4847'],
            ['name' => 'STORE_CONTACT', 'section' => 'Store', 'value' => 'Get Support via Email'],
            ['name' => 'STORE_ZIPCODE', 'section' => 'Store', 'value' => '97128'],

            //Hoopfolio
            ['name' => 'LEADERBOARD_PER_PAGE_RECORD', 'section' => 'Hoopfolio', 'value' => '20'],
            ['name' => 'MSG_PER_PAGE_RECORD', 'section' => 'Hoopfolio', 'value' => '6'],
            ['name' => 'TROPHIES_PER_PAGE_RECORD', 'section' => 'Hoopfolio', 'value' => '6'],
            ['name' => 'WALL_ATTACHMENT_PER_PAGE_RECORD', 'section' => 'Hoopfolio', 'value' => '6'],
            ['name' => 'WALL_PER_PAGE_RECORD', 'section' => 'Hoopfolio', 'value' => '5'],

            //Post Affiliate Pro
            ['name' => 'PAP_USER', 'section' => 'Post Affiliate Pro', 'value' => 'supe@superhandles.com'],
            ['name' => 'PAP_PASS', 'section' => 'Post Affiliate Pro', 'value' => 'RogerHildebrandt11'],
        ];


        foreach ($settings as $setting) {
            $settingObj = new Setting();
            $settingObj->setName($setting['name']);
            $settingObj->setSection($setting['section']);
            $settingObj->setValue($setting['value']);
            $manager->persist($settingObj);
        }
        $manager->flush();

    }

}
