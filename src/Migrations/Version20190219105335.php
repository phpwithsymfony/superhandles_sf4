<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190219105335 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_team_university_package CHANGE stripe_plan_id stripe_plan_id VARCHAR(100) NOT NULL, CHANGE is_delete is_delete SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE tbl_testimonials ADD is_delete SMALLINT DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tbl_team_university_package CHANGE stripe_plan_id stripe_plan_id VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE is_delete is_delete SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE tbl_testimonials DROP is_delete');
    }
}
