<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190128142501 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tbl_home_slider (id INT UNSIGNED AUTO_INCREMENT NOT NULL, id_static INT UNSIGNED DEFAULT NULL, title VARCHAR(45) NOT NULL, banner_file VARCHAR(100) NOT NULL, page_url VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, sort_order TINYINT(1) NOT NULL, created_by INT NOT NULL, updated_by INT NOT NULL, date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, date_updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX FK_tbl_home_slider_tbl_static (id_static), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tbl_home_slider ADD CONSTRAINT FK_B8A29922A6F79DC8 FOREIGN KEY (id_static) REFERENCES tbl_static (id)');
        $this->addSql('ALTER TABLE tbl_camps CHANGE additional_info additional_info VARCHAR(200) DEFAULT NULL, CHANGE created_by created_by INT NOT NULL, CHANGE updated_by updated_by INT NOT NULL, CHANGE date_created date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE date_updated date_updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE tbl_camps_slider CHANGE created_by created_by INT NOT NULL, CHANGE updated_by updated_by INT NOT NULL, CHANGE date_created date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE date_updated date_updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tbl_home_slider');
        $this->addSql('ALTER TABLE tbl_camps CHANGE additional_info additional_info LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE created_by created_by INT DEFAULT NULL, CHANGE updated_by updated_by INT DEFAULT NULL, CHANGE date_created date_created DATETIME DEFAULT CURRENT_TIMESTAMP, CHANGE date_updated date_updated DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE tbl_camps_slider CHANGE created_by created_by INT DEFAULT NULL, CHANGE updated_by updated_by INT DEFAULT NULL, CHANGE date_created date_created DATETIME DEFAULT CURRENT_TIMESTAMP, CHANGE date_updated date_updated DATETIME DEFAULT CURRENT_TIMESTAMP');
    }
}
