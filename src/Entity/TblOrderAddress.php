<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblOrderAddress
 *
 * @ORM\Table(name="tbl_order_address", indexes={@ORM\Index(name="id_customer", columns={"id_customer"}),
 *     @ORM\Index(name="id_user", columns={"id_user"})})
 *
 * @ORM\Entity(repositoryClass="App\Repository\OrderAddressRepository")
 */
class TblOrderAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_firstname", type="string", length=60, nullable=false)
     */
    private $billingFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_lastname", type="string", length=60, nullable=false)
     */
    private $billingLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_email", type="string", length=120, nullable=false)
     */
    private $billingEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address", type="string", length=255, nullable=false)
     */
    private $billingAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_city", type="string", length=60, nullable=false)
     */
    private $billingCity;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_state", type="string", length=60, nullable=false)
     */
    private $billingState;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_country", type="string", length=60, nullable=false)
     */
    private $billingCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_zipcode", type="string", length=15, nullable=false)
     */
    private $billingZipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_firstname", type="string", length=60, nullable=false)
     */
    private $shippingFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_lastname", type="string", length=60, nullable=false)
     */
    private $shippingLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_email", type="string", length=120, nullable=false)
     */
    private $shippingEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address", type="string", length=255, nullable=false)
     */
    private $shippingAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_city", type="string", length=255, nullable=false)
     */
    private $shippingCity;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_state", type="string", length=60, nullable=false)
     */
    private $shippingState;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_country", type="string", length=60, nullable=false)
     */
    private $shippingCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_zipcode", type="string", length=15, nullable=false)
     */
    private $shippingZipcode;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblCustomer
     *
     * @ORM\ManyToOne(targetEntity="TblCustomer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     * })
     */
    private $idCustomer;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getBillingFirstname(): ?string
    {
        return $this->billingFirstname;
    }

    /**
     * @param string $billingFirstname
     *
     * @return TblOrderAddress
     */
    public function setBillingFirstname(string $billingFirstname): self
    {
        $this->billingFirstname = $billingFirstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingLastname(): ?string
    {
        return $this->billingLastname;
    }

    /**
     * @param string $billingLastname
     *
     * @return TblOrderAddress
     */
    public function setBillingLastname(string $billingLastname): self
    {
        $this->billingLastname = $billingLastname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingEmail(): ?string
    {
        return $this->billingEmail;
    }

    /**
     * @param string $billingEmail
     *
     * @return TblOrderAddress
     */
    public function setBillingEmail(string $billingEmail): self
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingAddress(): ?string
    {
        return $this->billingAddress;
    }

    /**
     * @param string $billingAddress
     *
     * @return TblOrderAddress
     */
    public function setBillingAddress(string $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingCity(): ?string
    {
        return $this->billingCity;
    }

    /**
     * @param string $billingCity
     *
     * @return TblOrderAddress
     */
    public function setBillingCity(string $billingCity): self
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingState(): ?string
    {
        return $this->billingState;
    }

    /**
     * @param string $billingState
     *
     * @return TblOrderAddress
     */
    public function setBillingState(string $billingState): self
    {
        $this->billingState = $billingState;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingCountry(): ?string
    {
        return $this->billingCountry;
    }

    /**
     * @param string $billingCountry
     *
     * @return TblOrderAddress
     */
    public function setBillingCountry(string $billingCountry): self
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBillingZipcode(): ?string
    {
        return $this->billingZipcode;
    }

    /**
     * @param string $billingZipcode
     *
     * @return TblOrderAddress
     */
    public function setBillingZipcode(string $billingZipcode): self
    {
        $this->billingZipcode = $billingZipcode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingFirstname(): ?string
    {
        return $this->shippingFirstname;
    }

    /**
     * @param string $shippingFirstname
     *
     * @return TblOrderAddress
     */
    public function setShippingFirstname(string $shippingFirstname): self
    {
        $this->shippingFirstname = $shippingFirstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingLastname(): ?string
    {
        return $this->shippingLastname;
    }

    /**
     * @param string $shippingLastname
     *
     * @return TblOrderAddress
     */
    public function setShippingLastname(string $shippingLastname): self
    {
        $this->shippingLastname = $shippingLastname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingEmail(): ?string
    {
        return $this->shippingEmail;
    }

    /**
     * @param string $shippingEmail
     *
     * @return TblOrderAddress
     */
    public function setShippingEmail(string $shippingEmail): self
    {
        $this->shippingEmail = $shippingEmail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingAddress(): ?string
    {
        return $this->shippingAddress;
    }

    /**
     * @param string $shippingAddress
     *
     * @return TblOrderAddress
     */
    public function setShippingAddress(string $shippingAddress): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingCity(): ?string
    {
        return $this->shippingCity;
    }

    /**
     * @param string $shippingCity
     *
     * @return TblOrderAddress
     */
    public function setShippingCity(string $shippingCity): self
    {
        $this->shippingCity = $shippingCity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingState(): ?string
    {
        return $this->shippingState;
    }

    /**
     * @param string $shippingState
     *
     * @return TblOrderAddress
     */
    public function setShippingState(string $shippingState): self
    {
        $this->shippingState = $shippingState;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingCountry(): ?string
    {
        return $this->shippingCountry;
    }

    /**
     * @param string $shippingCountry
     *
     * @return TblOrderAddress
     */
    public function setShippingCountry(string $shippingCountry): self
    {
        $this->shippingCountry = $shippingCountry;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingZipcode(): ?string
    {
        return $this->shippingZipcode;
    }

    /**
     * @param string $shippingZipcode
     *
     * @return TblOrderAddress
     */
    public function setShippingZipcode(string $shippingZipcode): self
    {
        $this->shippingZipcode = $shippingZipcode;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblOrderAddress
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblOrderAddress
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblCustomer|null
     */
    public function getIdCustomer(): ?TblCustomer
    {
        return $this->idCustomer;
    }

    /**
     * @param TblCustomer|null $idCustomer
     *
     * @return TblOrderAddress
     */
    public function setIdCustomer(?TblCustomer $idCustomer): self
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param User|null $idUser
     *
     * @return TblOrderAddress
     */
    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }
}
