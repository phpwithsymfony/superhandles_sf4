<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TblProductType
 *
 * @ORM\Table(name="tbl_product_type",
 *     indexes={@ORM\Index(name="FK_tbl_product_clothing_type_tbl_users_2", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_product_clothing_type_tbl_users", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_product_type_tbl_product_type", columns={"id_parent"})})
 * @ORM\Entity
 */
class TblProductType
{
    const ACTIVE           = 1;
    const INACTIVE         = 0;
    const DELETE           = 1;
    const NOTDELETE        = 0;
    const ISCLOTH          = 1;
    const NOTCLOTH         = 0;
    const APP_PRODUCT_TYPE = 5;
    const ID_PARENT        = 6;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Plase enter Type"
     * )
     * @ORM\Column(name="product_type", type="string", length=200, nullable=false)
     */
    private $productType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="app_product_type", type="smallint", nullable=true)
     */
    private $appProductType;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_cloth", type="boolean", nullable=false,
     *     options={"default"="1"}, options={"comment"="0:any, 1:cloth"})
     */
    private $isCloth = self::ISCLOTH;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = self::ACTIVE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = self::NOTDELETE;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var TblProductType
     *
     * @ORM\ManyToOne(targetEntity="TblProductType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_parent", referencedColumnName="id")
     * })
     */
    private $idParent;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getProductType(): ?string
    {
        return $this->productType;
    }

    /**
     * @param string $productType
     *
     * @return TblProductType
     */
    public function setProductType(string $productType): self
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAppProductType(): ?int
    {
        return $this->appProductType;
    }

    /**
     * @param int|null $appProductType
     *
     * @return TblProductType
     */
    public function setAppProductType(?int $appProductType): self
    {
        $this->appProductType = $appProductType;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsCloth(): ?bool
    {
        return $this->isCloth;
    }

    /**
     * @param bool $isCloth
     *
     * @return TblProductType
     */
    public function setIsCloth(bool $isCloth): self
    {
        $this->isCloth = $isCloth;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblProductType
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     *
     * @return TblProductType
     */
    public function setIsDelete(bool $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return TblProductType
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface $updatedDate
     *
     * @return TblProductType
     */
    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblProductType
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblProductType
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return TblProductType|null
     */
    public function getIdParent(): ?self
    {
        return $this->idParent;
    }

    /**
     * @param self|null $idParent
     *
     * @return $this
     */
    public function setIdParent(?TblProductType $idParent): self
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getProductType();
    }
}
