<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblStreamingPdfDoc
 *
 * @ORM\Table(name="tbl_streaming_pdf_doc",
 *     indexes={@ORM\Index(name="idx_streaming_id", columns={"id_streaming_page_content"})})
 * @ORM\Entity
 */
class TblStreamingPdfDoc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     *
     * @Assert\File(
     *     mimeTypes = {"application/msword", "application/pdf"},
     *     mimeTypesMessage = "Please upload a valid file (doc or pdf)",
     *     maxSize="2M"
     * )
     */
    private $url;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="type", type="boolean", nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var TblStreamingPageContent
     *
     * @ORM\ManyToOne(targetEntity="TblStreamingPageContent", inversedBy="streamingPageContentFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_streaming_page_content", referencedColumnName="id")
     * })
     */
    private $idStreamingPageContent;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool|null
     */
    public function getType(): ?bool
    {
        return $this->type;
    }

    /**
     * @param bool|null $type
     */
    public function setType(?bool $type): void
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblStreamingPageContent
     */
    public function getIdStreamingPageContent(): ?TblStreamingPageContent
    {
        return $this->idStreamingPageContent;
    }

    /**
     * @param TblStreamingPageContent $idStreamingPageContent
     */
    public function setIdStreamingPageContent(?TblStreamingPageContent $idStreamingPageContent): void
    {
        $this->idStreamingPageContent = $idStreamingPageContent;
    }

    /**
     * @param IdStreamingPageContent|null $idStreamingPageContent
     */
    public function addIdStreamingPageContent(?IdStreamingPageContent $idStreamingPageContent)
    {
        if (!$this->idStreamingPageContent->contains($idStreamingPageContent)) {
            $this->idStreamingPageContent->add($idStreamingPageContent);
        }
    }

}
