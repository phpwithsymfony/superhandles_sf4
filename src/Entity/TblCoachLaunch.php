<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCoachLaunch
 *
 * @ORM\Table(name="tbl_coach_launch")
 * @ORM\Entity
 */
class TblCoachLaunch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="headline_title", type="text", length=65535, nullable=false)
     */
    private $headlineTitle;

    /**
     * @var bool
     *
     * @ORM\Column(name="headline_enable", type="boolean", nullable=false)
     */
    private $headlineEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="product_title", type="string", length=200, nullable=false)
     */
    private $productTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="product_description_title", type="string", length=200, nullable=false)
     */
    private $productDescriptionTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="product_description", type="text", length=65535, nullable=false)
     */
    private $productDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="product_description_enable", type="boolean", nullable=false)
     */
    private $productDescriptionEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="product_image", type="string", length=200, nullable=false)
     */
    private $productImage;

    /**
     * @var string
     *
     * @ORM\Column(name="banner_type", type="string", length=0, nullable=false)
     */
    private $bannerType;

    /**
     * @var string
     *
     * @ORM\Column(name="video_id", type="string", length=50, nullable=false)
     */
    private $videoId;

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="string", length=50, nullable=false)
     */
    private $imageUrl;

    /**
     * @var bool
     *
     * @ORM\Column(name="media_enable", type="boolean", nullable=false)
     */
    private $mediaEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="join_now_button_text1", type="string", length=50, nullable=false)
     */
    private $joinNowButtonText1;

    /**
     * @var bool
     *
     * @ORM\Column(name="first_join_now_enable", type="boolean", nullable=false)
     */
    private $firstJoinNowEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="join_now_button_text2", type="string", length=50, nullable=false)
     */
    private $joinNowButtonText2;

    /**
     * @var bool
     *
     * @ORM\Column(name="second_join_now_enable", type="boolean", nullable=false)
     */
    private $secondJoinNowEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="first_box_title", type="string", length=100, nullable=false)
     */
    private $firstBoxTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="first_box_description", type="text", length=65535, nullable=false)
     */
    private $firstBoxDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="first_box_enable", type="boolean", nullable=false)
     */
    private $firstBoxEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="second_box_title", type="string", length=50, nullable=false)
     */
    private $secondBoxTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="second_box_description", type="text", length=65535, nullable=false)
     */
    private $secondBoxDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="second_box_enable", type="boolean", nullable=false)
     */
    private $secondBoxEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_title", type="string", length=50, nullable=false)
     */
    private $featureboxTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_image1", type="string", length=100, nullable=false,
     *     options={"default"="a2102792f1545c828f1c31b7010ef7b604457216.png"})
     */
    private $featureboxImage1 = 'a2102792f1545c828f1c31b7010ef7b604457216.png';

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_image2", type="string", length=100, nullable=false,
     *     options={"default"="f1eab10fa8be0716560b13aa230225cceac1f929.png"})
     */
    private $featureboxImage2 = 'f1eab10fa8be0716560b13aa230225cceac1f929.png';

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_image3", type="string", length=100, nullable=false,
     *     options={"default"="f419d7b8e3a4e63a98b141112b6e0b44bcf7e18d.png"})
     */
    private $featureboxImage3 = 'f419d7b8e3a4e63a98b141112b6e0b44bcf7e18d.png';

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_image4", type="string", length=100, nullable=false,
     *     options={"default"="945dd935074f5652f829239fc50ae99b4cbe9a6b.png"})
     */
    private $featureboxImage4 = '945dd935074f5652f829239fc50ae99b4cbe9a6b.png';

    /**
     * @var string
     *
     * @ORM\Column(name="featurebox_image5", type="string", length=100, nullable=false,
     *     options={"default"="f3420669bd208f07d4da59c8224ee598be818f78.png"})
     */
    private $featureboxImage5 = 'f3420669bd208f07d4da59c8224ee598be818f78.png';

    /**
     * @var bool
     *
     * @ORM\Column(name="featurebox_image_enable", type="boolean", nullable=false)
     */
    private $featureboxImageEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_title1", type="string", length=100, nullable=false)
     */
    private $testimonialTitle1;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_image1", type="string", length=100, nullable=false)
     */
    private $testimonialImage1;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_description1", type="text", length=65535, nullable=false)
     */
    private $testimonialDescription1;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_title2", type="string", length=100, nullable=false)
     */
    private $testimonialTitle2;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_image2", type="string", length=100, nullable=false)
     */
    private $testimonialImage2;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_description2", type="text", length=65535, nullable=false)
     */
    private $testimonialDescription2;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_title3", type="string", length=100, nullable=false)
     */
    private $testimonialTitle3;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_image3", type="string", length=100, nullable=false)
     */
    private $testimonialImage3;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial_description3", type="text", length=65535, nullable=false)
     */
    private $testimonialDescription3;

    /**
     * @var bool
     *
     * @ORM\Column(name="testimonial_enable", type="boolean", nullable=false)
     */
    private $testimonialEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="after_testimonial_title", type="string", length=200, nullable=false)
     */
    private $afterTestimonialTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="after_testimonial_description", type="text", length=65535, nullable=false)
     */
    private $afterTestimonialDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="after_testimonial_enable", type="boolean", nullable=false)
     */
    private $afterTestimonialEnable;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="footer_text", type="string", length=255, nullable=true)
     */
    private $footerText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="footer_link", type="string", length=255, nullable=true)
     */
    private $footerLink;

    /**
     * @var string|null
     *
     * @ORM\Column(name="redirect_link", type="string", length=255, nullable=true)
     */
    private $redirectLink;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHeadlineTitle(): string
    {
        return $this->headlineTitle;
    }

    /**
     * @param string $headlineTitle
     */
    public function setHeadlineTitle(string $headlineTitle): void
    {
        $this->headlineTitle = $headlineTitle;
    }

    /**
     * @return bool
     */
    public function isHeadlineEnable(): bool
    {
        return $this->headlineEnable;
    }

    /**
     * @param bool $headlineEnable
     */
    public function setHeadlineEnable(bool $headlineEnable): void
    {
        $this->headlineEnable = $headlineEnable;
    }

    /**
     * @return string
     */
    public function getProductTitle(): string
    {
        return $this->productTitle;
    }

    /**
     * @param string $productTitle
     */
    public function setProductTitle(string $productTitle): void
    {
        $this->productTitle = $productTitle;
    }

    /**
     * @return string
     */
    public function getProductDescriptionTitle(): string
    {
        return $this->productDescriptionTitle;
    }

    /**
     * @param string $productDescriptionTitle
     */
    public function setProductDescriptionTitle(string $productDescriptionTitle): void
    {
        $this->productDescriptionTitle = $productDescriptionTitle;
    }

    /**
     * @return string
     */
    public function getProductDescription(): string
    {
        return $this->productDescription;
    }

    /**
     * @param string $productDescription
     */
    public function setProductDescription(string $productDescription): void
    {
        $this->productDescription = $productDescription;
    }

    /**
     * @return bool
     */
    public function isProductDescriptionEnable(): bool
    {
        return $this->productDescriptionEnable;
    }

    /**
     * @param bool $productDescriptionEnable
     */
    public function setProductDescriptionEnable(bool $productDescriptionEnable): void
    {
        $this->productDescriptionEnable = $productDescriptionEnable;
    }

    /**
     * @return string
     */
    public function getProductImage(): string
    {
        return $this->productImage;
    }

    /**
     * @param string $productImage
     */
    public function setProductImage(string $productImage): void
    {
        $this->productImage = $productImage;
    }

    /**
     * @return string
     */
    public function getBannerType(): string
    {
        return $this->bannerType;
    }

    /**
     * @param string $bannerType
     */
    public function setBannerType(string $bannerType): void
    {
        $this->bannerType = $bannerType;
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId(string $videoId): void
    {
        $this->videoId = $videoId;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return bool
     */
    public function isMediaEnable(): bool
    {
        return $this->mediaEnable;
    }

    /**
     * @param bool $mediaEnable
     */
    public function setMediaEnable(bool $mediaEnable): void
    {
        $this->mediaEnable = $mediaEnable;
    }

    /**
     * @return string
     */
    public function getJoinNowButtonText1(): string
    {
        return $this->joinNowButtonText1;
    }

    /**
     * @param string $joinNowButtonText1
     */
    public function setJoinNowButtonText1(string $joinNowButtonText1): void
    {
        $this->joinNowButtonText1 = $joinNowButtonText1;
    }

    /**
     * @return bool
     */
    public function isFirstJoinNowEnable(): bool
    {
        return $this->firstJoinNowEnable;
    }

    /**
     * @param bool $firstJoinNowEnable
     */
    public function setFirstJoinNowEnable(bool $firstJoinNowEnable): void
    {
        $this->firstJoinNowEnable = $firstJoinNowEnable;
    }

    /**
     * @return string
     */
    public function getJoinNowButtonText2(): string
    {
        return $this->joinNowButtonText2;
    }

    /**
     * @param string $joinNowButtonText2
     */
    public function setJoinNowButtonText2(string $joinNowButtonText2): void
    {
        $this->joinNowButtonText2 = $joinNowButtonText2;
    }

    /**
     * @return bool
     */
    public function isSecondJoinNowEnable(): bool
    {
        return $this->secondJoinNowEnable;
    }

    /**
     * @param bool $secondJoinNowEnable
     */
    public function setSecondJoinNowEnable(bool $secondJoinNowEnable): void
    {
        $this->secondJoinNowEnable = $secondJoinNowEnable;
    }

    /**
     * @return string
     */
    public function getFirstBoxTitle(): string
    {
        return $this->firstBoxTitle;
    }

    /**
     * @param string $firstBoxTitle
     */
    public function setFirstBoxTitle(string $firstBoxTitle): void
    {
        $this->firstBoxTitle = $firstBoxTitle;
    }

    /**
     * @return string
     */
    public function getFirstBoxDescription(): string
    {
        return $this->firstBoxDescription;
    }

    /**
     * @param string $firstBoxDescription
     */
    public function setFirstBoxDescription(string $firstBoxDescription): void
    {
        $this->firstBoxDescription = $firstBoxDescription;
    }

    /**
     * @return bool
     */
    public function isFirstBoxEnable(): bool
    {
        return $this->firstBoxEnable;
    }

    /**
     * @param bool $firstBoxEnable
     */
    public function setFirstBoxEnable(bool $firstBoxEnable): void
    {
        $this->firstBoxEnable = $firstBoxEnable;
    }

    /**
     * @return string
     */
    public function getSecondBoxTitle(): string
    {
        return $this->secondBoxTitle;
    }

    /**
     * @param string $secondBoxTitle
     */
    public function setSecondBoxTitle(string $secondBoxTitle): void
    {
        $this->secondBoxTitle = $secondBoxTitle;
    }

    /**
     * @return string
     */
    public function getSecondBoxDescription(): string
    {
        return $this->secondBoxDescription;
    }

    /**
     * @param string $secondBoxDescription
     */
    public function setSecondBoxDescription(string $secondBoxDescription): void
    {
        $this->secondBoxDescription = $secondBoxDescription;
    }

    /**
     * @return bool
     */
    public function isSecondBoxEnable(): bool
    {
        return $this->secondBoxEnable;
    }

    /**
     * @param bool $secondBoxEnable
     */
    public function setSecondBoxEnable(bool $secondBoxEnable): void
    {
        $this->secondBoxEnable = $secondBoxEnable;
    }

    /**
     * @return string
     */
    public function getFeatureboxTitle(): string
    {
        return $this->featureboxTitle;
    }

    /**
     * @param string $featureboxTitle
     */
    public function setFeatureboxTitle(string $featureboxTitle): void
    {
        $this->featureboxTitle = $featureboxTitle;
    }

    /**
     * @return string
     */
    public function getFeatureboxImage1(): string
    {
        return $this->featureboxImage1;
    }

    /**
     * @param string $featureboxImage1
     */
    public function setFeatureboxImage1(string $featureboxImage1): void
    {
        $this->featureboxImage1 = $featureboxImage1;
    }

    /**
     * @return string
     */
    public function getFeatureboxImage2(): string
    {
        return $this->featureboxImage2;
    }

    /**
     * @param string $featureboxImage2
     */
    public function setFeatureboxImage2(string $featureboxImage2): void
    {
        $this->featureboxImage2 = $featureboxImage2;
    }

    /**
     * @return string
     */
    public function getFeatureboxImage3(): string
    {
        return $this->featureboxImage3;
    }

    /**
     * @param string $featureboxImage3
     */
    public function setFeatureboxImage3(string $featureboxImage3): void
    {
        $this->featureboxImage3 = $featureboxImage3;
    }

    /**
     * @return string
     */
    public function getFeatureboxImage4(): string
    {
        return $this->featureboxImage4;
    }

    /**
     * @param string $featureboxImage4
     */
    public function setFeatureboxImage4(string $featureboxImage4): void
    {
        $this->featureboxImage4 = $featureboxImage4;
    }

    /**
     * @return string
     */
    public function getFeatureboxImage5(): string
    {
        return $this->featureboxImage5;
    }

    /**
     * @param string $featureboxImage5
     */
    public function setFeatureboxImage5(string $featureboxImage5): void
    {
        $this->featureboxImage5 = $featureboxImage5;
    }

    /**
     * @return bool
     */
    public function isFeatureboxImageEnable(): bool
    {
        return $this->featureboxImageEnable;
    }

    /**
     * @param bool $featureboxImageEnable
     */
    public function setFeatureboxImageEnable(bool $featureboxImageEnable): void
    {
        $this->featureboxImageEnable = $featureboxImageEnable;
    }

    /**
     * @return string
     */
    public function getTestimonialTitle1(): string
    {
        return $this->testimonialTitle1;
    }

    /**
     * @param string $testimonialTitle1
     */
    public function setTestimonialTitle1(string $testimonialTitle1): void
    {
        $this->testimonialTitle1 = $testimonialTitle1;
    }

    /**
     * @return string
     */
    public function getTestimonialImage1(): string
    {
        return $this->testimonialImage1;
    }

    /**
     * @param string $testimonialImage1
     */
    public function setTestimonialImage1(string $testimonialImage1): void
    {
        $this->testimonialImage1 = $testimonialImage1;
    }

    /**
     * @return string
     */
    public function getTestimonialDescription1(): string
    {
        return $this->testimonialDescription1;
    }

    /**
     * @param string $testimonialDescription1
     */
    public function setTestimonialDescription1(string $testimonialDescription1): void
    {
        $this->testimonialDescription1 = $testimonialDescription1;
    }

    /**
     * @return string
     */
    public function getTestimonialTitle2(): string
    {
        return $this->testimonialTitle2;
    }

    /**
     * @param string $testimonialTitle2
     */
    public function setTestimonialTitle2(string $testimonialTitle2): void
    {
        $this->testimonialTitle2 = $testimonialTitle2;
    }

    /**
     * @return string
     */
    public function getTestimonialImage2(): string
    {
        return $this->testimonialImage2;
    }

    /**
     * @param string $testimonialImage2
     */
    public function setTestimonialImage2(string $testimonialImage2): void
    {
        $this->testimonialImage2 = $testimonialImage2;
    }

    /**
     * @return string
     */
    public function getTestimonialDescription2(): string
    {
        return $this->testimonialDescription2;
    }

    /**
     * @param string $testimonialDescription2
     */
    public function setTestimonialDescription2(string $testimonialDescription2): void
    {
        $this->testimonialDescription2 = $testimonialDescription2;
    }

    /**
     * @return string
     */
    public function getTestimonialTitle3(): string
    {
        return $this->testimonialTitle3;
    }

    /**
     * @param string $testimonialTitle3
     */
    public function setTestimonialTitle3(string $testimonialTitle3): void
    {
        $this->testimonialTitle3 = $testimonialTitle3;
    }

    /**
     * @return string
     */
    public function getTestimonialImage3(): string
    {
        return $this->testimonialImage3;
    }

    /**
     * @param string $testimonialImage3
     */
    public function setTestimonialImage3(string $testimonialImage3): void
    {
        $this->testimonialImage3 = $testimonialImage3;
    }

    /**
     * @return string
     */
    public function getTestimonialDescription3(): string
    {
        return $this->testimonialDescription3;
    }

    /**
     * @param string $testimonialDescription3
     */
    public function setTestimonialDescription3(string $testimonialDescription3): void
    {
        $this->testimonialDescription3 = $testimonialDescription3;
    }

    /**
     * @return bool
     */
    public function isTestimonialEnable(): bool
    {
        return $this->testimonialEnable;
    }

    /**
     * @param bool $testimonialEnable
     */
    public function setTestimonialEnable(bool $testimonialEnable): void
    {
        $this->testimonialEnable = $testimonialEnable;
    }

    /**
     * @return string
     */
    public function getAfterTestimonialTitle(): string
    {
        return $this->afterTestimonialTitle;
    }

    /**
     * @param string $afterTestimonialTitle
     */
    public function setAfterTestimonialTitle(string $afterTestimonialTitle): void
    {
        $this->afterTestimonialTitle = $afterTestimonialTitle;
    }

    /**
     * @return string
     */
    public function getAfterTestimonialDescription(): string
    {
        return $this->afterTestimonialDescription;
    }

    /**
     * @param string $afterTestimonialDescription
     */
    public function setAfterTestimonialDescription(string $afterTestimonialDescription): void
    {
        $this->afterTestimonialDescription = $afterTestimonialDescription;
    }

    /**
     * @return bool
     */
    public function isAfterTestimonialEnable(): bool
    {
        return $this->afterTestimonialEnable;
    }

    /**
     * @param bool $afterTestimonialEnable
     */
    public function setAfterTestimonialEnable(bool $afterTestimonialEnable): void
    {
        $this->afterTestimonialEnable = $afterTestimonialEnable;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getFooterText(): ?string
    {
        return $this->footerText;
    }

    /**
     * @param string|null $footerText
     */
    public function setFooterText(?string $footerText): void
    {
        $this->footerText = $footerText;
    }

    /**
     * @return string|null
     */
    public function getFooterLink(): ?string
    {
        return $this->footerLink;
    }

    /**
     * @param string|null $footerLink
     */
    public function setFooterLink(?string $footerLink): void
    {
        $this->footerLink = $footerLink;
    }

    /**
     * @return string|null
     */
    public function getRedirectLink(): ?string
    {
        return $this->redirectLink;
    }

    /**
     * @param string|null $redirectLink
     */
    public function setRedirectLink(?string $redirectLink): void
    {
        $this->redirectLink = $redirectLink;
    }
}
