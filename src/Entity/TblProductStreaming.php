<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblProductStreaming
 *
 * @ORM\Table(name="tbl_product_streaming",
 *     indexes={@ORM\Index(name="FK_tbl_product_streaming_tbl_streaming_pages", columns={"id_streaming"}),
 *     @ORM\Index(name="FK_tbl_product_streaming_tbl_products", columns={"id_product"})})
 * @ORM\Entity
 */
class TblProductStreaming
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var TblStreamingPages
     *
     * @ORM\ManyToOne(targetEntity="TblStreamingPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_streaming", referencedColumnName="id")
     * })
     */
    private $idStreaming;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblProductStreaming
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblProductStreaming
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblProducts|null
     */
    public function getIdProduct(): ?TblProducts
    {
        return $this->idProduct;
    }

    /**
     * @param TblProducts|null $idProduct
     *
     * @return TblProductStreaming
     */
    public function setIdProduct(?TblProducts $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * @return TblStreamingPages|null
     */
    public function getIdStreaming(): ?TblStreamingPages
    {
        return $this->idStreaming;
    }

    /**
     * @param TblStreamingPages|null $idStreaming
     *
     * @return TblProductStreaming
     */
    public function setIdStreaming(?TblStreamingPages $idStreaming): self
    {
        $this->idStreaming = $idStreaming;

        return $this;
    }
}
