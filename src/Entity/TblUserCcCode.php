<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblUserCcCode
 *
 * @ORM\Table(name="tbl_user_cc_code", indexes={@ORM\Index(name="id_user", columns={"id_user"}),
 *     @ORM\Index(name="fk_tbl_user_cc_code_1_idx", columns={"id_streaming_page"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserCcCode")
 */
class TblUserCcCode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblStreamingPages
     *
     * @ORM\ManyToOne(targetEntity="TblStreamingPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_streaming_page", referencedColumnName="id")
     * })
     */
    private $idStreamingPage;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblUserCcCode
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblUserCcCode
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblStreamingPages|null
     */
    public function getIdStreamingPage(): ?TblStreamingPages
    {
        return $this->idStreamingPage;
    }

    /**
     * @param TblStreamingPages|null $idStreamingPage
     *
     * @return TblUserCcCode
     */
    public function setIdStreamingPage(?TblStreamingPages $idStreamingPage): self
    {
        $this->idStreamingPage = $idStreamingPage;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param User|null $idUser
     *
     * @return TblUserCcCode
     */
    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }
}
