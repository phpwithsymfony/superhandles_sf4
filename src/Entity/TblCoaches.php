<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblCoaches
 *
 * @ORM\Table(name="tbl_coaches",
 *     indexes={@ORM\Index(name="FK_tbl_coaches_tbl_users", columns={"id_user"}),
 *     @ORM\Index(name="FK_tbl_coaches_tbl_users_3", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_coaches_tbl_subscription_coaches_plan", columns={"id_subscription_coach_plan"}),
 *     @ORM\Index(name="FK_tbl_coaches_tbl_cart_coupons", columns={"id_cart_coupon"}),
 *     @ORM\Index(name="FK_tbl_coaches_tbl_subscription_plan", columns={"id_subscription_plan"}),
 *     @ORM\Index(name="FK_tbl_coaches_tbl_users_2", columns={"created_by"})
 * })
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CoachesRepository")
 */
class TblCoaches
{
    const ACTIVE          = 1;
    const INACTIVE        = 0;
    const DELETE          = 1;
    const NOTDELETE       = 0;
    const ISSUBSCRIPTION  = 1;
    const NOTSUBSCRIPTION = 0;
    const ISINVOICE       = 1;
    const NOTINVOICE      = 0;
    const ISPAID          = 1;
    const NOTPAID         = 0;

    const ACTIVE_LABEL   = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const SUBSCRIPTIONTYPE = [
        'Yes' => self::ISSUBSCRIPTION,
        'No'  => self::NOTSUBSCRIPTION
    ];

    const INVOICETYPE = [
        'Yes' => self::ISINVOICE,
        'No'  => self::NOTINVOICE
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * TblProducts constructor.
     */
    public function __construct()
    {
        $this->CoachUser = new ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="coaches", cascade={"persist"})
     * @ORM\JoinTable(name="tbl_coach_users")
     */
    private $CoachUser;

    /**
     * Add Coach User
     *
     * @param \App\Entity\User $user
     *
     * @return $this
     */
    public function addCoachUser(User $user)
    {
        $this->CoachUser[] = $user;

        return $this;
    }

    /**
     * Remove Coach User
     *
     * @param \App\Entity\User $user
     */
    public function removeCoachUser(User $user)
    {
        $this->CoachUser->removeElement($user);
    }

    /**
     * Get Coach User
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoachUser()
    {
        return $this->CoachUser;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="notification_days", type="integer", nullable=false)
     */
    private $notificationDays = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_amount", type="decimal", precision=10, scale=2,
     *     nullable=true, options={"default"="0.00"})
     */
    private $discountAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="final_amount", type="decimal", precision=10, scale=2,
     *     nullable=true, options={"default"="0.00"})
     */
    private $finalAmount;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_subscription", type="boolean", nullable=false)
     */
    private $isSubscription;

    /**
     * @var bool
     *
     * @ORM\Column(name="Is_invoice", type="boolean", nullable=false)
     */
    private $isInvoice;

    /**
     * @var string
     *
     * @ORM\Column(name="school_name", type="string", length=255, nullable=true)
     */
    private $schoolName;

    /**
     * @var string
     *
     * @ORM\Column(name="school_email", type="string", length=255, nullable=true)
     */
    private $schoolEmail;

    /**
     * @var int
     *
     * @ORM\Column(name="subscription_month", type="smallint", nullable=false, options={"default"="1"})
     */
    private $subscriptionMonth = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_paid", type="boolean", nullable=false)
     */
    private $isPaid = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true)
     */
    private $isDelete = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_temp_token", type="string", length=255, nullable=true)
     */
    private $stripeTempToken;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblSubscriptionCoachPlan
     *
     * @ORM\ManyToOne(targetEntity="TblSubscriptionCoachPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subscription_coach_plan", referencedColumnName="id")
     * })
     */
    private $idSubscriptionCoachPlan;

    /**
     * @var TblSubscriptionPlan
     *
     * @ORM\ManyToOne(targetEntity="TblSubscriptionPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subscription_plan", referencedColumnName="id")
     * })
     */
    private $idSubscriptionPlan;

    /**
     * @var TblCartCoupons
     *
     * @ORM\ManyToOne(targetEntity="TblCartCoupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart_coupon", referencedColumnName="id", nullable=true)
     * })
     */
    private $idCartCoupon;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     *
     * @Assert\Valid()
     */
    private $idUser;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNotificationDays(): int
    {
        return $this->notificationDays;
    }

    /**
     * @param int $notificationDays
     */
    public function setNotificationDays(int $notificationDays): void
    {
        $this->notificationDays = $notificationDays;
    }

    /**
     * @return bool
     */
    public function isSubscription(): ?bool
    {
        return $this->isSubscription;
    }

    /**
     * @param bool $isSubscription
     */
    public function setIsSubscription(bool $isSubscription): void
    {
        $this->isSubscription = $isSubscription;
    }

    /**
     * @return bool
     */
    public function isInvoice(): ?bool
    {
        return $this->isInvoice;
    }

    /**
     * @param bool $isInvoice
     */
    public function setIsInvoice(bool $isInvoice): void
    {
        $this->isInvoice = $isInvoice;
    }

    /**
     * @return int
     */
    public function getSubscriptionMonth(): int
    {
        return $this->subscriptionMonth;
    }

    /**
     * @param int $subscriptionMonth
     */
    public function setSubscriptionMonth(int $subscriptionMonth): void
    {
        $this->subscriptionMonth = $subscriptionMonth;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->isPaid;
    }

    /**
     * @param bool $isPaid
     */
    public function setIsPaid(bool $isPaid): void
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return bool|null
     */
    public function getisActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool|null
     */
    public function getisDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool|null $isDelete
     */
    public function setIsDelete(?bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate($updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblSubscriptionCoachPlan
     */
    public function getIdSubscriptionCoachPlan(): ?TblSubscriptionCoachPlan
    {
        return $this->idSubscriptionCoachPlan;
    }

    /**
     * @param TblSubscriptionCoachPlan $idSubscriptionCoachPlan
     */
    public function setIdSubscriptionCoachPlan(TblSubscriptionCoachPlan $idSubscriptionCoachPlan): void
    {
        $this->idSubscriptionCoachPlan = $idSubscriptionCoachPlan;
    }

    /**
     * @return TblSubscriptionPlan
     */
    public function getIdSubscriptionPlan(): ?TblSubscriptionPlan
    {
        return $this->idSubscriptionPlan;
    }

    /**
     * @param TblSubscriptionPlan $idSubscriptionPlan
     */
    public function setIdSubscriptionPlan(TblSubscriptionPlan $idSubscriptionPlan): void
    {
        $this->idSubscriptionPlan = $idSubscriptionPlan;
    }

    /**
     * @return User
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param User $idUser
     */
    public function setIdUser(?User $idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(?User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy(): User
    {
        return $this->updatedBy;
    }

    /**
     * @return string
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getSchoolName(): ?string
    {
        return $this->schoolName;
    }

    /**
     * @param string $schoolName
     */
    public function setSchoolName(string $schoolName): void
    {
        $this->schoolName = $schoolName;
    }

    /**
     * @return string
     */
    public function getSchoolEmail(): ?string
    {
        return $this->schoolEmail;
    }

    /**
     * @param string $schoolEmail
     */
    public function setSchoolEmail(string $schoolEmail): void
    {
        $this->schoolEmail = $schoolEmail;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return string
     */
    public function getDiscountAmount(): string
    {
        return $this->discountAmount;
    }

    /**
     * @param string $discountAmount
     */
    public function setDiscountAmount(string $discountAmount): void
    {
        $this->discountAmount = $discountAmount;
    }

    /**
     * @return string
     */
    public function getFinalAmount(): string
    {
        return $this->finalAmount;
    }

    /**
     * @param string $finalAmount
     */
    public function setFinalAmount(string $finalAmount): void
    {
        $this->finalAmount = $finalAmount;
    }

    /**
     * @return TblCartCoupons
     */
    public function getIdCartCoupon(): ?TblCartCoupons
    {
        return $this->idCartCoupon;
    }

    /**
     * @param TblCartCoupons $idCartCoupon
     */
    public function setIdCartCoupon(?TblCartCoupons $idCartCoupon): void
    {
        $this->idCartCoupon = $idCartCoupon;
    }

    /**
     * @return string|null
     */
    public function getStripeTempToken(): ?string
    {
        return $this->stripeTempToken;
    }

    /**
     * @param string|null $stripeTempToken
     */
    public function setStripeTempToken(?string $stripeTempToken): void
    {
        $this->stripeTempToken = $stripeTempToken;
    }
}
