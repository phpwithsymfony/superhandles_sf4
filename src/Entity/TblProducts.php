<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TblProducts
 *
 * @ORM\Table(name="tbl_products", indexes={@ORM\Index(name="IDX_E489BC77DE12AB56", columns={"created_by"}),
 *      @ORM\Index(name="IDX_E489BC7722F700FA", columns={"id_product_type"}),
 *      @ORM\Index(name="IDX_E489BC7716FE72E1", columns={"updated_by"}),
 *      @ORM\Index(name="IDX_E489BC77EE02655A", columns={"id_product_of_dvd"})
 * })
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 *
 */
class TblProducts
{

    const _ACTIVE    = 1;
    const _INACTIVE  = 0;
    const _DELETE    = 1;
    const _NOTDELETE = 0;
    const _VISIBLE   = 1;
    const _INVISIBLE = 0;

    const ACTIVE_LABEL   = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const PARENT = 6;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * TblProducts constructor.
     */
    public function __construct()
    {
        $this->productCategories = new ArrayCollection();
        $this->idProductSize     = [];
    }

    /**
     * @ORM\ManyToMany(targetEntity="TblProductCategory", inversedBy="products", cascade={"persist"})
     * @ORM\JoinTable(name="tbl_product_categories")
     */
    private $productCategories;

    /**
     * Add productCategories
     *
     * @param \App\Entity\TblProductCategory $productCategory
     *
     * @return $this
     */
    public function addCategory(TblProductCategory $productCategory)
    {
        $this->productCategories[] = $productCategory;

        return $this;
    }

    /**
     * Remove productCategories
     *
     * @param \App\Entity\TblProductCategory $productCategory
     */
    public function removeCategory(TblProductCategory $productCategory)
    {
        $this->productCategories->removeElement($productCategory);
    }

    /**
     * Get productCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductCategories()
    {
        return $this->productCategories;
    }

    /**
     * @ORM\OneToMany(targetEntity="TblStreamingPages", mappedBy="idProduct")
     */
    private $getStreamingPages;

    /**
     * @return mixed
     */
    public function getStreamingPages()
    {

        return $this->getStreamingPages;
    }

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_product_size", type="string", length=255, nullable=true)
     */
    private $idProductSize;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=100, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="decimal", precision=10, scale=0, nullable=true, options={"default"="0.00"})
     */
    private $weight;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     * @ORM\Column(name="product_url", type="string", length=100, nullable=false)
     */
    private $productUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255, nullable=true)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_price", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $discountPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="clothing_order", type="integer", nullable=true)
     */
    private $clothingOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="strip_id", type="string", length=50, nullable=true)
     */
    private $stripId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_last_activity", type="string", length=100, nullable=true)
     */
    private $stripeLastActivity;
    /**
     * @var int
     *
     * @ORM\Column(name="is_visible", type="smallint", nullable=true, options={"default"="1"})
     */
    private $isVisible = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false, options={"default"="0"})
     */
    private $isDelete = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var TblProductType
     *
     * @ORM\ManyToOne(targetEntity="TblProductType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_type", referencedColumnName="id")
     * })
     */
    private $idProductType;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_of_dvd", referencedColumnName="id")
     * })
     */
    private $idProductOfDvd;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TblProducts
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return TblProducts
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $price
     *
     * @return TblProducts
     */
    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     *
     * @return TblProducts
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return TblProducts
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductUrl(): ?string
    {
        return $this->productUrl;
    }

    /**
     * @param string $productUrl
     *
     * @return TblProducts
     */
    public function setProductUrl(string $productUrl): self
    {
        $this->productUrl = $productUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     *
     * @return TblProducts
     */
    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice;
    }

    /**
     * @param $discountPrice
     *
     * @return TblProducts
     */
    public function setDiscountPrice($discountPrice): self
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getClothingOrder(): ?int
    {
        return $this->clothingOrder;
    }

    /**
     * @param int $clothingOrder
     *
     * @return TblProducts
     */
    public function setClothingOrder(int $clothingOrder): self
    {
        $this->clothingOrder = $clothingOrder;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStripId(): ?string
    {
        return $this->stripId;
    }

    /**
     * @param string $stripId
     *
     * @return TblProducts
     */
    public function setStripId(string $stripId): self
    {
        $this->stripId = $stripId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsVisible(): ?int
    {
        return $this->isVisible;
    }

    /**
     * @param int $isVisible
     *
     * @return TblProducts
     */
    public function setIsVisible(int $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     *
     * @return TblProducts
     */
    public function setIsActive(int $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsDelete(): ?int
    {
        return $this->isDelete;
    }

    /**
     * @param int|null $isDelete
     *
     * @return TblProducts
     */
    public function setIsDelete(?int $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param $sortOrder
     *
     * @return TblProducts
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblProducts
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblProducts
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }


    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblProducts
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return TblProductType|null
     */
    public function getIdProductType(): ?TblProductType
    {
        return $this->idProductType;
    }

    /**
     * @param TblProductType|null $idProductType
     *
     * @return TblProducts
     */
    public function setIdProductType(?TblProductType $idProductType): self
    {
        $this->idProductType = $idProductType;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStripeLastActivity()
    {
        return $this->stripeLastActivity;
    }

    /**
     * @param string|null $stripeLastActivity
     */
    public function setStripeLastActivity($stripeLastActivity)
    {
        $this->stripeLastActivity = $stripeLastActivity;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @return TblProducts
     */
    public function getIdProductOfDvd(): ?TblProducts
    {
        return $this->idProductOfDvd;
    }

    /**
     * @param TblProducts $idProductOfDvd
     */
    public function setIdProductOfDvd(?TblProducts $idProductOfDvd): void
    {
        $this->idProductOfDvd = $idProductOfDvd;
    }

    /**
     * @return mixed
     */
    public function getGetStreamingPages()
    {
        return $this->getStreamingPages;
    }

    /**
     * @param mixed $getStreamingPages
     */
    public function setGetStreamingPages($getStreamingPages): void
    {
        $this->getStreamingPages = $getStreamingPages;
    }


    /**
     * @return array|null
     */
    public function getIdProductSize(): ?array
    {
        //dump($this->idProductSize); exit;
        return $this->idProductSize ? json_decode($this->idProductSize) : [];
    }

    /**
     * @param array|null $idProductSize
     *
     * @return TblProducts
     */
    public function setIdProductSize(?array $idProductSize): self
    {
        if ($idProductSize) {
            $sizes = [];
            foreach ($idProductSize as $size) {
                $sizes[] = $size->getId();
            }

            $this->idProductSize = json_encode($sizes);

            return $this;
        } else {
            $this->idProductSize = null;

            return $this;
        }
    }
}
