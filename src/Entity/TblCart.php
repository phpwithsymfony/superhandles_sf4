<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCart
 *
 * @ORM\Table(name="tbl_cart", indexes={@ORM\Index(name="IDX_2246507B91D29306", columns={"id_cart_coupon"}),
 *     @ORM\Index(name="FK_tbl_order_temp_tbl_order_address", columns={"id_order_address"}),
 *     @ORM\Index(name="FK_tbl_order_temp_tbl_payment_methods", columns={"id_payment_methods"})})
 * @ORM\Entity
 */
class TblCart
{
    const _ADDTOCART = 0;
    const _PLACEORDER = 1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="non_auth_id", type="string", length=100, nullable=true)
     */
    private $nonAuthId;
    /**
     * @var string|null
     *
     * @ORM\Column(name="host_name", type="string", length=100, nullable=true)
     */
    private $hostName;
    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="boolean", length=100, nullable=true, options={"default"="0"})
     */
    private $status = self::_ADDTOCART;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblCustomer
     *
     * @ORM\ManyToOne(targetEntity="TblCustomer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     * })
     */
    private $idCustomer;

    /**
     * @var TblCartCoupons
     *
     * @ORM\ManyToOne(targetEntity="TblCartCoupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart_coupon", referencedColumnName="id")
     * })
     */
    private $idCartCoupon;

    /**
     * @var TblOrderAddress
     *
     * @ORM\ManyToOne(targetEntity="TblOrderAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_order_address", referencedColumnName="id")
     * })
     */
    private $idOrderAddress;

    /**
     * @var TblPaymentMethods
     *
     * @ORM\ManyToOne(targetEntity="TblPaymentMethods")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_methods", referencedColumnName="id")
     * })
     */
    private $idPaymentMethods;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNonAuthId(): ?string
    {
        return $this->nonAuthId;
    }

    /**
     * @param string|null $nonAuthId
     *
     * @return TblCart
     */
    public function setNonAuthId(?string $nonAuthId): self
    {
        $this->nonAuthId = $nonAuthId;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblCart
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblCart
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblCustomer|null
     */
    public function getIdCustomer(): ?TblCustomer
    {
        return $this->idCustomer;
    }

    /**
     * @param TblCustomer|null $idCustomer
     *
     * @return TblCart
     */
    public function setIdCustomer(?TblCustomer $idCustomer): self
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * @return TblCartCoupons|null
     */
    public function getIdCartCoupon(): ?TblCartCoupons
    {
        return $this->idCartCoupon;
    }

    /**
     * @param TblCartCoupons|null $idCartCoupon
     *
     * @return TblCart
     */
    public function setIdCartCoupon(?TblCartCoupons $idCartCoupon): self
    {
        $this->idCartCoupon = $idCartCoupon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }


    /**
     * @param $status
     *
     * @return TblCart
     */
    public function setStatus($status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHostName(): ?string
    {
        return $this->hostName;
    }


    /**
     * @param string|null $hostName
     *
     * @return TblCart
     */
    public function setHostName(?string $hostName): self
    {
        $this->hostName = $hostName;

        return $this;
    }
    /**
     * @return TblOrderAddress|null
     */
    public function getIdOrderAddress(): ?TblOrderAddress
    {
        return $this->idOrderAddress;
    }


    /**
     * @param TblOrderAddress|null $idOrderAddress
     *
     * @return TblCart
     */
    public function setIdOrderAddress(?TblOrderAddress $idOrderAddress): self
    {
        $this->idOrderAddress = $idOrderAddress;

        return $this;
    }

    /**
     * @return TblPaymentMethods
     */
    public function getIdPaymentMethods(): ?TblPaymentMethods
    {
        return $this->idPaymentMethods;
    }

    /**
     * @param TblPaymentMethods $idPaymentMethods
     */
    public function setIdPaymentMethods(?TblPaymentMethods $idPaymentMethods): void
    {
        $this->idPaymentMethods = $idPaymentMethods;
    }
}
