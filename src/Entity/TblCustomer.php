<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCustomer
 *
 * @ORM\Table(name="tbl_customer",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_81398E09A76ED395", columns={"id_user"})})
 * @ORM\Entity
 */
class TblCustomer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"="0"})
     */
    private $active = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_stripe_id", type="string", length=128, nullable=true)
     */
    private $customerStripeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return TblCustomer
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return TblCustomer
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface $updatedDate
     *
     * @return TblCustomer
     */
    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param User|null $idUser
     *
     * @return TblCustomer
     */
    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerStripeId(): ?string
    {
        return $this->customerStripeId;
    }

    /**
     * @param string|null $customerStripeId
     */
    public function setCustomerStripeId(?string $customerStripeId): void
    {
        $this->customerStripeId = $customerStripeId;
    }
}
