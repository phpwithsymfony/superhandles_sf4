<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblStreamingPages
 *
 * @ORM\Table(name="tbl_streaming_pages",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="UNIQ_5239915A949958DC", columns={"product_cc_code"})
 *      },
 *      indexes={
 *          @ORM\Index(name="IDX_5239915A22F700FA", columns={"id_product_type"}),
 *          @ORM\Index(name="IDX_5239915ADE12AB56", columns={"created_by"}),
 *          @ORM\Index(name="IDX_5239915ADD7ADDD", columns={"id_product"}),
 *          @ORM\Index(name="IDX_5239915A16FE72E1", columns={"updated_by"})
 *      }
 * )
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"productCcCode"},
 *     message="This CC Code already use."
 * )
 * @UniqueEntity(
 *     fields={"pageUrl"},
 *     message="This URL already use."
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\StreamingPagesRepository")
 */
class TblStreamingPages
{
    const _ACTIVE = 1;
    const _INACTIVE = 0;
    const _DELETE = 1;
    const _NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="page_title", type="string", length=45, nullable=false)
     */
    private $pageTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="page_content", type="text", length=0, nullable=false)
     */
    private $pageContent;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="page_url", type="string", length=255, nullable=false, unique=true)
     */
    private $pageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="product_cc_code", type="string", length=255, nullable=false, unique=true)
     *
     */
    private $productCcCode;

    /**
     * @var int
     *
     * @ORM\Column(name="cc_code_month", type="smallint", nullable=true)
     */
    private $ccCodeMonth;

    /**
     * @var int
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false, options={"default"="0"})
     */
    private $isDelete = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var TblProductType
     *
     * @ORM\ManyToOne(targetEntity="TblProductType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_type", referencedColumnName="id")
     * })
     */
    private $idProductType;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts", inversedBy="getStreamingPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     *
     * @return TblStreamingPages
     */
    public function setPageTitle(string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPageContent(): ?string
    {
        return $this->pageContent;
    }

    /**
     * @param string $pageContent
     *
     * @return TblStreamingPages
     */
    public function setPageContent(string $pageContent): self
    {
        $this->pageContent = $pageContent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaTitle
     *
     * @return TblStreamingPages
     */
    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     *
     * @return TblStreamingPages
     */
    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     *
     * @return TblStreamingPages
     */
    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     *
     * @return TblStreamingPages
     */
    public function setPageUrl(string $pageUrl): self
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductCcCode(): ?string
    {
        return $this->productCcCode;
    }

    /**
     * @param string $productCcCode
     *
     * @return TblStreamingPages
     */
    public function setProductCcCode(string $productCcCode): self
    {
        $this->productCcCode = $productCcCode;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCcCodeMonth(): ?int
    {
        return $this->ccCodeMonth;
    }

    /**
     * @param int $ccCodeMonth
     *
     * @return TblStreamingPages
     */
    public function setCcCodeMonth(int $ccCodeMonth): self
    {
        $this->ccCodeMonth = $ccCodeMonth;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     *
     * @return TblStreamingPages
     */
    public function setIsActive(int $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsDelete(): ?int
    {
        return $this->isDelete;
    }

    /**
     * @param int $isDelete
     *
     * @return TblStreamingPages
     */
    public function setIsDelete(int $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param  $sortOrder
     *
     * @return TblStreamingPages
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblStreamingPages
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblStreamingPages
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblStreamingPages
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return TblProductType|null
     */
    public function getIdProductType(): ?TblProductType
    {
        return $this->idProductType;
    }

    /**
     * @param TblProductType|null $idProductType
     *
     * @return TblStreamingPages
     */
    public function setIdProductType(?TblProductType $idProductType): self
    {
        $this->idProductType = $idProductType;

        return $this;
    }

    /**
     * @return TblProducts|null
     */
    public function getIdProduct(): ?TblProducts
    {
        return $this->idProduct;
    }

    /**
     * @param TblProducts|null $idProduct
     *
     * @return TblStreamingPages
     */
    public function setIdProduct(?TblProducts $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblStreamingPages
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string|null
     */

    public function __toString()
    {
        return $this->getPageTitle()." ".$this->getProductCcCode();
    }
}
