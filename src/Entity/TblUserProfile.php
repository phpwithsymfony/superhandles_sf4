<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="tbl_user_profile", indexes={
 *     @ORM\Index(name="firstName", columns={"firstname"}),
 *     @ORM\Index(name="lastName", columns={"lastname"}),
 *     @ORM\Index(name="fk_tbl_static_1_idx", columns={"created_by"}),
 *     @ORM\Index(name="fk_tbl_static_2_idx", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_user_profile_tbl_coaches", columns={"is_coach"})
 * })
 */
class TblUserProfile
{
    const ISCOACH = 1;
    const NOTCOACH = 0;
    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     *
     */
    protected $email;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *  @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_customer_id", type="string", length=50, nullable=true)
     */
    private $stripeCustomerId;

    /**
     * @var string
     *  @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var bool
     *
     * @ORM\Column(name="gender", type="boolean", nullable=true, options={"default" : 1})
     */
    private $gender; // 1:Male, 0:Female

    /**
     * @var String
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes = { "image/png", "image/jpeg", "image/jpg" }
     *     )
     * @ORM\Column(name="profile_pic", type="string", length=255, nullable=true, options={"default" : "no_pic.png"})
     */
    private $profilePic = 'no_pic.png';

    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true, options={"default" : false})
     */
    private $isDelete = '0';

    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_newsletter_subscribed", type="boolean", nullable=true, options={"default" : false})
     */
    private $isNewsletterSubscribed;

    /**
     * @var TblCoaches
     *
     * @ORM\ManyToOne(targetEntity="TblCoaches")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="is_coach", referencedColumnName="id", nullable=true)
     * })
     */
    private $isCoach;


    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param string $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @return bool
     */
    public function isNewsletterSubscribed()
    {
        return $this->isNewsletterSubscribed;
    }


    /**
     * @param $is_newsletter_subscribed
     *
     * @return $this
     */
    public function setIsNewsletterSubscribed($is_newsletter_subscribed)
    {
        $this->isNewsletterSubscribed = $is_newsletter_subscribed;
        return $this;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * Get createdBy
     *
     * @return \App\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return $this
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * Get updatedBy
     *
     * @return \App\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return $this
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return String
     */
    public function getProfilePic()
    {
        return $this->profilePic;
    }

    /**
     * @param String $profilePic
     */
    public function setProfilePic($profilePic)
    {
        $this->profilePic = $profilePic;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getStripeCustomerId(): string
    {
        return $this->stripeCustomerId;
    }

    /**
     * @param string $stripeCustomerId
     */
    public function setStripeCustomerId(string $stripeCustomerId): void
    {
        $this->stripeCustomerId = $stripeCustomerId;
    }

    /**
     * @return TblCoaches
     */
    public function getIsCoach(): ?TblCoaches
    {
        return $this->isCoach;
    }

    /**
     * @param TblCoaches $isCoach
     */
    public function setIsCoach(?TblCoaches $isCoach): void
    {
        $this->isCoach = $isCoach;
    }


    /**
     * @return String
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->firstName;
    }
}
