<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCartProduct
 *
 * @ORM\Table(name="tbl_cart_product",
 *     indexes={@ORM\Index(name="IDX_809BB39C5ED23C43", columns={"id_product"}),
 *     @ORM\Index(name="IDX_809BB39C1BE1FB52", columns={"id_cart"}),
 *     @ORM\Index(name="FK_tbl_cart_product_tbl_products", columns={"id_product_size"}),
 *     })
 * @ORM\Entity
 */
class TblCartProduct
{
    const _ADDTOCART = 0;
    const _PLACEORDER = 1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $totalAmount;

    /**
     * @var double
     *
     * @ORM\Column(name="shipping_amount", type="decimal", precision=10, scale=2, options={"default"="0.00"},
     *     nullable=true)
     */
    private $shippingAmount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="boolean", length=100, nullable=true, options={"default"="0"})
     */
    private $status = self::_ADDTOCART;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblCart
     *
     * @ORM\ManyToOne(targetEntity="TblCart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart", referencedColumnName="id")
     * })
     */
    private $idCart;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var TblProductSize
     *
     * @ORM\ManyToOne(targetEntity="TblProductSize")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_size", referencedColumnName="id", nullable=true)
     * })
     */
    private $idProductSize;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     *
     * @return TblCartProduct
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }
    /**
     * @return string
     */
    public function getTotalAmount(): string
    {
        return $this->totalAmount;
    }


    /**
     * @param $totalAmount
     *
     * @return TblCartProduct
     */
    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }
    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return TblCartProduct
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblCartProduct
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblCartProduct
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblCart|null
     */
    public function getIdCart(): ?TblCart
    {
        return $this->idCart;
    }

    /**
     * @param TblCart|null $idCart
     *
     * @return TblCartProduct
     */
    public function setIdCart(?TblCart $idCart): self
    {
        $this->idCart = $idCart;

        return $this;
    }

    /**
     * @return TblProducts|null
     */
    public function getIdProduct(): ?TblProducts
    {
        return $this->idProduct;
    }

    /**
     * @param TblProducts|null $idProduct
     *
     * @return TblCartProduct
     */
    public function setIdProduct(?TblProducts $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }


    /**
     * @param string|null $status
     *
     * @return TblCartProduct
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @return TblProductSize|null
     */
    public function getIdProductSize(): ?TblProductSize
    {
        return $this->idProductSize;
    }


    /**
     * @param TblProductSize|null $idProductSize
     *
     * @return TblCartProduct
     */
    public function setIdProductSize(?TblProductSize $idProductSize): self
    {
        $this->idProductSize = $idProductSize;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAmount(): string
    {
        return $this->shippingAmount;
    }

    /**
     * @param string $shippingAmount
     */
    public function setShippingAmount(string $shippingAmount): void
    {
        $this->shippingAmount = $shippingAmount;
    }
}
