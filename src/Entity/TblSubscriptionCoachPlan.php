<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblSubscriptionCoachPlan
 *
 * @ORM\Table(name="tbl_subscription_coach_plan",
 *     indexes={@ORM\Index(name="FK_tbl_coaches_plan_tbl_users_2", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_coaches_plan_tbl_users", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_subscription_coach_plan_tbl_subscription_plan", columns={"id_subscription_plan"})
 * })
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionCoachPlanRepository")
 */
class TblSubscriptionCoachPlan
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter Plan name"
     * )
     * @ORM\Column(name="plan_name", type="string", length=255, nullable=false)
     */
    private $planName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="members", type="integer", length=255, nullable=false)
     */
    private $members;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter amount"
     * )
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="completed_amount", type="decimal", precision=10, scale=2,
     *     nullable=true, options={"default"="0.00"})
     */
    private $completedAmount = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="extracomplete_amount", type="decimal", precision=10, scale=2,
     *     nullable=true, options={"default"="0.00"})
     */
    private $extracompleteAmount = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="package_interval", type="text", length=255, nullable=true,
     *     options={"comment"="Monthly,Quarterly,Half Year,Yearly"})
     */
    private $packageInterval;

    /**
     * @var int|null
     *
     * @ORM\Column(name="trial_days", type="integer", nullable=true)
     */
    private $trialDays;

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_plan_id", type="string", length=50, nullable=true)
     */
    private $stripePlanId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_last_activity", type="string", length=255, nullable=true)
     */
    private $stripeLastActivity;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false, options={"default"="0"})
     */
    private $isDelete = '0';


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblSubscriptionPlan
     *
     * @ORM\ManyToOne(targetEntity="TblSubscriptionPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subscription_plan", referencedColumnName="id")
     * })
     */
    private $idSubscriptionPlan;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlanName()
    {
        return $this->planName;
    }

    /**
     * @param string $planName
     */
    public function setPlanName($planName): void
    {
        $this->planName = $planName;
    }

    /**
     * @return string|null
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param string|null $members
     */
    public function setMembers($members): void
    {
        $this->members = $members;
    }

    /**
     * @return string
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount(?string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getCompletedAmount(): ?string
    {
        return $this->completedAmount;
    }

    /**
     * @param string|null $completedAmount
     */
    public function setCompletedAmount(?string $completedAmount): void
    {
        $this->completedAmount = $completedAmount;
    }

    /**
     * @return string|null
     */
    public function getExtracompleteAmount(): ?string
    {
        return $this->extracompleteAmount;
    }

    /**
     * @param string|null $extracompleteAmount
     */
    public function setExtracompleteAmount(?string $extracompleteAmount): void
    {
        $this->extracompleteAmount = $extracompleteAmount;
    }

    /**
     * @return string|null
     */
    public function getPackageInterval(): ?string
    {
        return $this->packageInterval;
    }

    /**
     * @param string|null $packageInterval
     */
    public function setPackageInterval(?string $packageInterval): void
    {
        $this->packageInterval = $packageInterval;
    }

    /**
     * @return int|null
     */
    public function getTrialDays(): ?int
    {
        return $this->trialDays;
    }

    /**
     * @param int|null $trialDays
     */
    public function setTrialDays(?int $trialDays): void
    {
        $this->trialDays = $trialDays;
    }

    /**
     * @return string|null
     */
    public function getStripePlanId(): ?string
    {
        return $this->stripePlanId;
    }

    /**
     * @param string|null $stripePlanId
     */
    public function setStripePlanId(?string $stripePlanId): void
    {
        $this->stripePlanId = $stripePlanId;
    }

    /**
     * @return string|null
     */
    public function getStripeLastActivity(): ?string
    {
        return $this->stripeLastActivity;
    }

    /**
     * @param string|null $stripeLastActivity
     */
    public function setStripeLastActivity(?string $stripeLastActivity): void
    {
        $this->stripeLastActivity = $stripeLastActivity;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate($updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblSubscriptionPlan
     */
    public function getIdSubscriptionPlan(): ?TblSubscriptionPlan
    {
        return $this->idSubscriptionPlan;
    }

    /**
     * @param TblSubscriptionPlan $idSubscriptionPlan
     */
    public function setIdSubscriptionPlan(TblSubscriptionPlan $idSubscriptionPlan): void
    {
        $this->idSubscriptionPlan = $idSubscriptionPlan;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy(): User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return (string)$this->getMembers();
    }
}
