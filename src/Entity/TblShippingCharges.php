<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblShippingCharges
 *
 * @ORM\Table(name="tbl_shipping_charges",
 *     indexes={@ORM\Index(name="FK_tbl_shipping_charges_tbl_countries", columns={"id_country"}),
 *     @ORM\Index(name="FK_tbl_shipping_charges_tbl_users", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_shipping_charges_tbl_users_2", columns={"updated_by"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\ShhippingChargesRepository")
 */
class TblShippingCharges
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="min_weight", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $minWeight;

    /**
     * @var string
     *
     * @ORM\Column(name="max_weight", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $maxWeight;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var TblCountries
     *
     * @ORM\ManyToOne(targetEntity="TblCountries")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_country", referencedColumnName="id")
     * })
     */
    private $idCountry;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMinWeight(): string
    {
        return $this->minWeight;
    }

    /**
     * @param string $minWeight
     */
    public function setMinWeight(string $minWeight): void
    {
        $this->minWeight = $minWeight;
    }

    /**
     * @return string
     */
    public function getMaxWeight(): string
    {
        return $this->maxWeight;
    }

    /**
     * @param string $maxWeight
     */
    public function setMaxWeight(string $maxWeight): void
    {
        $this->maxWeight = $maxWeight;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate(): ?\DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate(?\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate(): ?\DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate(?\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblCountries
     */
    public function getIdCountry(): TblCountries
    {
        return $this->idCountry;
    }

    /**
     * @param TblCountries $idCountry
     */
    public function setIdCountry(TblCountries $idCountry): void
    {
        $this->idCountry = $idCountry;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy(): User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }
}
