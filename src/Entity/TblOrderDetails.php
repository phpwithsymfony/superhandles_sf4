<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblOrderDetails
 *
 * @ORM\Table(name="tbl_order_details",
 *     indexes={@ORM\Index(name="product_id", columns={"id_product"}),
 *     @ORM\Index(name="FK_tbl_order_details_tbl_users", columns={"updated_by"}),
 *     @ORM\Index(name="order_id", columns={"id_order"})
 * })
 * @ORM\Entity
 */
class TblOrderDetails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="product_qty", type="integer", nullable=false)
     */
    private $productQty;

    /**
     * @var string
     *
     * @ORM\Column(name="product_price", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $productPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false, options={"default"="0"})
     */
    private $status = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblOrders
     *
     * @ORM\ManyToOne(targetEntity="TblOrders", inversedBy="orderDetails")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_order", referencedColumnName="id")
     * })
     */
    private $idOrder;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var double
     *
     * @ORM\Column(name="shipping_amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $shippingAmount;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getProductQty(): ?int
    {
        return $this->productQty;
    }

    /**
     * @param int $productQty
     *
     * @return TblOrderDetails
     */
    public function setProductQty(int $productQty): self
    {
        $this->productQty = $productQty;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @param $productPrice
     *
     * @return TblOrderDetails
     */
    public function setProductPrice($productPrice): self
    {
        $this->productPrice = $productPrice;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return TblOrderDetails
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblOrderDetails
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblOrderDetails
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblOrders|null
     */
    public function getIdOrder(): ?TblOrders
    {
        return $this->idOrder;
    }

    /**
     * @param TblOrders|null $idOrder
     *
     * @return TblOrderDetails
     */
    public function setIdOrder(?TblOrders $idOrder): self
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * @return TblProducts|null
     */
    public function getIdProduct(): ?TblProducts
    {
        return $this->idProduct;
    }

    /**
     * @param TblProducts|null $idProduct
     *
     * @return TblOrderDetails
     */
    public function setIdProduct(?TblProducts $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * @return float
     */
    public function getShippingAmount(): float
    {
        return $this->shippingAmount;
    }

    /**
     * @param float $shippingAmount
     */
    public function setShippingAmount(float $shippingAmount): void
    {
        $this->shippingAmount = $shippingAmount;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblOrderDetails
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
