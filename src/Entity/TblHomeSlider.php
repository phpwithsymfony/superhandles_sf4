<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblHomeSlider
 *
 * @ORM\Table(name="tbl_home_slider",
 *     indexes={@ORM\Index(name="IDX_B8A2992216FE72E1", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_home_slider_tbl_static",
 *     columns={"id_static"}), @ORM\Index(name="IDX_B8A29922DE12AB56", columns={"created_by"})})
 *
 * @ORM\Entity(repositoryClass="App\Repository\HomeSliderRepository")
 */
class TblHomeSlider
{
    const _ACTIVE = 1;
    const _INACTIVE = 0;
    const _DELETE = 1;
    const _NOTDELETE = 0;

    const _ACTIVE_LABEL = 'Active';
    const _INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="banner_file", type="string", length=100, nullable=false)
     *
     * @Assert\File(
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/png"},
     *     mimeTypesMessage = "Please upload a valid image (jpeg, jpg & png)",
     *     maxSize="2M"
     * )
     */
    private $bannerFile;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter page url"
     * )
     * @ORM\Column(name="page_url", type="string", length=255, nullable=false)
     */
    private $pageUrl;

    /**
     * @var bool
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = self::_NOTDELETE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = self::_ACTIVE;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var TblStatic
     *
     * @ORM\ManyToOne(targetEntity="TblStatic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_static", referencedColumnName="id")
     * })
     */
    private $idStatic;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TblHomeSlider
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getBannerFile()
    {
        return $this->bannerFile;
    }

    /**
     * @param string $bannerFile
     *
     * @return TblHomeSlider
     */
    public function setBannerFile($bannerFile)
    {
        $this->bannerFile = $bannerFile;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     *
     * @return TblHomeSlider
     */
    public function setPageUrl(string $pageUrl): self
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     *
     * @return TblHomeSlider
     */
    public function setIsDelete(bool $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblHomeSlider
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param $sortOrder
     *
     * @return TblHomeSlider
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblHomeSlider
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblHomeSlider
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblHomeSlider
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblHomeSlider
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return TblStatic|null
     */
    public function getIdStatic(): ?TblStatic
    {
        return $this->idStatic;
    }

    /**
     * @param TblStatic|null $idStatic
     *
     * @return TblHomeSlider
     */
    public function setIdStatic(?TblStatic $idStatic): self
    {
        $this->idStatic = $idStatic;

        return $this;
    }
}
