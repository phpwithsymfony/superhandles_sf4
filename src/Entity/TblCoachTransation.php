<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCoachTransation
 *
 * @ORM\Table(name="tbl_coach_transation",
 *     indexes={@ORM\Index(name="FK_tbl_coaches_transation_tbl_cart_coupons", columns={"id_cart_coupon"}),
 *     @ORM\Index(name="FK_tbl_coach_transation_tbl_coaches", columns={"id_coach"})})
 * @ORM\Entity
 */
class TblCoachTransation
{
    const STATUS_INNCOMPLETE = "incomplete";
    const STATUS_INNCOMPLETE_EXPIRED = "incomplete_expired";
    const STSTUS_TRIALING = "trialing";
    const STSTUS_ACTIVE = "active";
    const STSTUS_PASTDUE = "past_due";
    const STSTUS_CANCELED = "canceled";
    const STSTUS_UNPAID = "unpaid";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_transaction", type="string", length=255, nullable=true)
     */
    private $idTransaction;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_subscription_id", type="string", length=50, nullable=false)
     */
    private $stripeSubscriptionId;

    /**
     * @var string
     *
     * @ORM\Column(name="payer_email", type="string", length=255, nullable=false)
     */
    private $payerEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_type", type="string", length=200, nullable=true)
     */
    private $transactionType;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=100, nullable=false)
     */
    private $amount;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="trial_start_date", type="datetime", nullable=true)
     */
    private $trialStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="trial_end_date", type="datetime", nullable=true)
     */
    private $trialEndDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_status", type="string", length=50, nullable=false)
     */
    private $stripeStatus;

    /**
     * @var TblCoaches
     *
     * @ORM\ManyToOne(targetEntity="TblCoaches")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coach", referencedColumnName="id")
     * })
     */
    private $idCoach;

    /**
     * @var TblCartCoupons
     *
     * @ORM\ManyToOne(targetEntity="TblCartCoupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart_coupon", referencedColumnName="id")
     * })
     */
    private $idCartCoupon;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIdTransaction(): string
    {
        return $this->idTransaction;
    }

    /**
     * @param string $idTransaction
     */
    public function setIdTransaction(string $idTransaction): void
    {
        $this->idTransaction = $idTransaction;
    }

    /**
     * @return string
     */
    public function getStripeSubscriptionId(): string
    {
        return $this->stripeSubscriptionId;
    }

    /**
     * @param string $stripeSubscriptionId
     */
    public function setStripeSubscriptionId(string $stripeSubscriptionId): void
    {
        $this->stripeSubscriptionId = $stripeSubscriptionId;
    }

    /**
     * @return string
     */
    public function getPayerEmail(): string
    {
        return $this->payerEmail;
    }

    /**
     * @param string $payerEmail
     */
    public function setPayerEmail(string $payerEmail): void
    {
        $this->payerEmail = $payerEmail;
    }

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     */
    public function setTransactionType(string $transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate(): ?\DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate(?\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getStripeStatus(): string
    {
        return $this->stripeStatus;
    }

    /**
     * @param string $stripeStatus
     */
    public function setStripeStatus(string $stripeStatus): void
    {
        $this->stripeStatus = $stripeStatus;
    }

    /**
     * @return TblCoaches
     */
    public function getIdCoach(): TblCoaches
    {
        return $this->idCoach;
    }

    /**
     * @param TblCoaches $idCoach
     */
    public function setIdCoach(TblCoaches $idCoach): void
    {
        $this->idCoach = $idCoach;
    }

    /**
     * @return TblCartCoupons
     */
    public function getIdCartCoupon(): ?TblCartCoupons
    {
        return $this->idCartCoupon;
    }

    /**
     * @param TblCartCoupons $idCartCoupon
     */
    public function setIdCartCoupon(?TblCartCoupons $idCartCoupon): void
    {
        $this->idCartCoupon = $idCartCoupon;
    }

    /**
     * @return \DateTime|null
     */
    public function getTrialStartDate(): ?\DateTime
    {
        return $this->trialStartDate;
    }

    /**
     * @param \DateTime|null $trialStartDate
     */
    public function setTrialStartDate($trialStartDate)
    {
        $trialStartDate = new \DateTimeImmutable(date('Y-m-d h:i:s', $trialStartDate));
        $this->trialStartDate = $trialStartDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getTrialEndDate(): ?\DateTime
    {
        return $this->trialEndDate;
    }

    /**
     * @param \DateTime|null $trialEndDate
     */
    public function setTrialEndDate($trialEndDate): void
    {
        $trialEndDate = new \DateTimeImmutable(date('Y-m-d h:i:s', $trialEndDate));
        $this->trialEndDate = $trialEndDate;
    }
}
