<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="tbl_users", indexes={
 *     @ORM\Index(name="fk_tbl_user_1_idx", columns={"id_profile"}),
 *     @ORM\Index(name="username", columns={"username"}),
 *     @ORM\Index(name="email", columns={"email"})})
 * @UniqueEntity(
 *     fields={"username"},
 *     message="Username already use."
 * )
 * @UniqueEntity(
 *     fields={"email"},
 *     message="email-id already use."
 * )
 *
 */
class User extends BaseUser
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @Assert\NotBlank()
     */
    protected $username;


    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     *
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var \App\Entity\TblUserProfile
     *
     * @ORM\OneToOne(targetEntity="App\Entity\TblUserProfile" , cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_profile", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $profile;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /****************************************
     * Getter and Setter methods
     ****************************************/

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Get profile
     *
     * @return TblUserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }


    public function setProfile(TblUserProfile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }
}
