<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblPaymentFields
 *
 * @ORM\Table(name="tbl_payment_fields", indexes={@ORM\Index(name="payment_id", columns={"id_payment_methods"})})
 * @ORM\Entity
 */
class TblPaymentFields
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=120, nullable=false)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="field_value", type="string", length=255, nullable=false)
     */
    private $fieldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var TblPaymentMethods
     *
     * @ORM\ManyToOne(targetEntity="TblPaymentMethods")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_methods", referencedColumnName="id")
     * })
     */
    private $idPaymentMethods;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFieldName(): ?string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     *
     * @return TblPaymentFields
     */
    public function setFieldName(string $fieldName): self
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFieldValue(): ?string
    {
        return $this->fieldValue;
    }

    /**
     * @param string $fieldValue
     *
     * @return TblPaymentFields
     */
    public function setFieldValue(string $fieldValue): self
    {
        $this->fieldValue = $fieldValue;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return TblPaymentFields
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblPaymentFields
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblPaymentFields
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblPaymentFields
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblPaymentMethods|null
     */
    public function getIdPaymentMethods(): ?TblPaymentMethods
    {
        return $this->idPaymentMethods;
    }

    /**
     * @param TblPaymentMethods|null $idPaymentMethods
     *
     * @return TblPaymentFields
     */
    public function setIdPaymentMethods(?TblPaymentMethods $idPaymentMethods): self
    {
        $this->idPaymentMethods = $idPaymentMethods;

        return $this;
    }
}
