<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblTransation
 *
 * @ORM\Table(name="tbl_transation",
 *     indexes={@ORM\Index(name="customer_id", columns={"id_customer"}),
 *     @ORM\Index(name="order_id", columns={"id_order"}),
 *     @ORM\Index(name="FK_tbl_transation_tbl_payment_methods", columns={"id_payment_methods"}),
 *     @ORM\Index(name="FK_tbl_coach_transation_tbl_coaches", columns={"id_coach"})
 * })
 * @ORM\Entity
 */
class TblTransation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subscr_id", type="string", length=70, nullable=true)
     */
    private $subscrId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payer_id", type="string", length=255, nullable=true)
     */
    private $payerId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payer_email", type="string", length=255, nullable=true)
     */
    private $payerEmail;

    /**
     * @var float
     *
     * @ORM\Column(name="sum", type="float", precision=12, scale=2, nullable=false)
     */
    private $sum;

    /**
     * @var float
     *
     * @ORM\Column(name="transation_json", type="text", nullable=false)
     */
    private $transationJson;

    /**
     * @var string|null
     *
     * @ORM\Column(name="transaction_history", type="string", length=1000, nullable=true,
     *     options={"default"="web_accept"}
     * )
     */
    private $transactionHistory = 'web_accept';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate = 'CURRENT_TIMESTAMP';

    /**
     * @var TblCustomer
     *
     * @ORM\ManyToOne(targetEntity="TblCustomer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     * })
     */
    private $idCustomer;

    /**
     * @var TblOrders
     *
     * @ORM\ManyToOne(targetEntity="TblOrders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_order", referencedColumnName="id")
     * })
     */
    private $idOrder;

    /**
     * @var TblPaymentMethods
     *
     * @ORM\ManyToOne(targetEntity="TblPaymentMethods")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_methods", referencedColumnName="id")
     * })
     */
    private $idPaymentMethods;

    /**
     * @var TblCoaches
     *
     * @ORM\ManyToOne(targetEntity="TblCoaches")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coach", referencedColumnName="id", nullable=true)
     * })
     */
    private $idCoach;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     *
     * @return TblTransation
     */
    public function setTransactionId(string $transactionId): self
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscrId(): ?string
    {
        return $this->subscrId;
    }

    /**
     * @param string|null $subscrId
     *
     * @return TblTransation
     */
    public function setSubscrId(?string $subscrId): self
    {
        $this->subscrId = $subscrId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPayerId(): ?string
    {
        return $this->payerId;
    }

    /**
     * @param string|null $payerId
     *
     * @return TblTransation
     */
    public function setPayerId(?string $payerId): self
    {
        $this->payerId = $payerId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPayerEmail(): ?string
    {
        return $this->payerEmail;
    }

    /**
     * @param string|null $payerEmail
     *
     * @return TblTransation
     */
    public function setPayerEmail(?string $payerEmail): self
    {
        $this->payerEmail = $payerEmail;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSum(): ?float
    {
        return $this->sum;
    }

    /**
     * @param float $sum
     *
     * @return TblTransation
     */
    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransactionHistory(): ?string
    {
        return $this->transactionHistory;
    }

    /**
     * @param string|null $transactionHistory
     *
     * @return TblTransation
     */
    public function setTransactionHistory(?string $transactionHistory): self
    {
        $this->transactionHistory = $transactionHistory;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return TblTransation
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return TblCustomer|null
     */
    public function getIdCustomer(): ?TblCustomer
    {
        return $this->idCustomer;
    }

    /**
     * @param TblCustomer|null $idCustomer
     *
     * @return TblTransation
     */
    public function setIdCustomer(?TblCustomer $idCustomer): self
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * @return TblOrders|null
     */
    public function getIdOrder(): ?TblOrders
    {
        return $this->idOrder;
    }

    /**
     * @param TblOrders|null $idOrder
     *
     * @return TblTransation
     */
    public function setIdOrder(?TblOrders $idOrder): self
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * @return TblPaymentMethods|null
     */
    public function getIdPaymentMethods(): ?TblPaymentMethods
    {
        return $this->idPaymentMethods;
    }

    /**
     * @param TblPaymentMethods|null $idPaymentMethods
     *
     * @return TblTransation
     */
    public function setIdPaymentMethods(?TblPaymentMethods $idPaymentMethods): self
    {
        $this->idPaymentMethods = $idPaymentMethods;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getTransationJson(): ?string
    {
        return $this->transationJson;
    }


    /**
     * @param string|null $transationJson
     *
     * @return TblTransation
     */
    public function setTransationJson(?string $transationJson): self
    {
        $this->transationJson = $transationJson;
        return $this;
    }

    /**
     * @return TblCoaches
     */
    public function getIdCoach(): ?TblCoaches
    {
        return $this->idCoach;
    }

    /**
     * @param TblCoaches $idCoach
     */
    public function setIdCoach(?TblCoaches $idCoach): void
    {
        $this->idCoach = $idCoach;
    }
}
