<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblOrderProductSize
 *
 * @ORM\Table(name="tbl_order_product_size",
 *     indexes={@ORM\Index(name="order_id", columns={"id_order_details", "id_product_size"}),
 *     @ORM\Index(name="size_id", columns={"id_product_size"}),
 *     @ORM\Index(name="IDX_605761073A7473A4", columns={"id_order_details"})})
 * @ORM\Entity
 */
class TblOrderProductSize
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate = 'CURRENT_TIMESTAMP';

    /**
     * @var TblOrderDetails
     *
     * @ORM\ManyToOne(targetEntity="TblOrderDetails")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_order_details", referencedColumnName="id")
     * })
     */
    private $idOrderDetails;

    /**
     * @var TblProductSize
     *
     * @ORM\ManyToOne(targetEntity="TblProductSize")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_size", referencedColumnName="id")
     * })
     */
    private $idProductSize;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return TblOrderProductSize
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return TblOrderDetails|null
     */
    public function getIdOrderDetails(): ?TblOrderDetails
    {
        return $this->idOrderDetails;
    }

    /**
     * @param TblOrderDetails|null $idOrderDetails
     *
     * @return TblOrderProductSize
     */
    public function setIdOrderDetails(?TblOrderDetails $idOrderDetails): self
    {
        $this->idOrderDetails = $idOrderDetails;

        return $this;
    }

    /**
     * @return TblProductSize|null
     */
    public function getIdProductSize(): ?TblProductSize
    {
        return $this->idProductSize;
    }

    /**
     * @param TblProductSize|null $idProductSize
     *
     * @return TblOrderProductSize
     */
    public function setIdProductSize(?TblProductSize $idProductSize): self
    {
        $this->idProductSize = $idProductSize;

        return $this;
    }
}
