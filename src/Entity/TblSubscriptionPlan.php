<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblSubscriptionPlan
 *
 * @ORM\Table(name="tbl_subscription_plan",
 *     indexes={@ORM\Index(name="FK_tbl_coaches_plan_tbl_users", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_coach_subscription_plan_tbl_subscription_package", columns={"id_subscription_package"}),
 *     @ORM\Index(name="FK_tbl_coaches_plan_tbl_users_2", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_subscription_plan_tbl_streaming_page", columns={"id_streaming_page"})
 * })
 * @ORM\Entity
 */
class TblSubscriptionPlan
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const UNIVERSITY_PACKAGE = 1;
    const TEAM_UNIVERSITY_PACKAGE = 2;
    const COACH_PACKAGE = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter Plan name"
     * )
     * @ORM\Column(name="plan_name", type="string", length=255, nullable=false)
     */
    private $planName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     * @Assert\NotBlank(
     *     message="Please select at list one member"
     * )
     * @ORM\Column(name="members", type="string",length=255, nullable=true)
     */
    private $members;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_plan_id", type="string", length=50, nullable=true)
     */
    private $stripePlanId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="stripe_last_activity", type="string", length=255, nullable=true)
     */
    private $stripeLastActivity;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="team_package_id", type="integer", nullable=true)
     */
    private $teamPackageId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="trial_days", type="integer", nullable=true)
     */
    private $trialDays;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please select interval"
     * )
     * @ORM\Column(name="package_interval", type="text",
     *     length=255, nullable=true, options={"comment"="Monthly,Quarterly,Half Year,Yearly"}
     * )
     */
    private $packageInterval;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblSubscriptionPackage
     *
     * @ORM\ManyToOne(targetEntity="TblSubscriptionPackage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subscription_package", referencedColumnName="id")
     * })
     */
    private $idSubscriptionPackage;

    /**
     * @var TblStreamingPages
     *
     * @ORM\ManyToOne(targetEntity="TblStreamingPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_streaming_page", referencedColumnName="id", nullable=true)
     * })
     */
    private $idStreamingPage;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlanName(): ?string
    {
        return $this->planName;
    }

    /**
     * @param string $planName
     */
    public function setPlanName(string $planName): void
    {
        $this->planName = $planName;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getMembers()
    {
        $membersArray = explode(",", $this->members);
        return $membersArray;
    }

    /**
     * @param array $members
     */
    public function setMembers($members)
    {
        $membersArray = implode($members, ",");
        $this->members = $membersArray;
    }

    /**
     * @return string
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getStripePlanId(): ?string
    {
        return $this->stripePlanId;
    }

    /**
     * @param string|null $stripePlanId
     */
    public function setStripePlanId(?string $stripePlanId): void
    {
        $this->stripePlanId = $stripePlanId;
    }

    /**
     * @return string|null
     */
    public function getStripeLastActivity(): ?string
    {
        return $this->stripeLastActivity;
    }

    /**
     * @param string|null $stripeLastActivity
     */
    public function setStripeLastActivity(?string $stripeLastActivity): void
    {
        $this->stripeLastActivity = $stripeLastActivity;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return int|null
     */
    public function getTeamPackageId(): ?int
    {
        return $this->teamPackageId;
    }

    /**
     * @param int|null $teamPackageId
     */
    public function setTeamPackageId(?int $teamPackageId): void
    {
        $this->teamPackageId = $teamPackageId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate($updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblSubscriptionPackage
     */
    public function getIdSubscriptionPackage(): ?TblSubscriptionPackage
    {
        return $this->idSubscriptionPackage;
    }

    /**
     * @param TblSubscriptionPackage $idSubscriptionPackage
     */
    public function setIdSubscriptionPackage(TblSubscriptionPackage $idSubscriptionPackage): void
    {
        $this->idSubscriptionPackage = $idSubscriptionPackage;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy(): User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return int|null
     */
    public function getTrialDays(): ?int
    {
        return $this->trialDays;
    }

    /**
     * @param int|null $trialDays
     */
    public function setTrialDays(?int $trialDays): void
    {
        $this->trialDays = $trialDays;
    }

    /**
     * @return string|null
     */
    public function getPackageInterval(): ?string
    {
        return $this->packageInterval;
    }

    /**
     * @param string|null $packageInterval
     */
    public function setPackageInterval(?string $packageInterval): void
    {
        $this->packageInterval = $packageInterval;
    }

    /**
     * @return TblStreamingPages
     */
    public function getIdStreamingPage(): TblStreamingPages
    {
        return $this->idStreamingPage;
    }

    /**
     * @param TblStreamingPages $idStreamingPage
     */
    public function setIdStreamingPage(TblStreamingPages $idStreamingPage): void
    {
        $this->idStreamingPage = $idStreamingPage;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        $intervals  = ($this->getPackageInterval()=="12") ? "Year" : "Month";
        return $this->getPlanName()." ($".$this->getAmount()." Per Player Per ".$intervals.")";
    }
}
