<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblTestimonials
 *
 * @ORM\Table(name="tbl_testimonials", indexes={
 *     @ORM\Index(name="FK_tbl_testimonial_post_type", columns={"id_testimonial_post_type"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\TestimonialsRepository")
 */
class TblTestimonials
{
    const _ACTIVE = 1;
    const _INACTIVE = 0;
    const _DELETE = 1;
    const _NOTDELETE = 0;

    const _ACTIVE_LABEL = 'Active';
    const _INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var TblTestimonialPostType
     *
     * @ORM\ManyToOne(targetEntity="TblTestimonialPostType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_testimonial_post_type", referencedColumnName="id")
     * })
     */
    private $idTestimonialPostType;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="author_name", type="string", length=255, nullable=false)
     */
    private $authorName;


    /**
     * @var string
     * @Assert\NotBlank(groups={"add"})
     * @Assert\File(
     *     mimeTypes = {"image/png", "image/jpg", "image/jpeg", "image/gif"},
     *     mimeTypesMessage = "Please upload a valid image Format"
     * )
     * @ORM\Column(name="author_pic", type="string", length=255, nullable=false)
     */
    private $authorPic;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="post_content", type="text", nullable=false)
     */
    private $postContent;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="twitter_username", type="string", length=255, nullable=false)
     */
    private $twitterUsername;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="smallint", nullable=false)
     */
    private $isDelete = self::_NOTDELETE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */

    private $isActive = self::_ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return TblTestimonialPostType
     */
    public function getIdTestimonialPostType()
    {
        return $this->idTestimonialPostType;
    }

    /**
     * @param TblTestimonialPostType $idTestimonialPostType
     */
    public function setIdTestimonialPostType($idTestimonialPostType)
    {
        $this->idTestimonialPostType = $idTestimonialPostType;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param string $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return string
     */
    public function getAuthorPic()
    {
        return $this->authorPic;
    }

    /**
     * @param string $authorPic
     */
    public function setAuthorPic($authorPic)
    {
        $this->authorPic = $authorPic;
    }

    /**
     * @return string
     */
    public function getPostContent()
    {
        return $this->postContent;
    }

    /**
     * @param string $postContent
     */
    public function setPostContent($postContent)
    {
        $this->postContent = $postContent;
    }

    /**
     * @return string
     */
    public function getTwitterUsername()
    {
        return $this->twitterUsername;
    }

    /**
     * @param string $twitterUsername
     */
    public function setTwitterUsername($twitterUsername)
    {
        $this->twitterUsername = $twitterUsername;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }
    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }
    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }
    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param $sortOrder
     *
     * @return TblTestimonials
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return bool|null
     */
    public function getSortOrder(): ?bool
    {
        return $this->sortOrder;
    }
}
