<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblCartCoupons
 *
 * @ORM\Table(name="tbl_cart_coupons",
 *     indexes={@ORM\Index(name="FK_tbl_cart_coupons_tbl_users", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_cart_coupons_tbl_product_type", columns={"id_product_type"}),
 *     @ORM\Index(name="id_product", columns={"id_product"}),
 *     @ORM\Index(name="FK_tbl_cart_coupons_tbl_users_2", columns={"updated_by"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CouponCodeRepository")
 *
 * @UniqueEntity(
 *     fields={"couponCode"},
 *     message="This Coupon Code already Added."
 * )
 */
class TblCartCoupons
{
    const DELETE    = 1;
    const NOTDELETE = 0;

    const ACTIVE         = 1;
    const INACTIVE       = 0;
    const ACTIVE_LABEL   = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const PERCENTAGE_BASED       = 1;
    const PRICE_BASED            = 2;
    const PERCENTAGE_BASED_LABEL = 'Percentage Based';
    const PRICE_BASED_LABEL      = 'Amount Based';

    const COUPON_TYPE = [
        self::PERCENTAGE_BASED_LABEL => self::PERCENTAGE_BASED,
        self::PRICE_BASED_LABEL      => self::PRICE_BASED
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="strip_id", type="string", length=50, nullable=true)
     */
    private $stripId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_type", type="smallint", nullable=false,
     *     options={"comment"="1:percentage based 2:price based"}
     * )
     * @Assert\NotBlank(
     *     message="Please select coupon Type"
     * )
     */
    private $couponType = '0';

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var string
     *  @Assert\NotBlank(
     *     message="Please enter coupon code"
     * )
     * @ORM\Column(name="coupon_code", type="string", length=50, nullable=false, unique=true)
     */
    private $couponCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter discount"
     * )
     * @Assert\Regex(
     *     pattern="/^\d+(\.\d+)?/",
     *     message="Please use only numbers."
     * )
     * @ORM\Column(name="discount", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $discount;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message="Please select Start date")
     * @ORM\Column(name="start_date", type="date", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message="Please select end date")
     * @Assert\Expression(
     *     "this.getStartDate() < this.getEndDate()",
     *     message="The end date must be after the start date"
     * )
     * @ORM\Column(name="end_date", type="date", nullable=false)
     */
    private $endDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isActive = self::ACTIVE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true, options={"default"="0"})
     */
    private $isDelete = self::NOTDELETE;

    /**
     * @var string|null
     *
     * @ORM\Column(name="affiliate_id", type="string", length=45, nullable=true)
     */
    private $affiliateId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campaign_id", type="string", length=45, nullable=true)
     */
    private $campaignId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var TblProductType
     *
     * @ORM\ManyToOne(targetEntity="TblProductType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_type", referencedColumnName="id")
     * })
     */
    private $idProductType;

    /**
     * @var TblProducts
     *
     * @ORM\ManyToOne(targetEntity="TblProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getStripId(): ?string
    {
        return $this->stripId;
    }

    /**
     * @param string|null $stripId
     *
     * @return TblCartCoupons
     */
    public function setStripId(?string $stripId): self
    {
        $this->stripId = $stripId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCouponType(): int
    {
        return $this->couponType;
    }

    /**
     * @param int $couponType
     */
    public function setCouponType(int $couponType): void
    {
        $this->couponType = $couponType;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TblCartCoupons
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCouponCode(): ?string
    {
        return $this->couponCode;
    }

    /**
     * @param string $couponCode
     *
     * @return TblCartCoupons
     */
    public function setCouponCode(string $couponCode): self
    {
        $this->couponCode = $couponCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param $discount
     *
     * @return TblCartCoupons
     */
    public function setDiscount($discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param  $startDate
     *
     * @return TblCartCoupons
     */
    public function setStartDate($startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param  $endDate
     *
     * @return TblCartCoupons
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblCartCoupons
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAffiliateId(): ?string
    {
        return $this->affiliateId;
    }

    /**
     * @param string|null $affiliateId
     *
     * @return TblCartCoupons
     */
    public function setAffiliateId(?string $affiliateId): self
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCampaignId(): ?string
    {
        return $this->campaignId;
    }

    /**
     * @param string|null $campaignId
     *
     * @return TblCartCoupons
     */
    public function setCampaignId(?string $campaignId): self
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblCartCoupons
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblCartCoupons
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return TblProductType|null
     */
    public function getIdProductType(): ?TblProductType
    {
        return $this->idProductType;
    }

    /**
     * @param TblProductType|null $idProductType
     *
     * @return TblCartCoupons
     */
    public function setIdProductType(?TblProductType $idProductType): self
    {
        $this->idProductType = $idProductType;

        return $this;
    }

    /**
     * @return TblProducts|null
     */
    public function getIdProduct(): ?TblProducts
    {
        return $this->idProduct;
    }

    /**
     * @param TblProducts|null $idProduct
     *
     * @return TblCartCoupons
     */
    public function setIdProduct(?TblProducts $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblCartCoupons
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblCartCoupons
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }
}
