<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCountries
 *
 * @ORM\Table(name="tbl_countries",
 *     indexes={@ORM\Index(name="FK_tbl_countries_tbl_users_2", columns={"updated_by"}),
 *     @ORM\Index(name="FK_tbl_countries_tbl_users", columns={"created_by"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class TblCountries
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const SHIPPING = 1;
    const NOT_SHIPPING = 1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $countryCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=45, nullable=false)
     */
    private $countryName = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_shipping", type="boolean", nullable=true)
     */
    private $isShipping = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isDelete = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }

    /**
     * @param string $countryName
     */
    public function setCountryName(string $countryName): void
    {
        $this->countryName = $countryName;
    }

    /**
     * @return bool|null
     */
    public function getisShipping(): ?bool
    {
        return $this->isShipping;
    }

    /**
     * @param bool|null $isShipping
     */
    public function setIsShipping(?bool $isShipping): void
    {
        $this->isShipping = $isShipping;
    }

    /**
     * @return bool|null
     */
    public function getisDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool|null $isDelete
     */
    public function setIsDelete(?bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return bool|null
     */
    public function getisActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate(): ?\DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate(?\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate(): ?\DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate(?\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return\User
     */
    public function getUpdatedBy(): User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getCountryName()." | ". $this->getCountryCode();
    }
}
