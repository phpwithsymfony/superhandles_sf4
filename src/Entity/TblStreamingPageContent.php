<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblStreamingPageContent
 *
 * @ORM\Table(name="tbl_streaming_page_content",
 *     indexes={@ORM\Index(name="FK_tbl_streaming_page_content_tbl_users", columns={"created_by"}),
 *     @ORM\Index(name="idx_streamingpage_id", columns={"id_streaming_page"}),
 *     @ORM\Index(name="FK_tbl_streaming_page_content_tbl_users_2", columns={"updated_by"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\StreamingContentRepository")
 */
class TblStreamingPageContent
{
    const _ACTIVE = 1;
    const _INACTIVE = 0;
    const _DELETE = 1;
    const _NOTDELETE = 0;
    const _SETWORKOUTDISPLAY = 1;
    const _UNSETWORKOUTDISPLAY = 0;
    const _SETWORKOUT = 1;
    const _UNSETWORKOUT = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var string|null
     *
     * @ORM\Column(name="audio", type="text", length=0, nullable=true)
     */
    private $audio;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = self::_ACTIVE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true, options={"default"="0"})
     */
    private $isDelete = self::_NOTDELETE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_workout_display", type="boolean", nullable=true)
     */
    private $isWorkoutDisplay = self::_SETWORKOUTDISPLAY;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_workout", type="boolean", nullable=true)
     */
    private $isWorkout = self::_SETWORKOUT;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var TblStreamingPages
     *
     * @ORM\ManyToOne(targetEntity="TblStreamingPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_streaming_page", referencedColumnName="id")
     * })
     */
    private $idStreamingPage;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @ORM\OneToMany(targetEntity="TblStreamingPdfDoc", cascade={"persist"}, mappedBy="idStreamingPageContent")
     */
    private $streamingPageContentFiles;

    public function __construct()
    {
        $this->streamingPageContentFiles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @param string|null $video
     */
    public function setVideo(?string $video): void
    {
        $this->video = $video;
    }

    /**
     * @return string|null
     */
    public function getAudio(): ?string
    {
        return $this->audio;
    }

    /**
     * @param string|null $audio
     */
    public function setAudio(?string $audio): void
    {
        $this->audio = $audio;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isIsDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return bool
     */
    public function isWorkoutDisplay(): bool
    {
        return $this->isWorkoutDisplay;
    }

    /**
     * @param bool $isWorkoutDisplay
     */
    public function setIsWorkoutDisplay(bool $isWorkoutDisplay): void
    {
        $this->isWorkoutDisplay = $isWorkoutDisplay;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }


    /**
     * @param $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDate(): ?\DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setCreatedDate(?\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDate(): ?\DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime|null $updatedDate
     */
    public function setUpdatedDate(?\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return TblStreamingPages
     */
    public function getIdStreamingPage(): ?TblStreamingPages
    {
        return $this->idStreamingPage;
    }

    /**
     * @param TblStreamingPages $idStreamingPage
     */
    public function setIdStreamingPage(?TblStreamingPages $idStreamingPage): void
    {
        $this->idStreamingPage = $idStreamingPage;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(?User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(?User $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return bool
     */
    public function isWorkout(): bool
    {
        return $this->isWorkout;
    }

    /**
     * @param bool $isWorkout
     */
    public function setIsWorkout(bool $isWorkout): void
    {
        $this->isWorkout = $isWorkout;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @return bool|null
     */
    public function getIsWorkoutDisplay(): ?bool
    {
        return $this->isWorkoutDisplay;
    }

    /**
     * @return bool|null
     */
    public function getIsWorkout(): ?bool
    {
        return $this->isWorkout;
    }

    /**
     * @return Collection|TblStreamingPdfDoc[]
     */
    public function getStreamingPageContentFiles(): Collection
    {
        return $this->streamingPageContentFiles;
    }

    /**
     * @param TblStreamingPdfDoc $streamingPageContentFile
     * @return TblStreamingPageContent
     */
    public function addStreamingPageContentFile(TblStreamingPdfDoc $streamingPageContentFile): self
    {
        if (!$this->streamingPageContentFiles->contains($streamingPageContentFile)) {
            $this->streamingPageContentFiles[] = $streamingPageContentFile;
            $streamingPageContentFile->setIdStreamingPageContent($this);
        }

        return $this;
    }

    /**
     * @param TblStreamingPdfDoc $streamingPageContentFile
     * @return TblStreamingPageContent
     */
    public function removeStreamingPageContentFile(TblStreamingPdfDoc $streamingPageContentFile): self
    {
        if ($this->streamingPageContentFiles->contains($streamingPageContentFile)) {
            $this->streamingPageContentFiles->removeElement($streamingPageContentFile);
            // set the owning side to null (unless already changed)
            if ($streamingPageContentFile->getIdStreamingPageContent() === $this) {
                $streamingPageContentFile->setIdStreamingPageContent(null);
            }
        }

        return $this;
    }
}
