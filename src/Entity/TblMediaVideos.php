<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblMediaVideos
 *
 * @ORM\Table(name="tbl_media_videos",
 *     indexes={@ORM\Index(name="IDX_6B80847A16FE72E1", columns={"updated_by"}),
 *     @ORM\Index(name="IDX_6B80847ADE12AB56", columns={"created_by"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 */
class TblMediaVideos
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const DELETE = 1;
    const NOTDELETE = 0;

    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';

    const MEDIAVIDEO = 1;
    const VICTIMVIDEO = 2;

    const MEDIAVIDEO_LABEL = 'Media Video';
    const VICTIMVIDEO_LABEL = 'Victim Video';

    const STATUS_ARRAY = [
        self::MEDIAVIDEO => self::MEDIAVIDEO_LABEL,
        self::VICTIMVIDEO => self::VICTIMVIDEO_LABEL
    ];
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter video title"
     * )
     * @ORM\Column(name="video_title", type="string", length=255, nullable=false)
     */
    private $videoTitle;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter description"
     * )
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter video url"
     * )
     * @ORM\Column(name="video_url", type="string", length=255, nullable=false)
     */
    private $videoUrl;

    /**
     * @var int
     * @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="is_victim_video", type="smallint", nullable=false)
     */
    private $isVictimVideo;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = self::NOTDELETE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = self::ACTIVE;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var int
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getVideoTitle(): ?string
    {
        return $this->videoTitle;
    }

    /**
     * @param string $videoTitle
     *
     * @return TblMediaVideos
     */
    public function setVideoTitle(string $videoTitle): self
    {
        $this->videoTitle = $videoTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return TblMediaVideos
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    /**
     * @param string $videoUrl
     *
     * @return TblMediaVideos
     */
    public function setVideoUrl(string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsVictimVideo(): ?int
    {
        return $this->isVictimVideo;
    }

    /**
     * @param int $isVictimVideo
     *
     * @return TblMediaVideos
     */
    public function setIsVictimVideo(int $isVictimVideo): self
    {
        $this->isVictimVideo = $isVictimVideo;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     *
     * @return TblMediaVideos
     */
    public function setIsDelete(bool $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblMediaVideos
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblMediaVideos
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblMediaVideos
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param $sortOrder
     *
     * @return TblMediaVideos
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblMediaVideos
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblMediaVideos
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getVideoTitle();
    }
}
