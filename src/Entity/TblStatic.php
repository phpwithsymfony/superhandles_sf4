<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblStatic
 *
 * @ORM\Table(name="tbl_static",
 *     indexes={
 *     @ORM\Index(name="fk_tbl_static_1_idx", columns={"created_by"}),
 *     @ORM\Index(name="fk_tbl_static_2_idx", columns={"updated_by"})
 * })
 * @ORM\Entity
 * @UniqueEntity("pageUrl")
 *
 * @ORM\Entity(repositoryClass="App\Repository\StaticRepository")
 */
class TblStatic
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const ACTIVE_LABEL = 'Active';
    const INACTIVE_LABEL = 'In-active';
    const DELETE = 1;
    const NOT_DELETE = 0;

    const STATUS_ARRAY = [
        self::ACTIVE => self::ACTIVE_LABEL,
        self::INACTIVE => self::INACTIVE_LABEL
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page_title", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $pageTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page_content", type="text", length=65535, nullable=true)
     * @Assert\NotBlank()
     */
    private $pageContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page_url", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     *
     */
    private $pageUrl;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     * @Assert\NotBlank()
     */
    private $isActive = self::ACTIVE;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=true, options={"default" : false})
     */
    private $isDelete = self::NOT_DELETE;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $dateUpdated;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param null|string $pageTitle
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    /**
     * @return null|string
     */
    public function getPageContent()
    {
        return $this->pageContent;
    }

    /**
     * @param null|string $pageContent
     */
    public function setPageContent($pageContent)
    {
        $this->pageContent = $pageContent;
    }

    /**
     * @return null|string
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param null|string $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return null|string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param null|string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return null|string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param null|string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return null|string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }

    /**
     * @param null|string $pageUrl
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $is_delete
     */
    public function setIsDelete($is_delete)
    {
        $this->isDelete = $is_delete;
    }

    /**
     * @return \DateTime|null
     */
    public function getcreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime|null $createdDate
     */
    public function setcreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * Get createdBy
     *
     * @return \App\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param \App\Entity\User $createdBy
     *
     * @return $this
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime|null $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
    * Get updatedBy
    *
    * @return \App\Entity\User
    */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return $this
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }
}
