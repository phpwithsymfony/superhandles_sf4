<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblCamps
 *
 * @ORM\Table(name="tbl_camps",
 *     indexes={@ORM\Index(name="IDX_937221FADE12AB56", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_camps_tbl_camps_category", columns={"id_camps_category"}),
 *     @ORM\Index(name="IDX_937221FA16FE72E1", columns={"updated_by"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CampsRepository")
 */
class TblCamps
{
    const _ACTIVE = 1;
    const _INACTIVE = 0;
    const _DELETE = 1;
    const _NOTDELETE = 0;

    const _ACTIVE_LABEL = 'Active';
    const _INACTIVE_LABEL = 'In-active';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Please enter title"
     * )
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please enter description"
     * )
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="event_date_time", type="datetime", nullable=true)
     */
    private $eventDateTime;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please enter event place"
     * )
     * @ORM\Column(name="event_place", type="string", length=100, nullable=true)
     */
    private $eventPlace;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please enter contact email"
     * )
     * @ORM\Column(name="contact_email", type="string", length=200, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please enter contact phone"
     * )
     * @ORM\Column(name="contact_phone", type="string", length=45, nullable=true)
     */
    private $contactPhone;

    /**
     * @var string|null
     * @Assert\NotBlank(
     *     message="Please enter additional info"
     * )
     * @ORM\Column(name="additional_info", type="text", length=0, nullable=true)
     */
    private $additionalInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_url", type="string", length=200, nullable=false)
     */
    private $youtubeUrl;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = self::_NOTDELETE;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = self::_ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(name="sort_order", type="smallint", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     message="Please use only positive numbers."
     * )
     */
    private $sortOrder;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @var TblCampsCategory
     *
     * @ORM\ManyToOne(targetEntity="TblCampsCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_camps_category", referencedColumnName="id")
     * })
     */
    private $idCampsCategory;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TblCamps
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return TblCamps
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEventDateTime()
    {
        return $this->eventDateTime;
    }

    /**
     * @param \DateTimeInterface|null $eventDateTime
     *
     * @return TblCamps
     */
    public function setEventDateTime($eventDateTime): self
    {
        $this->eventDateTime = $eventDateTime;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEventPlace(): ?string
    {
        return $this->eventPlace;
    }

    /**
     * @param string|null $eventPlace
     *
     * @return TblCamps
     */
    public function setEventPlace(?string $eventPlace): self
    {
        $this->eventPlace = $eventPlace;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    /**
     * @param string|null $contactEmail
     *
     * @return TblCamps
     */
    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * @param string|null $contactPhone
     *
     * @return TblCamps
     */
    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    /**
     * @param string|null $additionalInfo
     *
     * @return TblCamps
     */
    public function setAdditionalInfo(?string $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getYoutubeUrl(): ?string
    {
        return $this->youtubeUrl;
    }

    /**
     * @param string $youtubeUrl
     *
     * @return TblCamps
     */
    public function setYoutubeUrl(string $youtubeUrl): self
    {
        $this->youtubeUrl = $youtubeUrl;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDelete(): ?bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     *
     * @return TblCamps
     */
    public function setIsDelete(bool $isDelete): self
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return TblCamps
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param  $sortOrder
     *
     * @return TblCamps
     */
    public function setSortOrder($sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblCamps
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblCamps
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblCamps
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     *
     * @return TblCamps
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return TblCampsCategory|null
     */
    public function getIdCampsCategory(): ?TblCampsCategory
    {
        return $this->idCampsCategory;
    }

    /**
     * @param TblCampsCategory|null $idCampsCategory
     *
     * @return TblCamps
     */
    public function setIdCampsCategory(?TblCampsCategory $idCampsCategory): self
    {
        $this->idCampsCategory = $idCampsCategory;

        return $this;
    }
}
