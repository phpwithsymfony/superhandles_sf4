<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblOrders
 *
 * @ORM\Table(name="tbl_orders",
 *     indexes={@ORM\Index(name="coupon_id", columns={"id_cart_coupon"}),
 *     @ORM\Index(name="shipping_address_id", columns={"id_order_address"}),
 *     @ORM\Index(name="FK_tbl_orders_tbl_users_2", columns={"updated_by"}),
 *     @ORM\Index(name="customer_id", columns={"id_customer"}),
 *     @ORM\Index(name="user_id", columns={"id_user"}),
 *     @ORM\Index(name="FK_tbl_orders_tbl_payment_methods", columns={"id_payment_methods"})})
 * @ORM\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class TblOrders
{
    const STATUS_CONFIRM = 0;
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;
    const STATUS_ONHOLD = 4;
    const STATUS_PROCESSING = 5;
    const STATUS_CANCEL = 6;

    const _DELETE = 1;
    const _NOTDELETE = 0;

    const STATUS_ARRAY = [
        self::STATUS_CONFIRM => 'Confirm',
        self::STATUS_PENDING => 'Pending',
        self::STATUS_SUCCESS => 'Success',
        self::STATUS_FAILED => 'Failed',
        self::STATUS_ONHOLD => 'On hold',
        self::STATUS_PROCESSING => 'Processing',
        self::STATUS_CANCEL => 'Cancel'
    ];
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="TblOrderDetails", mappedBy="idOrder")
     */
    private $orderDetails;

    public function __construct()
    {
        $this->orderDetails = new ArrayCollection();
    }


    /**
     * @return Collection
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=7, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $totalAmount = '0.00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="discount_amount", type="decimal",
     *     precision=5, scale=2, nullable=true, options={"default"="0.00"}
     * )
     */
    private $discountAmount = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_amount", type="decimal",
     *     precision=5, scale=2, nullable=false, options={"default"="0.00"}
     * )
     */
    private $shippingAmount = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="final_order_amount", type="decimal",
     *     precision=5, scale=2, nullable=false, options={"default"="0.00"}
     * )
     */
    private $finalOrderAmount = '0.00';

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"default"="0"})
     */
    private $status = self::STATUS_CONFIRM;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_time", type="time", nullable=false)
     */
    private $orderTime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_transaction", type="string", length=100, nullable=true)
     */
    private $idTransaction;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedDate;

    /**
     * @var bool
     * @ORM\Column(name="is_delete", type="boolean", nullable=false)
     */
    private $isDelete = self::_NOTDELETE;

    /**
     * @var TblCartCoupons
     *
     * @ORM\ManyToOne(targetEntity="TblCartCoupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart_coupon", referencedColumnName="id")
     * })
     */
    private $idCartCoupon;

    /**
     * @var TblCustomer
     *
     * @ORM\ManyToOne(targetEntity="TblCustomer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     * })
     */
    private $idCustomer;

    /**
     * @var TblOrderAddress
     *
     * @ORM\ManyToOne(targetEntity="TblOrderAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_order_address", referencedColumnName="id")
     * })
     */
    private $idOrderAddress;

    /**
     * @var TblPaymentMethods
     *
     * @ORM\ManyToOne(targetEntity="TblPaymentMethods")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_methods", referencedColumnName="id")
     * })
     */
    private $idPaymentMethods;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param $totalAmount
     *
     * @return TblOrders
     */
    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * @param $discountAmount
     *
     * @return TblOrders
     */
    public function setDiscountAmount($discountAmount): self
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAmount()
    {
        return $this->shippingAmount;
    }

    /**
     * @param $shippingAmount
     *
     * @return TblOrders
     */
    public function setShippingAmount($shippingAmount): self
    {
        $this->shippingAmount = $shippingAmount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return TblOrders
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getOrderTime(): ?\DateTimeInterface
    {
        return $this->orderTime;
    }

    /**
     * @param \DateTimeInterface $orderTime
     *
     * @return TblOrders
     */
    public function setOrderTime(\DateTimeInterface $orderTime): self
    {
        $this->orderTime = $orderTime;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdTransaction(): ?string
    {
        return $this->idTransaction;
    }

    /**
     * @param string|null $idTransaction
     *
     * @return TblOrders
     */
    public function setIdTransaction(?string $idTransaction): self
    {
        $this->idTransaction = $idTransaction;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface|null $createdDate
     *
     * @return TblOrders
     */
    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface|null $updatedDate
     *
     * @return TblOrders
     */
    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return TblCartCoupons|null
     */
    public function getIdCartCoupon(): ?TblCartCoupons
    {
        return $this->idCartCoupon;
    }

    /**
     * @param TblCartCoupons|null $idCartCoupon
     *
     * @return TblOrders
     */
    public function setIdCartCoupon(?TblCartCoupons $idCartCoupon): self
    {
        $this->idCartCoupon = $idCartCoupon;

        return $this;
    }

    /**
     * @return TblCustomer|null
     */
    public function getIdCustomer(): ?TblCustomer
    {
        return $this->idCustomer;
    }

    /**
     * @param TblCustomer|null $idCustomer
     *
     * @return TblOrders
     */
    public function setIdCustomer(?TblCustomer $idCustomer): self
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * @return TblOrderAddress|null
     */
    public function getIdOrderAddress(): ?TblOrderAddress
    {
        return $this->idOrderAddress;
    }

    /**
     * @param TblOrderAddress|null $idOrderAddress
     *
     * @return TblOrders
     */
    public function setIdOrderAddress(?TblOrderAddress $idOrderAddress): self
    {
        $this->idOrderAddress = $idOrderAddress;

        return $this;
    }

    /**
     * @return TblPaymentMethods|null
     */
    public function getIdPaymentMethods(): ?TblPaymentMethods
    {
        return $this->idPaymentMethods;
    }

    /**
     * @param TblPaymentMethods|null $idPaymentMethods
     *
     * @return TblOrders
     */
    public function setIdPaymentMethods(?TblPaymentMethods $idPaymentMethods): self
    {
        $this->idPaymentMethods = $idPaymentMethods;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    /**
     * @param User|null $idUser
     *
     * @return TblOrders
     */
    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     *
     * @return TblOrders
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return string
     */
    public function getFinalOrderAmount()
    {
        return $this->finalOrderAmount;
    }


    /**
     * @param $finalOrderAmount
     *
     * @return TblOrders
     */
    public function setFinalOrderAmount($finalOrderAmount): self
    {
        $this->finalOrderAmount = $finalOrderAmount;
        return $this;
    }
}
