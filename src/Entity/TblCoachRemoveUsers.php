<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCoachRemoveUsers
 *
 * @ORM\Table(name="tbl_coach_remove_users",
 *     indexes={@ORM\Index(name="FK_tbl_coach_remove_users_tbl_users_2", columns={"created_by"}),
 *     @ORM\Index(name="FK_tbl_coach_remove_users_tbl_users", columns={"id_user"})
 * })
 * @ORM\Entity
 */
class TblCoachRemoveUsers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;


}
