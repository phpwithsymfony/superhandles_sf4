<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * TblProductLaunch
     *
     * @ORM\Table(name="tbl_product_launch",
     *      indexes={
     *          @ORM\Index(name="fk_tbl_product_launch_1_idx", columns={"created_by"}),
     *          @ORM\Index(name="fk_tbl_product_launch_2_idx", columns={"id_product"}),
     *          @ORM\Index(name="fk_tbl_product_launch_3_idx", columns={"updated_by"})
     *      }
     * )
     *
     * @ORM\Entity(repositoryClass="App\Repository\ProductLaunchRepository")
     */
    class TblProductLaunch
    {
        public const ACTIVE = 1;
        public const INACTIVE = 0;
        public const DELETE = 1;
        public const NOTDELETE = 0;

        public const ACTIVE_LABEL = 'Active';
        public const INACTIVE_LABEL = 'In-active';
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private $id;

        /**
         * @var User
         *
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
         * })
         */
        private $createdBy;

        /**
         * @var User
         *
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
         * })
         */
        private $updatedBy;

        /**
         * @var TblProducts
         *
         * @ORM\ManyToOne(targetEntity="TblProducts")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
         * })
         */
        private $idProduct;

        /**
         * @var string
         *
         * @ORM\Column(name="headline_title", type="text", length=0, nullable=true)
         */
        private $headlineTitle;

        /**
         * @var int
         *
         * @ORM\Column(name="headline_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $headlineEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="discount_text", type="string", length=100, nullable=true)
         */
        private $discountText;

        /**
         * @var int
         *
         * @ORM\Column(name="discount", type="integer", length=3, nullable=true)
         */
        private $discount;

        /**
         * @var string
         *
         * @ORM\Column(name="discount_price", type="decimal", precision=5, scale=2, nullable=true)
         */
        private $discountPrice;

        /**
         * @var int
         *
         * @ORM\Column(name="discount_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $discountEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="product_title", type="string", length=200, nullable=true)
         */
        private $productTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="product_description_title", type="string", length=200, nullable=true)
         */
        private $productDescriptionTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="product_description", type="text", length=0, nullable=true)
         */
        private $productDescription;

        /**
         * @var int
         *
         * @ORM\Column(name="product_description_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $productDescriptionEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="product_image", type="string", length=100, nullable=true)
         */
        private $productImage;

        /**
         * Image = 0 and Video = 1
         * @var int
         *
         * @ORM\Column(name="banner_type", type="boolean", nullable=false, options={"default"="0"})
         */
        private $bannerType = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="video_id", type="string", length=100, nullable=true)
         */
        private $videoId;

        /**
         * @var string
         *
         * @ORM\Column(name="image_url", type="string", length=100, nullable=true)
         */
        private $imageUrl;

        /**
         * @var int
         *
         * @ORM\Column(name="media_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $mediaEnable = '0';

        /**
         * @var \DateTime|null
         *
         * @ORM\Column(name="launching_date", type="datetime", nullable=true)
         */
        private $launchingDate;

        /**
         * @var int
         *
         * @ORM\Column(name="sale_days", type="integer", length=3, nullable=true)
         */
        private $saleDays;

        /**
         * @var string
         *
         * @ORM\Column(name="join_now_button_text1", type="string", length=100, nullable=true)
         */
        private $joinNowButtonText1;

        /**
         * @var string
         *
         * @ORM\Column(name="product_popup_text", type="string", length=100, nullable=true)
         */
        private $productPopupText;

        /**
         * @var string
         *
         * @ORM\Column(name="dvd_product_popup_text", type="string", length=100, nullable=true)
         */
        private $dvdProductPopupText;

        /**
         * @var int
         *
         * @ORM\Column(name="first_join_now_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $firstJoinNowEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="join_now_button_text2", type="string", length=100, nullable=true)
         */
        private $joinNowButtonText2;

        /**
         * @var int
         *
         * @ORM\Column(name="second_join_now_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $secondJoinNowEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="first_box_title", type="string", length=100, nullable=true)
         */
        private $firstBoxTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="first_box_description", type="text", length=0, nullable=true)
         */
        private $firstBoxDescription;

        /**
         * @var int
         *
         * @ORM\Column(name="first_box_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $firstBoxEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="second_box_title", type="string", length=100, nullable=true)
         */
        private $secondBoxTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="second_box_description", type="text", length=0, nullable=true)
         */
        private $secondBoxDescription;

        /**
         * @var int
         *
         * @ORM\Column(name="second_box_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $secondBoxEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_title", type="string", length=100, nullable=true)
         */
        private $featureboxTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_image1", type="string", length=100, nullable=true, options={"default"="featureboxImage1.png"})
         */
        private $featureboxImage1 = 'featureboxImage1.png';

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_image2", type="string", length=100, nullable=true, options={"default"="featureboxImage2.png"})
         */
        private $featureboxImage2 = 'featureboxImage2.png';

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_image3", type="string", length=100, nullable=true, options={"default"="featureboxImage3.png"})
         */
        private $featureboxImage3 = 'featureboxImage3.png';

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_image4", type="string", length=100, nullable=true, options={"default"="featureboxImage4.png"})
         */
        private $featureboxImage4 = 'featureboxImage4.png';

        /**
         * @var string
         *
         * @ORM\Column(name="featurebox_image5", type="string", length=100, nullable=true, options={"default"="featureboxImage5.png"})
         */
        private $featureboxImage5 = 'featureboxImage5.png';

        /**
         * @var int
         *
         * @ORM\Column(name="featurebox_image_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $featureboxImageEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_title1", type="string", length=100, nullable=true)
         */
        private $testimonialTitle1;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_image1", type="string", length=100, nullable=true)
         */
        private $testimonialImage1;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_description1", type="text", length=0, nullable=true)
         */
        private $testimonialDescription1;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_title2", type="string", length=100, nullable=true)
         */
        private $testimonialTitle2;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_image2", type="string", length=100, nullable=true)
         */
        private $testimonialImage2;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_description2", type="text", length=0, nullable=true)
         */
        private $testimonialDescription2;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_title3", type="string", length=100, nullable=true)
         */
        private $testimonialTitle3;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_image3", type="string", length=100, nullable=true)
         */
        private $testimonialImage3;

        /**
         * @var string
         *
         * @ORM\Column(name="testimonial_description3", type="text", length=0, nullable=true)
         */
        private $testimonialDescription3;

        /**
         * @var int
         *
         * @ORM\Column(name="testimonial_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $testimonialEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="after_testimonial_title", type="string", length=200, nullable=true)
         */
        private $afterTestimonialTitle;

        /**
         * @var string
         *
         * @ORM\Column(name="after_testimonial_description", type="text", length=0, nullable=true)
         */
        private $afterTestimonialDescription;

        /**
         * @var int
         *
         * @ORM\Column(name="after_testimonial_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $afterTestimonialEnable = '0';

        /**
         * @var int
         *
         * @ORM\Column(name="need_subscription", type="boolean", nullable=false, options={"default"="2"})
         */
        private $needSubscription = '1';

        /**
         * @var string
         *
         * @ORM\Column(name="subscription_checkbox_text", type="string", length=200, nullable=true)
         */
        private $subscriptionCheckboxText;

        /**
         * @var string
         *
         * @ORM\Column(name="url", type="string", length=250, nullable=true)
         */
        private $url;

        /**
         * @var int
         *
         * @ORM\Column(name="is_counter_enable", type="boolean", nullable=false, options={"default"="0"})
         */
        private $isCounterEnable = '0';

        /**
         * @var string
         *
         * @ORM\Column(name="counter_script_url", type="string", length=200, nullable=true)
         */
        private $counterScriptUrl;

        /**
         * @var string
         *
         * @ORM\Column(name="counter_script_small", type="string", length=250, nullable=true)
         */
        private $counterScriptSmall;

        /**
         * @var string
         *
         * @ORM\Column(name="counter_title", type="string", length=200, nullable=true)
         */
        private $counterTitle;

        /**
         * @var int
         *
         * @ORM\Column(name="add_dvd_product", type="boolean", nullable=false, options={"default"="0"})
         */
        private $addDvdProduct = '0';

        /**
         * @var TblProducts
         *
         * @ORM\ManyToOne(targetEntity="TblProducts")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="dvd_product", referencedColumnName="id")
         * })
         */
        private $dvdProduct;

        /**
         * @var string
         *
         * @ORM\Column(name="footer_text", type="string", length=255, nullable=true)
         */
        private $footerText;

        /**
         * @var string
         *
         * @ORM\Column(name="footer_link", type="string", length=255, nullable=true)
         */
        private $footerLink;

        /**
         * @var string
         *
         * @ORM\Column(name="redirect_link", type="string", length=255, nullable=true)
         */
        private $redirectLink;

        /**
         * @var int
         *
         * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
         */
        private $isActive = '1';

        /**
         * @var bool
         *
         * @ORM\Column(name="is_delete", type="boolean", nullable=false, options={"default"="0"})
         */
        private $isDelete = '0';

        /**
         * @var \DateTime|null
         *
         * @ORM\Column(name="created_date", type="datetime", nullable=true)
         */
        private $createdDate;

        /**
         * @var \DateTime|null
         *
         * @ORM\Column(name="updated_date", type="datetime", nullable=true)
         */
        private $updatedDate;

        /**
         * @return int|null
         */
        public function getId(): ?int
        {
            return $this->id;
        }

        /**
         * @return User|null
         */
        public function getCreatedBy(): ?User
        {
            return $this->createdBy;
        }

        /**
         * @param User|null $createdBy
         *
         * @return TblProductLaunch
         */
        public function setCreatedBy(?User $createdBy): self
        {
            $this->createdBy = $createdBy;

            return $this;
        }

        /**
         * @return User|null
         */
        public function getUpdatedBy(): ?User
        {
            return $this->updatedBy;
        }

        /**
         * @param User|null $updatedBy
         *
         * @return TblProductLaunch
         */
        public function setUpdatedBy(?User $updatedBy): self
        {
            $this->updatedBy = $updatedBy;

            return $this;
        }

        /**
         * @return TblProducts|null
         */
        public function getIdProduct(): ?TblProducts
        {
            return $this->idProduct;
        }

        /**
         * @param TblProducts|null $idProduct
         *
         * @return TblProductLaunch
         */
        public function setIdProduct(?TblProducts $idProduct): self
        {
            $this->idProduct = $idProduct;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getHeadlineTitle(): ?string
        {
            return $this->headlineTitle;
        }

        /**
         * @param string $headlineTitle
         * @return TblProductLaunch
         */
        public function setHeadlineTitle(string $headlineTitle): self
        {
            $this->headlineTitle = $headlineTitle;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getHeadlineEnable(): ?bool
        {
            return $this->headlineEnable;
        }

        /**
         * @param bool $headlineEnable
         * @return TblProductLaunch
         */
        public function setHeadlineEnable(bool $headlineEnable): self
        {
            $this->headlineEnable = $headlineEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getDiscountText(): ?string
        {
            return $this->discountText;
        }

        /**
         * @param string $discountText
         * @return TblProductLaunch
         */
        public function setDiscountText(string $discountText): self
        {
            $this->discountText = $discountText;

            return $this;
        }

        /**
         * @return int|null
         */
        public function getDiscount(): ?int
        {
            return $this->discount;
        }

        /**
         * @param int $discount
         * @return TblProductLaunch
         */
        public function setDiscount(int $discount): self
        {
            $this->discount = $discount;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getDiscountPrice(): ?string
        {
            return $this->discountPrice;
        }

        /**
         * @param string $discountPrice
         * @return TblProductLaunch
         */
        public function setDiscountPrice(string $discountPrice): self
        {
            $this->discountPrice = $discountPrice;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getDiscountEnable(): ?bool
        {
            return $this->discountEnable;
        }

        /**
         * @param bool $discountEnable
         * @return TblProductLaunch
         */
        public function setDiscountEnable(bool $discountEnable): self
        {
            $this->discountEnable = $discountEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getProductTitle(): ?string
        {
            return $this->productTitle;
        }

        /**
         * @param string $productTitle
         * @return TblProductLaunch
         */
        public function setProductTitle(string $productTitle): self
        {
            $this->productTitle = $productTitle;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getProductDescriptionTitle(): ?string
        {
            return $this->productDescriptionTitle;
        }

        /**
         * @param string $productDescriptionTitle
         * @return TblProductLaunch
         */
        public function setProductDescriptionTitle(string $productDescriptionTitle): self
        {
            $this->productDescriptionTitle = $productDescriptionTitle;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getProductDescription(): ?string
        {
            return $this->productDescription;
        }

        /**
         * @param string $productDescription
         * @return TblProductLaunch
         */
        public function setProductDescription(string $productDescription): self
        {
            $this->productDescription = $productDescription;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getProductDescriptionEnable(): ?bool
        {
            return $this->productDescriptionEnable;
        }

        /**
         * @param bool $productDescriptionEnable
         * @return TblProductLaunch
         */
        public function setProductDescriptionEnable(bool $productDescriptionEnable): self
        {
            $this->productDescriptionEnable = $productDescriptionEnable;

            return $this;
        }

        /**
         * @return string
         */
        public function getProductImage()
        {
            return $this->productImage;
        }

        /**
         * @param $productImage
         */
        public function setProductImage($productImage)
        {
            $this->productImage = $productImage;
        }

        /**
         * @return bool|null
         */
        public function getBannerType(): ?bool
        {
            return $this->bannerType;
        }

        /**
         * @param bool $bannerType
         * @return TblProductLaunch
         */
        public function setBannerType(bool $bannerType): self
        {
            $this->bannerType = $bannerType;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getVideoId(): ?string
        {
            return $this->videoId;
        }

        /**
         * @param string $videoId
         * @return TblProductLaunch
         */
        public function setVideoId(string $videoId): self
        {
            $this->videoId = $videoId;

            return $this;
        }

        /**
         * @return string
         */
        public function getImageUrl()
        {
            return $this->imageUrl;
        }

        /**
         * @param $imageUrl
         */
        public function setImageUrl($imageUrl)
        {
            $this->imageUrl = $imageUrl;
        }

        /**
         * @return bool|null
         */
        public function getMediaEnable(): ?bool
        {
            return $this->mediaEnable;
        }

        /**
         * @param bool $mediaEnable
         * @return TblProductLaunch
         */
        public function setMediaEnable(bool $mediaEnable): self
        {
            $this->mediaEnable = $mediaEnable;

            return $this;
        }

        /**
         * @return \DateTimeInterface|null
         */
        public function getLaunchingDate(): ?\DateTimeInterface
        {
            return $this->launchingDate;
        }

        /**
         * @param \DateTimeInterface|null $launchingDate
         * @return TblProductLaunch
         */
        public function setLaunchingDate(?\DateTimeInterface $launchingDate): self
        {
            $this->launchingDate = $launchingDate;

            return $this;
        }

        /**
         * @return int|null
         */
        public function getSaleDays(): ?int
        {
            return $this->saleDays;
        }

        /**
         * @param int $saleDays
         * @return TblProductLaunch
         */
        public function setSaleDays(int $saleDays): self
        {
            $this->saleDays = $saleDays;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getJoinNowButtonText1(): ?string
        {
            return $this->joinNowButtonText1;
        }

        /**
         * @param string $joinNowButtonText1
         * @return TblProductLaunch
         */
        public function setJoinNowButtonText1(string $joinNowButtonText1): self
        {
            $this->joinNowButtonText1 = $joinNowButtonText1;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getProductPopupText(): ?string
        {
            return $this->productPopupText;
        }

        /**
         * @param string $productPopupText
         * @return TblProductLaunch
         */
        public function setProductPopupText(string $productPopupText): self
        {
            $this->productPopupText = $productPopupText;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getDvdProductPopupText(): ?string
        {
            return $this->dvdProductPopupText;
        }

        /**
         * @param string $dvdProductPopupText
         * @return TblProductLaunch
         */
        public function setDvdProductPopupText(string $dvdProductPopupText): self
        {
            $this->dvdProductPopupText = $dvdProductPopupText;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getFirstJoinNowEnable(): ?bool
        {
            return $this->firstJoinNowEnable;
        }

        /**
         * @param bool $firstJoinNowEnable
         * @return TblProductLaunch
         */
        public function setFirstJoinNowEnable(bool $firstJoinNowEnable): self
        {
            $this->firstJoinNowEnable = $firstJoinNowEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getJoinNowButtonText2(): ?string
        {
            return $this->joinNowButtonText2;
        }

        /**
         * @param string $joinNowButtonText2
         * @return TblProductLaunch
         */
        public function setJoinNowButtonText2(string $joinNowButtonText2): self
        {
            $this->joinNowButtonText2 = $joinNowButtonText2;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getSecondJoinNowEnable(): ?bool
        {
            return $this->secondJoinNowEnable;
        }

        /**
         * @param bool $secondJoinNowEnable
         * @return TblProductLaunch
         */
        public function setSecondJoinNowEnable(bool $secondJoinNowEnable): self
        {
            $this->secondJoinNowEnable = $secondJoinNowEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getFirstBoxTitle(): ?string
        {
            return $this->firstBoxTitle;
        }

        /**
         * @param string $firstBoxTitle
         * @return TblProductLaunch
         */
        public function setFirstBoxTitle(string $firstBoxTitle): self
        {
            $this->firstBoxTitle = $firstBoxTitle;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getFirstBoxDescription(): ?string
        {
            return $this->firstBoxDescription;
        }

        /**
         * @param string $firstBoxDescription
         * @return TblProductLaunch
         */
        public function setFirstBoxDescription(string $firstBoxDescription): self
        {
            $this->firstBoxDescription = $firstBoxDescription;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getFirstBoxEnable(): ?bool
        {
            return $this->firstBoxEnable;
        }

        /**
         * @param bool $firstBoxEnable
         * @return TblProductLaunch
         */
        public function setFirstBoxEnable(bool $firstBoxEnable): self
        {
            $this->firstBoxEnable = $firstBoxEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getSecondBoxTitle(): ?string
        {
            return $this->secondBoxTitle;
        }

        /**
         * @return null|string
         */
        public function setSecondBoxTitle(string  $secondBoxTitle): string
        {
            return $this->secondBoxTitle=$secondBoxTitle;
        }

        /**
         * @return null|string
         */
        public function getSecondBoxDescription(): ?string
        {
            return $this->secondBoxDescription;
        }

        /**
         * @param string $secondBoxDescription
         * @return TblProductLaunch
         */
        public function setSecondBoxDescription(string $secondBoxDescription): self
        {
            $this->secondBoxDescription = $secondBoxDescription;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getSecondBoxEnable(): ?bool
        {
            return $this->secondBoxEnable;
        }

        /**
         * @param bool $secondBoxEnable
         * @return TblProductLaunch
         */
        public function setSecondBoxEnable(bool $secondBoxEnable): self
        {
            $this->secondBoxEnable = $secondBoxEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getFeatureboxTitle(): ?string
        {
            return $this->featureboxTitle;
        }

        /**
         * @param string $featureboxTitle
         * @return TblProductLaunch
         */
        public function setFeatureboxTitle(string $featureboxTitle): self
        {
            $this->featureboxTitle = $featureboxTitle;

            return $this;
        }

        /**
         * @return string
         */
        public function getFeatureboxImage1()
        {
            return $this->featureboxImage1;
        }

        /**
         * @param $featureboxImage1
         */
        public function setFeatureboxImage1($featureboxImage1)
        {
            $this->featureboxImage1 = $featureboxImage1;
        }

        /**
         * @return string
         */
        public function getFeatureboxImage2()
        {
            return $this->featureboxImage2;
        }

        /**
         * @param $featureboxImage2
         */
        public function setFeatureboxImage2($featureboxImage2)
        {
            $this->featureboxImage2 = $featureboxImage2;
        }

        /**
         * @return string
         */
        public function getFeatureboxImage3()
        {
            return $this->featureboxImage3;
        }

        /**
         * @param $featureboxImage3
         */
        public function setFeatureboxImage3($featureboxImage3)
        {
            $this->featureboxImage3 = $featureboxImage3;
        }

        /**
         * @return string
         */
        public function getFeatureboxImage4()
        {
            return $this->featureboxImage4;
        }

        /**
         * @param $featureboxImage4
         */
        public function setFeatureboxImage4($featureboxImage4)
        {
            $this->featureboxImage4 = $featureboxImage4;
        }

        /**
         * @return string
         */
        public function getFeatureboxImage5()
        {
            return $this->featureboxImage5;
        }

        /**
         * @param $featureboxImage5
         */
        public function setFeatureboxImage5($featureboxImage5)
        {
            $this->featureboxImage5 = $featureboxImage5;
        }

        /**
         * @return bool|null
         */
        public function getFeatureboxImageEnable(): ?bool
        {
            return $this->featureboxImageEnable;
        }

        /**
         * @param bool $featureboxImageEnable
         * @return TblProductLaunch
         */
        public function setFeatureboxImageEnable(bool $featureboxImageEnable): self
        {
            $this->featureboxImageEnable = $featureboxImageEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getTestimonialTitle1(): ?string
        {
            return $this->testimonialTitle1;
        }

        /**
         * @param null|string $testimonialTitle1
         * @return TblProductLaunch
         */
        public function setTestimonialTitle1(?string $testimonialTitle1): self
        {
            $this->testimonialTitle1 = $testimonialTitle1;

            return $this;
        }

        /**
         * @return string
         */
        public function getTestimonialImage1()
        {
            return $this->testimonialImage1;
        }

        /**
         * @param $testimonialImage1
         */
        public function setTestimonialImage1($testimonialImage1)
        {
            $this->testimonialImage1 = $testimonialImage1;
        }

        /**
         * @return null|string
         */
        public function getTestimonialDescription1(): ?string
        {
            return $this->testimonialDescription1;
        }

        /**
         * @param string $testimonialDescription1
         * @return TblProductLaunch
         */
        public function setTestimonialDescription1(string $testimonialDescription1): self
        {
            $this->testimonialDescription1 = $testimonialDescription1;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getTestimonialTitle2(): ?string
        {
            return $this->testimonialTitle2;
        }

        /**
         * @param null|string $testimonialTitle2
         * @return TblProductLaunch
         */
        public function setTestimonialTitle2(?string $testimonialTitle2): self
        {
            $this->testimonialTitle2 = $testimonialTitle2;

            return $this;
        }

        /**
         * @return string
         */
        public function getTestimonialImage2()
        {
            return $this->testimonialImage2;
        }

        /**
         * @param $testimonialImage2
         */
        public function setTestimonialImage2($testimonialImage2)
        {
            $this->testimonialImage2 = $testimonialImage2;
        }

        /**
         * @return null|string
         */
        public function getTestimonialDescription2(): ?string
        {
            return $this->testimonialDescription2;
        }

        /**
         * @param string $testimonialDescription2
         * @return TblProductLaunch
         */
        public function setTestimonialDescription2(string $testimonialDescription2): self
        {
            $this->testimonialDescription2 = $testimonialDescription2;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getTestimonialTitle3(): ?string
        {
            return $this->testimonialTitle3;
        }

        /**
         * @param null|string $testimonialTitle3
         * @return TblProductLaunch
         */
        public function setTestimonialTitle3(?string $testimonialTitle3): self
        {
            $this->testimonialTitle3 = $testimonialTitle3;

            return $this;
        }

        /**
         * @return string
         */
        public function getTestimonialImage3()
        {
            return $this->testimonialImage3;
        }

        /**
         * @param $testimonialImage3
         */
        public function setTestimonialImage3($testimonialImage3)
        {
            $this->testimonialImage3 = $testimonialImage3;
        }

        /**
         * @return null|string
         */
        public function getTestimonialDescription3(): ?string
        {
            return $this->testimonialDescription3;
        }

        /**
         * @param string $testimonialDescription3
         * @return TblProductLaunch
         */
        public function setTestimonialDescription3(string $testimonialDescription3): self
        {
            $this->testimonialDescription3 = $testimonialDescription3;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getTestimonialEnable(): ?bool
        {
            return $this->testimonialEnable;
        }

        /**
         * @param bool $testimonialEnable
         * @return TblProductLaunch
         */
        public function setTestimonialEnable(bool $testimonialEnable): self
        {
            $this->testimonialEnable = $testimonialEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getAfterTestimonialTitle(): ?string
        {
            return $this->afterTestimonialTitle;
        }

        /**
         * @param null|string $afterTestimonialTitle
         * @return TblProductLaunch
         */
        public function setAfterTestimonialTitle(?string $afterTestimonialTitle): self
        {
            $this->afterTestimonialTitle = $afterTestimonialTitle;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getAfterTestimonialDescription(): ?string
        {
            return $this->afterTestimonialDescription;
        }

        /**
         * @param string $afterTestimonialDescription
         * @return TblProductLaunch
         */
        public function setAfterTestimonialDescription(string $afterTestimonialDescription): self
        {
            $this->afterTestimonialDescription = $afterTestimonialDescription;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getAfterTestimonialEnable(): ?bool
        {
            return $this->afterTestimonialEnable;
        }

        /**
         * @param bool $afterTestimonialEnable
         * @return TblProductLaunch
         */
        public function setAfterTestimonialEnable(bool $afterTestimonialEnable): self
        {
            $this->afterTestimonialEnable = $afterTestimonialEnable;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getNeedSubscription(): ?bool
        {
            return $this->needSubscription;
        }

        /**
         * @param bool $needSubscription
         * @return TblProductLaunch
         */
        public function setNeedSubscription(bool $needSubscription): self
        {
            $this->needSubscription = $needSubscription;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getSubscriptionCheckboxText(): ?string
        {
            return $this->subscriptionCheckboxText;
        }

        /**
         * @param null|string $subscriptionCheckboxText
         * @return TblProductLaunch
         */
        public function setSubscriptionCheckboxText(?string $subscriptionCheckboxText): self
        {
            $this->subscriptionCheckboxText = $subscriptionCheckboxText;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getUrl(): ?string
        {
            return $this->url;
        }

        /**
         * @param null|string $url
         * @return TblProductLaunch
         */
        public function setUrl(?string $url): self
        {
            $this->url = $url;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getIsCounterEnable(): ?bool
        {
            return $this->isCounterEnable;
        }

        /**
         * @param bool $isCounterEnable
         * @return TblProductLaunch
         */
        public function setIsCounterEnable(bool $isCounterEnable): self
        {
            $this->isCounterEnable = $isCounterEnable;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getCounterScriptUrl(): ?string
        {
            return $this->counterScriptUrl;
        }

        /**
         * @param null|string $counterScriptUrl
         * @return TblProductLaunch
         */
        public function setCounterScriptUrl(?string $counterScriptUrl): self
        {
            $this->counterScriptUrl = $counterScriptUrl;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getCounterScriptSmall(): ?string
        {
            return $this->counterScriptSmall;
        }

        /**
         * @param null|string $counterScriptSmall
         * @return TblProductLaunch
         */
        public function setCounterScriptSmall(?string $counterScriptSmall): self
        {
            $this->counterScriptSmall = $counterScriptSmall;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getCounterTitle(): ?string
        {
            return $this->counterTitle;
        }

        /**
         * @param null|string $counterTitle
         * @return TblProductLaunch
         */
        public function setCounterTitle(?string $counterTitle): self
        {
            $this->counterTitle = $counterTitle;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getAddDvdProduct(): ?bool
        {
            return $this->addDvdProduct;
        }

        /**
         * @param bool $addDvdProduct
         * @return TblProductLaunch
         */
        public function setAddDvdProduct(bool $addDvdProduct): self
        {
            $this->addDvdProduct = $addDvdProduct;

            return $this;
        }

        /**
         * @return TblProducts|null
         */
        public function getDvdProduct(): ?TblProducts
        {
            return $this->dvdProduct;
        }

        /**
         * @param TblProducts|null $dvdProduct
         * @return TblProductLaunch
         */
        public function setDvdProduct(?TblProducts $dvdProduct): self
        {
            $this->dvdProduct = $dvdProduct;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getFooterText(): ?string
        {
            return $this->footerText;
        }

        /**
         * @param null|string $footerText
         * @return TblProductLaunch
         */
        public function setFooterText(?string $footerText): self
        {
            $this->footerText = $footerText;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getFooterLink(): ?string
        {
            return $this->footerLink;
        }

        /**
         * @param null|string $footerLink
         * @return TblProductLaunch
         */
        public function setFooterLink(?string $footerLink): self
        {
            $this->footerLink = $footerLink;

            return $this;
        }

        /**
         * @return null|string
         */
        public function getRedirectLink(): ?string
        {
            return $this->redirectLink;
        }

        /**
         * @param null|string $redirectLink
         * @return TblProductLaunch
         */
        public function setRedirectLink(?string $redirectLink): self
        {
            $this->redirectLink = $redirectLink;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getIsActive(): ?bool
        {
            return $this->isActive;
        }

        /**
         * @param bool $isActive
         * @return TblProductLaunch
         */
        public function setIsActive(bool $isActive): self
        {
            $this->isActive = $isActive;

            return $this;
        }

        /**
         * @return bool|null
         */
        public function getIsDelete(): ?bool
        {
            return $this->isDelete;
        }

        /**
         * @param bool $isDelete
         * @return TblProductLaunch
         */
        public function setIsDelete(bool $isDelete): self
        {
            $this->isDelete = $isDelete;

            return $this;
        }

        /**
         * @return \DateTimeInterface|null
         */
        public function getCreatedDate(): ?\DateTimeInterface
        {
            return $this->createdDate;
        }

        /**
         * @param \DateTimeInterface|null $createdDate
         * @return TblProductLaunch
         */
        public function setCreatedDate(?\DateTimeInterface $createdDate): self
        {
            $this->createdDate = $createdDate;

            return $this;
        }

        /**
         * @return \DateTimeInterface|null
         */
        public function getUpdatedDate(): ?\DateTimeInterface
        {
            return $this->updatedDate;
        }

        /**
         * @param \DateTimeInterface|null $updatedDate
         * @return TblProductLaunch
         */
        public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
        {
            $this->updatedDate = $updatedDate;

            return $this;
        }
    }
