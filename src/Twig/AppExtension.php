<?php
/**
 * Created by PhpStorm.
 * User: ilogix
 * Date: 30/1/19
 * Time: 12:43 PM
 */

namespace App\Twig;

use App\Entity\TblOrders;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
        ];
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$' . $price;

        return $price;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('area', [$this, 'calculateArea']),
            new TwigFunction('file_exists', [$this, 'fileExists']),
            new TwigFunction('order_status_text', [$this, 'orderStatusText']),
        ];
    }

    public function calculateArea(int $width, int $length)
    {
        return $width * $length;
    }

    public function fileExists($file)
    {
        return file_exists('.' . $file);
    }

    public function orderStatusText($status)
    {
        switch ($status) {
            case TblOrders::STATUS_CONFIRM:
                $data = "Confirm";
                break;
            case TblOrders::STATUS_PENDING:
                $data = "Pending";
                break;
            case TblOrders::STATUS_SUCCESS:
                $data = "Success";
                break;
            case TblOrders::STATUS_ONHOLD:
                $data = "On hold";
                break;
            case TblOrders::STATUS_PROCESSING:
                $data = "Processsing";
                break;
            case TblOrders::STATUS_CANCEL:
                $data = "Cancel";
                break;
            default:
                $data = "Other";
                break;
        }

        echo $data;
    }
}
