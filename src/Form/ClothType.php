<?php

namespace App\Form;

use App\Entity\TblProducts;
use App\Entity\TblProductSize;
use App\Entity\TblProductType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ClothType.
 */
class ClothType extends AbstractType
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $hide = "hide";
        if ($options['data']->getId() > 0) {
            if ($options['data']->getIdProductOfDvd()) {
                $hide = "";
            }
        }

        $builder
            ->add('idProductType', EntityType::class, [
                'class'         => TblProductType::class,
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('pc')
                                ->where('pc.idParent = :idParent')
                                ->setParameter('idParent', TblProducts::PARENT);
                    return $query;
                },
                'placeholder'   => '- Select Product Type -',
                'label'         => 'Product Type:'
            ])
            ->add('title', TextType::class, [
                'label' => 'Product Title:',
            ])
            ->add('sku', TextType::class, [
                'label'    => 'Sku:',
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description:',
                'attr'  => [
                    'class' => 'textarea',
                ],
            ])
            ->add('price', TextType::class, [
                'label' => 'Price:',
                'attr'  => ['class' => 'numbersOnly']
            ])
            ->add('quantity', TextType::class, [
                'label' => 'Product Quantity: ',
                'attr'  => ['class' => 'numbersOnly'],
            ])
            ->add('idProductSize', EntityType::class, [
                'class'       => TblProductSize::class,
                'placeholder' => '- Select Product Type -',
                'label'       => 'Product Category:',
                'multiple'    => true,
                'data'        => $options['selectedProductsize'] ? $options['selectedProductsize'] : []
            ])
            ->add('sortOrder', TextType::class, [
                'label' => 'Cloth Order:',
                'attr'  => ['class' => 'numbersOnly']
            ])
            ->add('productUrl', TextType::class, [
                'label'    => 'Product url (predefined:store/) :',
                'required' => true,
            ])
            ->add('image', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Image:',
                'attr'       => ['value' => $options['data']->getImage(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'          => TblProducts::class,
            'selectedProductsize' => ''
        ]);
    }
}
