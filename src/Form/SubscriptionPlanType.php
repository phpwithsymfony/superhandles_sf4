<?php

namespace App\Form;

use App\Entity\TblStreamingPages;
use App\Entity\TblSubscriptionPackage;
use App\Entity\TblSubscriptionPlan;
use App\Helper\StaticDataHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscriptionPlanType
 * @package App\Form
 */
class SubscriptionPlanType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $readOnly         = false;
        $disabled         = false;
        $selected_options = [];
        $hide             = "hide";
        if ($options['data']->getId() > 0) {
            $readOnly = true;
            if ($options['data']->getMembers()) {
                $selected_options = $options['data']->getMembers();
            }
            if ($selected_options[0]) {
                $hide = "show";
            }
            $disabled = true;
        }
        $staticData = new StaticDataHelper();

        $builder
            ->add('idSubscriptionPackage', EntityType::class, [
                'class'       => TblSubscriptionPackage::class,
                'placeholder' => '- Select Package -',
                'label'       => 'Package:',
                'attr'        => [
                    'readonly' => $readOnly,
                ],
                'disabled'    => $disabled

            ])
            ->add('idStreamingPage', EntityType::class, [
                'class'       => TblStreamingPages::class,
                'placeholder' => '- Select Streaming -',
                'label'       => 'Streaming Page CC-code:'
            ])
            ->add('planName', TextType::class, [
                'label' => 'Package Title:',
            ])
            ->add('members', ChoiceType::class, [
                'label'             => 'Member:',
                'required'          => false,
                'attr'              => [
                    'class' => $hide . ' isCoach multiselect'
                ],
                'label_attr'        => ['class' => $hide . ' isCoach'],
                'multiple'          => true,
                'choices'           => $this->getMemberData(),
                'preferred_choices' => $selected_options,
                'disabled'          => $disabled
            ])
            ->add('amount', NumberType::class, [
                'label'    => 'Price:',
                'attr'     => [
                    'readonly' => $readOnly,
                ],
                'disabled' => $disabled
            ])
//            ->add('completedAmount', NumberType::class, [
//                'label' => 'Complete Program Price:',
//                'required' => false,
//                'attr' => [
//                    'class' => $hide.' isCoach'
//                ],
//                'label_attr' => ['class' => $hide.' isCoach'],
//            ])
//            ->add('extracompleteAmount', NumberType::class, [
//                'label' => 'Extra Complete Price:',
//                'required' => false,
//                'attr' => [
//                    'class' => $hide.' isCoach'
//                ],
//                'label_attr' => ['class' => $hide.' isCoach'],
//            ])

            ->add('packageInterval', ChoiceType::class, [
                'choices'  => $staticData->subscriptionPlanOption(),
                'label'    => 'interval:',
                'attr'     => [
                    'readonly' => $readOnly,
                ],
                'disabled' => $disabled
            ])
            ->add('trialDays', NumberType::class, [
                'label'    => 'Trial Days:',
                'attr'     => [
                    'readonly' => $readOnly,
                ],
                'disabled' => $disabled,
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'label'    => 'Description:',
                'attr'     => [
                    'class' => 'textarea',
                ],
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblSubscriptionPlan::class,
        ]);
    }

    public function getMemberData()
    {
        $arrayData = [];

        for ($i = 2; $i <= 200; $i++) {
            $arrayData[$i] = $i;
        }
        return $arrayData;
    }
}
