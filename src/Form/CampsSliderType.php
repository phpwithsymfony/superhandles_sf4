<?php
namespace App\Form;

use App\Entity\TblCampsSlider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CampsSliderType
 * @package App\Form
 */
class CampsSliderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isBannerFileRequired = true;
        if ($options['data']->getId() !== null) {
            $isBannerFileRequired = false;
        }

        $builder
            ->add('title', TextType::class, [
                'label' => 'Slider Title:',
            ])
            ->add('pageUrl', TextType::class, [
                'label' => 'Page URL:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ])
            ->add('bannerFile', FileType::class, [
                'required'   => $isBannerFileRequired,
                'data_class' => null,
                'label'      => 'Slider:',
                'attr'       => ['class' => 'image-preview', 'id' => 'imgInp'],
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblCampsSlider::class,
        ]);
    }
}
