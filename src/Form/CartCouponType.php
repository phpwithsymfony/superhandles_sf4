<?php
namespace App\Form;

use App\Entity\TblCartCoupons;
use App\Entity\TblProducts;
use App\Entity\TblProductType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CartCouponType
 * @package App\Form
 */
class CartCouponType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $disabled = false;
        if ($options['data']->getId() > 0) {
            $disabled = true;
        }
        $builder
            ->add('idProductType', EntityType::class, [
                'class'         => TblProductType::class,
                'placeholder'   => '- Select Product Type -',
                'label'         => 'Product Type:',
                'required'      => false,
                'attr'          => ['disabled' => $disabled],
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('p')
                                ->where('p.isCloth = :isCloth')
                                ->setParameter('isCloth', 1);
                    return $query;
                },

            ])
            ->add('idProduct', EntityType::class, [
                'class'       => TblProducts::class,
                'placeholder' => '- Select Product -',
                'label'       => 'Product:',
                'required'    => false,
                'attr'        => ['disabled' => $disabled],
            ])
            ->add('couponType', ChoiceType::class, [
                'choices' => TblCartCoupons::COUPON_TYPE,
                'label'   => 'Coupon type:',
                'attr'    => ['readonly' => $disabled],
            ])
            ->add('title', TextType::class, [
                'label' => 'Title:',
            ])
            ->add('couponCode', TextType::class, [
                'label' => 'Coupon Code:',
                'attr'  => ['readonly' => $disabled],
            ])
            ->add('discount', TextType::class, [
                'label' => 'Discount:',
                'attr'  => ['class'       => 'numbersOnly',
                            'placeholder' => 'Only number',
                            'readonly'    => $disabled]

            ])
            ->add('startDate', DateType::class, [
                'label'  => 'Start Date:',
                'widget' => 'single_text',
                'format' => 'MM-dd-yyyy',
                'attr'   => [
                    'class'       => 'form-control input-inline js-startDate',
                    'placeholder' => 'MM-DD-YYYY',
                    'readonly'    => 'readonly'
                ]
            ])
            ->add('endDate', DateType::class, [
                'label'  => 'End Date:',
                'widget' => 'single_text',
                'format' => 'MM-dd-yyyy',
                'attr'   => [
                    'class'       => 'form-control input-inline js-endDate',
                    'placeholder' => 'MM-DD-YYYY',
                    'readonly'    => 'readonly'
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblCartCoupons::class,
        ]);
    }
}
