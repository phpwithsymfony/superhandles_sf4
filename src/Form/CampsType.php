<?php
namespace App\Form;

use App\Entity\TblCamps;
use App\Entity\TblCampsCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CampsType
 * @package App\Form
 */
class CampsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCampsCategory', EntityType::class, [
                'class'       => TblCampsCategory::class,
                'placeholder' => '- Select Category -',
                'label'       => 'Camps Category:',

            ])
            ->add('title', TextType::class, [
                'label' => 'Camps Title:',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description:',
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('eventDateTime', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm:ss',
                'input'  => 'datetime',
                'attr' => [
                    'class' => 'form-control input-inline js-datetimepicker',
                ]
            ])
            ->add('eventPlace', TextType::class, [
                'label' => 'Event Place:',
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => 'Contact Email:',
            ])
            ->add('contactPhone', NumberType::class, [
                'label' => 'Contact Phone:',
            ])
            ->add('additionalInfo', TextareaType::class, [
                'label' => 'Additional Information:',
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('youtubeUrl', UrlType::class, [
                'label' => 'YouTube URL:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblCamps::class,
        ]);
    }
}
