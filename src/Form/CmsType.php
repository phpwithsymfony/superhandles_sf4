<?php

namespace App\Form;

use App\Entity\TblStatic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CmsType
 * @package App\Form
 */
class CmsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pageTitle', TextType::class, [
                'label' => 'Page Title',
            ])
            ->add('pageContent', TextareaType::class, [
                'label' => 'Page Content',
                'attr' => [
                    'class' => 'textarea',
                ],
            ])
            ->add('metaTitle', TextType::class, [
                'label' => 'Meta Title',
                'required' => false,
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'Meta Keywords',
                'required' => false,
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Meta Description',
                'required' => false,
            ])
            ->add('pageUrl', TextType::class, [
                'label' => 'Page Url',
            ])
            ->add('isActive', ChoiceType::class, [
                'label' => 'Status',
                'choices' => [
                    '- Status -' => '',
                    'Yes' => 1,
                    'No' => 0,
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblStatic::class,
        ]);
    }
}
