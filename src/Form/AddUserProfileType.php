<?php

namespace App\Form;

use App\Entity\TblUserProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegProfileType
 * @package App\Form
 */
class AddUserProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => ['placeholder' => 'First Name'],
                'required' => false
            ])
            ->add('lastname', TextType::class, [
                'attr' => ['placeholder' => 'First Name'],
                'required' => false
            ])
            ->add('dob', DateType::class, [
                'required' => false,
                'label' => 'Date Of Birth',
                'widget' => 'single_text',
                'attr' => ['class' => ''],
            ])
            ->add('gender', ChoiceType::class, [
                'required' => false,
                'choices' => ['Male' => true, 'Female' => false,],
                'placeholder' => '-- Select Gender --',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblUserProfile::class,
            'show_legend' => false,
            'show_label' => false,
        ]);
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string|null
     */
    public function getBlockPrefix()
    {
        return 'app_profile';
    }
}
