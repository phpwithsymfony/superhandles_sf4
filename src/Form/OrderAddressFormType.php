<?php

namespace App\Form;

use App\Entity\TblCountries;
use App\Entity\TblOrderAddress;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType.
 */
class OrderAddressFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('billingFirstname', TextType::class, [
                'label' => 'First Name:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingFirstname', 'tabindex'=>1],
            ])
            ->add('billingLastname', TextType::class, [
                'label' => 'Last Name:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingLastname', 'tabindex'=>2],
            ])
            ->add('billingEmail', EmailType::class, [
                'label' => 'Billing Email:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingEmail', 'tabindex'=>3],
            ])
            ->add('billingAddress', TextareaType::class, [
                'label' => 'Billing Address:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingAddress', 'tabindex'=>8],
            ])
            ->add('billingCountry', EntityType::class, [
                'label'         => 'Billing Country:',
                'attr'          => ['status' => 'billing', 'dataName'=>'shippingCountry', 'tabindex'=>6],
                'choice_value'  => 'countryName',
                'class'         => TblCountries::class,
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('c')
                                ->where('c.isActive = :isActive')
                                ->andWhere('c.isDelete = :isDelete')
                                ->andWhere('c.isShipping = :isShipping')
                        ->setParameters(['isActive' =>TblCountries::ACTIVE,
                                         'isDelete' => TblCountries::NOTDELETE,
                                         'isShipping'=> TblCountries::SHIPPING
                        ])
                    ;
                    return $query;
                },
            ])
            ->add('billingState', TextType::class, [
                'label' => 'Billing State:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingState', 'tabindex'=>5],
            ])
            ->add('billingCity', TextType::class, [
                'label' => 'Billing City:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingCity', 'tabindex'=>4],
            ])
            ->add('billingZipcode', TextType::class, [
                'label' => 'Billing Zipcode:',
                'attr'  => ['status' => 'billing', 'dataName'=>'shippingZipcode', 'tabindex'=>7],
            ])
            //Shipping Details

            ->add('shippingFirstname', TextType::class, [
                'label' => 'First Name:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingFirstname', 'tabindex'=>9],
            ])
            ->add('shippingLastname', TextType::class, [
                'label' => 'Last Name:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingLastname', 'tabindex'=>10],
            ])
            ->add('shippingEmail', EmailType::class, [
                'label' => 'Shipping Email:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingEmail', 'tabindex'=>11],
            ])
            ->add('shippingAddress', TextareaType::class, [
                'label' => 'Shipping Address:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingAddress', 'tabindex'=>16],
            ])
            ->add('shippingCountry', EntityType::class, [
                'label' => 'Shipping Country:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingCountry', 'tabindex'=>14],
                'class'         => TblCountries::class,
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('c')
                                ->where('c.isActive = :isActive')
                                ->andWhere('c.isDelete = :isDelete')
                                ->andWhere('c.isShipping = :isShipping')
                                ->setParameters(['isActive' =>TblCountries::ACTIVE,
                                    'isDelete' => TblCountries::NOTDELETE,
                                    'isShipping'=> TblCountries::SHIPPING
                                ])
                    ;
                    return $query;
                },
            ])
            ->add('shippingState', TextType::class, [
                'label' => 'Shipping State:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingState', 'tabindex'=>13],
            ])
            ->add('shippingCity', TextType::class, [
                'label' => 'Shipping City:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingCity', 'tabindex'=>12],
            ])
            ->add('shippingZipcode', TextType::class, [
                'label' => 'Shipping Zipcode:',
                'attr'  => ['status' => 'shipping', 'dataName'=>'billingZipcode', 'tabindex'=>15],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblOrderAddress::class,
        ]);
    }
}
