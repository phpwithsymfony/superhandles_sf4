<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddUserType
 * @package App\Form
 */
class AddUserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('username', TextType::class, [
                'attr' => ['autofocus' => true],
                'attr' => ['placeholder' => 'User Name']
            ])
            ->add('profile', AddUserProfileType::class, [
                'label' => false,
                'attr' => ['viewFields' => 'firstname, lastname, dob, gender']
            ])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Email']])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => ($options['data']->getId() !== null) ? false : true,
                'invalid_message' => 'Password not matched with Repeat Password. .',
                'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password']],
                'second_options' => ['label' => 'Repeat Password', 'attr' => ['placeholder' => 'Repeat Password']],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'allow_extra_fields' => true,
        ]);
    }
}
