<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType.
 */
class PaymentFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $month = [
            "January"=> 1, "February"=> 2, "March"=> 3, "April"=> 4,
            "May"=> 5, "June"=> 6, "July"=> 7, "August"=> 8,
            "September"=> 9, "October"=> 10, "November"=> 11, "December"=> 12
        ];

        $expiryYear = [];
        for ($i = 0; $i <= 15; $i++) {
            $expiryYear[Date('Y')] = Date('Y');
        }

        $current_year = date('Y');
        $range = range($current_year, $current_year+10);
        $expiryYear = array_combine($range, $range);

        $builder
            ->add('cardNumber', NumberType::class, [
                'label' => 'Card Number:',
                'attr'  => ['maxlength' => '16'],
//                'constraints' => [
//                    new Assert\NotBlank(),
//                    new Assert\CardSchemeValidator(),
//                ]
            ])
            ->add('expiryMonth', ChoiceType::class, [
                'label' => 'Expiry Month:',
                'choices' => $month,
//                'constraints' => [
//                    new Assert\NotBlank()
//                ]

            ])
            ->add('expiryYear', ChoiceType::class, [
                'label' => 'Expiry Year:',
                'choices' => $expiryYear,
//                'constraints' => [
//                    new Assert\NotBlank(),
//                ]

            ])
            ->add('cvc', NumberType::class, [
                'label' => 'CVC:',
                'attr'  => ['maxlength' => '3'],
//                'constraints' => [
//                    new Assert\NotBlank(),
//                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Complete Order",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => null,
            // enable/disable CSRF protection for this form
            'csrf_protection' => null,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => '_token',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_item',
        ]);
    }
}
