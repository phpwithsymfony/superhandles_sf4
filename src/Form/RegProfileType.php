<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\TblUserProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegProfileType
 * @package App\Form
 */
class RegProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (isset($options['attr']['viewFields'])) {
            if (strpos($options['attr']['viewFields'], 'firstname') !== false) {
                $builder
                    ->add('firstname', TextType::class, [
                        'attr' => ['placeholder' => 'First Name'],
                    ]);
            }
            if (strpos($options['attr']['viewFields'], 'lastname') !== false) {
                $builder
                    ->add('lastname', TextType::class, [
                        'attr' => ['placeholder' => 'First Name'],
                    ]);
            }
            if (strpos($options['attr']['viewFields'], 'dob') !== false) {
                $builder
                    ->add('dob', DateType::class, [
                        'label'  => 'Date Of Birth',
                        'widget' => 'single_text',
                        'attr'   => ['class' => ''],
                    ]);
            }
            if (strpos($options['attr']['viewFields'], 'profilePic') !== false) {
                $builder
                    ->add('profilePic', FileType::class, [
                        'data_class' => null,
                    ]);
            }
        } else {
            $builder
                ->add('firstname', TextType::class, [
                    'attr' => ['placeholder' => 'First Name'],
                ])
                ->add('lastname', TextType::class, [
                    'attr' => ['placeholder' => 'Last Name'],
                ])
                ->add('isNewsletterSubscribed', CheckboxType::class, [
                    'required' => false,
                ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'  => TblUserProfile::class,
            'show_legend' => false,
            'show_label'  => false,
        ]);
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return 'app_profile';
    }
}
