<?php

namespace App\Form;

use App\Entity\TblSubscriptionCoachPlan;
use App\Helper\StaticDataHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscriptionPlanType.php
 * @package App\Form
 */
class SubscriptionCoachPlanType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('members', ChoiceType::class, [
                'label'    => 'Member:',
                'required' => false,
                'multiple' => true,
                'attr'     => [
                    'class' => 'multiselect'
                ],
                'choices'  => $this->getMemberData($options['data']),
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,//TblSubscriptionCoachPlan::class,
        ]);
    }

    public function getMemberData($data)
    {
        $arrayData = [];

        for ($i = 2; $i <= 200; $i++) {
            if (!in_array($i, $data)) {
                $arrayData[$i] = $i;
            }
        }
        return $arrayData;
    }
}
