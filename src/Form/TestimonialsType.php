<?php
namespace App\Form;

use App\Entity\TblTestimonialPostType;
use App\Entity\TblTestimonials;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TestimonialsType
 * @package App\Form
 */
class TestimonialsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isBannerFileRequired = true;
        if ($options['data']->getId() !== null) {
            $isBannerFileRequired = false;
        }

        $builder
            ->add('idTestimonialPostType', EntityType::class, [
                'class'       => TblTestimonialPostType::class,
                'placeholder' => '- Select Category -',
                'label'       => 'Camps Category:',

            ])
            ->add('authorName', TextType::class, [
                'label' => 'Author Name:',
            ])
            ->add('postContent', TextareaType::class, [
                'label' => 'Description:',
                'attr'  => [
                    'class' => 'textarea',
                ],
            ])
            ->add('twitterUsername', TextType::class, [
                'label' => 'Twitter User Name:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ])
            ->add('authorPic', FileType::class, [
                'required'   => $isBannerFileRequired,
                'data_class' => null,
                'label'      => 'Author Pic',
                'attr'       => [
                    "value" => $options['data']->getAuthorPic(),
                    'class' => 'image-preview',
                    'id' => 'imgInp',
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblTestimonials::class,
        ]);
    }
}
