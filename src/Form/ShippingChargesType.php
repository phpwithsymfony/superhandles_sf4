<?php

namespace App\Form;

use App\Entity\TblCountries;
use App\Entity\TblShippingCharges;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShippingChargesType
 * @package App\Form
 */
class ShippingChargesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minWeight', NumberType::class, [
                'label' => 'Min Weight',
                'attr' => ['autofocus' => true, 'placeholder' => 'Min Weight', 'class' => 'allownumericwithdecimal'],

            ])
            ->add('maxWeight', NumberType::class, [
                'label' => 'Max Weight',
                'attr' => ['placeholder' => 'Max Weight', 'class' => 'allownumericwithdecimal']
            ])
            ->add('price', NumberType::class, [
                'label' => 'Price',
                'attr' => ['placeholder' => 'Price', 'class' => 'allownumericwithdecimal']
            ])
            ->add('idCountry', EntityType::class, [
                'class'       => TblCountries::class,
                'placeholder' => '-- Select Country --',
                'label'       => 'Country:',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblShippingCharges::class
        ]);
    }
}
