<?php
namespace App\Form;

use App\Entity\TblProductCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductCategoryType
 * @package App\Form
 */
class ProductCategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Category Title:',
            ])
            ->add('categoryUrl', TextType::class, [
                'label' => 'Category Url:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort Order:',
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblProductCategory::class,
        ]);
    }
}
