<?php
namespace App\Form;

use App\Entity\TblHomeSlider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HomeSliderType
 * @package App\Form
 */
class HomeSliderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isBannerFileRequired = true;
        if ($options['data']->getId() !== null) {
            $isBannerFileRequired = false;
        }
        $builder
            ->add('title', TextType::class, [
                'label' => 'Slider Title:',
            ])
            ->add('pageUrl', TextType::class, [
                'label' => 'Page URL:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ])
            ->add('bannerFile', FileType::class, [
                'required'   => $isBannerFileRequired,
                'data_class' => null,
                'label'      => 'Slider:(Only for jpeg, png, jpg)',
                'attr'       => [
                    "value" => $options['data']->getBannerFile(),
                    'class' => 'image-preview', 'id' => 'imgInp',
                ],
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblHomeSlider::class,
        ]);
    }
}
