<?php
namespace App\Form;

use App\Entity\TblStreamingPages;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProductType.
 */
class UserCcCodeType extends AbstractType
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('usercode', ChoiceType::class, [
                'label'             => 'User CC Code:',
                'choices'           => self::getAllStreamingPage(),
                'multiple'          => true,
                'attr' => [
                    'class' => 'pre-selected-options',
                ],
                'required'=>true,
            ])
        ;
    }

    private function getAllStreamingPage()
    {
        $array = [];
        $pageData = $this->entityManager->getRepository(TblStreamingPages::class)->findKeyValArray();
        foreach ($pageData as $data) {
            $array[$data['titleName']] = $data['id'];
        }
        return $array;
    }
}
