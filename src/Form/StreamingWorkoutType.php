<?php

namespace App\Form;

use App\Entity\TblStreamingPageContent;
use App\Entity\TblStreamingPages;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StreamingPageType
 * @package App\Form
 */
class StreamingWorkoutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idStreamingPage', EntityType::class, [
                'class' => TblStreamingPages::class,
                'placeholder' => '- Select Streaming Page -',
                'label' => 'Streaming Page:',
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('p')
                        ->where('p.isActive = :isActive')
                        ->andWhere('p.isDelete = :isDelete')
                        ->setParameters(
                            ['isActive' => TblStreamingPageContent::_ACTIVE,
                                'isDelete' => TblStreamingPageContent::_NOTDELETE,
                            ]
                        );
                    return $query;
                }
            ])
            ->add('title', TextType::class, [
                'label' => 'Workout Title:',
            ])
            ->add('video', TextType::class, [
                'label' => 'Vimeo Video:',
                'required' => false,
            ])
            ->add('audio', TextType::class, [
                'label' => 'Soundcolud URL:',
                'required' => false,
            ])
            ->add('streamingPageContentFiles', CollectionType::class, array(
                'entry_type' => StreamingPdfDocType::class,
                'entry_options' => array('label' => false ),
                 'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblStreamingPageContent::class,
        ]);
    }
}
