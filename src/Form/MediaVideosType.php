<?php
namespace App\Form;

use App\Entity\TblMediaVideos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class TblMediaVideos
 * @package App\Form
 */
class MediaVideosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('videoTitle', TextType::class, [
                'label' => 'Video title:',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description:',
                'attr'  => [
                    'class' => 'textarea',
                ],
            ])
            ->add('videoUrl', UrlType::class, [
                'label' => 'Video url:',
            ])
            ->add('isVictimVideo', ChoiceType::class, [
                'label'   => 'Video Type:',
                'choices' => [
                    'Media Videos'  => 1,
                    'Victim Videos' => 2,
                ],
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblMediaVideos::class,
        ]);
    }
}
