<?php
namespace App\Form;

use App\Entity\TblTestimonialsVideo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CampsSliderType
 * @package App\Form
 */
class TestimonialsVideoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('videoTitle', TextType::class, [
                'label' => 'Video Title:',
            ])
            ->add('videoUrl', TextType::class, [
                'label' => 'Video URL:',
            ])
            ->add('sortOrder', TextType::class, [
                'label'    => 'Sort order:',
                'required' => false,
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblTestimonialsVideo::class,
        ]);
    }
}
