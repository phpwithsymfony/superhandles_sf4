<?php

namespace App\Form;

use App\Entity\TblProductLaunch;
use App\Entity\TblProducts;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType.
 */
class ProductLaunchType extends AbstractType
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $hide = "hide";
        $readOnly = false;

        $builder
            ->add('idProduct', EntityType::class, [
                'class'       => TblProducts::class,
                'placeholder' => '- Select Product -',
                'label'       => 'Select Related Product:',

            ])
            ->add('headlineTitle', TextareaType::class, [
                'label' => 'Headline Title:',
                'required' => false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('headlineEnable', CheckboxType::class, [
                'required' => false,
                'label' => 'Headline enable:',
            ])
            ->add('discountText', TextType::class, [
                'label' => 'Discount Text:',
                'required'=>false,
            ])
            ->add('discount', NumberType::class, [
                'label' => 'Sale Discount:',
                'required'=>false,
            ])
            ->add('discountPrice', NumberType::class, [
                'label' => 'Launching Discount: (Only valid for launching)',
                'required'=>false,
                'scale' => 2
            ])
            ->add('discountEnable', CheckboxType::class, [
                'label' => 'Discount enable:',
                'required'=>false,
            ])
            ->add('productTitle', TextType::class, [
                'label' => 'Product Title:',
                'required'=>false,
            ])
            ->add('productDescriptionTitle', TextType::class, [
                'label' => 'Product Description Title:',
                'required'=>false,
            ])
            ->add('productDescription', TextareaType::class, [
                'label' => 'Product Description:',
                'required'=>false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('productDescriptionEnable', CheckboxType::class, [
                'label' => 'Product Description Enable:',
                'required'=>false,
            ])
            ->add('productImage', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Product Image (350 x 350):',
                'attr'       => ['value' => $options['data']->getProductImage(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('bannerType', ChoiceType::class, [
                'label' => 'Select Banner Type:',
                'choices' => [
                    '- Status -' => '',
                    'Image' => 0,
                    'Video' => 1,
                ],
            ])
            ->add('videoId', TextType::class, [
                'label' => 'Video Id:',
                'required'=>false,
            ])
            ->add('imageUrl', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Select Banner Image:',
                'attr'       => ['value' => $options['data']->getImageUrl(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('mediaEnable', CheckboxType::class, [
                'label' => 'Media Enable:',
                'required'=>false,
            ])
            ->add('joinNowButtonText1', TextType::class, [
                'label' => 'First Join Now Button Text:',
                'required'=>false,
            ])
            ->add('productPopupText', TextType::class, [
                'label' => 'Product Popup Text:',
                'required'=>false,
            ])
            ->add('dvdProductPopupText', TextType::class, [
                'label' => 'Dvd Product Popup Text:',
                'required'=>false,
            ])
            ->add('firstJoinNowEnable', CheckboxType::class, [
                'label' => 'First Join Now Enable:',
                'required'=>false,
            ])
            ->add('joinNowButtonText2', TextType::class, [
                'label' => 'Second Join Now Button Text:',
                'required'=>false,
            ])
            ->add('secondJoinNowEnable', CheckboxType::class, [
                'label' => 'Second Join Now Enable:',
                'required'=>false,
            ])
            ->add('firstBoxTitle', TextType::class, [
                'label' => 'First Box Title:',
                'required'=>false,
            ])
            ->add('firstBoxDescription', TextareaType::class, [
                'label' => 'First Box Description:',
                'required'=>false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('firstBoxEnable', CheckboxType::class, [
                'label' => 'First Box Enable:',
                'required'=>false,
            ])
            ->add('secondBoxTitle', TextType::class, [
                'label' => 'Second Box Title:',
                'required'=>false,
            ])
            ->add('secondBoxDescription', TextareaType::class, [
                'label' => 'Second Box Description:',
                'required'=>false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('secondBoxEnable', CheckboxType::class, [
                'label' => 'Second Box Enable:',
                'required'=>false,
            ])
            ->add('featureboxTitle', TextType::class, [
                'label' => 'Feature Box Title:',
                'required'=>false,
            ])
            ->add('featureboxImageEnable', CheckboxType::class, [
                'label' => 'Featurebox Image Enable:',
                'required'=>false,
            ])
            ->add('testimonialTitle1', TextType::class, [
                'label' => 'First Testimonial Title:',
                'required'=>false,
            ])
            ->add('testimonialImage1', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'First Testimonial Image (125 x 125):',
                'attr'       => ['value' => $options['data']->getTestimonialImage1(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('testimonialDescription1', TextareaType::class, [
                'label' => 'First Testimonial Description:',
                'required'   => false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('testimonialTitle2', TextType::class, [
                'label' => 'Second Testimonial Title:',
                'required'=>false,
            ])
            ->add('testimonialImage2', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Second Testimonial Image (125 x 125):',
                'attr'       => ['value' => $options['data']->getTestimonialImage2(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('testimonialDescription2', TextareaType::class, [
                'label' => 'Second Testimonial Description:',
                'required'   => false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('testimonialTitle3', TextType::class, [
                'label' => 'Third Testimonial Title:',
                'required'=>false,
            ])
            ->add('testimonialImage3', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Third Testimonial Image (125 x 125):',
                'attr'       => ['value' => $options['data']->getTestimonialImage3(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('testimonialDescription3', TextareaType::class, [
                'label' => 'Third Testimonial Description:',
                'required'   => false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('testimonialEnable', CheckboxType::class, [
                'label' => 'Testimonial Enable:',
                'required'=>false,
            ])
            ->add('afterTestimonialTitle', TextType::class, [
                'label' => 'After Testimonial Title:',
                'required'=>false,
            ])
            ->add('afterTestimonialDescription', TextareaType::class, [
                'label' => 'After Testimonial Description:',
                'required'   => false,
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('afterTestimonialEnable', CheckboxType::class, [
                'label' => 'After Testimonial Enable:',
                'required'=>false,
            ])
            ->add('needSubscription', CheckboxType::class, [
                'label' => 'Need Subscription:',
                'required'=>false,
            ])
            ->add('subscriptionCheckboxText', TextType::class, [
                'label' => 'Subscription Checkbox Text:',
                'required'=>false,
            ])
            ->add('url', TextType::class, [
                'label' => 'Product Buy Now Page URL(/launch/):',
                'required'=>true,
            ])
            ->add('isCounterEnable', ChoiceType::class, [
                'label' => 'Display Countdown Monkey Timer:',
                'choices' => [
                    '- Status -' => '',
                    'Yes' => 1,
                    'No' => 0,
                ],
            ])
            ->add('counterScriptUrl', TextType::class, [
                'label' => 'Counter Script:',
                'required'=>false,
            ])
            ->add('counterScriptSmall', TextType::class, [
                'label' => 'Script For Small Device:',
                'required'=>false,
            ])
            ->add('counterTitle', TextType::class, [
                'label' => 'Counter Title:',
                'required'=>false,
            ])
            ->add('addDvdProduct', CheckboxType::class, [
                'label' => 'Allow Buy Now For DVD Product:',
                'required'=>false,
            ])
            ->add('dvdProduct', EntityType::class, [
                'class'         => TblProducts::class,
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('p')
                        ->where('p.isActive = :isActive')
                        ->andWhere('p.isVisible = :isVisible')
                        ->andWhere('p.isDelete = :isDelete')
                        ->andWhere('p.idProductOfDvd is not null')
                        ->setParameters(
                            ['isActive'=>TblProducts::_ACTIVE,
                                'isVisible'=>TblProducts::_VISIBLE,
                                'isDelete'=>TblProducts::_NOTDELETE]
                        );
                    return $query;
                },
                'placeholder'   => '- Select Product of DVD -',
                'label'         => 'Product of DVD:',
                'attr'  => [
                    'class' => $hide.' showIdProductOfDvdSelect'
                ],
                'label_attr' => ['class' => $hide.' showIdProductOfDvdLabel'],
                'required' => false

            ])
            ->add('footerText', TextType::class, [
                'label' => 'Footer Link Text:',
                'required'=>false,
            ])
            ->add('footerLink', TextType::class, [
                'label' => 'Footer Link:',
                'required'=>false,
            ])
            ->add('redirectLink', TextType::class, [
                'label' => 'Purchase Redirect Link:',
                'required'=>false,
            ])
            ->add('featureboxImage1', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Feature Box Image1 (130 x 51):',
                'attr'       => ['value' => $options['data']->getFeatureboxImage1(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('featureboxImage2', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Feature Box Image2 (130 x 51):',
                'attr'       => ['value' => $options['data']->getFeatureboxImage2(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('featureboxImage3', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Feature Box Image3 (130 x 51):',
                'attr'       => ['value' => $options['data']->getFeatureboxImage3(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('featureboxImage4', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Feature Box Image4 (130 x 51):',
                'attr'       => ['value' => $options['data']->getFeatureboxImage4(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
            ->add('featureboxImage5', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Feature Box Image5 (130 x 51):',
                'attr'       => ['value' => $options['data']->getFeatureboxImage5(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TblProductLaunch::class,
            'selectedProductsize' => ""
        ]);
    }
}