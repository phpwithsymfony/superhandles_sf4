<?php

namespace App\Form\Front;

use App\Entity\TblUserProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CoachRegProfileType
 * @package App\Form
 */
class CoachRegProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => ['placeholder' => 'First Name'],
            ])
            ->add('lastname', TextType::class, [
                'attr' => ['placeholder' => 'Last Name'],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'  => TblUserProfile::class,
            'show_legend' => false,
            'show_label'  => false,
        ]);
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return 'app_profile';
    }
}
