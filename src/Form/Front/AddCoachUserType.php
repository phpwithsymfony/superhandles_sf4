<?php

namespace App\Form\Front;

use App\Entity\TblUserProfile;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddCoachUserType.
 */
class AddCoachUserType extends AbstractType
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('CoachUser', ChoiceType::class, [
                'label'    => 'Assign Coach Members:',
                'choices'  => self::getUnsignCoachUser(),
                'multiple' => true,
                'attr'     => [
                    'class' => 'pre-selected-options',
                ],
                'required' => true,
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //'data_class' => TblCoaches::class,
            'data_class' => null,
        ]);
    }

    private function getUnsignCoachUser()
    {
        $userDataArray = [];

        $user = $this->entityManager->createQueryBuilder()
                                    ->select(['U'])
                                    ->from(User::class, 'U')
                                    ->innerJoin(TblUserProfile::class, 'UP', 'UP.id = U.profile')
                                    ->where('U.enabled = :enable')
                                    ->andWhere('U.roles LIKE :roles')
                                    ->setParameters(
                                        ['enable' => true,
                                         'roles'  => '%"ROLE_REGISTER_USER"%']
                                    )->getQuery()->getResult();
        foreach ($user as $u) {
            if (!$u->getProfile()->getIsCoach()) {
                $userDataArray[$u->getUsername() . " | " .
                               $u->getProfile()->getFirstname() . " " .
                               $u->getProfile()->getLastname()] = $u->getId();
            }
        }
        return $userDataArray;
    }
}
