<?php

namespace App\Form\Front;

use App\Entity\TblCamps;
use App\Entity\TblCampsCategory;
use App\Entity\TblCoaches;
use App\Entity\TblSubscriptionCoachPlan;
use App\Entity\TblSubscriptionPackage;
use App\Entity\TblSubscriptionPlan;
use App\Form\PaymentFormType;
use App\Form\RegisterType;
use App\Form\RegProfileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CoachRegisterType
 * @package App\Form
 */
class CoachRegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idUser', CoachUserType::class, [
                'label'    => false,
            ])
            ->add('idSubscriptionPlan', EntityType::class, [
                'class'         => TblSubscriptionPlan::class,
                'placeholder'   => '- Select plan -',
                'label'         => 'Select plan',
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('sp')
                                ->where('sp.idSubscriptionPackage = :idSubscriptionPackage')
                                ->setParameter('idSubscriptionPackage', TblSubscriptionPlan::COACH_PACKAGE);
                    return $query;
                },

            ])
            ->add('idSubscriptionCoachPlan', EntityType::class, [
                'class'       => TblSubscriptionCoachPlan::class,
                'placeholder' => '- Select Number Of Players -',
                'label'       => 'Number Of Players:',
            ])
            ->add('amount', TextType::class, [
                'label'      => 'Price Per Year',
                'label_attr' => ['class' => "lebelAmount"],
                'attr'       => ['class' => 'textAmount', 'readonly' => true]
            ])
            ->add('isSubscription', ChoiceType::class, [
                'placeholder' => '- Select Subscription Type-',
                'choices'     => TblCoaches::SUBSCRIPTIONTYPE,
                'label'       => 'Subscription',
                'help'        => '* Select "Yes" to have your payment auto deducted on each payment due date and
                "No" like to manually pay your bill on each payment due date'
            ])
            ->add('isInvoice', ChoiceType::class, [
                'placeholder' => '- Select Invoice Type -',
                'label'       => 'Invoice',
                'choices'     => TblCoaches::INVOICETYPE,
                'help'        => '* Select "Yes" if your school is paying and an invoice is needed prior to payment.'
            ])
            ->add('schoolName', TextType::class, [
                'label'      => 'School Name',
                'label_attr' => ['class' => 'invoiceType hidden'],
                'attr'       => ['class' => 'invoiceType hidden'],
                'required'   => false
            ])
            ->add('schoolEmail', EmailType::class, [
                'label'      => 'School Email',
                'label_attr' => ['class' => "invoiceType hidden"],
                'attr'       => ['class' => 'invoiceType hidden'],
                'required'   => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblCoaches::class,
            'data' => ['data'=>'paymentForm']
        ]);
    }
}
