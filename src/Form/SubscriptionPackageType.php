<?php

namespace App\Form;

use App\Entity\TblSubscriptionPackage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscriptionPackageType
 * @package App\Form
 */
class SubscriptionPackageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('packageName', TextType::class, [
                'label' => 'Package Title:',
            ])
            ->add('description', TextareaType::class, [
                'label'    => 'Description:',
                'required' => false,
                'attr'     => [
                    'class' => 'textarea',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblSubscriptionPackage::class,
        ]);
    }
}
