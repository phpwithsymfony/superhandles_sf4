<?php
namespace App\Form;

use App\Entity\TblProducts;
use App\Entity\TblProductType;
use App\Entity\TblStreamingPages;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StreamingPageType
 * @package App\Form
 */
class StreamingPageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $readOnly = false;
        if ($options['data']->getId() > 0) {
            $readOnly = true;
        }

        $builder
            ->add('idProductType', EntityType::class, [
                'class'       => TblProductType::class,
                'placeholder' => '- Select Product Type -',
                'label'       => 'Product Type:',
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('p')
                                ->where('p.id IN (:id)')
                                ->setParameter('id', [1,3]);
                    return $query;
                },
                'attr'        => [
                    'readonly' => $readOnly
                ]

            ])
            ->add('idProduct', EntityType::class, [
                'class'       => TblProducts::class,
                'placeholder' => '- Select Product -',
                'label'       => 'Product:',
                'attr'        => [
                    'readonly' => $readOnly,
                ]

            ])
            ->add('pageTitle', TextType::class, [
                'label' => 'Streaming Page Title:',
            ])
            ->add('pageContent', TextareaType::class, [
                'label' => 'Streaming Page Content:',
                'attr'  => [
                    'class' => 'textarea',
                ]
            ])
            ->add('metaTitle', TextType::class, [
                'label' => 'Streaming Meta Title:',
                'required'=>false,
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'Streaming Meta Keywords:',
                'required'=>false,
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Streaming Meta Description:',
                'required'=>false,
            ])
            ->add('pageUrl', TextType::class, [
                'label' => 'Streaming Page URL(/streaming-access/):',
            ])
            ->add('productCcCode', TextType::class, [
                'label' => 'Product CC Code:',
            ])
            ->add('ccCodeMonth', TextType::class, [
                'label' => 'Month of CC Code:(This filed for subscription page only):',
                'attr'  => [
                    'readonly' => 'readonly',
                    'value'    => 1,
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblStreamingPages::class,
        ]);
    }
}
