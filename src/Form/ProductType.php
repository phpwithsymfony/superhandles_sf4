<?php
namespace App\Form;

use App\Entity\TblProductCategory;
use App\Entity\TblProducts;
use App\Entity\TblProductSize;
use App\Entity\TblProductType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType.
 */
class ProductType extends AbstractType
{
    /**
     * The EntityManager used by this QueryBuilder.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $hide = "hide";
        if ($options['data']->getId() > 0) {
            if ($options['data']->getIdProductOfDvd()) {
                $hide = "";
            }
        }

        $builder
            ->add('title', TextType::class, [
                'label' => 'Package Title:',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description:',
                'attr'  => [
                    'class' => 'textarea',
                ],
            ])
            ->add('price', NumberType::class, [
                'label' => 'Price:',
            ])
            ->add('weight', NumberType::class, [
                'label' => 'Product Weight (In Pound): ',
            ])
            ->add('quantity', NumberType::class, [
                'label' => 'Product Quantity: ',
                'required' => false
            ])
            ->add('idProductType', EntityType::class, [
                'class'         => TblProductType::class,
                 'query_builder' => function (EntityRepository $em) {
                     $query = $em->createQueryBuilder('pc')
                                ->where('pc.isCloth = :isCloths')
                               ->setParameter('isCloths', 0);
                     return $query;
                 },
                'placeholder'   => '- Select Product Type -',
                'label'         => 'Product Type:',
                'attr'  => [
                    'class' => 'showIdProductOfDvd'
                ],
            ])
            ->add('idProductOfDvd', EntityType::class, [
                'class'         => TblProducts::class,
                'query_builder' => function (EntityRepository $em) {
                    $query = $em->createQueryBuilder('p')
                                ->where('p.isActive = :isActive')
                                ->andWhere('p.isVisible = :isVisible')
                                ->andWhere('p.isDelete = :isDelete')
                                ->andWhere('p.idProductOfDvd is null')
                                ->setParameters(
                                    ['isActive'=>TblProducts::_ACTIVE,
                                     'isVisible'=>TblProducts::_VISIBLE,
                                     'isDelete'=>TblProducts::_NOTDELETE]
                                );
                    return $query;
                },
                'placeholder'   => '- Select Product of DVD -',
                'label'         => 'Product of DVD:',
                'attr'  => [
                    'class' => $hide.' showIdProductOfDvdSelect'
                ],
                'label_attr' => ['class' => $hide.' showIdProductOfDvdLabel'],
                'required' => false

            ])
            ->add('productCategories', EntityType::class, [
                'class'         => TblProductCategory::class,
                'placeholder'       => '- Select Product Type -',
                'label'             => 'Product Category:',
                'help'             => '(cntrl+select category for multiple):',
                'multiple'          => true,
                //'choices'           => $this->getCategoryData(),
                //'preferred_choices' => $selected_options
            ])
            ->add('productUrl', TextType::class, [
                'label'    => 'Product url (predefined:products/) :',
                'required' => true,
            ])
            ->add('idProductSize', HiddenType::class, [])
            ->add('image', FileType::class, [
                'required'   => false,
                'data_class' => null,
                'label'      => 'Image:',
                'attr'       => ['value' => $options['data']->getImage(), 'class' => 'image-preview', 'id' => 'imgInp'],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TblProducts::class,
            'selectedProductsize' => ""
        ]);
    }
}
