<?php
namespace App\EventListener;

use App\Entity\TblCartCoupons;
use App\Entity\TblCoaches;
use App\Entity\TblProducts;
use App\Entity\TblSubscriptionCoachPlan;
use App\Entity\TblSubscriptionPackage;
use App\Entity\TblSubscriptionPlan;
use App\Helper\CartCouponHelper;
use App\Helper\StripeProductsHelper;
use App\Helper\SubscriptionPackageHelper;
use App\Helper\SubscriptionPlanHelper;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class StripeListener
 * @package App\EventListener
 */
class StripeListener
{
    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TblProducts) {
            $stripeproducthelper = new StripeProductsHelper();
            $stripe = $stripeproducthelper->createProduct($entity);

            $entity->setStripId($stripe['id']);
        } elseif ($entity instanceof  TblSubscriptionPackage) {
            $stripeSubPlan = new SubscriptionPackageHelper();
            $stripe = $stripeSubPlan->createPackages($entity);

            $entity->setStripeProductId($stripe['id']);
        } elseif ($entity instanceof  TblSubscriptionPlan) {
            $stripeSubPlan = new SubscriptionPlanHelper();
            $stripe = $stripeSubPlan->createPlanforUniversity($entity);

            $entity->setStripePlanId($stripe['id']);
        } elseif ($entity instanceof  TblSubscriptionCoachPlan) {
            $stripeSubPlan = new SubscriptionPlanHelper();
            $stripe = $stripeSubPlan->createPlanforCoach($entity);

            $entity->setStripePlanId($stripe['id']);
        } elseif ($entity instanceof  TblCartCoupons) {
            $stripeCoupon = new CartCouponHelper();
            $stripe = $stripeCoupon->createCoupon($entity);

            $entity->setStripId($stripe['id']);
        }
    }
    /**
     * @param PreUpdateEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TblProducts) {
            $stripeproducthelper = new StripeProductsHelper();
            if ($entity->getStripId()) {
                $stripeproducthelper->updateProduct($entity, $entity->getStripId());
            } else {
                $stripe = $stripeproducthelper->createProduct($entity);
                $entity->setStripId($stripe['id']);
            }
        } elseif ($entity instanceof  TblSubscriptionPlan) {
            $stripeSubPlan = new SubscriptionPlanHelper();
            if ($entity->getStripePlanId()) {
                $stripeSubPlan->updatePlan($entity, $entity->getStripePlanId());
            } else {
                $stripe = $stripeSubPlan->createPlanforUniversity($entity);
                $entity->setStripePlanId($stripe['id']);
            }
        } elseif ($entity instanceof  TblSubscriptionPackage) {
            $stripeSubPackage = new SubscriptionPackageHelper();
            if ($entity->getStripeProductId()) {
                $stripeSubPackage->updatePackages($entity, $entity->getStripeProductId());
            } else {
                $stripe = $stripeSubPackage->createPackages($entity);
                $entity->setStripeProductId($stripe['id']);
            }
        } elseif ($entity instanceof  TblCartCoupons) {
            $stripeCoupon = new CartCouponHelper();
            if ($entity->getStripId()) {
                $stripeCoupon->updateCoupon($entity, $entity->getStripId());
            } else {
                $stripe = $stripeCoupon->createCoupon($entity);
                $entity->setStripId($stripe['id']);
            }
        }
//        elseif ($entity instanceof  TblCoaches) {
//            $stripeSubPackage = new SubscriptionPackageHelper();
//            $stripeSubPackage->coachSubscription($entity);
//        }
    }
}
