<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class ProjectListener
 * @package App\EventListener
 */
class ProjectListener
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * ProjectListener constructor.
     *
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (is_null($this->tokenStorage->getToken())) {
            return;
        }
        if (method_exists($entity, 'setCreatedBy')) {
            if ($this->tokenStorage->getToken()->getUser() instanceof User) {
                $entity->setCreatedBy($this->tokenStorage->getToken()->getUser());
            } else {
                $entity->setCreatedBy(null);
            }
        }
        if (method_exists($entity, 'setCreatedDate')) {
            $entity->setCreatedDate(new \DateTime());
        }
    }

    /**
     * @param PreUpdateEventArgs $args
     *
     * @throws \Exception
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if (is_null($this->tokenStorage->getToken())) {
            return;
        }
        if (method_exists($entity, 'setUpdatedBy')) {
            if ($this->tokenStorage->getToken()->getUser() instanceof User) {
                $entity->setUpdatedBy($this->tokenStorage->getToken()->getUser());
            } else {
                $entity->setUpdatedBy(null);
            }
        }
        if (method_exists($entity, 'setUpdatedDate')) {
            $entity->setUpdatedDate(new \DateTime());
        }
    }
}
