<?php

namespace App\EventListener;

use App\Entity\TblCampsSlider;
use App\Entity\TblHomeSlider;
use App\Entity\TblProductLaunch;
use App\Entity\TblProducts;
use App\Entity\TblStreamingPdfDoc;
use App\Entity\TblTestimonials;
use App\Entity\TblUserProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Service\FileUploader;

/**
 * Class FileUploadListener
 * @package App\EventListener
 */
class FileUploadListener
{
    /**
     * @var FileUploader
     */
    private $uploader;

    /**
     * @var array
     */
    private $upload_path;

    /**
     * FileUploadListener constructor.
     *
     * @param FileUploader $uploader
     * @param ContainerInterface $container
     */
    public function __construct(FileUploader $uploader, array $upload_path)
    {
        $this->uploader = $uploader;
        $this->upload_path = $upload_path;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->uploadFile($entity, $args);
    }


    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {

        // Retrieve Form as Entity
        $entity = $args->getEntity();
        $this->uploadFile($entity, $args);
    }

    /**
     * @param      $entity
     * @param null $args
     */
    private function uploadFile($entity, $args = null)
    {
        $getEntity = self::getEntityPath($entity);
        $files = $getEntity['files'];
        $targetFolder = $getEntity['targetFolder'];

        if (isset($targetFolder)) {
            $this->uploader->setTargetDirectory($targetFolder);
        }


        foreach ($files as $key => $value) {
            $previousFilename = null;
            if (method_exists($args, 'getEntityChangeSet')) {
                $changes = $args->getEntityChangeSet();
                $previousFilename = null;
                if (array_key_exists($key, $changes)) {
                    $previousFilename = $changes[$key][0];
                }
            }
            if (is_null($entity->{'get' . $key}())) {
                $entity->{'set' . $key}($previousFilename);
            } else {
                if (!is_null($previousFilename)) {
                    $pathPreviousFile = $this->uploader->getTargetDirectory() . "/" . $previousFilename;
                    if (file_exists($pathPreviousFile)) {
                        unlink($pathPreviousFile);
                    }
                }

                if ($value instanceof UploadedFile) {
                    $fileName = $this->uploader->upload($value);
                    $entity->{'set' . $key}($fileName);
                } elseif ($value instanceof File) {
                    $entity->{'set' . $key}($value->getFilename());
                }

            }

        }
    }

    /**
     * @param $entity
     *
     * @return array
     */
    private function getEntityPath($entity)
    {
        $targetFolder = "";
        $files = [];
        if ($entity instanceof TblCampsSlider) {
            $targetFolder = $this->upload_path['CAMPS_SLIDER_IMAGES'];
            $files['bannerFile'] = $entity->getBannerFile();
        } elseif ($entity instanceof TblHomeSlider) {
            $targetFolder = $this->upload_path['HOME_SLIDER_IMAGES'];
            $files['bannerFile'] = $entity->getBannerFile();
        } elseif ($entity instanceof TblUserProfile) {
            $targetFolder = $this->upload_path['USERS_IMAGES'];
            $files['profilePic'] = $entity->getProfilePic();
        } elseif ($entity instanceof TblTestimonials) {
            $targetFolder = $this->upload_path['TESTIMONIAL_AUTHOR_IMAGES'];
            $files['AuthorPic'] = $entity->getAuthorPic();
        } elseif ($entity instanceof TblProducts) {
            $targetFolder = $this->upload_path['PRODUCTS_IMAGES'];
            $files['Image'] = $entity->getImage();
        } elseif ($entity instanceof TblStreamingPdfDoc) {
            $targetFolder = $this->upload_path['STREAMING_CONTENT_DOCS'];
            $files['Url'] = $entity->getUrl();
        } elseif ($entity instanceof TblProductLaunch) {
            $targetFolder = $this->upload_path['PRODUCTS_LAUNCH_IMAGES'];
            $files['productImage'] = $entity->getProductImage();
            $files['imageUrl'] = $entity->getImageUrl();
            $files['featureboxImage1'] = $entity->getFeatureboxImage1();
            $files['featureboxImage2'] = $entity->getFeatureboxImage2();
            $files['featureboxImage3'] = $entity->getFeatureboxImage3();
            $files['featureboxImage4'] = $entity->getFeatureboxImage4();
            $files['featureboxImage5'] = $entity->getFeatureboxImage5();
            $files['testimonialImage1'] = $entity->getTestimonialImage1();
            $files['testimonialImage2'] = $entity->getTestimonialImage2();
            $files['testimonialImage3'] = $entity->getTestimonialImage3();
        }
        return ["files" => $files, "targetFolder" => $targetFolder];
    }


    /**
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $getEntity = self::getEntityPath($entity);
        $files = $getEntity['files'];
        $targetFolder = $getEntity['targetFolder'];

        foreach ($files as $key => $value) {
            $this->uploader->setTargetDirectory($targetFolder);
            if ($fileName = $entity->{'get' . $key}()) {
                if (file_exists($this->uploader->getTargetDirectory() . '/' . $fileName)) {
                    $entity->{'set' . $key}(new File($this->uploader->getTargetDirectory() . '/' . $fileName));
                }
            }
        }
    }
}
