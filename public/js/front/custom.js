$(document).ready(function () {
    GVSubmitActor = {name: 'none', value: 'none'};
    // initialization of custom select
    $.HSCore.components.HSSelect.init('.js-custom-select');

    // initialization of step form
    $.HSCore.components.HSStepForm.init('.js-step-form');
    // toastr
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1500",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };


    // Select "Yes" if your school is paying and an invoice is needed prior to payment.
    $("#coach_register_isInvoice").change(function () {
        if ($(this).val() == 1) {
            $(".invoiceType").removeClass('hidden');
        } else {
            $(".invoiceType").addClass('hidden');
        }
    });

    //get using plan of playes
    getPlayesofPlan();

    //get using plan of playes
    getAmountofPlayes();

    //coach register form
    coachRegisterForm();

    //coach promocode
    coachPromocode();

    // showIdProductOfDvd
    $(".showIdProductOfDvd").change(function () {
        if ($(this).val() == 2) {
            $(".showIdProductOfDvdSelect").addClass('hide');
            $(".showIdProductOfDvdLabel").addClass('hide');
        } else {
            $(".showIdProductOfDvdSelect").removeClass('hide');
            $(".showIdProductOfDvdLabel").removeClass('hide');
        }
    });


    $(".addtoCart").click(function () {

        $productId = $(this).attr('productId');
        $buynow = $(this).attr('buynow');
        $productQty = ($("#productQty").val() === undefined) ? 1 : $("#productQty").val();
        $productSize = ($("#productSize").val() === undefined) ? 1 : $("#productSize").val();
        quickAjax(Routing.generate('front_cart_ajax_cart_add'), {
            "productId": $productId,
            "productQty": $productQty,
            "productSize": $productSize
        }, function (data) {
            if (data === 0) {
                toastr["error"]("Product already added in your cart");
            } else {
                if ($buynow) {
                    window.location.href = Routing.generate('front_checkout_cart');
                } else {
                    toastr["info"]("Product added in cart");
                    cartCount();
                }
            }
        });
    });

    $(".addtoCartwithDvd").click(function () {
        var productId = $(this).attr('productId');
        $buynow = $(this).attr('buynow');
        quickAjax(Routing.generate('front_cart_ajax_cart_dual_product'), {"productId": productId}, function (data) {
            $(".streamProductId").attr('productId', data.idProductOfDvd);
            $(".dvdProductId").attr('productId', data[0].id);
            if ($buynow) {
                $(".streamProductId").attr('buynow', 'true');
                $(".dvdProductId").attr('buynow', 'true');
            }
            $("#product_list-detail-dvd").modal("show");
        });
    });

    $(".addtoCartwithCloth").click(function () {
        var productId = $(this).attr('productId');
        quickAjax(Routing.generate('front_cart_ajax_cart_dual_product'), {"productId": productId}, function (data) {
            $("#clothTitle").text(data[0].title);
            $("#clothPrice").text('$' + data[0].price);
            $("#clothDescription").html(data[0].description);
            $("#clothImage").attr("src", '../uploads/product/' + data[0].image);
            quickAjax(Routing.generate('front_cart_ajax_cart_product_size'), {"size": data[0].idProductSize}, function (data) {

                $('#productSize').empty();
                $option = '';
                for (var i = 0; i < data.length; i++) {
                    $option += '<option value="' + data[i].id + '">' + data[i]['size'] + '</option>';
                }
                $('#productSize').append($option);
            });
            $(".productId").attr('productId', productId);

            $("#product_list-detail").modal("show");
        });
    });

    // get Tutorials
    $(".subscription-link").click(function () {
        $streamingid = $(this).attr('streamingid');
        $checkWorkouts = $(this)[0].hasAttribute("tw") ? 1 : 0;
        $isWorkouts = $checkWorkouts ? $(this).attr('tw') : 0;
        if ($isWorkouts === "1") {
            $("#workouts").addClass('active-strbtn');
            $("#tutorials").removeClass('active-strbtn');
        } else {
            $("#workouts").removeClass('active-strbtn');
            $("#tutorials").addClass('active-strbtn');
        }
        quickAjax(Routing.generate('front_hoopfolio_streaming_page_content'), {
            "streamingid": $streamingid,
            "isWorkouts": $isWorkouts
        }, function (data) {
            if (data.length) {
                $('#setworkouts').empty();
                $option = '';
                for (var i = 0; i < data.length; i++) {
                    $option += '<option value="' + data[i]['video'] + '~' + data[i]['title'] + '">' + data[i]['title'] + '</option>';
                }
                $('#workouts').attr('streamingid', $streamingid);
                $('#tutorials').attr('streamingid', $streamingid);
                $('#labelWorkout').text(data[0]['title']);
                $("#myIframe").attr('src', 'http://player.vimeo.com/video/' + data[0]['video'] + '?byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" width="100%" height="400');

                $('#setworkouts').append($option);
                $("#streamingContent").show();
                $("#mystreaming").hide();
                $(".select2-single").select2();
            } else {
                toastr["info"]("Does't found any Tutorials.");
            }
        });
    });
    $("#setworkouts").change(function () {
        $video = ($(this).val()).split('~');
        $('#labelWorkout').text($video[1]);
        $("#myIframe").attr('src', 'http://player.vimeo.com/video/' + $video[0] + '?byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" width="100%" height="400');
    });
});

// ajax Quick Post
function quickAjax($url, $data, $func)
{
    $.ajax({
        type: "POST",
        url: $url,
        data: $data,
        dataType: "json",
        success: function (data) {
            $func(data);
        },
        error: function () {
            alert('error handing here');
        }
    });
}

function cartCount()
{
    quickAjax(Routing.generate('front_cart_ajax_cart_detail'), "", function (data) {
        $("#cartCount").text(data);
    });
}

function getPlayesofPlan()
{
    $('#coach_register_idSubscriptionPlan').change(function () {
        quickAjax(Routing.generate('front_coach_subscription_plan'), {"idSubscriptionPlan": $(this).val()}, function (jsondata) {
            var jsondataSelect = $("#coach_register_idSubscriptionCoachPlan");
            // Remove current options
            jsondataSelect.html('');

            $.each(jsondata, function (key, jsondata) {
                jsondataSelect.append('<option value="' + jsondata.id + '" playeramount="'+ jsondata.amount +'" interval="'+ jsondata.packageInterval +'">' + jsondata.members + '</option>');
            });
            $("#coach_register_amount").val(jsondata[0].amount);
            $(".player").text(jsondata[0].members);
            $(".selectedPlan").text($("#coach_register_idSubscriptionPlan").children("option:selected").text());
            $(".payAmount").text("$" + jsondata[0].amount)
        });
    });
}

function getAmountofPlayes()
{
    $('#coach_register_idSubscriptionCoachPlan').change(function () {
        $("#coach_register_amount").val($(this).children("option:selected").attr('playeramount'));
        $(".player").text($(this).children("option:selected").text());
        $(".payAmount").text("$" + $(this).children("option:selected").attr('playeramount'));
    });
}

function coachRegisterForm()
{
    $(document).on("click", "#formSubmit", function (e) {
        e.preventDefault();
        showLoader();
        var url = Routing.generate('front_coach_coach_register')+"/"+$('#urlPlanName').val();
        var formSerialize = $("#coachRegistrationForm").serialize();
        if ($("#coachRegistrationForm").valid()) {
            $.post(url, formSerialize, function (response) {
                //your callback here
                if (response.status) {
                    toastr["info"]("Coach is created");
                    $("#totalAmt").text('$'+response.totalAmt);
                    $("#totalAmt").attr('totalAmt', response.totalAmt);
                    $("#remAmt").text('$'+response.totalAmt);
                    $("#remAmt").attr('remAmt', response.totalAmt);
                    $("#coachId").val(response.id);
                    $("#formSubmitClick").click();
                } else {
                    $user = response.data;
                    $.each($user, function (key, value) {
                        $.each(value, function (dataKey, dataValue) {
                            if (dataValue) {
                                if (dataKey == "plainPassword") {
                                    toastr["error"](dataValue.first);
                                    $("#coach_register_"+key+"_"+dataKey+"_second").parent().after('<label class="error" for="coach_register_'+key+'_'+dataKey+'_second">'+dataValue.first+'</label>')
                                } else {
                                    toastr["error"](dataValue);
                                    $("#coach_register_" + key + "_" + dataKey).parent().after('<label class="error" for="coach_register_' + key + '_' + dataKey + '">' + dataValue + '</label>')
                                }
                            }

                        });

                    });
                }
            }, 'JSON');
        }
        hideLoader();
    });
}

function coachPromocode()
{
    $(document).on("click", "#coachApplyPromocode", function (e) {
        e.preventDefault();
        if ($('#coachPromocode').val()) {
            quickAjax(Routing.generate('front_coach_ajax_coachpromocode'), {"promocode": $('#coachPromocode').val(), "coachId": $('#coachId').val()}, function (jsondata) {
                if (jsondata) {
                    if (jsondata.couponApplyed) {
                        $("#applysuccess").show();
                        $("#applyerror").hide();
                        $("#blankcode").hide();
                    } else {
                        $("#applysuccess").hide();
                        $("#applyerror").show();
                        $("#blankcode").hide();
                    }
                    $("#discountAmt").text("$" + jsondata.discountAmount.toFixed(2));
                    $("#remAmt").text("$" + (jsondata.remainingAmount).toFixed(2));
                    $(".payAmount").text("$" + jsondata.remainingAmount.toFixed(2))
                } else {
                    $("#applysuccess").hide();
                    $("#applyerror").show();
                    $("#blankcode").hide();
                }
            });
        } else {
            $remAmount = $("#remAmt").attr('remAmt');
            $("#discountAmt").text("$0.00");
            $("#remainingAmount").text("$"+$remAmount);
            $("#applysuccess").hide();
            $("#applyerror").hide();
            $("#blankcode").show();
            $(".payAmount").text("$"+$remAmount)
        }
    });
}

function showLoader()
{
    $("#loader").show(500);
}

function hideLoader()
{
    $("#loader").hide(500);
}

function loadModalURL(obj)
{
    $("#ajaxModal").remove();
    $(".modal-backdrop").remove();

    var $this = $(obj);
    $remote = $this.data('href') || $this.attr('href');
    $modal = $('<div  class="modal" tabindex="-1" role="dialog" id="ajaxModal"><div class="modal-body"></div></div>');
    $('body').append($modal);
    $modal.modal({backdrop: 'static'});
    $modal.load($remote, function (result) {
        modal_form_submit_handler();
    });
    return false;
}

function initModalForms()
{
    $('[data-toggle="ajaxModal"]').on('click', function (e) {
            e.preventDefault();
            $("#ajaxModal").remove();
            $(".modal-backdrop").remove();

            var $this = $(this);
            $remote = $this.data('remote') || $this.attr('href');
            $modal = $('<div  class="modal" tabindex="-1" role="dialog" id="ajaxModal"><div class="modal-body"></div></div>');
            $('body').append($modal);
            $modal.modal({backdrop: 'static'});
            $modal.load($remote, function (result) {
                modal_form_submit_handler();
            });
            return false;
    });
    var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };

}

function modal_form_submit_handler()
{
    $('[data-ride="ajaxModalForm"]').on("submit", function (event) {

        event.preventDefault();
        event.stopPropagation();

        $url = $(this).attr('action');
        $method = $(this).attr('method');
        //$data = $(this).serialize();
        $data = new FormData(this);
        $data.append(GVSubmitActor.name, GVSubmitActor.value);
        $(this).prop('disabled', 'disabled');
        //showLoader();
        $.ajax({
            type: $method, url: $url,
            data: $data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                //hideLoader();
                if (response.js != undefined) {
                    $('[data-dismiss="modal"]').click();
                    if (response.js != "") {
                        eval(response.js);
                    } else {
                        toastr["success"]("operation successful", "");
                    }
                }
                if (response.redirect) {
                    $('[data-dismiss="modal"]').click();
                    toastr["success"]("Redirecting..", "");
                    setTimeout(function () {
                        window.location.reload();
                    }, 250);


                }
                if (response.toast) {
                    $('[data-dismiss="modal"]').click();
                    toastr["success"](response.toast, "");
                    if (response.toastjs != "") {
                        eval(response.toastjs);
                    } else {
                        setTimeout(function () {
                            window.location = response.redirect;
                        }, 400);
                    }


                }
                if (response == "") {
                    $('[data-dismiss="modal"]').click();
                    toastr["success"]("operation successful .. refreshing", "");
                    setTimeout(function () {
                        window.location.reload();
                    }, 250);

                } else {
                    $("#ajaxModal").html(response);
                    modal_form_submit_handler();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {

                $(".modal-body").html(textStatus);
                $('[data-ride="ajaxModalForm"] .modal-footer').hide();
            }
        });

        return false;

    });

    $('[data-ride="ajaxModalForm"] [type="submit"]').click(function () {
        GVSubmitActor.name = this.name;
        GVSubmitActor.value = this.value;
//        $(this).text("Please Wait..");
//        $(this).prop('disabled', 'disabled');
//        $(this).parents('form').submit();
//
    });

    // ReMap Events Controls
    //uiManager(".modal ");

}






