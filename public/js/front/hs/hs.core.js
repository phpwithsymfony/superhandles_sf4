/**
 * HSCore -
 *
 * @author HtmlStream
 * @version 1.0
 */
;
(function ($) {

    'use strict';

    $.HSCore = {
        components: {},
        settings: {
            rtl: false
        }

    };

})(jQuery);
